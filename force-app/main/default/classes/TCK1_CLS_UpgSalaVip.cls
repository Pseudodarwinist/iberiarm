public class TCK1_CLS_UpgSalaVip {

  /*-------------------------------------------------------------------------------------------------------------------------------------------------------
 Author:         Adrian Romero, Victor Gonzalez, Iván Lopez
 Company:        The Cocktail
 Description:    Method that indicates Upg Salas Vip.
 
 IN:         
 
 OUT:           ----
 
 History:
 
 <Date>              <Author>            							<Description>
 15/07/2019          Adrian Romero, Victor Gonzalez, Iván Lopez     Initial version
 --------------------------------------------------------------------------------------------------------------------------------------------------------*/
 
   public static void accesoUpg(R1_VIP_Lounge_Access__c acceso) {
     try{
       R1_byPass__c byPass = R1_byPass__c.getOrgDefaults();
       
       //custom setting that activates or deactivates this function
       if (byPass.byPass_Sala_Vip__c && acceso.R1_VLI_PKL_Vip_lounge_position__c !='Fast Track') {
         String tramo = acceso.R1_VLI_TXT_Origin__c + acceso.R1_VLI_TXT_Destination__c;
         String clasePasajero = acceso.R1_VLI_TXT_Class__c;
         String estado = acceso.R1_VLI_PKL_Access_status__c;
         String compannia = acceso.R1_VLI_TXT_Comp_Flight__c;
 
         String compannyMetadata = Acceso_Sala_VIP__c.getValues('Company')!=null && Acceso_Sala_VIP__c.getValues('Company').ValorTexto__c!=null? Acceso_Sala_VIP__c.getValues('Company').ValorTexto__c:null;
         Boolean companiaOk = false;
         if(acceso.R3_VLI_FOR_NombreVuelo__c != null && compannyMetadata!=null && compannyMetadata.contains((acceso.R3_VLI_FOR_NombreVuelo__c).substring(0, 2))){
            companiaOk = true;
         }
         system.debug('companiaOk: '+companiaOk);
         if (companiaOk) {
 
           //if the client is denied the function is not activated
 
           String vuelo = acceso.R1_VLI_TXT_Flight_number__c;
           Date diaVuelo = acceso.R1_VLI_DAT_Date__c;
           Datetime auxDate = diaVuelo;
           String diaVueloFormato = auxDate.format('yyyyMMdd');
 
           //the message is not activated if access does not have a client lookup
           if (acceso.R1_VLI_LOO_Cliente__c != null) {
 
           account pasajero = [select R1_ACC_NUM_Number_of_Avios__c, Name, R2_ACC_NUM_ICAR_num_avios_total__c
             from account
             WHERE Id =: acceso.R1_VLI_LOO_Cliente__c limit 1
           ];
 
             if (pasajero != null) {
 
               Decimal aviosPasajero = pasajero.R1_ACC_NUM_Number_of_Avios__c;
               Decimal aviosPasajero2 = pasajero.R2_ACC_NUM_ICAR_num_avios_total__c;
               string nameCliente = pasajero.Name;
 
               //Custom Setting Tabla de precios
               Tarifas_UPG__c tr = Tarifas_UPG__c.getValues(tramo);
               Integer aviosPE = tr.Precio_Avio_PE_BUS__c.intValue();
               Integer aviosTUR1 = tr.Precio_Avio_TUR_PE__c.intValue();
               Integer aviosTUR2 = tr.Precio_Avio_TUR_BUS__c.intValue();
 
               //custom setting Sala VIP  
               List < Acceso_Sala_VIP__c > salasVIP = new List < Acceso_Sala_VIP__c > ();
               Acceso_Sala_VIP__c salasclase1 = Acceso_Sala_VIP__c.getValues('ClasePE');
               Acceso_Sala_VIP__c salasclase2 = Acceso_Sala_VIP__c.getValues('ClaseTUR');
               Acceso_Sala_VIP__c salasclase3 = Acceso_Sala_VIP__c.getValues('ClaseBUS');
               salasVIP.add(salasclase1);
               salasVIP.add(salasclase2);
               salasVIP.add(salasclase3);
 
               String clase;
               List < Acceso_Sala_VIP__c > margenesSalaVIP = Acceso_Sala_VIP__c.getall().values();
               for (Acceso_Sala_VIP__c margenSalaVIP: margenesSalaVIP) {
                 if((margenSalaVIP.Name == 'ClasePE' || margenSalaVIP.Name == 'ClaseTUR' || margenSalaVIP.Name == 'ClaseBUS') && margenSalaVIP.ValorTexto__c.contains(clasePasajero)){
                   clase = margenSalaVIP.Name;
                 }
               }
               
               //Switch to select the passenger class
               switch on clase {
                 when 'ClaseTUR' {
                   //Check passenger avios before calling the web service
                   if (aviosPasajero >= aviosTUR2 || aviosPasajero2 >= aviosTUR2 || (aviosPasajero2 >= aviosTUR1 && aviosPasajero2 < aviosTUR2) ||(aviosPasajero >= aviosTUR1 && aviosPasajero < aviosTUR2) ) {
                   //Check Callouts status
                     if(checkCallNumber()){
                       //WEBSERVICE
                       TCK1_CLS_WSResiber.getDatos(aviosPasajero, aviosPasajero2, aviosPE, aviosTUR1, aviosTUR2, clase, compannia, vuelo, diaVueloFormato, acceso.R1_VLI_TXT_Origin__c, acceso.R1_VLI_TXT_Destination__c, '666', '777', nameCliente, diaVuelo, acceso.Id, acceso.R1_VLI_LOO_Cliente__c,tramo);
                     }
                   
                   }
                 }
                 when 'ClasePE' {
                   //Check passenger avios before calling the web service
                   if (aviosPasajero >= aviosPE || aviosPasajero2 >= aviosPE) {
                   //WEBSERVICE
                     if(checkCallNumber()){  
                       TCK1_CLS_WSResiber.getDatos(aviosPasajero, aviosPasajero2, aviosPE, aviosTUR1, aviosTUR2, clase, compannia, vuelo, diaVueloFormato, acceso.R1_VLI_TXT_Origin__c, acceso.R1_VLI_TXT_Destination__c, '666', '777', nameCliente, diaVuelo, acceso.Id, acceso.R1_VLI_LOO_Cliente__c,tramo);
                     
                     }
                   }
                 }
               }
             }
           }
         }
       }
     }catch(Exception e){
       R1_CLS_LogHelper.generateErrorLog('TCK1_CLS_UpgSalaVip.accesoUpg', '', e.getmessage() + ', ' + e.getLineNumber(), 'UPG SalasVIP');
     }
   }
 
  public static boolean checkCallNumber() {
   boolean toRet = false;
   ResiberCallsNumber__c callsInfo = ResiberCallsNumber__c.getValues('CallNumbers');
   Integer dayNow = (datetime.now()).dayGmt();
   try {
     if(callsInfo.Active__c){
       if (callsInfo.ActualCallNumbers__c < callsInfo.MaxCallNumbers__c && dayNow == callsInfo.day__c) {
         callsInfo.ActualCallNumbers__c = callsInfo.ActualCallNumbers__c + 1;
         update callsInfo;
         toRet = true;
       }else if (dayNow != callsInfo.day__c){
         callsInfo.ActualCallNumbers__c = 1;
         callsInfo.day__c = dayNow;
         update callsInfo;
         toRet = true;
       }
     }else{
       toRet = true;
     }
   }catch (DmlException exc) {
     R1_CLS_LogHelper.generateErrorLog('checkCallNumber', '', exc.getmessage() + ', ' + exc.getLineNumber(), 'Resiber Calls Number CustomSetting');
   }catch (Exception e) {
     R1_CLS_LogHelper.generateErrorLog('checkCallNumber', '', e.getmessage() + ', ', 'Resiber Calls Number CustomSetting');
   }
   return toRet;
   }
 }