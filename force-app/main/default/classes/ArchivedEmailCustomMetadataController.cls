public with sharing class ArchivedEmailCustomMetadataController {

    @TestVisible private static List<Archived_email_search_configuration__mdt> mockConfigs = null;

    @AuraEnabled(cacheable = true) 
    public static Decimal getDefaultLinesForSearchResult() {
        Decimal defaultLines;
        if (Schema.sObjectType.Archived_email_search_configuration__mdt.isAccessible()) {
            List<Archived_email_search_configuration__mdt> configurations = [SELECT EmailSearchResultLines__c FROM Archived_email_search_configuration__mdt];
        
            if((mockConfigs != null && mockConfigs.size() > 0) || (configurations != null && configurations.size() > 0)) {
                defaultLines = mockConfigs != null ? mockConfigs[0].EmailSearchResultLines__c : configurations[0].EmailSearchResultLines__c;
            }
        }
        return defaultLines;
    }

    @AuraEnabled(cacheable = true) 
    public static String getDefaultEmailSearchDates() {
         
        String defaultSearchDate;
        if (Schema.sObjectType.Archived_email_search_configuration__mdt.isAccessible()) {
            List<Archived_email_search_configuration__mdt> configurations = [SELECT ArchivedEmailTypeDefaultValue__c FROM Archived_email_search_configuration__mdt];
      
            if((mockConfigs != null && mockConfigs.size() > 0) || (configurations != null && configurations.size() > 0)) {
                defaultSearchDate = mockConfigs != null ? mockConfigs[0].ArchivedEmailTypeDefaultValue__c : configurations[0].ArchivedEmailTypeDefaultValue__c;
            }
        }
        return defaultSearchDate;
    }

    @AuraEnabled(cacheable = true) 
    public static Boolean isSuperUser() {

        Boolean hasCustomPermission = FeatureManagement.checkPermission('EmailSearchSuperUser');
        return hasCustomPermission;
    }
}