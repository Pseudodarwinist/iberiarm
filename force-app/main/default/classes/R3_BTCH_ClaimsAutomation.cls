/*---------------------------------------------------------------------------------------------------------------------
    Author:         ICA-LCS
    Company:        LeadClic
    Description:    Batch execution for Claims Automation II

    History:
     <Date>                     <Author>                         <Change Description>
    12/11/2019             		ICA                  				Initial Version
 ----------------------------------------------------------------------------------------------------------------------*/
/**
* @author LCS
* @date 12/11/2019 
* @description Batch class fired by an scheduled Job
*/
global class R3_BTCH_ClaimsAutomation implements Database.batchable<sObject>,Database.Stateful{
	
	global List<R3_CLS_AUX_ClaimsAutomation> rulesList = new List<R3_CLS_AUX_ClaimsAutomation>();

	/**
    * @author LCS
    * @date 12/11/2019 
    * @description Selection of the Cases to be analyzed and processed in the Batch
    * @param Database.BatchableContext Context for the batch job
    */	
	global Database.QueryLocator start(Database.BatchableContext BC){
		//First, deletes all previous logErrors
		delete [Select Id From R3_LogErrors__c where R3_LOG_TXT_JobName__c = 'Claims Auto II'];
		Id rtExp = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('R2_File').getRecordTypeId();
		rulesList.addAll(R3_CLS_AUX_ClaimsAutomation.getAllRules());
		String query =  'SELECT Id, Account.PersonContactId,Account.PersonEmail,CreatedDate, RecordType.Name,R2_CAS_PKL_client_type__c, OwnerId, Status, '+
                                +' R2_CAS_CHK_AUT_Do_not_automate_flag__c, R2_CAS_NUM_Count_Number_Reopen__c,R2_CAS_PKL_Vip_Type__c, '+
                                +' Type, R1_CAS_PKL_Subtype__c, R1_CAS_PKL_Idioma__c, R2_CAS_FOR_carrier_code_oper__c, '+
                                +' R2_CAS_FOR_UK__c, R2_CAS_PKL_Manage__c,R2_CAS_PKL_File_type__c,R1_CAS_LOO_Flight__r.R1_FLG_TXT_Flight_number__c,R1_CAS_LOO_Flight__r.R3_FLG_FOR_RealCompany__c, '+
                                +' R1_CAS_LOO_Flight__r.R2_FLG_TXT_Stop_automation__c,(Select Id From Cases) '
								+' FROM Case '+
                                +' WHERE RecordTypeId=:rtExp AND R2_CAS_CHK_Automation__c = true';
		
		return (!rulesList.isEmpty())?Database.getQueryLocator(query):Database.getQueryLocator('Select Id from Case limit 0');

	}

	/**
    * @author LCS
    * @date 12/11/2019  
    * @description Process the batch of cases and update the owner if meet the criteria. Later, it will send an email per case in blocks of 10 due to SF limits.
    * @param SchedulableContext SchedulableContext for the execution.
	* @param List<Case> scope List of cases for the execution.
    */
	global void execute(Database.BatchableContext BC, List<Case> scope){
		Map<Id,R3_Claims_Automation_Rules__c> idCaseRuleMap = new Map<Id,R3_Claims_Automation_Rules__c>();
		List<R3_LogErrors__c> errorLogList = new List<R3_LogErrors__c>();
		List<Case> cases2Update = new List<Case>();
		List<CaseComment> caseComent2Insert = new List<CaseComment>();
		for(Case c : scope){
			If(c.Cases.Size()<=1){
				R3_Claims_Automation_Rules__c rule;
				for(R3_CLS_AUX_ClaimsAutomation rw : rulesList){
					if(rw.isCaseSuitable(c,null)){
						rule = rw.rule;
						if(rw.meetAllConditions(c)){
							idCaseRuleMap.put(c.Id,rule);
							c.OwnerId=rule.OwnerId;
							c.Status=rule.R3_CAR_PKL_Final_Status__c;
							c.R2_CAS_PKL_Manage__c = rule.R3_CAS_PKL_FinalManage__c;
							c.R2_CAS_CHK_Automation__c=false;
							if(c.Account==null || c.Account.PersonEmail==null){
								errorLogList.add(
								new R3_LogErrors__c(R3_LOG_TXT_RecordId__c=c.Id,
									R3_LOG_TXT_JobName__c='Claims Auto II',
									R3_LOG_TXT_ObjectName__c='Case',
									R3_LOG_TXT_ErrorDescription__c='The related contact of this case has no email.'));
							}else{
								cases2Update.add(c);
								caseComent2Insert.add(new CaseComment(CommentBody ='Case automated by rule: ' + rule.Name,ParentId = c.Id));
							}
							
							break;
						}
					}
				}
				//The rule has been deleted since the case was marked as automated
				if (rule==null) {
					c.R2_CAS_CHK_Automation__c=false;
					cases2Update.add(c);
				}
			}else{
				//If the case has more than 1 children, it's not evaluated(Business requirement)
				c.R2_CAS_CHK_Automation__c=false;
				cases2Update.add(c);
			}
			
		}
		Database.SaveResult[] saveComentResultList = Database.insert(caseComent2Insert,false);
		Database.SaveResult[] saveCasesResultList = Database.update(cases2Update,false);
		Integer iterator = 0;//From documentation: SaveResult Class saves the order of the input array, so the first element in the SaveResult array matches the first element passed in the sObject array, the second element corresponds with the second element, and so on.
		for(Database.SaveResult sr :saveCasesResultList){
			if (sr.isSuccess()) {
			// Operation was successful, so get the ID of the record that was processed
			}else {
				// Operation failed, so get all errors                
				for(Database.Error err : sr.getErrors()) {
					idCaseRuleMap.remove(cases2Update.get(iterator).Id);
					errorLogList.add(
						new R3_LogErrors__c(R3_LOG_TXT_RecordId__c=cases2Update.get(iterator).Id,
							R3_LOG_TXT_JobName__c='Claims Auto II',
							R3_LOG_TXT_ObjectName__c='Case',
							R3_LOG_TXT_ErrorDescription__c=err.getStatusCode() + ': ' + err.getMessage()
						)
					);
				}
			}
			iterator++;
		}
		Map<String,OrgwideEmailAddress> mapEmailWithOWAId = R3_CLS_AUX_ClaimsAutomation.getOwaMap(idCaseRuleMap);
		iterator =0;
		List<Case> case2SendEmail = new List<Case>();

		for(Case c : cases2Update){
			iterator++;
			case2SendEmail.add(c);
			if(iterator == 9){
				iterator = 0;
				errorLogList.addAll(R3_CLS_AUX_ClaimsAutomation.sendEmails(case2SendEmail,idCaseRuleMap,mapEmailWithOWAId));
				case2SendEmail = new List<Case>();
			}
		}
		if(!case2SendEmail.isEmpty()){
			errorLogList.addAll(R3_CLS_AUX_ClaimsAutomation.sendEmails(case2SendEmail,idCaseRuleMap,mapEmailWithOWAId));
		}
		insert errorLogList;
	}
	/**
    * @author LCS
    * @date 12/11/2019  
    * @description Final method of batch process.
    * @param SchedulableContext SchedulableContext for the execution.
    */
	global void finish(Database.BatchableContext BC){}
}