public class ExtDupGoldenIDDataController{
    public String sDate{get;set;}
    public String eDate{get;set;}
    public List<AccountWrapper> accWrapperLst{get;set;}
    
    public ExtDupGoldenIDDataController(){
        sDate = ApexPages.currentPage().getParameters().get('sd');
        if(sDate!=null && sDate!=''){
            init();
        }
    }
    
    public void init(){
        List<String> stDate = sDate.split('-');
        DateTime cDate = DateTime.newInstanceGmt(Integer.valueOf(stDate.get(0)), Integer.valueOf(stDate.get(1)), Integer.valueOf(stDate.get(2)), Integer.valueOf(stDate.get(3)), Integer.valueOf(stDate.get(4)), Integer.valueOf(stDate.get(5)));
        String query = 'SELECT ID,R1_ACC_TXT_Id_Golden_record__c, CreatedDate FROM Account'
                +' WHERE CreatedDate>=:cDate and R1_ACC_TXT_Id_Golden_record__c!=null'
                +' ORDER BY CreatedDate ASC LIMIT 10000';
        Map<String,Account> accMap = new Map<String,Account>();
        Set<ID> acIDs = new Set<ID>();
        List<Account> accLst = Database.query(query);
        for(Account a : accLst){
            accMap.put(a.R1_ACC_TXT_Id_Golden_record__c,a);
            acIDs.add(a.ID);
        }
        if(accLst.size()>0){
            eDate = String.valueOf(accLst.get(accLst.size()-1).CreatedDate);
        }
        Set<String> gIDs = accMap.keySet();
        query = 'SELECT ID,R1_ACC_TXT_Id_Golden_record__c, CreatedDate FROM Account'
            +' WHERE R1_ACC_TXT_Id_Golden_record__c IN :gIDs AND ID NOT IN :acIDs';
        List<Account> acc1Lst = Database.query(query);
        accWrapperLst = new List<AccountWrapper>();
        for(Account a : acc1Lst){
            if(accMap.containsKey(a.R1_ACC_TXT_Id_Golden_record__c)){
                accWrapperLst.add(new AccountWrapper(a,accMap.get(a.R1_ACC_TXT_Id_Golden_record__c)));
            }
        }
    }
    
    public Class AccountWrapper{
        public Account acc{get;set;}
        public Account dupAcc{get;set;}
        public AccountWrapper(Account acc, Account dupAcc){
            this.acc = acc;
            this.dupAcc = dupAcc;
        }
    }
}