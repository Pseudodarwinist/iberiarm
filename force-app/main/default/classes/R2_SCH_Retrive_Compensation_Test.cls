/*---------------------------------------------------------------------------------------------------------------------
	Author:         Jaime Ascanta
	Company:        Accenture
	Description:    Apex test para la clase apex 'R2_SCH_Retrive_Compensation'
	IN:
	OUT:

	History:
	 <Date>                     <Author>                         <Change Description>
	08/11/2017       	   				Jaime Ascanta                   Initial Version
----------------------------------------------------------------------------------------------------------------------*/
@isTest
private class R2_SCH_Retrive_Compensation_Test {

	/*---------------------------------------------------------------------------------------------------------------------
	Author:         Jaime Ascanta
	Company:        Accenture
	Description:    Metodo test para la funcion execute()
	IN:
	OUT:

	History:
	 <Date>                     <Author>                         <Change Description>
	 08/11/2017       	   				Jaime Ascanta                   Initial Version
	----------------------------------------------------------------------------------------------------------------------*/
	@isTest
	static void execute_Test() {

		R1_CLS_LogHelper.throw_exception = false;

		String CRON_EXP = '0 0 0 3 9 ? 2022';
		String CRON_EXP_IN_DATETIME = '2022-09-03 00:00:00';
		String CRON_JOB_NAME = 'R2_SCH_Retrive_Compensation_Job';

		List<Case> listCases = [SELECT Id, Parent.Status,CreatedDate, R2_CAS_FOR_Destination__c,R2_CAS_PKL_Country__c, R2_CAS_TXT_TKT_Ticket__c,R1_CAS_TXT_PNR__c,Type FROM Case];

		System.debug('*** cases test' + listCases);

		Test.startTest();
				// Schedule the test job
				String jobId = System.schedule(CRON_JOB_NAME, CRON_EXP, new R2_SCH_Retrive_Compensation());
				// Get the information from the CronTrigger API object
				CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime, CronJobDetailId FROM CronTrigger WHERE Id = :jobId];
				//verica que se agrego la tarea
				System.assertNotEquals(null, ct);
				//verificamos nombre tarea
				CronJobDetail cjd = [SELECT Name FROM CronJobDetail WHERE Id= :ct.CronJobDetailId];
				System.assertEquals(CRON_JOB_NAME, cjd.Name);
				// verifica la expresion
				System.assertEquals(CRON_EXP, ct.CronExpression);
				// verifica la froxima fecha de ejecucion
		    System.assertEquals(CRON_EXP_IN_DATETIME, String.valueOf(ct.NextFireTime));
				// verifica que el trabajo no se ha ejecutado
		    System.assertEquals(0, ct.TimesTriggered);

		Test.stopTest();

		System.assertEquals(0, [SELECT count() FROM R1_Log__c]);
	}

	@testSetup
	static void setupTest(){

		Id rtExpediente = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Expediente').getRecordTypeId();
		Id rtPasaje = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Pasaje').getRecordTypeId();

		// vuelo
		R1_Flight__c vuelo = new R1_Flight__c();
		vuelo.Name = 'VueloTest';
		vuelo.R1_FLG_TXT_Origin__c = 'origen';
		vuelo.R1_FLG_TXT_Destination__c = 'destino';
		vuelo.R1_FLG_TXT_Airport_depart__c = 'FRA';
		vuelo.R1_FLG_TXT_Airport_arrive__c = 'MAD';
		vuelo.R1_FLG_TXT_Carrier_code__c = 'IB';
		vuelo.R1_FLG_DAT_Flight_date_local__c = Date.newInstance(2017, 12, 25);
		vuelo.R1_FLG_TXT_Flight_number__c = '1111';
		vuelo.R2_FLG_TXT_Stop_automation__c = false;
		insert vuelo;

		// expediente
		Case casExpe = new Case();
		casExpe.RecordTypeId = rtExpediente;
		casExpe.R1_CAS_LOO_Flight__c = vuelo.Id;
		casExpe.Status = 'Abierto';
		casExpe.Origin = 'Email';
		casExpe.Type = 'Retraso';
		casExpe.R2_CAS_CHK_AUT_Do_not_automate_flag__c = false;
		casExpe.R2_CAS_PKL_Exp_Pago_Claims_Auto__c = null;
		insert casExpe;

		// caso pasaje
		Case casPasaje = new Case();
		casPasaje.ParentId = casExpe.Id;
		casPasaje.RecordTypeId = rtPasaje;
		casPasaje.Status = 'Abierto';
		casPasaje.Origin = 'Email';
		casPasaje.Type = 'Retraso';
		casPasaje.R2_CAS_PKL_Country__c = 'es';
		casPasaje.R1_CAS_TXT_PNR__c = 'HF5DN';
		casPasaje.R2_CAS_TXT_TKT_Ticket__c = '0752371820782';
		insert casPasaje;
	}
}