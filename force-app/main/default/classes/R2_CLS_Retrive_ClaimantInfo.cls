global class R2_CLS_Retrive_ClaimantInfo {

	public class ClaimantInfoWebRequestWP{
        public String ticketNumber;
        public String pnr;
    }

	public class ClaimantInfoWebResponseWP{
        public String goldenRecordID;
        public List<FlightDetailsWP> flightDetails;
		public PassengerDetailsWP passengerDetails;
		public String pnrdate;
    }

	public class FlightDetailsWP{
        public String carrierCode;
        public String flightNumber;
		public String flightDate;
		public String journeyOrigin;
		public String journeyDestination;
		public String segmentOrigin;
		public String segmentDestination;
		public String cabinClassIndicator;
    }

	public class PassengerDetailsWP{
        public String passengerName;
        public String passengerSurname;
		public String telephoneNumber;
		public String email;
		public String frequentFlyerID;
		public String countryOfResidence;
		public String identificationDocumentType;
		public String identificationDocumentNumber;
    }


    //@future(callout=true)
	webservice static String getClaimantInfo(String pnr, String tkt, String caseId){
    	try{
    	 if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}

         String result = 'false';

		 ClaimantInfoWebRequestWP req = new ClaimantInfoWebRequestWP();
		 req.pnr = pnr;
		 req.ticketNumber = tkt;

		 ClaimantInfoWebResponseWP res = retriveClaimantInfo(req, 0);

		 System.debug('*** respuesta : ' + res);

		 if(res!=null){

			 List<Case> listCase = [SELECT Id, R2_CAS_TXT_PASS_Name_Passenger__c,R2_CAS_TXT_PASS_LastName_Passenger__c,R2_CAS_TLF_PASS_Phone_Passenger__c,
			 								R2_CAS_EMA_PASS_Email_Passenger__c,R2_CAS_TXT_Primary_Frecuent_Flyer_Card__c,R2_CAS_PKL_Country__c,
											 R2_CAS_PKL_Type_document__c, R2_CAS_TXT_Identification_number__c
			 						FROM Case 
									WHERE R1_CAS_TXT_PNR__c=:pnr AND R2_CAS_TXT_TKT_Ticket__c=:tkt AND Id=:caseId];

			 if(!listCase.isEmpty()){

                    if(res.goldenRecordID!=null && res.goldenRecordID!=''){
                        Account acc = getAccount(res.goldenRecordID);
                        if(acc!=null){
                            listCase[0].AccountId = acc.Id;
                        }
                    }
				 	
				 	listCase[0].R2_CAS_TXT_PASS_Name_Passenger__c = res.passengerDetails.passengerName;
				 	listCase[0].R2_CAS_TXT_PASS_LastName_Passenger__c = res.passengerDetails.passengerSurname;
				 	listCase[0].R2_CAS_TLF_PASS_Phone_Passenger__c = res.passengerDetails.telephoneNumber;
					listCase[0].R2_CAS_EMA_PASS_Email_Passenger__c = res.passengerDetails.email;
					listCase[0].R2_CAS_TXT_Primary_Frecuent_Flyer_Card__c = res.passengerDetails.frequentFlyerID!=null ? formatFrecuentFly(res.passengerDetails.frequentFlyerID) : null;
					listCase[0].R2_CAS_PKL_Country__c = res.passengerDetails.countryOfResidence;
					listCase[0].R2_CAS_PKL_Type_document__c = res.passengerDetails.identificationDocumentType;
					listCase[0].R2_CAS_TXT_Identification_number__c = res.passengerDetails.identificationDocumentNumber;

					if(res.flightDetails!=null && !res.flightDetails.isEmpty()){
						R1_Flight__c flg = getflight(res.flightDetails[0]);
						if(flg!=null){
							listCase[0].R1_CAS_LOO_Flight__c = flg.Id;
						}

                        listCase[0].R1_CAS_Fecha_vuelo__c = res.flightDetails[0].flightDate!=null ? Date.valueOf(res.flightDetails[0].flightDate) : null;
                        listCase[0].R1_CAS_TXT_Vuelo__c = res.flightDetails[0].flightNumber;
                        listCase[0].R2_CAS_PKL_Class_flown__c = res.flightDetails[0].cabinClassIndicator;
						listCase[0].R2_CAS_TXT_Flight_Origin__c = res.flightDetails[0].journeyOrigin;
						listCase[0].R2_CAS_TXT_Flight_Desnity__c = res.flightDetails[0].journeyDestination;
                        
					}

				 update listCase[0];

                 result = 'true';
			 }
		 }

         return result;
         
        }catch(Exception exc){
			R1_CLS_LogHelper.generateErrorLog('R2_CLS_Retrive_ClaimantInfo.getClaimantInfo()', '', exc.getmessage()+', '+exc.getLineNumber(), 'Case');
            return 'false';
		}
		
    }

    public static String formatFrecuentFly(String ffl){
		try{
            String result = null;
            List<String> lstFF = ffl.split('/');
            if(lstFF!=null && lstFF.size()>1){
                result = lstFF[1];
            }
            return result;
        }catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_CLS_Retrive_ClaimantInfo.formatFrecuentFly()', '', exc.getmessage()+', '+exc.getLineNumber(), 'Case');
            return null;
        }
	}

	public static Account getAccount(String goldenRecordId){
		try{
			if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}
			Account result = null;
			List<Account> listAcc = [SELECT Id FROM Account WHERE R1_ACC_TXT_Id_Golden_record__c=:goldenRecordId ];
			if(listAcc!=null && !listAcc.isEmpty()){
				result = listAcc[0];
			}
			return result;
		}catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_CLS_Retrive_ClaimantInfo.getAccount()', '', exc.getmessage()+', '+exc.getLineNumber(), 'Case');
            return null;
        }
	}

	public static R1_Flight__c getflight(FlightDetailsWP fliData){
		try{
			if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}
			R1_Flight__c result = null;
			
            String flgName = formatflightName(fliData.flightNumber, fliData.flightDate);

            System.debug('*** flgName: ' + flgName);

			List<R1_Flight__c> listFlg = [SELECT Id, Name FROM R1_Flight__c WHERE Name=:flgName];
			if(listFlg!=null && !listFlg.isEmpty()){
				result = listFlg[0];

                System.debug('*** listFlg: ' + listFlg);
			}
			return result;
		}catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_CLS_Retrive_ClaimantInfo.getflight()', '', exc.getmessage()+', '+exc.getLineNumber(), 'Case');
            return null;
        }
	}

    public static String formatflightName(String flightNumber, String flightDate){
		try{
            String result = null;
            List<String> lstDate = flightDate.split('-');
			result = flightNumber+'_'+lstDate[0]+lstDate[1]+lstDate[2];
            return result;
        }catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_CLS_Retrive_ClaimantInfo.formatflightName()', '', exc.getmessage()+', '+exc.getLineNumber(), 'Case');
            return null;
        }
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Jaime Ascanta
    Company:        Accenture
    Description:    Retrive Info Claimant

    IN:
    OUT:

    History:
    <Date>                     <Author>                <Change Description>
    16/08/2018                 Jaime Ascanta 		    Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static ClaimantInfoWebResponseWP retriveClaimantInfo (ClaimantInfoWebRequestWP requestWRP, Integer intentos){

        try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');} //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;

            System.debug('*** entra en la integracion retriveClaimantInfo()');

            String wsMethod = 'R2_Retrive_Claimant_info';

            if(R1_CS_Endpoints__c.getInstance(wsMethod) == null || !R1_CS_Endpoints__c.getInstance(wsMethod).R1_CHK_Activo__c){
                	return null;
            }

            String token = R1_CLS_Utilities.getCache('local.sessionCache.token');
            if(token ==null){
                if(intentos<3){
                    intentos= intentos+1;
                    R1_CLS_SendCustomerMDM.login();
                    return retriveClaimantInfo(requestWRP, intentos);
                }else{
                    R1_CLS_LogHelper.generateErrorLog('R2_CLS_Retrive_ClaimantInfo.retriveClaimantInfo()', '', 'No se puede conectar con Intelligence Integration', 'Claim info');
                    return null;
                }
            }


            HttpRequest req = new HttpRequest();
            String endPoint = R1_CS_Endpoints__c.getInstance(wsMethod).R1_TXT_EndPoint__c;
            String bodyReq = JSON.serialize(requestWRP);

            req.setHeader('Authorization', 'Bearer ' + token);
            req.setEndpoint(endPoint);
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/json');
            req.setTimeout(15000);
            req.setBody(bodyReq);

            System.debug(req);

            System.debug('*** req body: ' + bodyReq);

            Http http = new Http();
            HTTPResponse res;


            res = http.send(req);

            System.debug('*** response status code: '+res.getStatusCode());
            System.debug('*** response body: '+res.getBody());

            if(res.getStatusCode()==200 || res.getStatusCode()==201 || res.getStatusCode()==202 || res.getStatusCode()==203){

                System.debug('*** antes de deserialize: ');
                ClaimantInfoWebResponseWP resp = (ClaimantInfoWebResponseWP)JSON.deserialize(res.getBody(), ClaimantInfoWebResponseWP.class);

                System.debug('wp to return: '+resp);
                return resp;

            }else{

                if(intentos<3 && res.getStatusCode() == 401){
                    intentos+=1;
                        R1_CLS_SendCustomerMDM.login();
                        return retriveClaimantInfo(requestWRP, intentos);
                }else{
                    return null;
                }
            }

        }catch(CalloutException exc){
            if(intentos<3){
                    intentos+=1;
                    return retriveClaimantInfo(requestWRP, intentos);
            }else{
                System.debug('*** retrieveCompensation() CalloutException  : ' + exc.getmessage());
                return null;
            }
        }catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_CLS_Retrive_ClaimantInfo.retriveClaimantInfo()', '', exc.getmessage()+', '+exc.getLineNumber(), 'Case');
            return null;
        }
    }


}