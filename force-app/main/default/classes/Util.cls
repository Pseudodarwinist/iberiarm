public with sharing class Util {

    
    public static Datetime formatStringDateToCST (String dateString) {
        
        Date d = Date.valueOf(dateString);
        DateTime dt = DateTime.newInstance(d, Time.newInstance(0, 0, 0, 0));

        Datetime dateInCST = dateInCSTFromUserTimeZone(dt);

        return dateInCST;
    }

    public static String convertDateToHumanReadable(Datetime dateToConvert) {

        String humanReadableDate = '';

        if (dateToConvert != null) {
            humanReadableDate = dateInUserTimeZoneFromCST(dateToConvert).format();
        }

        return humanReadableDate;
    }

    public static String convertDateToHumanReadableWithNoTime(Datetime dateToConvert) {

        String humanReadableDate = '';

        if (dateToConvert != null) {
            humanReadableDate = dateInUserTimeZoneFromCST(dateToConvert).date().format();
        }

        return humanReadableDate;
    }
    

    private static Datetime dateInUserTimeZoneFromCST(Datetime dateInCST) {
        return dateInCST.addSeconds(secondsFromTimeZoneToUserTimeZone('CST'));
    }

    private static DateTime dateInCSTFromUserTimeZone(DateTime dateInUTC) {
        return dateInUTC.addSeconds(-secondsFromTimeZoneToUserTimeZone('CST'));
    }

    private static Integer secondsFromTimeZoneToUserTimeZone(String userTimeZone) {

        Integer offset1 = UserInfo.getTimeZone().getOffset(DateTime.now()); // offset from user zone to UTC
        Integer offset2 = TimeZone.getTimeZone(userTimeZone).getOffset(DateTime.now()); // offset from given time zone to UTC
        
        return (offset1 - offset2) / 1000;
    }

    private static String getCurrentDateTimeFormat() {
        // Hack to know the default date format 
        String knownDate = Datetime.newInstance(1980, 10, 27, 23, 30, 55).format();

        String currentFormat = knownDate
            .replace('1980', 'YYYY')
            .replace('10', 'MM')
            .replace('27', 'dd')
            .replace('23', 'HH')
            .replace('11', 'h')
            .replace('30', 'mm')
            .replace('55', 'ss');

        if(currentFormat.contains('h:mm')) {

            currentFormat = currentFormat.replace('PM', '').replace('AM', '');
            currentFormat += ' aa';
        }

        currentFormat += ' zz';

        return currentFormat;
    }

    private static String getCurrentDateFormat() {
        // Hack to know the default date format 
        String knownDate = Date.newInstance(1980, 10, 27).format();

        String currentFormat = knownDate
            .replace('1980', 'YYYY')
            .replace('10', 'MM')
            .replace('27', 'dd');


        return currentFormat;
    }

}