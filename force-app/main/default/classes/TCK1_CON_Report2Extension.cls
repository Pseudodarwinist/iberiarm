/**
* @author TCK&LCS
* @date 01/08/2019
* @description PDFRepor2 controller extension class.Generate a PDF version for Report 2
*/
public class TCK1_CON_Report2Extension {
    public string existReport {get;set;}
    public string logo {get;set;} 


	/**
    * @author TCK&LCS
    * @date 01/08/2019
    * @description Class constructor controller. Populates the Visualforce with the appearance of PDF version.
    * @param ApexPages.standardController Report 2 Standard Controller.
    */
    public TCK1_CON_Report2Extension(ApexPages.standardController stdCtr){
        String reportId = ApexPages.currentPage().getParameters().get('id');
        List<ContentDocumentLink> cdl = new List<ContentDocumentLink>();
        cdl = [SELECT Id,ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: reportId];
        List<String> cdlIds = new List<String>();
        List<ContentDocument> cont = new List<ContentDocument>();
        List<String> contDoc = new List<String>();
        for(ContentDocumentLink aux:cdl){
            cdlIds.add(aux.Id);
            contDoc.add(aux.ContentDocumentId);
        }
        cont = [SELECT Id FROM ContentDocument WHERE Id IN:contDoc AND Title LIKE 'Report 2%'];
        system.debug('cont:: '+cont);
        if(cont.isEmpty()){
            existReport =  'false';
        }else{
            existReport =  'true';
        }
    


     TCK1_Report2__c qryReport2 = [ SELECT TCK1_RP2_Expediente__r.R1_CAS_LOO_Flight__c,TCK1_RP2_Expediente__r.R1_CAS_LOO_Flight__r.R1_FLG_TXT_Carrier_code_oper__c,TCK1_RP2_Expediente__r.R1_CAS_LOO_Flight__r.R1_FLG_TXT_Flight_number__c FROM TCK1_Report2__c WHERE id =:reportId LIMIT 1];  
        system.debug('Compañia operadora: '+ qryReport2.TCK1_RP2_Expediente__r.R1_CAS_LOO_Flight__r.R1_FLG_TXT_Carrier_code_oper__c);
        logo = qryReport2.TCK1_RP2_Expediente__r.R1_CAS_LOO_Flight__r.R1_FLG_TXT_Carrier_code_oper__c;
        if(logo == 'IB'){
            if(integer.valueof(qryReport2.TCK1_RP2_Expediente__r.R1_CAS_LOO_Flight__r.R1_FLG_TXT_Flight_number__c) >= 2600 && integer.valueof(qryReport2.TCK1_RP2_Expediente__r.R1_CAS_LOO_Flight__r.R1_FLG_TXT_Flight_number__c) <= 2999){
                logo = 'LV';
            }else if(integer.valueof(qryReport2.TCK1_RP2_Expediente__r.R1_CAS_LOO_Flight__r.R1_FLG_TXT_Flight_number__c) >= 3600 && integer.valueof(qryReport2.TCK1_RP2_Expediente__r.R1_CAS_LOO_Flight__r.R1_FLG_TXT_Flight_number__c) <= 3999){
                logo = 'I2';
            }else if(integer.valueof(qryReport2.TCK1_RP2_Expediente__r.R1_CAS_LOO_Flight__r.R1_FLG_TXT_Flight_number__c) >= 8000 && integer.valueof(qryReport2.TCK1_RP2_Expediente__r.R1_CAS_LOO_Flight__r.R1_FLG_TXT_Flight_number__c) <= 8999){
                logo = 'YW';
            }
      }
    }
	/**
    * @author TCK&LCS
    * @date 01/08/2019
    * @description Generates the PDF report based on Report 2 parameters
    */
    @RemoteAction
    public static void makePDFReport2(){
        String reportId = ApexPages.currentPage().getParameters().get('id');
        integer versionReport1 = getReport2(reportId);
        PageReference ref = new PageReference('/apex/TCK1_RP2_PDFReport2?id=' + reportId);
        ContentVersion cont = new ContentVersion();
        cont.Title = 'Report 2 v.'+versionReport1;
        cont.PathOnClient = 'report2v'+versionReport1+'.pdf';
        if(Test.isRunningTest()){
            cont.VersionData = blob.valueOf('Test');
        }else{
            cont.VersionData = ref.getContentAsPdf();
        }
        cont.Origin = 'H';
        insert cont;
        ContentDocument cd = new ContentDocument();
        cd = [SELECT Id FROM ContentDocument WHERE LatestPublishedVersionId =: cont.id LIMIT 1];
        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.ContentDocumentId = cd.Id;
        cdl.LinkedEntityId = reportId;
        cdl.ShareType = 'I';
        insert cdl;
    }

	/**
    * @author TCK&LCS
    * @date 01/08/2019
    * @description Get the number of PDF versions created
	* @param String The recordId of Report 2 to be generated as PDF
	* @return Integer Return the number + 1 of PDF versions created
    */
    public static Integer getReport2(String recordId){
        List<ContentDocumentLink> cdl = new List<ContentDocumentLink>();
        cdl = [SELECT Id,ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: recordId];
        List<String> cdlIds = new List<String>();
        List<ContentDocument> cont = new List<ContentDocument>();
        List<String> contDoc = new List<String>();
        for(ContentDocumentLink aux:cdl){
            cdlIds.add(aux.Id);
            contDoc.add(aux.ContentDocumentId);
        }
        cont = [SELECT Id FROM ContentDocument WHERE Id IN:contDoc AND Title LIKE 'Report 2%'];
        return cont.size()+1;
    }
}