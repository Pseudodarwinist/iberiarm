/*
* @author TCK&LCS
* @date 25/07/2019 
* @description Handler class of Report 1 Trigger. When Report1 is Created, update the lookup from Flight to become the main Report1

<pre>
FECHA				AUTOR					ACCION
25/07/2019			NPG-LCS					Create class
09/08/2019			ICA-LCS					Modification. When Report1 is Created, update the lookup from Flight to become the main Report1
09/08/2019			ICA-LCS					Modification. Changed to without sharing to avoid giving permissions to flight object only for fill the lookup field
</pre>
*/
public without sharing class TCK1_Report1Trigger_Handler 
{

	/**
    * @author TCK&LCS
    * @date 25/07/2019 
    * @description Method executed on after Report 1 insert. When Report1 is Created, update the lookup from Flight to become the main Report1
    * @param List<TCK1_Report1__c> Trigger new List
	* @param Map<Id,TCK1_Report1__c> Trigger new Map
    */
	public static void onAfterInsert(List<TCK1_Report1__c> newListReports1,Map<Id,TCK1_Report1__c> newMapReports1){
		Map<Id,Id> mapFlight2Report1 = new Map<Id,Id>();
		List<R1_Flight__c> flightList2Update = new List<R1_Flight__c>();
		//Map flight Ids with the new corresponding main Reports
		for(TCK1_Report1__c r1 : newListReports1){
			mapFlight2Report1.put(r1.TCK1_RP1_LOO_Vuelo__c,r1.Id);
		}
		//Query the related list of flights and update with their report
		List<R1_Flight__c> flightList = new List<R1_Flight__c>([Select Id,Report_1__c From R1_Flight__c where id in:mapFlight2Report1.keySet()]);
		for(R1_Flight__c flight: flightList){
			flight.Report_1__c = mapFlight2Report1.get(flight.Id);
			flightList2Update.add(flight);
		}
		update flightList2Update;
    }
}