global class R2_BTCH_CheckStatusSegment implements Database.Batchable<sObject> {
	
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:        Alvaro Garcia Tapia
	Company:       Accenture
	Description:   Get all the segment that have to send to SAP

	
	History: 
	
	<Date>                  <Author>                <Change Description>
	14/02/2018              Alvaro Garcia Tapia        Initial Version 
	16/05/2018              Jaime Ascanta 		       todos los segmentos con 60 dias independiente de si hay emails o no. eliminado (AND R2_SEG_CHK_Message_Received__c=false)     
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	global Id lastIdEvaluated;

	global R2_BTCH_CheckStatusSegment(Id lastId) {
		System.debug('*** init R2_BTCH_CheckStatusSegment');
		Id lastIdEvaluated = lastId;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		try{
    		if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}
			System.debug('*** R2_BTCH_CheckStatusSegment Star batch');

			Date fecha = Date.today().addDays(-60);
			//Date fecha = System.today().addDays(1);
			
			String query = 'SELECT Id, R2_SEG_PKL_Estado__c, R2_SEG_DAT_Date_Msg_Sent__c FROM R2_Segmento__c' +
								+ ' WHERE R2_SEG_PKL_Estado__c = \'Prorrateo OK\' AND R2_SEG_PKL_Segment_Status__c!=\'Rechazado\' AND R2_SEG_DAT_Date_Msg_Sent__c  < :fecha AND Id > :lastIdEvaluated';

			//Para probar con una hora
			// DateTime fecha = System.now().addHours(-1);
			// String query = 'SELECT Id, R2_SEG_PKL_Estado__c, R2_SEG_DAT_Date_Msg_Sent__c FROM R2_Segmento__c' +
			// 					+ ' WHERE R2_SEG_PKL_Estado__c = \'Prorrateo OK\' AND LastModifiedDate  < :fecha AND Id > :lastIdEvaluated';					

			return Database.getQueryLocator(query);

		}catch(Exception exc){
			R1_CLS_LogHelper.generateErrorLog('R2_BTCH_CheckStatusSegment.start()', '', exc.getmessage()+', '+exc.getLineNumber(), 'R2_Segmento__c');
			return null;
		}
	}

   	global void execute(Database.BatchableContext BC, List<R2_Segmento__c> scope) {
		try{
    		if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');} //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;    

	   		System.debug('Batch: R2_BTCH_CheckStatusSegment --> scope: ' + scope);
			
			List<R2_Segmento__c> segmentUpdateLst = new List<R2_Segmento__c>();
	   		for (R2_Segmento__c segment : scope) {
   				segment.R2_SEG_PKL_Estado__c = 'A cobrar';
   				segment.R2_SEG_PKL_Segment_Status__c = 'Aceptado';
   				segmentUpdateLst.add(segment);

	   			lastIdEvaluated = segment.Id;
	   		}

	   		if (!segmentUpdateLst.isEmpty()) {
				System.debug('*** segmentUpdateLst: ' + segmentUpdateLst);
	   			update segmentUpdateLst;
	   		}
	   			
		}catch(Exception exc){

			R1_CLS_LogHelper.generateErrorLog('R2_BTCH_CheckStatusSegment.execute()', '', exc.getmessage()+', '+exc.getLineNumber(), 'R2_Segmento__c');
		}
	}
	
	global void finish(Database.BatchableContext BC) {
		try{

			System.debug('*** R2_BTCH_CheckStatusSegment finish batch');

    		if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');} //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;    

    		//query para comprobar si hay mas registros no procesados, en ese caso llamar al batch
    		List <R2_Segmento__c> segmentLst = [SELECT Id, R2_SEG_PKL_Estado__c, R2_SEG_DAT_Date_Msg_Sent__c FROM R2_Segmento__c 
    														WHERE R2_SEG_PKL_Estado__c = 'Prorrateo OK'
															AND  R2_SEG_PKL_Segment_Status__c != 'Rechazado'
    														//AND R2_SEG_CHK_Message_Received__c = false 
    														AND R2_SEG_DAT_Date_Msg_Sent__c  < :Date.today().addDays(-60)
    														AND Id > :lastIdEvaluated
    														LIMIT 1];

    		if (segmentLst.size() > 0) {
    			Database.executeBatch(new R2_BTCH_CheckStatusSegment(lastIdEvaluated));
    		}
    		else {//en otro caso llamada al job con today +1
 
    			Datetime fecha = DateTime.newInstance(date.today().addDays(1).year(), date.today().addDays(1).month(), date.today().addDays(1).day(), 00, 00, 00);
    			//Datetime fecha = DateTime.now();
    			//DateTime fechaAux = fecha.addMinutes(2);
				//Datetime fechaHora = fecha.addHours(R1_Job_Helper__c.getInstance('R1_Merge_JOB').R1_DT_Hora_inicio_Job__c.hour());
				//System.debug('fecha: ' + fecha);
				//System.debug('fechaAux: ' + fechaAux);
    			//Programamos el job para la siguiente ejecucion 
    			System.schedule('CheckStatusSegment_JOB' + DateTime.now(), R1_CLS_Utilities.generateStrProg(fecha), new R2_JOB_CheckStatusSegment());
    		}

		
		}catch(Exception exc){

			R1_CLS_LogHelper.generateErrorLog('R2_BTCH_CheckStatusSegment.finish()', '', exc.getmessage()+', '+exc.getLineNumber(), 'R1_Cliente_duplicado__c');
		}
	}
	
}