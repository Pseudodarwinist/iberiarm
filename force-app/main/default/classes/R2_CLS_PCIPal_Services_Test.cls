@IsTest
public with sharing class R2_CLS_PCIPal_Services_Test {

    @IsTest
    public static void login(){
        R1_CLS_LogHelper.throw_exception = false;
        List<R1_CS_Endpoints__c> lst_ep=new List<R1_CS_Endpoints__c>();
        
        R1_CS_Endpoints__c ep=new R1_CS_Endpoints__c();
        ep.Name='PCI Pal Services';
        ep.R1_CHK_Activo__c=true;
        ep.R1_TXT_EndPoint__c='PCIPalLogin';
        lst_ep.add(ep);
        insert lst_ep;

        Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());

        
        Test.startTest();
        R2_CLS_PCIPal_Services.loginPCIPal();
        Test.stopTest();


    }

     @IsTest
    public static void session(){
        R1_CLS_LogHelper.throw_exception = false;
        List<R1_CS_Endpoints__c> lst_ep=new List<R1_CS_Endpoints__c>();
        
        R1_CS_Endpoints__c ep=new R1_CS_Endpoints__c();
        ep.Name='PCI Pal SessionId';
        ep.R1_CHK_Activo__c=true;
        ep.R1_TXT_EndPoint__c='PCIPalSession';
        lst_ep.add(ep);
        insert lst_ep;

        Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());

        
        Test.startTest();
        R2_CLS_PCIPal_Services.obtainSessionId('accesstoken');
        Test.stopTest();


    }
}