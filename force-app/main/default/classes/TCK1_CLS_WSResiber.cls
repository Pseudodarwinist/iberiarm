public class TCK1_CLS_WSResiber {

  @future(Callout = true)
  public static void getDatos(Decimal aviosPasajero, Decimal aviosPasajero2, Integer aviosPE, Integer aviosTUR1, Integer aviosTUR2, String clase, String carrierCode, String flightNumber, String localLegDate, String departureStation, String arrivalStation, String requestingCarrier, String requestingSystem, String nameCliente, Date diaVuelo, String acceso, String clienteID, String tramo) {
    TCK1_CLS_WSResiber_RQ requestParams = new TCK1_CLS_WSResiber_RQ();
    requestParams.carrierCode = carrierCode;
    requestParams.flightNumber = flightNumber;
    requestParams.localLegDate = localLegDate;
    requestParams.departureStation = departureStation;
    requestParams.arrivalStation = arrivalStation;
    requestParams.requestingCarrier = requestingCarrier;
    requestParams.requestingSystem = requestingSystem;
    HttpResponse result = sendRequest(requestParams);
    TCK1_CLS_WSResiber_RP respuesta = new TCK1_CLS_WSResiber_RP();
    respuesta = analizeResponse(result);
    verifyingData(respuesta, aviosPasajero, aviosPasajero2, aviosPE, aviosTUR1, aviosTUR2, clase, carrierCode, flightNumber, nameCliente, diaVuelo, acceso, clienteID, tramo);
  }

  public static HttpResponse sendRequest(TCK1_CLS_WSResiber_RQ request) {

    //Ask the token to the WS
    Http http = new Http();
    TCK1_MTD_WSInfo__mdt WS_Resiber = [SELECT WSR_TEX_EndPoint__c, WSR_NUM_TimeOut__c, WSR_TEX_ContentType__c, R3_WSR_TEX_ApiKey__c, WSR_TXT_Method__c FROM TCK1_MTD_WSInfo__mdt WHERE MasterLabel = 'WSResiber'
        LIMIT 1
    ];
    HttpRequest req = new HttpRequest();

    /*MODIFICADO TOKEN 06/08/2019*/
    Integer contadorResiber = 0;
    String token = R1_CLS_Utilities.getCache('local.sessionCache.tokenResiber');
    //First try to get the token from cache
    System.debug('Token cache: ' + token);
    if (token == null) {
      //3 trys to get the token from the WS
      for (Integer i = 0; i < 3; ++i) {
        token = TCK1_CLS_Token.loginToken();
        R1_CLS_Utilities.putCache('local.sessionCache.tokenResiber', token);
        System.debug('Token loginToken: ' + token);
        if (token != null) {
          break;
        }
      }
    }
    /*FIN MODIFICADO TOKEN 06/08/2019*/

    req.setHeader('Authorization', 'Bearer ' + token);
    req.setHeader('Accept', WS_Resiber.WSR_TEX_ContentType__c);
    req.setHeader('Content-Type', WS_Resiber.WSR_TEX_ContentType__c);
    req.setHeader('api_key', WS_Resiber.R3_WSR_TEX_ApiKey__c);
    String endPoint = WS_Resiber.WSR_TEX_EndPoint__c.replace('FLIGHTNUMBER', request.CarrierCode + request.flightNumber).replace('LOCALLEGDATE', request.LocalLegDate).replace('DEPARTURESTATION', request.DepartureStation).replace('ARRIVALSTATION', request.ArrivalStation);
    req.setMethod(WS_Resiber.WSR_TXT_Method__c);
    req.setEndpoint(endPoint);
    system.debug(endPoint);
    req.setTimeout(WS_Resiber.WSR_NUM_TimeOut__c.intValue());
    system.debug('reqBody:' + req.getBody());
    HttpResponse result;

    //3 trys to get the req from the WS
    for (Integer j = 0; j <= 3; ++j) {
      result = http.send(req);
      system.debug('result code: ' + result.getStatusCode());
      system.debug('result body: ' + result.getBody());
      if (result.getStatusCode() == 200) {
        break;
      } else if (result.getStatusCode() == 401) {
        token = TCK1_CLS_Token.loginToken();
        system.debug('token nuevo: ' + token);
        if (token != null)
        req.setHeader('Authorization', 'Bearer ' + token);
      }
    }

    system.debug('result: ' + result);
    system.debug('result body: ' + result.getBody());
    return result;
  }

  private static TCK1_CLS_WSResiber_RP analizeResponse(HttpResponse respuesta) {
    TCK1_CLS_WSResiber_RP response = new TCK1_CLS_WSResiber_RP();
    try {
      if (respuesta.getbody() != null && respuesta != null && respuesta.getStatusCode() == 200) {
        response = (TCK1_CLS_WSResiber_RP) System.JSON.deserialize(respuesta.getBody(), TCK1_CLS_WSResiber_RP.class);
        system.debug('respuesta : ' + response);
      }
      /*response = (TCK1_CLS_WSResiber_RP) System.JSON.deserialize('{"legData":{"carrierCode":"IB","flightNumber":"730","departureDateTime":"2019-08-09T00:00:00.000Z","origin":"MAD","destination":"BCN","legCabinData":[{"cabinCode":"J","actualCapacity":"24","authorizedCapacity":"25","seatsSold":"7","seatsAvailable":"12"},{"cabinCode":"W","actualCapacity":"24","authorizedCapacity":"25","seatsSold":"7","seatsAvailable":"12"},{"cabinCode":"Y","actualCapacity":"144","authorizedCapacity":"154","seatsSold":"99","seatsAvailable":"35"}]}}', TCK1_CLS_WSResiber_RP.class);
      system.debug('respuesta : ' + response);*/
    } catch (exception e) {
      system.debug('error ' + e.getMessage() + ' al analizar la response ' + respuesta.getBody() + ' en el WSResiber - Linea ' + e.getLineNumber());
      return null;
    }
    return response;
  }

  public static void verifyingData(TCK1_CLS_WSResiber_RP respuesta, Decimal aviosPasajero, Decimal aviosPasajero2, Integer aviosPE, Integer aviosTUR1, Integer aviosTUR2, String clase, String carrierCode, String flightNumber, String nameCliente, Date diaVuelo, String acceso, String clienteID, String tramo) {
    try{
      List < TCK1_CLS_WSResiber_RP.LegCabinData > lista = new List < TCK1_CLS_WSResiber_RP.LegCabinData > ();
      String claseCabina;
      Integer numAsientosBusLibres;
      Integer numAsientosPELibres;
      for (TCK1_CLS_WSResiber_RP.LegCabinData aux: respuesta.LegData.legCabinData) {
        //custom setting Sala VIP
        //NPG
        List < Acceso_Sala_VIP__c > salasVIP = new List < Acceso_Sala_VIP__c >() ;
        Acceso_Sala_VIP__c salasclase1 = Acceso_Sala_VIP__c.getValues('ClasePE');
        Acceso_Sala_VIP__c salasclase2 = Acceso_Sala_VIP__c.getValues('ClaseTUR');
        Acceso_Sala_VIP__c salasclase3 = Acceso_Sala_VIP__c.getValues('ClaseBUS');
        salasVIP.add(salasclase1);
        salasVIP.add(salasclase2);
        salasVIP.add(salasclase3);
          
        for (Acceso_Sala_VIP__c salaVIP: salasVIP) {
          if (salaVIP.ValorTexto__c.contains(aux.cabinCode)) {
          claseCabina = salaVIP.Name;
          }
        }
        if (claseCabina == 'ClaseBUS') {
          numAsientosBusLibres = integer.valueof(aux.seatsAvailable);
        }
        else if (claseCabina == 'ClasePE') {
          numAsientosPELibres = integer.valueof(aux.seatsAvailable);
        }
      }

      String margBusLibres;
      String margPeLibres;
      String vuelo = carrierCode + flightNumber;
      List < Acceso_Sala_VIP__c > margenesSalaVIP = Acceso_Sala_VIP__c.getall().values();
      for (Acceso_Sala_VIP__c margenSalaVIP: margenesSalaVIP) {
        if (margenSalaVIP.Name == 'Margen_BUS') {
          margBusLibres = margenSalaVIP.ValorTexto__c;
        } else if (margenSalaVIP.Name == 'Margen_PE') {
          margPeLibres = margenSalaVIP.ValorTexto__c;
        }
      }

      switch on clase {
        when 'ClaseTUR' {
          if (aviosPasajero >= aviosTUR2 || aviosPasajero2 >= aviosTUR2) {
            if (margBusLibres != null && margPeLibres != null && numAsientosBusLibres >= Integer.valueOf(margBusLibres) && numAsientosPELibres >= Integer.valueOf(margPeLibres)) {
              //Mensaje doble
              String mensaje = 'Mssg TUR/PE-BUS';
              insertMsg(mensaje, nameCliente, aviosPE, aviosTUR1, aviosTUR2, vuelo, diaVuelo, acceso, clienteID,clase,tramo);

            } else if (margBusLibres != null && numAsientosBusLibres >= Integer.valueOf(margBusLibres)) {
              //mensaje Bus 
              String mensaje = 'Mssg TUR-BUS';
              insertMsg(mensaje, nameCliente, aviosPE, aviosTUR1, aviosTUR2, vuelo, diaVuelo, acceso, clienteID,clase,tramo);

            } else if (margPeLibres != null && numAsientosPELibres >= Integer.valueOf(margPeLibres)) {
              //mensaje PE
              String mensaje = 'Mssg TUR-PE';
              insertMsg(mensaje, nameCliente, aviosPE, aviosTUR1, aviosTUR2, vuelo, diaVuelo, acceso, clienteID,clase,tramo);
            }
          }
          if ((aviosPasajero >= aviosTUR1 && aviosPasajero < aviosTUR2) || (aviosPasajero2 >= aviosTUR1 && aviosPasajero2 < aviosTUR2)) {

            if (margPeLibres != null && numAsientosPELibres >= Integer.valueOf(margPeLibres)) {
              //mensaje PE   
              String mensaje = 'Mssg TUR-PE';
              insertMsg(mensaje, nameCliente, aviosPE, aviosTUR1, aviosTUR2, vuelo, diaVuelo, acceso, clienteID,clase,tramo);
            }
          }
        }
        when 'ClasePE' {
          if (aviosPasajero >= aviosPE || aviosPasajero2 >= aviosPE) {
            if (margBusLibres != null && numAsientosBusLibres >= Integer.valueOf(margBusLibres)) {
              //mensaje Bus
              String mensaje = 'Mssg PE-BUS';
              insertMsg(mensaje, nameCliente, aviosPE, aviosTUR1, aviosTUR2, vuelo, diaVuelo, acceso, clienteID, clase,tramo);
            }
          }
        }
      }
    }catch (Exception exc) {
      R1_CLS_LogHelper.generateErrorLog('TCK1_CLS_WSResiber.verifyingData()', '', exc.getmessage() + ', ' + exc.getLineNumber(), 'WSResiber');
    }
  }

  public static void insertMsg(string mensaje, string nameCliente, Integer aviosPE, Integer aviosTUR1, Integer aviosTUR2, string vuelo, Date diaVuelo, String acceso, String clienteID,String clase, String tramo) {
    try {
      string mmsgVIP;
      List < Mensajes_Sala_VIP__c > mensajesSalaVIP = Mensajes_Sala_VIP__c.getall().values();
      for (Mensajes_Sala_VIP__c mssgSalaVIP: mensajesSalaVIP) {
        if (mssgSalaVIP.Name.contains(mensaje)) {
        mmsgVIP = mssgSalaVIP.MensajesSIP__c;
      }
    }

    Datetime auxDate = diaVuelo;
    String fecha = auxDate.format('dd-MM-yyyy');
    R1_Alert__c mssg = new R1_Alert__c();
    mssg.R1_ATA_LOO_Client__c = clienteID;
    mssg.Name = label.Upgrade_de_Sala_VIP;
    mssg.R1_ATA_DATH_Expiry_date__c = DateTime.Now().addDays(1);
    mssg.R2_ATA_PKL_Type__c = 'Promoción/Venta';
    mssg.R1_ATA_ATXTL_Description__c = mmsgVIP;
    mssg.R1_ATA_ATXTL_Description__c = mssg.R1_ATA_ATXTL_Description__c.replace('[nameClient]', nameCliente);
    mssg.R1_ATA_ATXTL_Description__c = mssg.R1_ATA_ATXTL_Description__c.replace('[vuelo]', vuelo);
    mssg.R1_ATA_ATXTL_Description__c = mssg.R1_ATA_ATXTL_Description__c.replace('[aviosTUR1]', String.valueOf(aviosTUR1));
    mssg.R1_ATA_ATXTL_Description__c = mssg.R1_ATA_ATXTL_Description__c.replace('[aviosTUR2]', String.valueOf(aviosTUR2));
    mssg.R1_ATA_ATXTL_Description__c = mssg.R1_ATA_ATXTL_Description__c.replace('[aviosPE]', String.valueOf(aviosPE));
    mssg.R1_ATA_ATXTL_Description__c = mssg.R1_ATA_ATXTL_Description__c.replace('[diaVuelo]', String.valueOf(fecha));
    mssg.R3_ATA_TXT_Tramo__c = tramo;
    mssg.R1_ATA_PKL_Alert_for__c = 'Vip Lounge;Admin';
    mssg.R3_ATA_NUM_CostPE__c = aviosTUR1;
    if(clase == 'ClasePE'){
    mssg.R3_ATA_NUM_CostBUS__c = aviosPE;
    }else if(clase == 'ClaseTUR'){
    mssg.R3_ATA_NUM_CostBUS__c = aviosTUR2;
    }
    
    insert mssg;

    system.debug('Mensaje insertado: ' + mssg);

    //R1_VIP_Lounge_Access__c updateAcces = [SELECT Id,R1_VLI_TXT_Comp_Flight__c from R1_VIP_Lounge_Access__c WHERE Id =: acceso];
    //updateAcces.R1_VLI_TXT_Comp_Flight__c ='IB ';
    //update updateAcces;

    }catch (DmlException exc) {
      R1_CLS_LogHelper.generateErrorLog('TCK1_CLS_WSResiber.insertMsg()', '', exc.getmessage() + ', ' + exc.getLineNumber(), 'WSResiber');
    }
  }
}