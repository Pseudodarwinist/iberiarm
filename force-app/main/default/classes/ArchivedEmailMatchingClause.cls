public with sharing class ArchivedEmailMatchingClause {
    
    
    public String name {get { return name != null ? name.toUpperCase().trim(): null;} set;}
    public String surname {get{ return surname != null ? surname.toUpperCase().trim(): null; } set;}
    public String email {get{ return email != null ? email.toLowerCase().trim(): null; } set;}
    public String ibPlus {get{ return ibPlus != null ? ibPlus.toUpperCase(): null; } set;}
    public String tkt {get{ return tkt != null ? tkt.toUpperCase(): null; } set;}
    public String goldenRecord {get{ return goldenRecord != null ? goldenRecord.toUpperCase().trim(): null; } set;}
    public String pnrAmadeus {get{ return pnrAmadeus != null ? pnrAmadeus.toUpperCase().trim(): null; } set;}
    public String pnrResiber {get{ return pnrResiber != null ? pnrResiber.toUpperCase().trim(): null; } set;}
    public String clientType {get{ return clientType != null ? clientType.toUpperCase().trim(): null; } set;}
    public String flightNumber{get{ return flightNumber != null ? flightNumber.toUpperCase().trim(): null; } set;}
    public String contactKey {get{ return contactKey != null ? contactKey.trim(): null; } set;}
    public String subscriberID {get{ return subscriberID != null ? subscriberID.trim(): null; } set;} 
    public String emailType {get{ return emailType != null ? emailType.trim(): null; } set;} 
    public String startDate {get; set;}
    public String endDate {get; set;}
    public String flightDate {get; set;}
    public String sentDate {get; set;}
    public Double jobID {get; set;}
    public String joiner {get; set;}

    public String generateMatchingString() {
        
        List<String> filters = new List<String> ();
        String matchingString = '';
        Boolean andNeeded = false;

        if (email != null && !email.equals('')) {
            filters.add('email__c = \'' + String.escapeSingleQuotes(email) + '\'');
        }

        if(goldenRecord != null && !goldenRecord.equals('')) {
            filters.add('id_golden_record__c = \'' + String.escapeSingleQuotes(goldenRecord) + '\'');
        }

        if(contactKey != null && !contactKey.equals('')) {
            filters.add('contactkey__c = \'' + String.escapeSingleQuotes(contactKey) + '\'');
        }

        if(ibPlus != null && !ibPlus.equals('')) {
            filters.add('num_tarjeta_ibp_st__c = \'' + String.escapeSingleQuotes(ibPlus) + '\'');
        }

        String joinString = (joiner != null && !joiner.equals('')) ? joiner : 'AND';

        if (filters.size() > 0) {
            matchingString = '(' + String.format('{0}', new List<String> { String.join(filters, ' ' + joinString + ' ') }) + ')';
            filters = new List<String> ();
            andNeeded = true;
        }

        // These need AND as joiner
        if(tkt != null && !tkt.equals('')) {
            filters.add('tkt__c = \'' + String.escapeSingleQuotes(tkt) + '\'');
        }

        if(pnrAmadeus != null && !pnrAmadeus.equals('')) {
            filters.add('pnr_amadeus__c = \'' + String.escapeSingleQuotes(pnrAmadeus) + '\'');
        }

        if(pnrResiber != null && !pnrResiber.equals('')) {
            filters.add('pnr_resiber__c = \'' + String.escapeSingleQuotes(pnrResiber) + '\'');
        }

        if(clientType != null && !clientType.equals('')) {
            filters.add('tipo_cliente_bl__c = \'' + String.escapeSingleQuotes(clientType) + '\'');
        }

        if(sentDate != null && !sentDate.equals('')) {
            DateTime sDateTimeStart = Util.formatStringDateToCST(sentDate);
            DateTime sDateTimeEnd = sDateTimeStart.addHours(23).addMinutes(59).addSeconds(59);

            String formatedDateTimeStart = sDateTimeStart.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
            String formatedDateTimeEnd = sDateTimeEnd.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');

            filters.add('eventdate__c > ' + formatedDateTimeStart);
            filters.add('eventdate__c < ' + formatedDateTimeEnd);
        }

        if(flightDate != null && !flightDate.equals('')) {
            Date d = Date.valueOf(flightDate);
            DateTime dt = DateTime.newInstance(d, Time.newInstance(0, 0, 0, 0));
            filters.add('flight_date__c = ' + dt.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\''));
        }

        if(startDate != null && !startDate.equals('')) {
            Datetime startDatetime = Util.formatStringDateToCST(startDate);
            String formatedDateTime = startDatetime.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');

            filters.add('eventdate__c > ' + formatedDateTime);
        }

        if(endDate != null && !endDate.equals('')) {
            Datetime endDatetime = Util.formatStringDateToCST(endDate);
            endDatetime.addHours(23).addMinutes(59).addSeconds(59);
            String formatedDateTime = endDatetime.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');

            filters.add('eventdate__c < ' + formatedDateTime);
        }

        if(emailType != null && !emailType.equals('')) {
            filters.add('emailtype__c = \'' + String.escapeSingleQuotes(emailType) + '\'');
        }

        if(jobID != null) {
            filters.add('jobid__c = ' + jobID);
        }

        if(subscriberID != null && !subscriberID.equals('')) {
            filters.add('subid__c = \'' + String.escapeSingleQuotes(subscriberID) + '\'');
        }

        if(flightNumber != null && !flightNumber.equals('')) {
            filters.add('flight_id__c = \'' + String.escapeSingleQuotes(flightNumber) + '\'');
        }

        if(name != null && !name.equals('')) {
            filters.add('nombre_st__c = \'' + String.escapeSingleQuotes(name) + '\'');
        }

        if(surname != null && !surname.equals('')) {
            filters.add('primer_apellido_st__c = \'' + String.escapeSingleQuotes(surname) + '\'');
        }

        if (filters.size() > 0) {
            matchingString += (andNeeded ? ' AND ': '') + '(' + String.format('{0}', new List<String> { String.join(filters, ' AND ') }) + ')';
        }

        return matchingString;
    }
}