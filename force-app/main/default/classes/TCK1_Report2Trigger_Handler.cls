/*
* @author TCK&LCS
* @date 25/07/2019 
* @description Handler class of Report 2 Trigger

<pre>
FECHA               AUTOR                   ACCION
25/07/2019          NPG-LCS                 Create class
22/08/2019			ICA-LCS					Modification. Changed from custom setting to metadata, to avoid 100 characters limit.
</pre>
*/
public class TCK1_Report2Trigger_Handler {
	public static Map<String,String> quickTextMap;

	/**
    * @author TCK&LCS
    * @date 25/07/2019 
    * @description Method executed on before Report 2 insert. Fills the TCK1_RP2_TEX_ExplicacionAlCliente__c field based on keyfields and TCK1_R2_QuickTexts__mdt metadata.
    * @param List<TCK1_Report2__c> Trigger new List
	* @param Map<Id,TCK1_Report2__c> Trigger new Map
    */
    public static void onBeforeInsert(List<TCK1_Report2__c> newListReports2,Map<Id,TCK1_Report2__c> newMapReports2){
		initializeQuickTextMap();
		//Gathers Case lookup to query in related Map
		Set<Id> setCaseId = new Set<Id>();
		Map<Id,Case> caseMap = new Map<Id,Case>();//Related Case per Report2
		for(TCK1_Report2__c r2 : newListReports2){
			setCaseId.add(r2.TCK1_RP2_Expediente__c);
		}
		if(!setCaseId.isEmpty()){
			caseMap = new Map<Id,Case>([Select Id, R1_CAS_FOR_Case_Number__c, Type, R1_CAS_PKL_Subtype__c from case where Id in:setCaseId]);
		}
		for(TCK1_Report2__c r2 : newListReports2)
        {
            String quickText='';
            if(quickTextMap.containsKey(r2.TCK1_RP2_TEX_ClaveTextoPredefinido__c))
            {
                quickText=quickTextMap.get(r2.TCK1_RP2_TEX_ClaveTextoPredefinido__c);
                if(quickText.indexOf('[PASAJERO]')>-1&&r2.TCK1_RP2_FOR_Client__c!=null) quickText=quickText.replace('[PASAJERO]', r2.TCK1_RP2_FOR_Client__c);
                if(quickText.indexOf('[MOTIVO]')>-1) quickText=quickText.replace('[MOTIVO]', r2.TCK1_RP2_TEX_MedidasAdoptadas__c==null?'REPLACE':r2.TCK1_RP2_TEX_MedidasAdoptadas__c);
                if(quickText.indexOf('[EXPEDIENTE]')>-1&&r2.Numero_expediente__c!=null) quickText=quickText.replace('[EXPEDIENTE]', caseMap.get(r2.TCK1_RP2_Expediente__c).R1_CAS_FOR_Case_Number__c);
                if(quickText.indexOf('[NUMERO VUELO]')>-1&&r2.TCK1_RP2_FOR_Vuelo__c!=null) quickText=quickText.replace('[NUMERO VUELO]', r2.TCK1_RP2_FOR_Vuelo__c);
                if(quickText.indexOf('[TIPO]')>-1&&r2.Subtipo_expediente__c!=null) quickText=quickText.replace('[TIPO]', caseMap.get(r2.TCK1_RP2_Expediente__c).Type);
				if(quickText.indexOf('[SUBTIPO]')>-1&&r2.Subtipo_expediente__c!=null) quickText=quickText.replace('[SUBTIPO]', + ' - ' + caseMap.get(r2.TCK1_RP2_Expediente__c).R1_CAS_PKL_Subtype__c);
                
                if(quickText.indexOf('[FECHA VUELO]')>-1&&r2.TCK1_RP2_FOR_Fecha_Vuelo__c!=null){
                    Datetime d = Date.Valueof(r2.TCK1_RP2_FOR_Fecha_Vuelo__c);   
                    String dateStr = d.formatGMT('dd/MM/yyyy'); 
                    quickText=quickText.replace('[FECHA VUELO]', dateStr);
                }
                r2.TCK1_RP2_TEX_ExplicacionAlCliente__c=quickText;
            }
        }
    }

	/**
    * @author TCK&LCS
    * @date 22/08/2019
    * @description Method to initializate the quick text Map used to fill automatically the Report 2.
    */
	private static void initializeQuickTextMap(){
		quickTextMap = new Map<String,String>();
		for(TCK1_R2_QuickTexts__mdt qt: [Select id,MasterLabel, DeveloperName, Value__c from TCK1_R2_QuickTexts__mdt]){
			quickTextMap.put(qt.MasterLabel,qt.Value__c);
		}
	}
}