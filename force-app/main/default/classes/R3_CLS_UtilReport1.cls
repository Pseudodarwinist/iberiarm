/*---------------------------------------------------------------------------------------------------------------------
    Author:         ICA
    Company:        LeadClic
    Description:    Report1 Utils

    History:
     <Date>                     <Author>                         <Change Description>
    31/07/2019             		ICA-LCS                				Initial Version
	19/09/2019					ICA-LCS                				Updated. Change the condition R2_REA_PKL_Delay_Code__c='CE' to use R2_REA_TXT_Delay_No__c ='93.0' instead
	19/09/2019					ICA-LCS								Updated. Change the condition R2_REA_TXT_Delay_No__c ='93.0' to be controlled by metadata TCK_CodigoCausaCE__mdt records
 ----------------------------------------------------------------------------------------------------------------------*/
/**
* @author TCK&LCS
* @date 31/07/2019
* @description Report 1 Util Class
*/
public with sharing class R3_CLS_UtilReport1 {

    /**
	* @author TCK&LCS
	* @date 31/07/2019
	* @description Checks conditions of Set of flights without report 1 and create new reports if they meet the requirements: Have at least 3(number controlled by Label.R3CasesPerReport) cases recordtype 'Expediente' having, at least, 1 reason with delay code 'CE'
	*/
	public static void checkReport1Creation(Set<Id> idObjects) {
		Map<Id,List<R2_Reasons__c>> mapaCausas = new Map<Id,List<R2_Reasons__c>>();
		Map<Id,List<Case>> mapaCasos = new Map<Id,List<Case>>();
		Map<Id,String> mapaValoresCausa = new Map<Id,String>();
		List<TCK1_Report1__c> rList2Insert = new List<TCK1_Report1__c>();
		//Group metadata controlling special conditions parameter
		Map<String,String> mapCERestrictions = new Map<String,String>();
		for(TCK_CodigoCausaCE__mdt ce : [SELECT Id,MasterLabel, TCK_Aeropuerto_salida__c  FROM TCK_CodigoCausaCE__mdt]){
			mapCERestrictions.put(ce.MasterLabel,ce.TCK_Aeropuerto_salida__c);
		}
		try{
			//Select the reasons with Delay Code 'CE' and map them by flightID
			for(R2_Reasons__c c : [Select Id,R2_REA_TXT_Delay_No__c,R2_REA_TXT_Delay_Code__c,
					R2_REA_NUM_Delay_Time__c,R2_REA_MSDT_Incidence__r.R2_INC_LOO_Flight__c,R2_REA_MSDT_Incidence__r.R2_INC_LOO_Flight__r.R1_FLG_TXT_Airport_depart__c   
					from R2_Reasons__c 
					where R2_REA_MSDT_Incidence__r.R2_INC_LOO_Flight__c IN: idObjects AND R2_REA_TXT_Delay_No__c IN: mapCERestrictions.keySet()]){
				
				//If reason has a flight
				if(c.R2_REA_MSDT_Incidence__r.R2_INC_LOO_Flight__c != null){
					//If there is a flight restriction, if it contains '!', checks if departure airport is different from metadata value, or same value if there is not '!'
					if(String.isBlank(mapCERestrictions.get(c.R2_REA_TXT_Delay_No__c)) 
					|| (mapCERestrictions.get(c.R2_REA_TXT_Delay_No__c).contains('!')? 
						!mapCERestrictions.get(c.R2_REA_TXT_Delay_No__c).remove('!').equalsIgnoreCase(c.R2_REA_MSDT_Incidence__r.R2_INC_LOO_Flight__r.R1_FLG_TXT_Airport_depart__c):
						mapCERestrictions.get(c.R2_REA_TXT_Delay_No__c).equalsIgnoreCase(c.R2_REA_MSDT_Incidence__r.R2_INC_LOO_Flight__r.R1_FLG_TXT_Airport_depart__c))){
							if(mapaCausas.containsKey(c.R2_REA_MSDT_Incidence__r.R2_INC_LOO_Flight__c)){
								mapaCausas.get(c.R2_REA_MSDT_Incidence__r.R2_INC_LOO_Flight__c).add(c);
							}else{
								mapaCausas.put(c.R2_REA_MSDT_Incidence__r.R2_INC_LOO_Flight__c,new List<R2_Reasons__c>{c});
							}
					}
				}
			}
        }catch(Exception ex){
			R1_CLS_LogHelper.generateErrorLog('TCK1_CLS_UtilReport1.checkReport1Creation', '', ex.getmessage()+', '+ex.getLineNumber(), 'TCK1_Report1__c');
		}
		//For each flight, create de code to be saved as delay causes in the report1
		for(Id idVuelo : mapaCausas.keySet()){
			String causas='';
			Integer cont = 0;
			for(R2_Reasons__c c : mapaCausas.get(idVuelo)){					
				causas += c.R2_REA_TXT_Delay_No__c + ','+c.R2_REA_TXT_Delay_Code__c+','+c.R2_REA_NUM_Delay_Time__c+'\r\n';
				cont++;
			}
			if(cont>0){
				mapaValoresCausa.put(idVuelo,causas);
			}
		}
		//Select cases and map them by flight. If there are more than 3 with recordtype file, a report1 will be created automatically
		if(mapaCausas.keySet().size() > 0){
			for(Case c: [Select Id,R1_CAS_LOO_Flight__c,R1_CAS_LOO_Flight__r.Report_1__c from Case where R1_CAS_LOO_Flight__c IN: idObjects AND RecordType.DeveloperName =: Label.RTExpedienteCaso]){
				if(mapaCasos.containsKey(c.R1_CAS_LOO_Flight__c)){
					mapaCasos.get(c.R1_CAS_LOO_Flight__c).add(c);
				}else{
					mapaCasos.put(c.R1_CAS_LOO_Flight__c,new List<Case>{c});
				}
			}
			for(Id idVuelo : mapaCasos.keySet()){
				
				if(!mapaCasos.get(idVuelo).isEmpty() && mapaCasos.get(idVuelo)[0].R1_CAS_LOO_Flight__r.Report_1__c== null && mapaCasos.get(idVuelo).size()>=Integer.valueOf(Label.R3CasesPerReport)  && mapaValoresCausa.containsKey(idVuelo)){
					rList2Insert.add(new TCK1_Report1__c(Name = 'v1', TCK1_RP1_TEX_Causas_Retraso__c=mapaValoresCausa.get(idVuelo), TCK1_RP1_LOO_Vuelo__c=idVuelo,TCK1_RP1_Estado__c='Asignado',TCK1_RP1_Prioridad__c='Baja'));
				}
			}
			try{
				insert rList2Insert;
			}catch(DmlException ex){
				R1_CLS_LogHelper.generateErrorLog('TCK1_CLS_UtilReport1.checkReport1Creation', 'DML Exception', ex.getmessage()+', '+ex.getLineNumber(), 'TCK1_Report1__c');
			}catch(Exception ex){
				R1_CLS_LogHelper.generateErrorLog('TCK1_CLS_UtilReport1.checkReport1Creation', '', ex.getmessage()+', '+ex.getLineNumber(), 'TCK1_Report1__c');
			}
		}
    }
}