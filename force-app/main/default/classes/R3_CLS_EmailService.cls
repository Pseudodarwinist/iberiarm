/*---------------------------------------------------------------------------------------------------------------------
   Author:         TCK
   Company:        The Cocktail
   Description:    Email Service Class.

   History:
    <Date>                     <Author>                         <Change Description>
   20/08/2019             TCK                 Initial Version
----------------------------------------------------------------------------------------------------------------------*/
global class R3_CLS_EmailService implements Messaging.InboundEmailHandler {

    /**
    * @author TCK&LCS
    * @date 03/09/2019
    * @description 
    * @param Messaging.inboundEmail email, Messaging.InboundEnvelope env
    * @return Messaging.InboundEmailResult 
    */  
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.inboundEmail email, Messaging.InboundEnvelope env){
        system.debug('env:  '+env);
        system.debug('email: '+email);
        Case casoFromThreadId = existsCase(email);
        if(casoFromThreadId != null){
            createEmailMessage(casoFromThreadId,email,env);
        }else{
            String[] buzonAddress = (env.toAddress).split('@');
            EmailServicesAddress buzon = [SELECT DeveloperName,EmailDomainName FROM EmailServicesAddress WHERE EmailDomainName =:buzonAddress[1]];

            R3_Name_Buzones__c ccpo = R3_Name_Buzones__c.getValues('CCPO');
            R3_Name_Buzones__c serviberia = R3_Name_Buzones__c.getValues('ServIberia');
            R3_Name_Buzones__c others = R3_Name_Buzones__c.getValues('Others');
            String subject = String.valueOf(email.Subject);
            String description = String.valueOf(email.plainTextBody);
            String CcString = '';
            if(email.ccAddresses != null){
                String auxCc = String.join(email.ccAddresses,',');
                auxCc = auxCc.replace('(','');
                CcString = auxCc.replace(')','');
            }
            description = 'Email from: ' +email.fromAddress + '\n' +'Email to: ' +env.fromAddress + '\n' +'Cc: ' + CcString + '\n'+'Subject: ' +email.Subject + '\n'+'Date:  ' + datetime.now().addHours(1)  + '\n' + '\n\n'+description;
            R3_CLS_UtilEmailHandler utilHandler = new R3_CLS_UtilEmailHandler();
            String ff = utilHandler.returnFF(subject);    
            system.debug('ff:'+ff);    
            if((ccpo.R3_TXT_Values__c).contains(buzon.DeveloperName) || (serviberia.R3_TXT_Values__c).contains(buzon.DeveloperName)){
                if(ff != ''){
                    createCasobyFF(ff,subject,description,email, buzon.DeveloperName,env);
                }else{
                    createCasoByMail(email,buzon.DeveloperName,env,subject,description);
                }
            }else if((others.R3_TXT_Values__c).contains(buzon.DeveloperName)){
                if(ff != ''){
                    createCasobyFF(ff,subject,description,email,buzon.DeveloperName,env);
                }else{
                    List<Account> cuentaList = utilHandler.findCuentaByMail(String.valueOf(email.fromAddress));
                    if(cuentaList != null && cuentaList.size()>0){
                        createCasoByMail(email,buzon.DeveloperName,env,subject,description);
                    }else{
                        Case c = utilHandler.createCaso(subject,description,email.fromAddress,email.fromAddress, null,'');
                        TCK1_CON_ContactIberiaExtension aux1 = new TCK1_CON_ContactIberiaExtension();
                        Group colaID;        
                        if(buzon.DeveloperName == 'hotelesconavios'){
                            colaID = aux1.getAssign('hotelesconavios',false,'');
                        }else if(buzon.DeveloperName == 'Serviberia_IbPlusMenores'){
                            colaID = aux1.getAssign('Serviberia_IbPlusMenores',false,'');
                        }else if(buzon.DeveloperName == 'Serviberia_MenoresIberia'){
                            colaID = aux1.getAssign('Serviberia_MenoresIberia',false,'');
                        }
                        if(colaID.id != null){
                            c.OwnerId = colaID.Id;
                        }
                        insert c; 
                        createEmailMessage(c,email,env);                      
                    }    
                } 
            }
        }
        return null;
    }

    /**
    * @author TCK&LCS
    * @date 03/09/2019
    * @description create case by email
    * @param Messaging.inboundEmail email, String buzonDestino, Messaging.InboundEnvelope env, String subject, String description
    */
    public void createCasoByMail (Messaging.inboundEmail email, String buzonDestino, Messaging.InboundEnvelope env, String subject, String description){
        system.debug('------Envia a buzon ServIberia------');
        R3_CLS_UtilEmailHandler utilHandler = new R3_CLS_UtilEmailHandler();
        system.debug('env::'+env);
        system.debug('email::'+email);
        if(buzonDestino == 'CCPO_Singular'){
        sendKoSingular(email,utilHandler); // Aqui para los que se envien a singular pero no tengan el FF
        }else{
        sendKo(email,utilHandler);
        }

        /////A partir del Sprint 3 de nov 2019 se solicitó que no se creará ningun caso sin FF ////  
    }

    /**
    * @author TCK&LCS
    * @date 03/09/2019
    * @description KO for the form
    * @param Messaging.inboundEmail email,R3_CLS_UtilEmailHandler utilHandler
    */
    public void sendKo(Messaging.inboundEmail email,R3_CLS_UtilEmailHandler utilHandler){
        EmailTemplate et = utilHandler.findTemplate();
        String emailRemitente =String.valueOf(email.fromAddress);
        system.debug('emailRemitenteKO:'+emailRemitente);
        utilHandler.sendEmail(emailRemitente, et);
    }

    public void sendKoSingular(Messaging.inboundEmail email,R3_CLS_UtilEmailHandler utilHandler){
        EmailTemplate et = utilHandler.findTemplateSingular();
        String emailRemitente =String.valueOf(email.fromAddress);
        system.debug('emailRemitenteKO:'+emailRemitente);
        utilHandler.sendEmail(emailRemitente, et);
    }

    /**
    * @author TCK&LCS
    * @date 03/09/2019
    * @description create case with FF
    * @param String ff,String subject, String description,Messaging.inboundEmail email, String buzonDestino
    */
    public void createCasobyFF(String ff,String subject, String description,Messaging.inboundEmail email, String buzonDestino,Messaging.InboundEnvelope env ){
        R3_CLS_UtilEmailHandler utilHandler = new R3_CLS_UtilEmailHandler();
        Account cuenta = utilHandler.findCuentaByFF(ff);

        if(cuenta != null && cuenta.R1_ACC_CHK_Flag_Iberia_Singular__c == true && buzonDestino == 'CCPO_Singular'){
            

                Case c = utilHandler.createCaso(subject,description,cuenta.PersonEmail,email.fromAddress, cuenta,ff);
                TCK1_CON_ContactIberiaExtension aux = new TCK1_CON_ContactIberiaExtension();
                R3_CS_ColasBuzones__c singularIberia = R3_CS_ColasBuzones__c.getValues('CCPO_Singular');
                Group colaID = aux.getQueue(singularIberia.Api_Name__c); 
                if(colaID.id   != null){
                    c.OwnerId = colaID.Id;
                }
                insert c;
                createEmailMessage(c,email,env);
        }else if(cuenta != null && cuenta.R1_ACC_CHK_Flag_Iberia_Singular__c == false &&  buzonDestino == 'CCPO_Singular'){
                sendKoSingular(email,utilHandler); //Aqui iria la plantilla que nos manden para los que se envian a Singular pero sin el check activado
        }else{
            if(cuenta != null){
                Boolean isClasica = false; 
            if(cuenta.R1_ACC_PKL_Card_Type__c == '2'){
                        isClasica = true;
            }
                if(isClasica && (buzonDestino == 'Serviberia_Plata'  || buzonDestino == 'CCPO_Infinita'  || buzonDestino == 'CCPO_InfinitaPrime' || buzonDestino == 'CCPO_Oro' || buzonDestino == 'CCPO_Platino')){
                    sendKo(email,utilHandler);
                }else{
                    
                
                    Case c = utilHandler.createCaso(subject,description,cuenta.PersonEmail,email.fromAddress, cuenta,ff);
                    TCK1_CON_ContactIberiaExtension aux = new TCK1_CON_ContactIberiaExtension();
                    Group colaID;
                    if(buzonDestino == 'CCPO_Baby'){
                        colaID = aux.getAssign('CCPO_Baby',false,'');
                    }else if(buzonDestino == 'hotelesconavios'){
                        colaID = aux.getAssign('hotelesconavios',false,'');
                    }else{
                        colaID = aux.getAssign(cuenta.R1_ACC_PKL_Card_Type__c,cuenta.R1_ACC_CHK_Flag_Iberia_Singular__c,env.fromAddress);
                    }
                    if(colaID.id   != null){
                        c.OwnerId = colaID.Id;
                    }
                    insert c;
                    createEmailMessage(c,email,env);
                }
                
            }else{
                if(buzonDestino != 'CCPO_Singular'){
                    sendKo(email,utilHandler);
                }else{
                    sendKoSingular(email,utilHandler); //Aqui va para los que se envien a Singular pero no se encuentre la cuenta imagino que es lo mismo que sin poner el ff
                }
            }
        }
    }
    

    /**
    * @author TCK&LCS
    * @date 03/09/2019
    * @description get queue for new case
    * @param list<account> listAccount,String buzonDestino
    * @return String toReturn
    */    
    public String queueLevel(list<account> listAccount,String buzonDestino){
        String aux = '';
        String toReturn= '';
        for(Account acc:listAccount){
            if(aux == ''){
                aux = acc.R1_ACC_PKL_Card_Type__c;
            }
            if(acc.R1_ACC_PKL_Card_Type__c != aux ){
                toReturn = buzonDestino;
                break;
            }
            toReturn = aux;
        }
        return toReturn;
    }
    
    /**
    * @author TCK&LCS
    * @date 12/11/2019
    * @description 
    * @param Case c,Messaging.inboundEmail email, Messaging.InboundEnvelope env
    */  
    public static void createEmailMessage(Case c, Messaging.inboundEmail email, Messaging.InboundEnvelope env){
        TCK1_CON_ContactIberiaExtension queueCase = new TCK1_CON_ContactIberiaExtension();
        List<R3_CS_ColasBuzones__c> csColasAllValues= R3_CS_ColasBuzones__c.getall().values();
        String csColasAllStr ='';
        for(R3_CS_ColasBuzones__c aux:csColasAllValues){
            if(env.fromAddress == aux.R3_CS_TXT_Email_Asociated__c){
                csColasAllStr = aux.Name;
            }
        }
        R3_Name_Buzones__c valuesBuzonesOthers= R3_Name_Buzones__c.getValues('Others');
        Group caseGroup;
        if((valuesBuzonesOthers.R3_TXT_Values__c).contains(csColasAllStr)){
            caseGroup = queueCase.getAssign(csColasAllStr,false, env.fromAddress);
        }else{
            Account accCaseId = [SELECT Id,R1_ACC_PKL_Card_Type__c,R1_ACC_CHK_Flag_Iberia_Singular__c FROM account WHERE Id=: c.AccountId];
            caseGroup = queueCase.getAssign(accCaseId.R1_ACC_PKL_Card_Type__c,accCaseId.R1_ACC_CHK_Flag_Iberia_Singular__c, env.fromAddress);
        }
        Group caseGroupName = [SELECT Id,Name FROM Group WHERE Id =: caseGroup.Id];
        R3_CS_ColasBuzones__c toEmailQueue = R3_CS_ColasBuzones__c.getValues(caseGroupName.Name);
        EmailMessage emailMessage = new EmailMessage();
        emailMessage.status = '0'; // email was sent
        emailMessage.Incoming = true;
        emailMessage.ParentID = c.Id;
        emailMessage.fromAddress = email.fromAddress;
        emailMessage.CcAddress = getCCfromEmail(email);
        emailMessage.subject = String.valueOf(email.Subject);
        emailMessage.htmlBody = String.valueOf(email.htmlBody);
        // Contact, Lead or User Ids of recipients
        String[] toIds = new String[]{c.AccountId};
        //emailMessage.toIds = toIds;
        emailMessage.toAddress = toEmailQueue.R3_CS_TXT_Email_Asociated__c;
        emailMessage.R3_EMA_TXT_Original_To_Address__c = env.fromAddress;
        insert emailMessage;
        R3_CLS_UtilEmailHandler utilHandler = new R3_CLS_UtilEmailHandler();
        utilHandler.insertAttatchments(emailMessage,email);
        if(c.Status != null && c.Status != 'Abierto'){
            utilHandler.updateCaso(c,env);
        }
    }

    /**
    * @author TCK&LCS
    * @date 14/11/2019
    * @description 
    * @param Messaging.inboundEmail email
    * @return Case 
    */  
    public static Case existsCase(Messaging.inboundEmail email) {
        Case casoToRet;
        if (email.plainTextBody != null && email.plainTextBody.contains('ref:') && email.plainTextBody.contains(':ref')) {
            String threadIdIn = (email.plainTextBody).Substring(email.plainTextBody.indexof('ref:'), email.plainTextBody.indexof(':ref') + 4);
            List < Case > casoList = [SELECT Id, AccountId, Status, R2_CAS_NUM_Number_of_rebounds__c, R2_CAS_DATH_Rebounds_Last_Modify__c, R2_CAS_FOR_Manual_Thread__c FROM Case WHERE R2_CAS_FOR_Manual_Thread__c =: threadIdIn];
            if (casoList != null && !casoList.isEmpty()) {
                for (Case caso: casoList) {
                    if (caso.R2_CAS_FOR_Manual_Thread__c.equals(threadIdIn)) {
                        casoToRet = caso;
                    }
                }
            }
        }
        return casoToRet;
    }
    
    public static String getCCfromEmail (Messaging.inboundEmail email){
        String retCC = '';
        system.debug('111:'+email.ccAddresses);
        if(email.ccAddresses != null && !email.ccAddresses.isEmpty()){
            system.debug('222:'+email.ccAddresses);
            for(String aux:email.ccAddresses){
                system.debug('111:'+aux);
                retCC = retCC +', '+ aux;
            }
        }
        system.debug('retCC:'+retCC);
        return retCC;
    }
}