@IsTest
public class R2_BTCH_MergeClients_DupPorId_Test
{
    /*---------------------------------------------------------------------------------------------------------------------
    Author:         Raúl Julián González
    Company:        Accenture
    Description:    Apex test de la clase apex "R2_CLS_MergeAccount_DupPorId"
    IN:             
    OUT:            

    History: 
     <Date>                     <Author>                         <Change Description>
    07/07/2018             Raúl Julián González                   Initial Version
    ----------------------------------------------------------------------------------------------------------------------*/
    static testMethod void executeBachTest(){
        R1_CLS_LogHelper.throw_exception = false;  
      
        List<Account> lstClientes = new List<Account>();
        
        Id recordTypeIdAcc = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cuenta personal').getRecordTypeId();
        Id recordTypeIdCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Asistencia').getRecordTypeId();
        
        Account ids = new Account();
        ids.RecordTypeId= recordTypeIdAcc;
        ids.LastName ='IDS';
        ids.R1_ACC_TLF_Phone_Marketing__c = '123456787';
        ids.R1_ACC_TXT_Id_Golden_record__c = 'master';
        insert ids;
        System.debug('ID PARA EL GR ' + ids.Id);
        Account accMaster = new Account();
        accMaster.RecordTypeId = recordTypeIdAcc;
        accMaster.LastName = 'Master';
        accMaster.R1_ACC_TLF_Phone_Marketing__c = '123456788';
        accMaster.R1_ACC_TXT_Id_Golden_record__c = 'master1' ;
        lstClientes.add(accMaster);
        Account accDup = new Account();
        accDup.RecordTypeId = recordTypeIdAcc;
        accDup.LastName = 'Master';
        accDup.R1_ACC_TLF_Phone_Marketing__c = '123456789';
        accDup.R1_ACC_TXT_Id_Golden_record__c = 'master2' ;
        lstClientes.add(accDup);
        insert lstClientes;
        
        System.debug('Master GR:' + accMaster.R1_ACC_TXT_Id_Golden_record__c);
        System.debug('DUP GR:' + accDup.R1_ACC_TXT_Id_Golden_record__c);
        R1_Cliente_duplicado__c clienteDup = new R1_Cliente_duplicado__c();
        clienteDup.R1_DUP_TXT_Golden_Record_Duplicado__c = accDup.R1_ACC_TXT_Id_Golden_record__c;
        clienteDup.R1_DUP_TXT_Golden_Record_Master__c = accMaster.R1_ACC_TXT_Id_Golden_record__c;

        insert clienteDup;
        
        Case caso = new Case();
        caso.RecordTypeId = recordTypeIdCase;
        caso.AccountId = accDup.Id;
        caso.Type = 'Asistencia';
        caso.Origin = 'Email';
        caso.Status = 'Abierto';
        insert caso;
        
        List<R1_IntegrationObject__c> lstIntObjec = new List<R1_IntegrationObject__c>();
        R1_IntegrationObject__c intObject = new R1_IntegrationObject__c(Name = 'Case', R1_Field__c = 'AccountId', R1_TXT_GR_previous__c = 'R1_CAS_TXT_Golden_Record_Previous__c');
        R1_IntegrationObject__c intObject2 = new R1_IntegrationObject__c(Name = 'Entitlement', R1_Field__c = 'AccountId', R1_TXT_GR_previous__c = 'R1_ENT_TXT_Golden_Record_Previous__c');
        lstIntObjec.add(intObject);
        lstIntObjec.add(intObject2);
        insert lstIntObjec;

        R1_Job_Helper__c jobHelper = new R1_Job_Helper__c(Name = 'R1_Merge_JOB', R1_DT_Hora_inicio_Job__c = System.now());
        insert jobHelper;

        Test.startTest();
            
            Integer cont = 3;
            R2_BTCH_MergeClients_DupPorId mergeBatch = new R2_BTCH_MergeClients_DupPorId(cont);
            Database.executeBatch(mergeBatch, 2);

        Test.stopTest();

    }

/*---------------------------------------------------------------------------------------------------------------------
    Author:         Raúl Julián González
    Company:        Accenture
    Description:    Apex test de la clase apex "R2_CLS_MergeAccount_DupPorId"
    IN:             
    OUT:            

    History: 
     <Date>                     <Author>                         <Change Description>
    07/07/2018             Raúl Julián González                   Initial Version
    ----------------------------------------------------------------------------------------------------------------------*/
    static testMethod void exception_Test(){
        R1_CLS_LogHelper.throw_exception = true;
        Integer cont = 0;
        R2_BTCH_MergeClients_DupPorId mergeBatch = new R2_BTCH_MergeClients_DupPorId(cont);
        Database.executeBatch(mergeBatch);       
    }
    
}