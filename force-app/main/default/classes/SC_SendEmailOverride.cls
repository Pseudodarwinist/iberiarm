/**
* @name     SC_SendEmailOverride
* @author   Salesforce Team - Najlae   
* @since    2018                    
* @desc     Used to specify the default values for the standard Email action in the case feed  
*			You can use this interface to pre-populate EmailMessage fields based on the context (Case Record) where the action is displayed.
*			This Apex Class is called from the Support setting "Enable Default Email Templates or the Default Handler for Email Action".
*			Ref. https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_interface_QuickAction_QuickActionDefaultsHandler.htm
*			Ref. Case#19149620 (SF Support)
* @history
*/
global class SC_SendEmailOverride implements QuickAction.QuickActionDefaultsHandler{
    // Empty constructor
    global SC_SendEmailOverride() {
    }
    // The main interface method
    global void onInitDefaults(QuickAction.QuickActionDefaults[] defaults) {
        QuickAction.SendEmailQuickActionDefaults sendEmailDefaults = null;
        
        // Check if the quick action is the standard case feed Send Email action
        for (Integer j = 0; j < defaults.size(); j++) {
            if (defaults.get(j) instanceof QuickAction.SendEmailQuickActionDefaults && 
                defaults.get(j).getTargetSObject().getSObjectType() == 
                EmailMessage.sObjectType && 
                defaults.get(j).getActionName().equals('Case.Email') && 
                defaults.get(j).getActionType().equals('Email')) {
                    sendEmailDefaults = (QuickAction.SendEmailQuickActionDefaults)defaults.get(j);
                    break;
                }
        }
        
        if (sendEmailDefaults != null) {
            Case cas = [SELECT Id,R2_CAS_EMA_Email__c 
                        FROM Case 
                        WHERE Id=:sendEmailDefaults.getContextId()];
            
            EmailMessage emailMessage = (EmailMessage)sendEmailDefaults.getTargetSObject();    
            
            // Set ToAddress address to make sure each email goes for audit
            emailMessage.ToAddress  = cas.R2_CAS_EMA_Email__c ;
        }
    }
}