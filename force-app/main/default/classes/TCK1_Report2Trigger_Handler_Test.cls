/*---------------------------------------------------------------------------------------------------------------------
    Author:         ICA
    Company:        LeadClic
    Description:    Report1 Utils

    History:
     <Date>                     <Author>                         <Change Description>
    15/08/2019             		ICA-LCS                				Initial Version
	19/09/2019					ICA-LCS                				Updated. Change the Setup method, delete R2_REA_PKL_Delay_Code__c='CE' to use R2_REA_TXT_Delay_No__c ='93.0' instead
 ----------------------------------------------------------------------------------------------------------------------*/

/**
* @author TCK&LCS
* @date 15/08/2019
* @description Test class for TCK1_Report2Trigger_Handler and its trigger
*/
@isTest
public with sharing class TCK1_Report2Trigger_Handler_Test {

	/**
	* @author TCK&LCS
	* @date 15/08/2019
	* @description Setup method to insert required objects to test execution
	*/	
	@testSetup
	public static void setup(){
		List<Case> case2Insert = new List<Case>();
		List<R2_Reasons__c> reasons2Insert = new List<R2_Reasons__c>();
		List<R1_Incident__c> incident2Insert = new List<R1_Incident__c>();

		R1_Flight__c vuelo = new R1_Flight__c();
        vuelo.Name = 'TestVuelo';
        vuelo.R1_FLG_TXT_Flight_number__c='1234';
        vuelo.R1_FLG_DAT_Flight_date_local__c= Date.newInstance(1991, 2, 4);
        vuelo.R1_FLG_DATH_Schedule_depart_time__c = Datetime.newInstance(1991, 2, 4);
		vuelo.R1_FLG_DATH_Onblocks_act__c = Datetime.now().addHours(4);
		vuelo.R1_FLG_DATH_Sched_arrive__c= Datetime.now();
        vuelo.R1_FLG_TXT_Carrier_code__c='IB';
        vuelo.R1_FLG_TXT_Destination__c='BCN';
        vuelo.R1_FLG_TXT_Origin__c='MAD';
        insert vuelo;

		//RT del caso expendiente
        Id rt_expediente = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('R2_File').getRecordTypeId();
        Case caso1 = new Case();
        caso1.RecordTypeId = rt_expediente;
        caso1.Status = 'Abierto';
        caso1.Origin = 'Email';
        caso1.Type = 'Retraso';
		caso1.R1_CAS_LOO_Flight__c = vuelo.Id;
		case2Insert.add(caso1);

		Case caso2 = new Case();
        caso2.RecordTypeId = rt_expediente;
        caso2.Status = 'Abierto';
        caso2.Origin = 'Email';
        caso2.Type = 'Retraso';
		caso2.R1_CAS_LOO_Flight__c = vuelo.Id;
		case2Insert.add(caso2);

		Case caso3 = new Case();
        caso3.RecordTypeId = rt_expediente;
        caso3.Status = 'Abierto';
        caso3.Origin = 'Email';
        caso3.Type = 'Retraso';
		caso3.R1_CAS_LOO_Flight__c = vuelo.Id;
		case2Insert.add(caso3);

		insert case2Insert;

		Case ch = new Case();
        ch.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Equipaje').getRecordTypeId();
        ch.origin = 'sample2';
        ch.parentId = caso1.Id;
        insert ch;

		R1_Incident__c incidencia1 = new R1_Incident__c();
        incidencia1.R1_INC_LOO_Case__c = ch.Id;
        incidencia1.R2_INC_LOO_Case__c = caso1.Id;
		incidencia1.R2_INC_LOO_Flight__c = vuelo.Id;
        insert incidencia1;

		R2_Reasons__c reason1 = new R2_Reasons__c();
		reason1.R2_REA_TXT_Delay_No__c ='DN1';
		reason1.R2_REA_TXT_Delay_Code__c ='DC1';
		reason1.R2_REA_NUM_Delay_Time__c = 10;
		reason1.R2_REA_TXT_Delay_No__c ='93.0';
		reason1.R2_REA_MSDT_Incidence__c = incidencia1.Id;
		reasons2Insert.add(reason1);

		R2_Reasons__c reason2 = new R2_Reasons__c();
		reason2.R2_REA_TXT_Delay_No__c ='DN2';
		reason2.R2_REA_TXT_Delay_Code__c ='DC2';
		reason2.R2_REA_NUM_Delay_Time__c = 20;
		reason2.R2_REA_TXT_Delay_No__c ='93.0';
		reason2.R2_REA_MSDT_Incidence__c = incidencia1.Id;
		reasons2Insert.add(reason2);

		R2_Reasons__c reason3 = new R2_Reasons__c();
		reason3.R2_REA_TXT_Delay_No__c ='DN3';
		reason3.R2_REA_TXT_Delay_Code__c ='DC3';
		reason3.R2_REA_NUM_Delay_Time__c = 30;
		reason3.R2_REA_TXT_Delay_No__c ='93.0';
		reason3.R2_REA_MSDT_Incidence__c = incidencia1.Id;
		reasons2Insert.add(reason3);
		insert reasons2Insert;
	}

	/**
	* @author TCK&LCS
	* @date 15/08/2019
	* @description Checks if Report2 trigger is working as expected. Insert a Report 2 record, checks if only one has been created and the test if field TCK1_RP2_TEX_ExplicacionAlCliente__c matchs.
	*/
	@isTest
	public static void testOK(){
		Test.startTest();
		List<TCK1_Report2__c> r2List = new List<TCK1_Report2__c>();
		Case c = [Select Id From Case limit 1];
		TCK1_Report2__c r2 = new TCK1_Report2__c(TCK1_RP2_Expediente__c=c.Id,TCK1_RP2_TEX_ClaveTextoPredefinido__c='C_NO-A_PAG',TCK1_RP2_TEX_InformacionAdicional__c='TestOK',TCK1_RP2_TEX_MedidasAdoptadas__c='Medidas Adoptadas',TCK1_RP2_TEX_ObservacionesEvidencias__c='TCK1_RP2_TEX_ObservacionesEvidencias__c');		
		insert r2;

		r2List = [Select Id, TCK1_RP2_TEX_ExplicacionAlCliente__c From TCK1_Report2__c];
		system.AssertEquals(r2List.size(),1,'Check only 1 report2 has been created');
		system.AssertEquals(String.isNotBlank(r2List.get(0).TCK1_RP2_TEX_ExplicacionAlCliente__c),true,'The explanation field has been filled');
		Test.stopTest();
	}
}