global class deleteInfoCon implements Database.Batchable<sObject> {
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        system.debug('<<<0>>>');
        return Database.getQueryLocator('SELECT ID,R1_CD_PKL_IBPlus_References__c FROM R1_Contact_Data__c WHERE (R1_CD_PKL_IBPlus_References__c=\'00\' OR R1_CD_PKL_IBPlus_References__c=\'-\') Order By CreatedDate ASC LIMIT 100000');
    }

    global void execute(Database.BatchableContext BC, List<R1_Contact_Data__c> scope) {
        system.debug('<<<1>>>'+scope.size());
        delete scope;
        system.debug('<<<2>>>'+scope.size());
    }
    
    global void finish(Database.BatchableContext BC) {
        if(!Test.isRunningTest()){
            database.executeBatch(new deleteInfoCon(),2000);
        }else{
            system.debug('<<<3>>>All Success');
        }
    }
    
}