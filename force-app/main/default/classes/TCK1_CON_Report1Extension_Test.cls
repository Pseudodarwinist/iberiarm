/*---------------------------------------------------------------------------------------------------------------------
    Author:         ICA
    Company:        LeadClic
    Description:    Report1 Utils

    History:
     <Date>                     <Author>                         <Change Description>
    05/09/2019             		ICA-LCS                				Initial Version
	19/09/2019					ICA-LCS                				Updated. Change the Setup method, delete R2_REA_PKL_Delay_Code__c='CE' to use R2_REA_TXT_Delay_No__c ='93.0' instead
 ----------------------------------------------------------------------------------------------------------------------*/

/**
* @author TCK&LCS
* @date 05/09/2019
* @description Test class for TCK1_CON_Report1Extension class
*/
@isTest
public class TCK1_CON_Report1Extension_Test {

	/**
	* @author TCK&LCS
	* @date 05/09/2019
	* @description Setup method to insert required objects to test execution
	*/
   @testSetup
	public static void setup(){
		List<Case> case2Insert = new List<Case>();
		List<R2_Reasons__c> reasons2Insert = new List<R2_Reasons__c>();
		List<R1_Incident__c> incident2Insert = new List<R1_Incident__c>();

		R1_Flight__c vuelo = new R1_Flight__c();
        vuelo.Name = 'TestVuelo';
        vuelo.R1_FLG_TXT_Flight_number__c='1234';
        vuelo.R1_FLG_DAT_Flight_date_local__c= Date.newInstance(1991, 2, 4);
        vuelo.R1_FLG_DATH_Schedule_depart_time__c = Datetime.newInstance(1991, 2, 4);
		vuelo.R1_FLG_DATH_Onblocks_act__c = Datetime.now().addHours(4);
		vuelo.R1_FLG_DATH_Sched_arrive__c= Datetime.now();
        vuelo.R1_FLG_TXT_Carrier_code__c='IB';
        vuelo.R1_FLG_TXT_Destination__c='BCN';
        vuelo.R1_FLG_TXT_Origin__c='MAD';
        insert vuelo;

		//RT del caso expendiente
        Id rt_expediente = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('R2_File').getRecordTypeId();
        Case caso1 = new Case();
        caso1.RecordTypeId = rt_expediente;
        caso1.Status = 'Abierto';
        caso1.Origin = 'Email';
        caso1.Type = 'Retraso';
		caso1.R1_CAS_LOO_Flight__c = vuelo.Id;
		case2Insert.add(caso1);


		insert case2Insert;

		Case ch = new Case();
        ch.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Equipaje').getRecordTypeId();
        ch.origin = 'sample2';
        ch.parentId = caso1.Id;
        insert ch;

		R1_Incident__c incidencia1 = new R1_Incident__c();
        incidencia1.R1_INC_LOO_Case__c = ch.Id;
        incidencia1.R2_INC_LOO_Case__c = caso1.Id;
		incidencia1.R2_INC_LOO_Flight__c = vuelo.Id;
        insert incidencia1;

		R2_Reasons__c reason1 = new R2_Reasons__c();
		reason1.R2_REA_TXT_Delay_No__c ='DN1';
		reason1.R2_REA_TXT_Delay_Code__c ='DC1';
		reason1.R2_REA_NUM_Delay_Time__c = 10;
		reason1.R2_REA_TXT_Delay_No__c ='93.0';
		reason1.R2_REA_MSDT_Incidence__c = incidencia1.Id;
		reasons2Insert.add(reason1);

		insert reasons2Insert;

		TCK1_Report1__c r1 = new TCK1_Report1__c(Name = 'v1', TCK1_RP1_TEX_Causas_Retraso__c=reason1.R2_REA_TXT_Delay_Code__c, TCK1_RP1_LOO_Vuelo__c=vuelo.Id,TCK1_RP1_Estado__c='Asignado');

		insert r1;
	}

	/**
	* @author TCK&LCS
	* @date 05/09/2019
	* @description Simulates the creation of 2 PDF version Report 1
	*/
	@isTest
	public static void docPDFCreation(){
		TCK1_Report1__c r1 =[Select Id from TCK1_Report1__c Limit 1];
		ApexPages.CurrentPage().getparameters().put('id', r1.id);      
        
        Apexpages.StandardController sc = new Apexpages.StandardController(r1);
        TCK1_CON_Report1Extension ext = new TCK1_CON_Report1Extension(sc); 
        
        TCK1_CON_Report1Extension.makePDFReport1();
		TCK1_CON_Report1Extension ext1 = new TCK1_CON_Report1Extension(sc); 
		TCK1_CON_Report1Extension.makePDFReport1();
		List<ContentDocumentLink> listAux = new List<ContentDocumentLink>([SELECT Id,ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: r1.id]);
		System.assertEquals(2, listAux.size(),'PDF Versions may have been created');

	}
}