/*---------------------------------------------------------------------------------------------------------------------
    Author:         ICA-LCS
    Company:        LeadClic
    Description:    Job schedule for Report 1 proactive creation

    History:
     <Date>                     <Author>                         <Change Description>
    08/08/2019             		ICA                  				Initial Version
 ----------------------------------------------------------------------------------------------------------------------*/
/**
* @author TCK&LCS
* @date 08/08/2019
* @description Job schedule for Report 1 proactive creation
*/
global with sharing class R3_JOB_ExecuteReport1Creation implements Schedulable{

	/**
    * @author TCK&LCS
    * @date 08/08/2019 
    * @description Execute method from Schedulable class. Sends for its evaluation a set of delayed flights without Report 1 which one of its cases where created in the last hour. The delay considered is controlled by label R3minutsForR1Proactive. This Job should be scheduled hourly.
    * @param SchedulableContext SchedulableContext for the execution.
    */	
    global void execute(SchedulableContext sc) {
		Integer hour = (Datetime.now().hourGmt()!=0)?Datetime.now().hourGmt():24;
		Integer previousHour = hour-1;
		String day = hour!=24?'TODAY':'YESTERDAY';
		Integer minuts = Integer.valueOf(Label.R3minutsForR1Proactive);
		String query = 'Select Id,R1_CAS_LOO_Flight__c,R1_CAS_LOO_Flight__r.R3_FLG_FOR_TiempoRetraso__c  From case Where CreatedDate = '+ day + ' AND HOUR_IN_DAY(CreatedDate) <= '+ hour + ' AND HOUR_IN_DAY(CreatedDate) >= '+ previousHour + ' AND R1_CAS_LOO_Flight__r.Report_1__c = null AND RecordType.DeveloperName = \''+ Label.RTExpedienteCaso+'\' AND R1_CAS_LOO_Flight__r.R3_FLG_FOR_TiempoRetraso__c > ' + minuts;
		Set<Id> fligthSet = new Set<Id>();
		for(Case c: Database.query(query)){
			fligthSet.add(c.R1_CAS_LOO_Flight__c);
		}
		if(!fligthSet.isEmpty()){
			R3_CLS_UtilReport1.checkReport1Creation(fligthSet);
		}
	}
}