global with sharing class R2_JOB_CheckStatusSegment implements Schedulable {

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:       	Alvaro Garcia Tapia
    Company:       	Accenture
    Description: 	Job to active the process to send the segment to SAP
    
    IN:				
	OUT:			

    History: 
     <Date>                     <Author>                <Change Description>
    14/02/2018                  Alvaro Garcia Tapia            Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	public R2_JOB_CheckStatusSegment (){

	}

	global void execute(SchedulableContext sc) {

		try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');} //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;    
			
			System.abortJob(sc.getTriggerId());
		}
		catch(Exception exc){}
		System.debug('*** star R2_BTCH_CheckStatusSegment');
		
		Database.executeBatch(new R2_BTCH_CheckStatusSegment(null));

	}
}