/*---------------------------------------------------------------------------------------------------------------------
    Author:         Jorge Diaz-Blanco Sanjuan
    Company:        Accenture
    Description:    Apex test para la clase apex 'R2_INC_VF_CreatePDF_CTRL'
    IN:             
    OUT:            

    History: 
     <Date>                     <Author>                         <Change Description>
    17/01/2019             Jorge Diaz-Blanco Sanjuan                  Initial Version
    ----------------------------------------------------------------------------------------------------------------------*/
@isTest
public class R2_INC_VF_CreatePDF_PIR_CTRL_TEST {
     
    @isTest
	static void R2_INC_VF_CreatePDF_PIR_CTRL_getInfo()
	{
          PageReference thePage = Page.R2_INC_VF_CreatePDF_PIR;
          Test.setCurrentPage(thePage);
          R1_Incident__c Inc = new R1_Incident__c();
          insert Inc;
          ApexPages.currentPage().getParameters().put('id', Inc.id);
          R2_INC_VF_CreatePDF_PIR_CTRL controller = new R2_INC_VF_CreatePDF_PIR_CTRL();
          Test.StartTest();
          Test.stopTest();
     }
}