public class R1_CLS_CTI_VF_CTRL{
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Borja Gay
    Company:        Accenture
    Description:    CTI VisualForce Controller Class.
    
    History:
    
    <Date>              <Author>            <Description>
    04/07/2018          Borja Gay          Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
   
    public String ani = '';
    public String dnis = '';
    public String callid = '';
    public String recordid = '';
    public String tipo {get;set;}
    public String subtipo {get;set;}
    public String servicio {get;set;}
    public String tiempoesp = '';
    public String ultimoscasos = '';
    public String url {get;set;}
    public String urlCliente {get;set;}
    public Map <String,String> mapParameters = new Map<String,String>();
    public boolean esPureCloud {get;set;}
    public String idc{get;set;}
    public String urlCaso{get;set;}
    public List<Account> accDup {get;set;}
    public List<Account> acc {get;set;}
    public boolean noCliente {get;set;}
    public boolean clienteMult {get;set;}
    public static String idAccCaso {get;set;}
    public boolean internal {get;set;}
    public String timeInQueue {get;set;} //DBI 23/08/2017
    public List<String> lstCasesId {get;set;}
    List<String> idreconocidos = new List<String>();
    List<String> idDup = new List<String>();
    
    /*---------------------------------------------------------------------------------------------------------------------
  Author:         Borja Gay Flores
  Company:        Accenture
  Description:    Constructor de la clase
  IN:             
  OUT:            

  History: 
   <Date>                     <Author>                         <Change Description>
  05/07/2017               Borja Gay Flores                       Initial Version
  23/08/2017                  David Barco Infante               Add the timeInQueue field in the VF (value in seconds)
  ----------------------------------------------------------------------------------------------------------------------*/ 
    
    public R1_CLS_CTI_VF_CTRL(ApexPages.StandardController controller) {
      try{
        if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');} //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;  
        
        servicio = '';
        ani = ApexPages.currentPage().getParameters().get('ani');//telefono llamante
        dnis = ApexPages.currentPage().getParameters().get('dnis');//telefono al que se llama, con esto podemos ver el servcio mediante un CS
        callid = ApexPages.currentPage().getParameters().get('callid');
        recordid = ApexPages.currentPage().getParameters().get('recordid'); //id de Account
        tipo = ApexPages.currentPage().getParameters().get('tipo');
        subtipo = ApexPages.currentPage().getParameters().get('subtipo');
        servicio = ApexPages.currentPage().getParameters().get('servicio');
        tiempoesp = ApexPages.currentPage().getParameters().get('tiempoesp');
        ultimoscasos = ApexPages.currentPage().getParameters().get('ultimoscasos');
              
        List<String> lstRecordsId = new List<String>();
        lstCasesId = new List<String>();
        urlCaso = '';

        //List<String> lstCasesIdAux = ultimoscasos.split(';');
        //for (String aux : lstCasesIdAux) {
        //  lstCasesId.add('"/' + aux + '"');
        //}
        System.debug('recordid: '+recordid);
        noCliente=false;
        internal=false;
        clienteMult = false;
        
        if(recordId!=null && recordId!=''){
          idreconocidos = recordId.split(';');
          if(idreconocidos.size() != 0){
            clienteMult = true;
            acc=[SELECT Id, Name, FirstName, LastName, R1_ACC_PKL_identification_Type__c,R1_ACC_TXT_Identification_number__c,R1_ACC_FOR_Primary_FF_Card_CCPO__c,
                  R1_ACC_PKL_Card_Type__c, IsPersonAccount, R1_ACC_PKL_Preference_language_desc__c, PersonEmail, PersonBirthdate
                  FROM Account WHERE Id IN :idreconocidos];
          }
        }
        if(acc==null || acc.size()<=0){
          System.debug('No Cliente');
          noCliente=true;
        }
          
        //DBI 23/8/2017
        //Add value to timeInQueue for fill the new column in the visualforce R1_VF_CTI
        timeInQueue = '';
        if(tiempoesp != null && tiempoesp != ''){
          Integer seg = 0; //Integer.valueOf(tiempoesp);
          Integer resta = seg-40;
          if(resta > 0){
            timeInQueue = R1_CLS_TaskTriggerMethods.devuelveStringDate(resta);
          }else{
            timeInQueue = R1_CLS_TaskTriggerMethods.devuelveStringDate(0);
          }     
        }
      }catch(Exception exc){
        R1_CLS_LogHelper.generateErrorLog('R1_CLS_VF_CTRL__c.R1_CLS_VF_CTRL', '', exc.getmessage()+', '+exc.getLineNumber(), 'R1_CLS_VF_CTRL__c');
      }
    }
 
    /*---------------------------------------------------------------------------------------------------------------------
  Author:         Borja Gay Flores
  Company:        Accenture
  Description:    Metodo que asigna valor con la ID de un Caso a la variable urlCaso
  IN:             
  OUT:            

  History: 
   <Date>                     <Author>                         <Change Description>
  05/07/2017               Borja Gay Flores                       Initial Version
  ----------------------------------------------------------------------------------------------------------------------*/ 
    public void asignarCaso(){
      try{
        if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');} //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;  
        
        if(internal==false && noCliente==false){
          if(recordId!=''){
            clienteMult = true;
          }
          else{
            System.debug('No Cliente Abajo');
            noCliente=true;
          }
        }
      }catch(Exception exc){
        R1_CLS_LogHelper.generateErrorLog('R1_CLS_VF_CTRL__c.asignarCaso()', '', exc.getmessage()+', '+exc.getLineNumber(), 'R1_CLS_VF_CTRL__c');
      }
    }
    
/*---------------------------------------------------------------------------------------------------------------------
  Author:         Borja Gay Flores
  Company:        Accenture
  Description:    Método que crea un caso con los datos de la llamada
  IN:             
  OUT: String "/"+id caso+"/e"            

  History: 
   <Date>                     <Author>                         <Change Description>
  05/07/2017               Borja Gay Flores                       Initial Version
  ----------------------------------------------------------------------------------------------------------------------*/ 
    public String cargarCaso (){      
      try{
        if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');} //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;  
            
        Case caso = new Case();
        List <Case> lstCasos = new List<Case>();
        lstCasos = [SELECT id from Case where R1_CAS_TXT_Call__c = :callid];
        System.debug('lstCasos: ' + lstCasos);
        if(lstCasos.isEmpty()){
          caso.R1_CAS_TXT_Call__c=callid;
          Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Comunicaciones entrantes').getRecordTypeId();  
          caso.recordTypeId=recordTypeId;
          caso.status='Cerrado';
          caso.Type = tipo;
          caso.R1_CAS_PKL_Subtype__c = subtipo;
          caso.R1_CAS_PKL_SubtypeCE__c = subtipo;
          caso.R1_CAS_PKL_TypeCE__c = tipo;
          caso.Origin = 'Llamada';
          caso.AccountId = idAccCaso;
          insert caso;
          System.debug('ID Caso: '+ caso.id );
          System.debug('ID Caso: '+ caso );
          return '/'+caso.id+'/e';
        }
        else{
          return '/'+lstCasos[0].id+'/e';
        }
      }catch(Exception exc){
        R1_CLS_LogHelper.generateErrorLog('R1_CLS_VF_CTRL__c.cargarCaso', '', exc.getmessage()+', '+exc.getLineNumber(), 'R1_CLS_VF_CTRL__c');
        return '';
      }
    }

    /*---------------------------------------------------------------------------------------------------------------------
  Author:         Alvaro Garcia Tapia
  Company:        Accenture
  Description:    Metodo que crea un Caso y lo mete en la variable urlCaso si se ha encontrado clientes multiples
  IN:             
  OUT:            

  History: 
   <Date>                     <Author>                         <Change Description>
  11/07/2018               Alvaro Garcia Tapia                       Initial Version
  ----------------------------------------------------------------------------------------------------------------------*/ 
  public void asignarCasoAccMult(){
    try{
      if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');} //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;  
      
      if(clienteMult){
        urlCaso = cargarCaso();
        obtenerultCasos();
      }
    }catch(Exception exc){
      R1_CLS_LogHelper.generateErrorLog('R1_CLS_VF_CTRL__c.asignarCasoAccMult()', '', exc.getmessage()+', '+exc.getLineNumber(), 'R1_CLS_VF_CTRL__c');
    }
  }

      /*---------------------------------------------------------------------------------------------------------------------
  Author:         Alvaro Garcia Tapia
  Company:        Accenture
  Description:    Metodo que obtiene los ultimos 5 casos abiertos si se ha encontrado clientes multiples
  IN:             
  OUT:            

  History: 
   <Date>                     <Author>                         <Change Description>
  11/07/2018               Alvaro Garcia Tapia                       Initial Version
  ----------------------------------------------------------------------------------------------------------------------*/ 
  public void obtenerultCasos(){
    try{
      if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');} //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;  

      List<Case> ultCases = [SELECT Id FROM Case 
                  WHERE Status != 'Cerrado' AND Status != 'Cancelado' AND  AccountId = :idAccCaso 
                  AND (RecordType.Name = 'Asistencia' OR RecordType.Name = 'Call Pass Througth JBA' 
                  OR RecordType.Name = 'Campañas salientes' OR RecordType.Name = 'Felicitación' 
                  OR RecordType.Name = 'Help DeskOW' OR RecordType.Name = 'Iberia.com' 
                  OR RecordType.Name = 'Iberia Plus' OR RecordType.Name = 'Proactividad' 
                  OR RecordType.Name = 'Reclamación' OR RecordType.Name = 'Salientes' 
                  OR RecordType.Name = 'Transferidas' OR RecordType.Name = 'Vuelos en Obtención' 
                  OR RecordType.Name = 'Vuelos en Redención' OR RecordType.Name = 'Gestión de Reservas'
                  OR RecordType.Name = 'Gestión de Llamadas' OR RecordType.Name = 'Ayuda a la Navegación'
                  OR RecordType.Name = 'Programa Fidelización' OR RecordType.Name = 'Información General') 
                  ORDER BY LastModifiedDate DESC LIMIT 5];

      if (!ultCases.isEmpty()) {
        lstCasesId = new List<String>();
        for (Case aux : ultCases) {
          lstCasesId.add('"/' + aux.Id + '"');
        }
      }
      System.debug('lstCasesId: ' + lstCasesId);
    }catch(Exception exc){
      R1_CLS_LogHelper.generateErrorLog('R1_CLS_VF_CTRL__c.obtenerultCasos()', '', exc.getmessage()+', '+exc.getLineNumber(), 'R1_CLS_VF_CTRL__c');
    }
  }
}