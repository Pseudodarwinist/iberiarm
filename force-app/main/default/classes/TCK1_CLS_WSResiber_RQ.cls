public class TCK1_CLS_WSResiber_RQ {
    public String CarrierCode {get; set;}
    public String FlightNumber {get; set;}
    public String LocalLegDate {get; set;}
    public String DepartureStation {get; set;}
    public String ArrivalStation {get; set;}
    public String RequestingCarrier {get; set;}
    public String RequestingSystem {get; set;}
}