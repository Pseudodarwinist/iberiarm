@IsTest
public class R2_CLS_MergeAccount_DupPorId_Test
{
    /*---------------------------------------------------------------------------------------------------------------------
    Author:         Raúl Julián González
    Company:        Accenture
    Description:    Apex test de la clase apex "R2_CLS_MergeAccount_DupPorId"
    IN:             
    OUT:            

    History: 
     <Date>                     <Author>                         <Change Description>
    07/07/2018             Raúl Julián González                   Initial Version
    ----------------------------------------------------------------------------------------------------------------------*/
    static testMethod void mergeAccounts_Test(){
        R1_CLS_LogHelper.throw_exception = false;   
        Id recordTypeIdAcc = [SELECT Id FROM recordType WHERE Name like 'Cuenta personal' LIMIT 1].Id;
        Id recordTypeIdCase = [SELECT Id FROM recordType WHERE Name like 'Asistencia' LIMIT 1].Id;
        List<Account> lstAcc = new List<Account>();
            Account accTest = new Account();
            accTest.LastName = 'Test';
            accTest.RecordTypeId = recordTypeIdAcc;
            accTest.R1_ACC_TXT_Id_Golden_record__c = '1';
            accTest.PersonBirthdate = Date.newInstance(1990, 12, 31);
            lstAcc.add(accTest);
            Account accTest1 = new Account();
            accTest1.LastName = 'Test1';
            accTest1.RecordTypeId = recordTypeIdAcc;
            accTest1.R1_ACC_TXT_Id_Golden_record__c = '2';
            accTest1.PersonBirthdate = Date.newInstance(1990, 12, 31);
            lstAcc.add(accTest1);
        insert lstAcc;      
        
        Map<Id, Id> testMap = new Map<Id, Id>();
        testMap.put(lstAcc[0].Id, lstAcc[1].Id);

        Test.startTest();
            R2_CLS_MergeAccount_DupPorId.mergeAccounts(testMap);
        Test.stopTest();        
    }

    /*---------------------------------------------------------------------------------------------------------------------
    Author:         Raúl Julián González
    Company:        Accenture
    Description:    Apex test de la clase apex "R2_CLS_MergeAccount_DupPorId"
    IN:             
    OUT:            

    History: 
     <Date>                     <Author>                         <Change Description>
    07/07/2018             Raúl Julián González                   Initial Version
    ----------------------------------------------------------------------------------------------------------------------*/
    static testMethod void updateObject_Test(){
        R1_CLS_LogHelper.throw_exception = false;   
        Id recordTypeIdAcc = [SELECT Id FROM recordType WHERE Name like 'Cuenta personal' LIMIT 1].Id;
        Id recordTypeIdCase = [SELECT Id FROM recordType WHERE Name like 'Asistencia' LIMIT 1].Id;
        List<Account> lstAcc = new List<Account>();
            Account accTest = new Account();
            accTest.LastName = 'Test';
            accTest.RecordTypeId = recordTypeIdAcc;
            accTest.R1_ACC_TXT_Id_Golden_record__c = '1';
            accTest.PersonBirthdate = Date.newInstance(1990, 12, 31);
            lstAcc.add(accTest);
            Account accTest1 = new Account();
            accTest1.LastName = 'Test1';
            accTest1.RecordTypeId = recordTypeIdAcc;
            accTest1.R1_ACC_TXT_Id_Golden_record__c = '2';
            accTest1.PersonBirthdate = Date.newInstance(1990, 12, 31);
            lstAcc.add(accTest1);
        insert lstAcc;  
        
        Case caso = new Case();
        caso.RecordTypeId = recordTypeIdCase;
        caso.AccountId = lstAcc[0].Id;
        caso.Type = 'Asistencia';
        caso.Origin = 'Email';
        caso.Status = 'Abierto';
        insert caso;

        R1_Alert__c alerta = new R1_Alert__c();
        alerta.R1_ATA_LOO_Client__c = lstAcc[0].Id;
        insert alerta;
        
        Map<Id, Id> idTestMap = new Map<Id, Id>();
        idTestMap.put(lstAcc[0].Id, lstAcc[1].Id);
        Map<Id, Id> idTestMap2 = new Map<Id, Id>();
        
        Test.startTest();
        R2_CLS_MergeAccount_DupPorId.updateObject(idTestMap, idTestMap2, 'Case', 'AccountId', 'R1_CAS_TXT_Golden_Record_Previous__c');
        R2_CLS_MergeAccount_DupPorId.updateObject(idTestMap, idTestMap2, 'R1_Alert__c', 'AccountId', 'R1_CAS_TXT_Golden_Record_Previous__c');
        Test.stopTest();
    }
    /*---------------------------------------------------------------------------------------------------------------------
    Author:         Raúl Julián González
    Company:        Accenture
    Description:    Apex test de la clase apex "R2_CLS_MergeAccount_DupPorId"
    IN:             
    OUT:            

    History: 
     <Date>                     <Author>                         <Change Description>
    07/07/2018             Raúl Julián González                   Initial Version
    ----------------------------------------------------------------------------------------------------------------------*/
    static testMethod void exception_Test(){
        R1_CLS_LogHelper.throw_exception = true;
        Map<Id, Id> testMap = new Map<Id, Id>();
        R2_CLS_MergeAccount_DupPorId.mergeAccounts(testMap);
        R2_CLS_MergeAccount_DupPorId.updateObject(null, null, '', '', '');
    }
}