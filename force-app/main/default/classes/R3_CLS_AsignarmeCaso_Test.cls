/*---------------------------------------------------------------------------------------------------------------------
    Author:         ICA
    Company:        TCK
    Description:   Asignarme caso 

    History:
     <Date>                     <Author>                         <Change Description>
    08/08/2019            		ICA-LCS                				Initial Version
	19/09/2019					ICA-LCS                				Updated. Change the Setup method, delete R2_REA_PKL_Delay_Code__c='CE' to use R2_REA_TXT_Delay_No__c ='93.0' instead
 ----------------------------------------------------------------------------------------------------------------------*/

/**
* @author TCK&LCS
* @date 08/08/2019
* @description Test class for R3_CLS_AsignarmeCaso class
*/
@isTest

public class R3_CLS_AsignarmeCaso_Test {
    
    @isTest

    public static void test1(){
        Case caso = new Case();
        caso.Status = 'Abierto';
        caso.Origin = 'Email';
        caso.Type = 'Retraso';
		
        test.startTest();
        insert caso;
        R3_CLS_AsignarmeCaso.asignarmeCaso(caso.Id);
        Case statusPrueba = [SELECT Status from Case where Id = :caso.Id];
        System.assertEquals(statusPrueba.Status , 'Trabajando');
        test.stopTest();
    }
}