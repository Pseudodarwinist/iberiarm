/*---------------------------------------------------------------------------------------------------------------------
Author:         Alvaro Garcia Tapia
Company:        Accenture
Description:    Clase de metodos para el trigger de social persona
IN:
OUT:

History:
History:
<Date>                     <Author>                         <Change Description>
31/08/2018             Alvaro Garcia Tapia                   Initial Version
----------------------------------------------------------------------------------------------------------------------*/
public without sharing class R2_CLS_SocialPersonaTriggerMethods {
	
	/*---------------------------------------------------------------------------------------------------------------------
	Author:         Alvaro Garcia Tapia
	Company:        Accenture
	Description:    Change the associated contact y account of all the case linked to this social Persona and the field whoId of social post linked to the persona
	IN:
	OUT:

	History:
	<Date>                     <Author>                         <Change Description>
	31/08/2018             Alvaro Garcia Tapia                   Initial Version
	----------------------------------------------------------------------------------------------------------------------*/
	public static void updateAccountInCases(List<SocialPersona> new_lstSPersona, List<SocialPersona> old_lstSPersona){
		try{
			if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');} //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;

			Set<Id> set_oldParentId = new Set<Id>();
			Set<Id> set_newParentId = new Set<Id>();
			Set<Id> set_IdPersona = new Set<Id>();
			Map<Id,Id> map_parentId = new Map<Id,Id>();
			//Se cogen los clientes asociados al social persona, tanto el old como el new
			for(Integer i = 0; i < new_lstSPersona.size(); i++) {
				if (old_lstSPersona[i].ParentId != new_lstSPersona[i].ParentId && new_lstSPersona[i].ParentId != null) {
					set_oldParentId.add(old_lstSPersona[i].ParentId);
					set_newParentId.add(new_lstSPersona[i].ParentId);
					set_IdPersona.add(new_lstSPersona[i].Id);
					//mapa con clave cliente old y valor cliente new
					map_parentId.put(old_lstSPersona[i].ParentId, new_lstSPersona[i].ParentId);
				}
			}

			if(!set_newParentId.isEmpty()) {
				//Obtenemos los peronContactId de los clientes que se han asociado a las social personas
				Map<Id,Account> map_Account = new Map<Id,Account> ([SELECT Id, PersonContactId FROM Account WHERE Id IN :set_newParentId]);
				//Social Post asociados a las social personas
				List<SocialPost> lst_SPost = [SELECT Id, WhoId, ParentId FROM SocialPost WHERE PersonaId IN :set_IdPersona];
				Set<Id> set_caseId = new Set<Id>();
				//creo un set con los id de los casos relacionados a los social post
				for (SocialPost post : lst_SPost) {
					if (!set_caseId.contains(post.ParentId) && post.ParentId != null) 
					{set_caseId.add(post.ParentId);}	
				}

				//Obtenemos los casos asociados a los post que estamos tratando 
				List<Case> lst_Case = [SELECT Id, AccountId, ContactId FROM Case WHERE Id IN :set_caseId OR (AccountId IN :set_oldParentId AND ParentId IN :set_caseId)];
				System.debug('!!!map_Account: ' + map_Account);
				System.debug('!!!lst_SPost: ' + lst_SPost);
				System.debug('!!!lst_Case: ' + lst_Case);

				List<SocialPost> sPostToUpdate = new List<SocialPost>();
				List<Case> caseToUpdate = new List<Case>();
				//se recorre la lista de old account asociadas a las social personas que son los clientes que aun estan asociadas a los casos y post que se actualizaran en este metodo
				for(Id idParent : set_oldParentId) {
					System.debug('!!!idParentOld: ' + idParent);
					System.debug('!!!idParentNew: ' + map_parentId.get(idParent));
					//Se actualiza el account asociado al post con el new account que viene en el social persona
					for (SocialPost sPostAux : lst_SPost) {
						System.debug('!!!map_Account.get(map_parentId.get(idParent)).Id: ' + map_Account.get(map_parentId.get(idParent)).Id);
						sPostAux.WhoId = map_Account.get(map_parentId.get(idParent)).Id;
						sPostToUpdate.add(sPostAux);
						System.debug('!!!sPostToUpdate: ' + sPostToUpdate);
					}
					
					//Se actualiza el account asociado al Caso con el new account y contact que viene en el social persona
					for (Case caseAux : lst_Case) {
						System.debug('!!!map_Account.get(map_parentId.get(idParent)).Id: ' + map_Account.get(map_parentId.get(idParent)).Id);
						caseAux.AccountId = map_Account.get(map_parentId.get(idParent)).Id;
						System.debug('!!!map_Account.get(map_parentId.get(idParent)).PersonContactId: ' + map_Account.get(map_parentId.get(idParent)).PersonContactId);
						caseAux.ContactId = map_Account.get(map_parentId.get(idParent)).PersonContactId;
						caseAux.R2_CAS_CHK_copy_contact_information__c = true;
						caseToUpdate.add(caseAux);
						System.debug('!!!caseToUpdate: ' + caseToUpdate);
					}	
					
				}
				System.debug('!!!sPostToUpdate2: ' + sPostToUpdate);
				if (!sPostToUpdate.isEmpty()) {
					update sPostToUpdate;
				}
				System.debug('!!!caseToUpdate2: ' + caseToUpdate);
				if (!caseToUpdate.isEmpty()) {
					update caseToUpdate;
				}
			}
		}
		catch(Exception exc){
			R1_CLS_LogHelper.generateErrorLog('R2_CLS_SocialPersonaTriggerMethods.updateAccountInCases()', '', exc.getmessage()+', '+exc.getLineNumber(), 'SocialPersona');
		}
	}
}