global class R2_EMAIL_Prorrateos implements Messaging.InboundEmailHandler {

	static string ORG_WIDE_ADDRESS = '';
	private static string prorrateosEmailBccAddr;
	static{
		prorrateosEmailBccAddr = Label.R2_LB_Prorrateos_BCC_Address;
		List<OrgwideEmailAddress> listOrgEmail = [SELECT Id, Address FROM OrgwideEmailAddress WHERE displayname='UCAC Prorrateos' OR Address='ucacprorrateos@iberia.es' LIMIT 1];
		if(listOrgEmail!=null && !listOrgEmail.isEmpty() ){
			ORG_WIDE_ADDRESS = listOrgEmail[0].Address;
		}
	}

	global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope env) {

		// Create an InboundEmailResult object for returning the result of the Apex Email Service
		Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();

		System.debug('*** email: '+ JSON.serialize(email) );

		try{
			if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}

			if(ORG_WIDE_ADDRESS != email.fromAddress){

				Pattern pat = Pattern.compile('(?:.*)(?:\\[sfref\\:)(?:.*)(?:\\:sfref\\])(?:.*)');
				Matcher matSubj = pat.matcher(email.subject!=null ? email.subject : '');
				Matcher matBody = pat.matcher(email.plainTextBody!=null ? email.plainTextBody : '');

				if(matSubj.matches() || matBody.matches()){
					System.debug('*** email of segment');
					seveEmailOfSegement(email);
				}else{
					System.debug('*** email to case ok');
					seveEmailToCaseCustom(email);
				}

			}

			// Set the result to true. No need to send an email back to the user with an error message
			result.success = true;

			// Return the result for the Apex Email Service
			return result;

		}catch(Exception exc){
			R1_CLS_LogHelper.generateErrorLog('R2_EMAIL_Prorrateos.handleInboundEmail()', '', exc.getmessage()+', '+exc.getLineNumber(), '');

			// Set the result to true. No need to send an email back to the user with an error message
			result.success = false;

			// Return the result for the Apex Email Service
			return result;
		}

	}


	public void seveEmailOfSegement(Messaging.InboundEmail email){
		Savepoint sp = Database.setSavepoint();
		try{
			if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}

			Id idSeg = R2_CLS_EmailMessageTriggerMethods.getRefSegId(email.subject);
			idSeg = idSeg!=null ? idSeg : R2_CLS_EmailMessageTriggerMethods.getRefSegId(email.plainTextBody);
			idSeg = idSeg!=null ? idSeg : R2_CLS_EmailMessageTriggerMethods.getRefSegId(email.htmlBody);

			List<R2_Segmento__c> listSeg = [SELECT Id, R2_SEG_CHK_Message_Received__c,R2_SEG_PKL_Segment_Status__c,R2_SEG_MSDT_Prorrate__c FROM R2_Segmento__c WHERE Id=:idSeg];

			if(!listSeg.isEmpty()){
				EmailMessage e = makeInboundEmailToSave(email);
				e.R2_EMA_LOO_Segmento__c = listSeg[0].Id;
				e.BccAddress = prorrateosEmailBccAddr!='' ? prorrateosEmailBccAddr : null;
				Database.SaveResult resInsEmail = Database.insert(e, true);
				if (resInsEmail.isSuccess()) {
					listSeg[0].R2_SEG_CHK_Message_Received__c = true;
					listSeg[0].R2_SEG_PKL_Segment_Status__c = 'Rechazado';
					Database.SaveResult resUpdSeg = Database.update(listSeg[0], true);
					if (resUpdSeg.isSuccess()) {
						List<R2_Prorrateo__c> listPro= [SELECT Id,R2_PRO_PKL_Status__c FROM R2_Prorrateo__c WHERE Id=:listSeg[0].R2_SEG_MSDT_Prorrate__c];
						if(listPro!=null && !listPro.isEmpty()){
							listPro[0].R2_PRO_PKL_Status__c = 'Pendiente respuesta';
							Database.SaveResult resUpdPro = Database.update(listPro[0], true);

							if (!resUpdSeg.isSuccess()) Database.rollback(sp);
						}
					}else{
						Database.rollback(sp);
					}
				}else {
					Database.rollback(sp);
				}
			}

		}catch(Exception exc){
			Database.rollback(sp);
			R1_CLS_LogHelper.generateErrorLog('R2_EMAIL_Prorrateos.seveEmailOfSegement()', '', exc.getmessage()+', '+exc.getLineNumber(), '');
		}
	}

	public void seveEmailToCaseCustom(Messaging.InboundEmail email){
		try{
			if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}

			Id rtId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Prorrateo').getRecordTypeId();
			Id ownrId = [SELECT id from group where Type = 'Queue' and Name='SYR Prorrateos' Limit 1].Id;

			Case cas = new Case();
			cas.RecordTypeId = rtId;
			cas.OwnerId = ownrId;
			cas.status = 'Abierto';
			cas.Origin = 'Email';
			cas.Priority = 'Media';

			cas.Subject = email.subject;
			cas.Description = email.plainTextBody;

			cas.SuppliedEmail = email.fromAddress;
			cas.SuppliedName = email.fromName;

			insert cas;

			List<Attachment> listAtt = getAttachments(email, cas.Id);
			if(listAtt!=null && !listAtt.isEmpty()){
				insert listAtt;
			}


			EmailMessage e = makeInboundEmailToSave(email);
			if(e!=null){
				e.BccAddress = prorrateosEmailBccAddr!='' ? prorrateosEmailBccAddr : null;
				e.ParentId = cas.Id;
				insert e;
			}


		}catch(Exception exc){
			R1_CLS_LogHelper.generateErrorLog('R2_EMAIL_Prorrateos.seveEmailToCaseCustom()', '', exc.getmessage()+', '+exc.getLineNumber(), '');
		}
	}

	public EmailMessage makeInboundEmailToSave(Messaging.InboundEmail email){
		try{
			EmailMessage e = new EmailMessage();
			e.Incoming = true;
			e.Status = '0';
			e.MessageDate = Datetime.now();
			e.ToAddress = email.toAddresses!=null ? String.join(email.toAddresses,',') : '';
			e.Subject = email.subject;
			//e.CcAddress = email.ccAddresses!=null ? String.join(email.ccAddresses,',') : '';
			e.FromAddress = email.fromAddress;
			e.FromName = email.fromName;
			e.HtmlBody = email.htmlBody;
			e.TextBody = email.plainTextBody;
			return e;
		}catch(Exception exc){
			R1_CLS_LogHelper.generateErrorLog('R2_EMAIL_Prorrateos.makeEmailToSave()', '', exc.getmessage()+', '+exc.getLineNumber(), '');
			return null;
		}
	}

	public List<Attachment> getAttachments(Messaging.InboundEmail email, Id idParent){
		try{

			System.debug('*** init getAttachments() ');

			if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}
			List<Attachment> listAtt = new List<Attachment>();

			if(email!=null){

				if(email.textAttachments!=null){
					for (Messaging.Inboundemail.TextAttachment att : email.textAttachments) {
						Attachment attachment = new Attachment();
						attachment.Name = att.fileName;
						attachment.Body = Blob.valueOf(att.body);
						attachment.ParentId = idParent;
						listAtt.add(attachment);
					}
				}

				if(email.binaryAttachments!=null){
					for (Messaging.Inboundemail.BinaryAttachment att : email.binaryAttachments) {
						Attachment attachment = new Attachment();
						attachment.Name = att.fileName;
						attachment.Body = att.body;
						attachment.ParentId = idParent;
						listAtt.add(attachment);
					}
				}
			}

			return listAtt;

		}catch(Exception exc){
			R1_CLS_LogHelper.generateErrorLog('R2_EMAIL_Prorrateos.seveEmailToCaseCustom()', '', exc.getmessage()+', '+exc.getLineNumber(), '');
			return null;
		}
	}

}