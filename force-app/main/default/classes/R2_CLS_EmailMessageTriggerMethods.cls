public with sharing class R2_CLS_EmailMessageTriggerMethods {

     public static void populateRefID(List<EmailMessage> news){
        try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}
            if(!news.isEmpty()){
                for(EmailMessage e: news){
                    if(e.TextBody!=null && e.TextBody.Contains('ref:_') && (e.HtmlBody==null || !e.HtmlBody.Contains('ref:_'))){
                        String refID = e.TextBody.substring(e.TextBody.IndexOf('ref:_'),e.TextBody.IndexOf('ref:_')+30);
                        e.HtmlBody = e.HtmlBody!=null ? e.HtmlBody+'<br/>'+refID : refID;
                    }
                    
                    if(e.HtmlBody!=null && e.HtmlBody.Contains('ref:_') && (e.TextBody==null || !e.TextBody.Contains('ref:_'))){
                        String refID = e.HtmlBody.substring(e.HtmlBody.IndexOf('ref:_'),e.HtmlBody.IndexOf('ref:_')+30);
                        e.TextBody = e.TextBody!=null ? e.TextBody+'\n'+refID : refID;
                    }
                }
            }
        }catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_CLS_EmailMessageTriggerMethods.populateRefID()', '', exc.getmessage()+', '+exc.getLineNumber(), 'EmailMessage');
        }
    }

    public static void relatedEmailSegment(List<EmailMessage> news){
        try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}

            if(!news.isEmpty()){

                for(EmailMessage email: news){
                    Id idSeg = getRefSegId(email.Subject);
                    idSeg = idSeg!=null ? idSeg : getRefSegId(email.TextBody);
                    idSeg = idSeg!=null ? idSeg : getRefSegId(email.HtmlBody);
                    if(idSeg!=null){
                        if(email.R2_EMA_LOO_Segmento__c==null){
                            email.R2_EMA_LOO_Segmento__c = idSeg;
                            email.ParentId = getCaseIdOfProrrate(idSeg);
                        }else if(email.R2_EMA_LOO_Segmento__c!=null){
                            email.ParentId = getCaseIdOfProrrate(email.R2_EMA_LOO_Segmento__c);
                        }
                    }
                }
            }
        }catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_CLS_EmailMessageTriggerMethods.relatedEmailSegment()', '', exc.getmessage()+', '+exc.getLineNumber(), 'EmailMessage');
        }
    }

    @TestVisible
    private static Id getCaseIdOfProrrate(String idSeg){
        try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}

            R2_Segmento__c seg = [SELECT Id, R2_SEG_MSDT_Prorrate__r.R2_PRO_LOO_Case__r.Id FROM R2_Segmento__c WHERE Id=:idSeg];
            return seg.R2_SEG_MSDT_Prorrate__r.R2_PRO_LOO_Case__r.Id;
        }catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_CLS_EmailMessageTriggerMethods.getCaseIdOfProrrate()', '', exc.getmessage()+', '+exc.getLineNumber(), '');
            return null;
        }
    }

    public static Id getRefSegId(String body){
        try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}

            Id resultId = null;
            if(body!=null && body!=''){
                String ref = body.substringAfter('[sfref:');
                ref = ref.substringBefore(':sfref]');
                ref = ref.deleteWhitespace();
                try{
                    resultId = ref!='' ? Id.valueOf(ref) : null;
                }catch(Exception exc){
                    resultId = null;
                }

            }
            return resultId;
        }catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_CLS_EmailMessageTriggerMethods.getRefSegId()', '', exc.getmessage()+', '+exc.getLineNumber(), '');
            return null;
        }
    }


     /*-----------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Raúl Julián González
    Company:        Accenture
    Description:    update related case of email

    IN:     
    OUT:    

    History:
    <Date>                      <Author>                         <Change Description>
        ?                           ?                                      ?
    21/09/2018               Raúl Julián González                 added logic of social studio
    -----------------------------------------------------------------------------------------*/
    public static void updatecasoPadreEmail(List<EmailMessage> news){
        if(news[0].R2_EMA_LOO_Segmento__c==null){
            List<Case> caso = [SELECT Id, Status, OwnerId,RecordTypeId, Type, Origin FROM Case Where Id = :news[0].ParentId LIMIT 1];        
            List<Case> lst_upd = new List<Case>();
            Id rtIdExp = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Expediente').getRecordTypeId();
            Id rtIdComEnt = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Comunicaciones entrantes').getRecordTypeId();
            if(news[0].incoming == true){
                if(caso[0].RecordTypeId==rtIdExp){
                    Map<String,String> mapColas;
                    mapColas = new Map<String,String>();
                    List<Group> lstColas = new List<Group>();
                    lstColas = [SELECT id,Name from Group where Type = 'Queue'];
                    for(Group cola : lstColas){
                        mapColas.put(cola.Name,cola.id);
                    }
                    String idPasaje;
                    String idEquipaje;
                    idPasaje = mapColas.get('SYR Gestión Pasaje');
                    idEquipaje = mapColas.get('SYR Gestión Equipaje');
                    System.debug('Antes de comprobar si cerrado o cancelado');
                    if(caso[0].Status == 'Cerrado' || caso[0].Status == 'Cancelado' || caso[0].Status == 'Abierto a la espera de info de Iberia' || caso[0].Status == 'Abierto a la espera de la info del Cliente' || caso[0].Status == 'Escalado' ){
                        caso[0].Status = 'Reabierto';
                        System.debug('Va a ser de tipo deterioro');
                        String  idAutomatica = mapColas.get('Automáticos');
                        if(caso[0].R2_CAS_PKL_Exp_Pago_Claims_Auto__c == 'No'){
                            caso[0].OwnerId = idAutomatica;
                        }else{
                            if(caso[0].Type == 'Demora' || caso[0].Type == 'Deterioro' || caso[0].Type == 'Extravío' || caso[0].Type == 'Falta de contenido' ){
                                caso[0].OwnerId = idEquipaje;
                            }else{
                                caso[0].OwnerId = idPasaje;
                            }
                            System.debug('Se ha hecho el update del caso cambiando la cola y el estado ' + caso[0]);
                            lst_upd.add(caso[0]);
                        }                
                    }
                }else if(caso[0].RecordTypeId==rtIdComEnt && caso[0].Origin=='Redes sociales' && caso[0].Status != 'Abierto'){
                    caso[0].Status = 'Abierto';
                    lst_upd.add(caso[0]);
                }  
            }else if(caso[0].RecordTypeId == rtIdComEnt && caso[0].Origin=='Redes sociales' && caso[0].Status !='Abierto a la espera de info de Iberia'){
                caso[0].Status = 'Abierto a la espera de info de Iberia';
                lst_upd.add(caso[0]);
            }
            if(!lst_upd.isEmpty()){
                Database.DMLOptions dmo =new Database.DMLOptions();
                    dmo.AssignmentRuleHeader.UseDefaultRule= true;
                    Database.update(lst_upd,dmo);
            }
        }  
    }
    /*-----------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Alberto Puerto Collado
    Company:        Accenture
    Description:    Management of queues:
                    calling to R2_CLS_CaseTriggerMethods.updateQueueOwnerBasedOnCategoriesFields(casosPadres);
                    (after update)

    IN:     Case
    OUT:    

    History:
    <Date>                      <Author>                         <Change Description>
    11/07/2018               Alberto Puerto Collado                 Initial Version
    12/02/2019               Jaime Ascanta                          filter only incoming email
    -----------------------------------------------------------------------------------------*/
    public static void managementMailQeues(List<EmailMessage> news){
        try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');} //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;
            System.debug('::::::managementMailQeues init::::');

            List<Case> casosPadres = new List<Case>();
            Set<Id> setParentNews = new Set<Id>();
            for(EmailMessage mail : news){
                if(mail.parentId != null && mail.Incoming==true && mail.R2_EMA_LOO_Segmento__c==null){
                    setParentNews.add(mail.parentId);
                }
            }

            
            casosPadres = [SELECT Id, R2_CAS_PKL_Vip_Type__c, R2_CAS_PKL_client_type__c, RecordtypeId, Status, R2_CAS_FOR_Flight_date_local__c,
                                    R1_CAS_FOR_Card_Type__c, R2_CAS_PKL_Platinum_level_description__c, R2_CAS_FOR_Iberia_singular__c, Owner.Name, Owner.Type, Case.R2_CAS_PKL_Class_flown__c
                            FROM Case 
                            WHERE Id IN :setParentNews 
                            ];
            System.debug('::::::casosPadres: ' + casosPadres);

            if(!casosPadres.isEmpty()){
                R2_CLS_CaseTriggerMethods.updateQueueOwnerBasedOnCategoriesFields(casosPadres);
            }
            
            
        }catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_CLS_EmailMessageTriggerMethods.managementMailQeues()', '', exc.getmessage()+', '+exc.getLineNumber(), 'EmailMessage');
        }
    }
}