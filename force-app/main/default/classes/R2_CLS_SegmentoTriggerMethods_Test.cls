/*---------------------------------------------------------------------------------------------------------------------
Author:         Jaime Ascanta
Company:        Accenture
Description:
IN:
OUT:

History:
<Date>                     <Author>                         <Change Description>
14/03/2018 					jaime ascanta                  	Initial Version
----------------------------------------------------------------------------------------------------------------------*/
@isTest
private class R2_CLS_SegmentoTriggerMethods_Test {
	
	/*---------------------------------------------------------------------------------------------------------------------
	Author:         Jaime Ascanta
	Company:        Accenture
	Description:
	IN:
	OUT:

	History:
	 <Date>                     <Author>                         <Change Description>
	14/03/2018       	   		Jaime Ascanta                  		 Initial Version
	24/05/2018       	   		Alberto Puerto                  	 fulfilling seg1.R2_SEG_TXT_Company__c (required field)
	10/10/2018    				Eva Sanchez							Actualizado el valor R2_NUM_Rate_to_EUR__c a dimonca. 
	----------------------------------------------------------------------------------------------------------------------*/
	@isTest static void checkProrateStatus_test() {
		R1_CLS_LogHelper.throw_exception = false;
		// endpoints
		List<R1_CS_Endpoints__c> lst_ep = new List<R1_CS_Endpoints__c>();
		R1_CS_Endpoints__c epLogin = new R1_CS_Endpoints__c();
		epLogin.Name = 'ETL_Login';
		epLogin.R1_CHK_Activo__c = true;
		epLogin.R1_TXT_EndPoint__c = 'PruebaLogin';
		lst_ep.add(epLogin);
		R1_CS_Endpoints__c ep = new R1_CS_Endpoints__c();
		ep.Name='R2_DelayedBags';
		ep.R1_CHK_Activo__c = true;
		ep.R1_TXT_EndPoint__c = 'R2_DelayedBags';
		lst_ep.add(ep);
		insert lst_ep;
		//timeOut
		TimeOut_Integrations__c cst = new TimeOut_Integrations__c();
		cst.Name = 'WorldTracer';
		cst.setTimeOut__c = 8000;
		insert cst;
		//R2_SDRtoEUR__c
		//Cambiado
		R2_ExchangeRate__c exchange = new R2_ExchangeRate__c();
		exchange.Name = 'XDR_960';
		exchange.R2_NUM_Rate_to_EUR__c = 1.1788;
		exchange.R2_TXT_External_Id__c = 'test';
		insert exchange;

		// prorrateos
		R2_Prorrateo__c pro1 = new R2_Prorrateo__c();
		pro1.R2_PRO_PKL_Status__c = 'Prorrateo OK';
		insert pro1;

		Test.startTest();
		
			R2_Segmento__c seg1 = new R2_Segmento__c();
			seg1.R2_SEG_TXT_company_code__c = 'test1';
			seg1.R2_SEG_MSDT_Prorrate__c = pro1.Id;
			seg1.R2_SEG_CHK_guilty_company__c = true;
			seg1.R2_SEG_TXT_Company__c = 'IBERIA';
			seg1.R2_SEG_PKL_Estado__c = 'Prorrateo OK';
			insert seg1;

		Test.stopTest();

	//	System.assertEquals(0, [SELECT count() FROM R1_Log__c]);
	}

}