global class R2_CLS_Rebound_Status implements Schedulable{ //implements Schedulable
    global static void execute(SchedulableContext ctx) { //SchedulableContext ctx
        Id rtIdExp = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Expediente').getRecordTypeId();
        Integer days = (Integer) Integer.valueOf(Label.R2_Days_for_rebound);
        //List<Case> lstCaso = [SELECT Id,R2_CAS_DATH_Rebounds_Last_Modify__c, R2_CAS_NUM_Number_of_rebounds__c,R2_CAS_DIV_AUT_Compensation__c, Status FROM Case WHERE RecordTypeId = : rtIdExp AND R2_CAS_NUM_Number_of_rebounds__c != 0 AND (Status != 'Cerrado' OR Status != 'Cancelado') AND R2_CAS_DATH_Rebounds_Last_Modify__c = LAST_N_DAYS: days];
        String dias = 'LAST_N_DAYS:' + days;
        System.debug('dias ' + dias);
        String query = 'SELECT Id,status,recordtype.name,R2_CAS_DATH_Rebounds_Last_Modify__c, R2_CAS_NUM_Number_of_rebounds__c,R2_CAS_DIV_AUT_Compensation__c,R2_CAS_PKL_Exp_Pago_Claims_Auto__c FROM Case WHERE RecordTypeId = : rtIdExp and R2_CAS_PKL_Exp_Pago_Claims_Auto__c != null AND R2_CAS_NUM_Number_of_rebounds__c != 0 AND R2_CAS_NUM_Number_of_rebounds__c != null AND (Status != \'Cerrado\' AND Status != \'Cancelado\') AND Owner.name =\'Automáticos\' AND R2_CAS_DATH_Rebounds_Last_Modify__c <='+ dias;
        System.debug('Query: ' + query);
        List<Case> lstCaso = database.query(query);
        System.debug('esta es la lista de casos ' + lstCaso); 
        List<Case> lstCasosHijo =  [SELECT Id, ParentId FROM Case WHERE ParentId in :lstCaso AND (R2_CAS_DIV_AUT_Compensation__c <= 0 AND R2_CAS_DIV_AUT_Compensation_Vouchers__c <= 0 AND R2_CAS_NUM_AUT_Compensation_in_Avios__c <= 0) ];
        System.debug('Lista de los casos hijo ' + lstCasosHijo);
        
        
        List<String> lstIdsPadre = new List<String>();
        List<Case> casoUpdate = new List<Case>(); 

        for(Case caso:lstCasosHijo){
            if(!lstIdsPadre.contains(caso.ParentId)){
                System.debug('LIsta de ids ' + lstIdsPadre);
                lstIdsPadre.add(caso.ParentId);
            }
        }
        System.debug('LIsta de ids ' + lstIdsPadre);
        for(Case cas:lstCaso){
            if(lstIdsPadre.contains(cas.Id)){
                cas.R2_CAS_TXT_Rebound_Status__c = 'Tramitar';
                casoUpdate.add(cas);
            }
        }
        System.debug('Estos son los casos a actualizar: ' + casoUpdate);
        update(casoUpdate);  
    }
    
}