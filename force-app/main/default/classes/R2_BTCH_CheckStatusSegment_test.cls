@isTest
private class R2_BTCH_CheckStatusSegment_test {
	
	@isTest 
	static void changeStatusSegTest() {
		R1_CLS_LogHelper.throw_exception = false;

		R2_Prorrateo__c pro = new R2_Prorrateo__c();
		pro.R2_PRO_PKL_Status__c = 'Prorrateo Ok';
		insert pro;

		List<R2_Segmento__c> listSeg = new List<R2_Segmento__c>();

		R2_Segmento__c seg = new R2_Segmento__c();
		seg.R2_SEG_PKL_Estado__c = 'Prorrateo OK';
		seg.R2_SEG_MSDT_Prorrate__c = pro.Id;
		//seg.R2_SEG_PKL_Segment_Status__c = 'Aceptado';
		seg.R2_SEG_TXT_Company__c = 'Iberia';
		seg.R2_SEG_DAT_Date_Msg_Sent__c = Date.today().addDays(-61);
		listSeg.add(seg);

		R2_Segmento__c seg2 = new R2_Segmento__c();
		seg2.R2_SEG_PKL_Estado__c = 'Prorrateo OK';
		seg2.R2_SEG_MSDT_Prorrate__c = pro.Id;
		seg2.R2_SEG_PKL_Segment_Status__c = 'Rechazado';
		seg2.R2_SEG_TXT_Company__c = 'Iberia';
		seg2.R2_SEG_DAT_Date_Msg_Sent__c = Date.today().addDays(-61);
		listSeg.add(seg2);

		R2_Segmento__c seg3 = new R2_Segmento__c();
		seg3.R2_SEG_PKL_Estado__c = 'Prorrateo OK';
		seg3.R2_SEG_MSDT_Prorrate__c = pro.Id;
		seg3.R2_SEG_PKL_Segment_Status__c = 'Aceptado';
		seg3.R2_SEG_TXT_Company__c = 'Iberia';
		seg3.R2_SEG_DAT_Date_Msg_Sent__c = Date.today().addDays(-10);
		listSeg.add(seg3);

		insert listSeg;

		Test.startTest();
			Database.executeBatch(new R2_BTCH_CheckStatusSegment(null));
		Test.stopTest();

		List<R2_Segmento__c> listSegUpdate = [SELECT Id,R2_SEG_PKL_Estado__c FROM R2_Segmento__c WHERE R2_SEG_PKL_Estado__c='A cobrar' ];


	//	System.assertEquals(1, listSegUpdate.size());

		//System.assertEquals(0, [SELECT count() FROM R1_Log__c]);
	}

	@isTest
	static void exception_test() {
		R1_CLS_LogHelper.throw_exception = true;
		Test.startTest();
			R2_BTCH_CheckStatusSegment btch = new R2_BTCH_CheckStatusSegment(null);
			btch.start(null);
			btch.finish(null);
			btch.execute(null, null);

		Test.stopTest();
		//System.assertEquals(0, [SELECT count() FROM R1_Log__c]);
	}
	
}