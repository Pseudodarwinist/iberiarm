@isTest
private class ExtDupGoldenIDDataControllerTest {
    static testMethod void init_Test(){
        Id recordTypeIdAcc = [SELECT Id FROM recordType WHERE Name like 'Cuenta personal' LIMIT 1].Id;
        Account acc = new Account();
        acc.RecordTypeId = recordTypeIdAcc;
        acc.LastName = 'TestAcc';
        acc.R1_ACC_TXT_Id_Golden_record__c='1234565';
        acc.PersonBirthdate = Date.newInstance(1990, 12, 31);
        insert acc;
        Test.startTest();
        PageReference pageRef = Page.ExportDuplicateAccount;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('sd',String.valueOf(System.now().Year())+'-'+String.valueOf(System.now().month())+'-'+String.valueOf(System.now().day())+'-0-0-0');
        ExtDupGoldenIDDataController e = new ExtDupGoldenIDDataController();
        Test.stopTest();
    }
}