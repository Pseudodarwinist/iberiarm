global with sharing class R2_SCH_DeletePassengers  implements Schedulable{//implements Schedulable 
	public R2_SCH_DeletePassengers() {
		
	}
	global static void execute(System.SchedulableContext sc) { //System.SchedulableContext sc
        try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}
            	Integer days = (Integer) Integer.valueOf(Label.R2_CL_DeletePassengers);
            	DateTime daysAgo = DateTime.now().addDays(-days);
            	System.debug('dias: ' + daysAgo);
               List<R2_CKI_info__c> listaPasajeros = [SELECT id FROM R2_CKI_info__c where CreatedDate <= :daysAgo];
               //String query = 'SELECT id FROM R2_CKI_info__c where CreatedDate != LAST_N_DAYS:'+ days;
               //List<R2_CKI_info__c> listaPasajeros = database.query(query);
               System.debug('Esta es la lista de pasajeros: ' + listaPasajeros);
               delete(listaPasajeros);

        }catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_SCH_DeletePassengers.execute()', '', exc.getmessage()+', '+exc.getLineNumber(), '');
        }
    }
}