/*---------------------------------------------------------------------------------------------------------------------
    Author:         Jorge Diaz-Blanco Sanjuan
    Company:        Accenture
    Description:    Clase que recupera la informacion para pintar el documento pdf PIR
    IN:             
    OUT:            

    History: 
     <Date>                     <Author>                         <Change Description>
    15/01/2019             Jorge Diaz-Blanco Sanjuan                  Initial Version
    ----------------------------------------------------------------------------------------------------------------------*/
public class R2_INC_VF_CreatePDF_PIR_CTRL {
     public R1_Incident__c Incident {get;set;}
     public List<R2_Baggage__c> Bag {get;set;}
     public List<R2_Auxiliar_Data_Incident__c> Match {get;set;}
     public String renderAs {get;set;} //Siempre tendra valor 'pdf' excepto si en la variable viene 'html'
     public Integer numMaletas{get;set;}
     
     public R2_INC_VF_CreatePDF_PIR_CTRL(){
          System.debug('R2_INC_VF_CreatePDF_PIR_CTRL | Recuperando id de la url...');
          Id id = ApexPages.currentPage().getParameters().get('id');
         renderAs = ApexPages.currentPage().getParameters().get('renderAs');
         
          if(id != null){
               System.debug('R2_INC_VF_CreatePDF_PIR_CTRL | Realizando Query sobre INCIDENT...');
               Incident = [SELECT Id, 
                         Name, 
                         RecordType.DeveloperName, 
                         R2_INC_DATH_PIR_Incidence_closed_date__c, 
                         R2_INC_FOR_PIR_AR__c,
                         R2_INC_TXT_PIR_Reference_Number__c ,
                         CreatedDate ,
                         R2_INC_TXT_PIR_Baggage_Weight_piece__c ,
                         R2_INC_PKL_MI_Incidence_type__c,
                         R2_INC_PKL_PIR_Incidence_type__c,
                         R2_INC_PKL_PIR_Status_incidence__c, 
                         R2_INC_TXT_II_Passenger_lastname1__c,
                         R2_INC_TXT_II_Passenger_lastname2__c, 
                         R2_INC_TXT_MI_Incident_External_ID__c, 
                         R2_INC_TXT_PIR_Associated_incidence_AR__c,
                         R2_INC_TXT_PIR_Damaged_content__c,
                         R2_INC_TXT_PIR_EA_External_ID__c, 
                         R2_INC_TXT_PIR_FF_Identificator__c, 
                         R2_INC_TXT_PIR_Last_name2__c, 
                         R2_INC_TXT_PIR_Last_name__c, 
                         R2_INC_TXT_PIR_Lost_content1__c, 
                         R2_INC_TXT_PIR_Lost_content2__c, 
                         R2_INC_TXT_PIR_Lost_reason__c, 
                         R2_INC_TXT_PIR_Lost_reason_causes__c, 
                         R2_INC_TXT_PIR_PIR_incident__c, 
                         R2_INC_TXT_PIR_Passenger_language__c,
                         R2_INC_TXT_PIR_Shared_code__c, 
                         R2_INC_TXT_PIR_Special_lost_reason__c, 
                         R2_INC_TXT_PIR_Validity_temporary_addres__c, 
                         R2_INC_TXT_Status_incidence__c, 
                         R2_INC_TXT_lastname1__c, 
                         R2_INC_TXT_lastname2__c, 
                         R2_LTXT_List_Id_Passenger__c, 
                         R2_INC_RUS_MI_Delay_on_Departure__c, 
                         R2_INC_DATH_Sched_depart__c, 
                         R2_INC_DATH_Sched_depart_orig__c, 
                         R2_INC_FOR_PIR__c, 
                         R2_INC_DAT_PIR_Validity_temporal_address__c,
                         R2_INC_LOO_PIR_Baggage__c,
                         R2_INC_TXT_II_Passenger_name__c,
                         R2_INC_TLF_PIR_Mobile_phone_number1__c,
                         R2_INC_TXT_PIR_Temporal_Zip_Code__c ,
                         R2_INC_TXT_PIR_Temporal_address1__c,
                         R2_INC_TLF_PIR_Temporal_phone__c,
                         R2_INC_ATXT_Reason_description__c,
                         R2_INC_TXT_PIR_Route__c,
                         R2_INC_TXT_PIR_Baggage_route__c,
                         R2_INC_ATXT_PIR_Delivery_information__c ,
                         R2_INC_DIV_PIR_Damage_cost__c,
                         R2_INC_TXT_PIR_Zip_code__c,
                         R2_INC_TXT_PIR_Ticket_number__c,
                         R2_INC_DAT_PIR_Search_start_date__c,
                         R2_INC_DAT_PIR_Date_back_primary_search__c,
                         R2_INC_TXT_PIR_Name__c,
                         R2_INC_TXT_PIR_Initial__c,
                         R2_INC_TXT_PIR_Initial2__c,
                         R2_INC_TXT_PIR_Initial3__c,
                         R2_INC_TXT_PIR_Treatment__c,
                         R2_INC_TXT_PIR_Passenger_category__c,
                         R2_INC_TXT_PIR_Responsible_Scale__c,
                         R2_INC_TXT_PIR_Delivery_local_informatio__c,
                         R2_INC_TXT_PIR_Deliver_local_informatio2__c,
                         R2_INC_TXT_PIR_Email1__c,R2_INC_TXT_PIR_Email2__c,
                         R2_INC_TXT_PIR_Permanent_address1__c,
                         R2_INC_TXT_PIR_Permanent_address2__c,
                         R2_INC_TXT_PIR_Temporal_address2__c,
                         R2_INC_TXT_PIR_Send_SMS_email__c,
                         R2_INC_TXT_PIR_flight_Number__c,
                         R2_INC_PKL_Delay_Code__c,
                         R2_INC_ATXTL_PIR_Delivery_service__c,
                         R2_INC_LOO_PIR_Match__c,
                         R2_INC_LOO_PIR_Match__r.Name,
                         R2_INC_TXT_FD__c,
                         R2_INC_DAT_PIR_Claims_Created_Date__c
                    from R1_Incident__c 
                    where Id =: id];
               System.debug('R2_INC_VF_CreatePDF_PIR_CTRL | Realizando Query sobre BAGs...');
               Bag = [SELECT id,
                              name,
                              R2_BAG_FOR_Colour_Type_Description__c,
                              R2_BAG_TXT_Description__c,
                              R2_BAG_NUM_LN_TagType__c ,
                              R2_BAG_TXT_Type__c,
                              R2_BAG_TXT_Brand_Info__c,
                              R2_BAG_TXT_Trademark__c,
                              R2_BAG_CHK_Combination_lock__c,
                              R2_BAG_TXT_Ticket_number__c,
                              R2_BAG_PKL_Colour__c,
                              R2_BAG_TXT_Aditional_damage_information__c,
                              R2_BAG_TXT_Damage_detail__c,
                              R2_BAG_TXT_StationBagReceived__c
                         from R2_Baggage__c 
                         where R2_BAG_MSDT_Incident__c = :Incident.id];
               numMaletas = Bag.Size();

               Match = [SELECT id,
                              R2_ADI_ATXTL_DY_Delivery_Info__c, 
                              R2_ADI_ATXTL_DS_Delivery_service__c,
                              R2_ADI_ATXTL_MR_Match__c,
                              R2_ADI_MSDT_Incident__c 
                    from R2_Auxiliar_Data_Incident__c 
                    WHERE R2_ADI_MSDT_Incident__c =: Incident.id];
                    System.debug(Incident);
                    System.debug(Bag);
          }else{
               System.debug('R2_INC_VF_CreatePDF_PIR_CTRL | Falta información.');
          }
     }
}