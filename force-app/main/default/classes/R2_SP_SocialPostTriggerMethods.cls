global class R2_SP_SocialPostTriggerMethods {
	

	/*---------------------------------------------------------------------------------------------------------------------
        Author:         Raúl Julián González
        Company:        Accenture
        Description:    Method that complete milestone when Iberia replys a post

        History: 
         <Date>                     <Author>                         <Change Description>
        07/07/2018             Raúl Julián González                   Initial Version
    ----------------------------------------------------------------------------------------------------------------------*/
	public static void completeMilestone(List<SocialPost> news){
		try{
			Id tinkle = [Select id FROM Group WHERE Type='Queue' and Name='Tinkle'].Id;
			List<Case> lst_cas = [SELECT id, Status, OwnerId FROM Case WHERE Id =: news[0].parentId];
			if(!lst_cas.isEmpty()){
				//List <SocialPost> lst_sp = [SELECT id, PersonaId,CreatedDate FROM SocialPost WHERE parentId=:lst_cas[0].id order by CreatedDate desc];
				if(news[0].PersonaId == null){
					List<CaseMilestone> cmsToUpdate = [SELECT Id, completionDate FROM CaseMilestone 
														WHERE caseId =: lst_cas[0].Id 
														AND (MilestoneType.Name = 'Tiempo Primera respuesta a Cliente' OR 
																MilestoneType.Name = 'Tiempo de nueva respuesta a Cliente')
														AND completionDate = null limit 1];								
			        if (!cmsToUpdate.isEmpty()){
			        	cmsToUpdate[0].completionDate = Datetime.now();	
			        	update cmsToUpdate;
					}
			    }else if(news[0].PersonaId != null && news[0].OwnerId ==tinkle){
			    	List<CaseMilestone> cmsToUpdate = [SELECT Id, completionDate FROM CaseMilestone 
														WHERE caseId =: lst_cas[0].Id 
														AND MilestoneType.Name = 'Tiempo de escalado a Cliente'
														AND completionDate = null limit 1];							
			        if (!cmsToUpdate.isEmpty()){
			        	cmsToUpdate[0].completionDate = Datetime.now();	
			        	update cmsToUpdate;
					}
			    }
			}
		}
		catch(Exception exc){
			R1_CLS_LogHelper.generateErrorLog('R2_SP_SocialPostTriggerMethods.completeMilestone()', '', exc.getmessage()+', '+exc.getLineNumber(), 'SocialPost');
		}
	}

	/*---------------------------------------------------------------------------------------------------------------------
        Author:         Raúl Julián González
        Company:        Accenture
        Description:    Delete a RRSS case that dont have socialposts

        History: 
         <Date>                     <Author>                         <Change Description>
        04/10/2018             Raúl Julián González                   Initial Version
    ----------------------------------------------------------------------------------------------------------------------*/
	public static void deleteCasewithoutPosts(List<SocialPost> news, List<SocialPost> olds){
		try{
			System.debug('deleteCasewithoutPosts');
			if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}
			Set<Id> old_ids = new Set<Id>();
			for(integer i=0; i<news.size(); i++){
				if(news[i].ParentId != olds[i].ParentId && olds[i].ParentId != null){
					old_ids.add(olds[i].ParentId);
				}
			}
			System.debug('set: ' + old_ids);
			if(!old_ids.isEmpty()){
				List<Case> lst_del = new List<Case>();
				//if(!lst_sp.isEmpty()){
					Set<Id> parents_id = new Set<Id>();
					for(SocialPost sp : [SELECT id, ParentId FROM SocialPost WHERE ParentId IN :old_ids ORDER BY ParentId]){
						parents_id.add(sp.ParentId);
					}
					for(Id old : old_ids){
						if(!parents_id.contains(old)){
							Case caso = new Case();
							caso.Id = old;
							lst_del.add(caso);
						}
					}
				//}else{
				if (parents_id.isEmpty()){
					for(id iden : old_ids){
						Case caso = new Case();
						caso.Id = iden;
						lst_del.add(caso);
					}
				}
				if(!lst_del.isEmpty()){
					delete lst_del;
				}
				
			}
		}
		catch(Exception exc){
			R1_CLS_LogHelper.generateErrorLog('R2_SP_SocialPostTriggerMethods.completeMilestone()', '', exc.getmessage()+', '+exc.getLineNumber(), 'SocialPost');
		}
	}
}