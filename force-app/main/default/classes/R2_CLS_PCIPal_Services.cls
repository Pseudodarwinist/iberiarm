global with sharing class R2_CLS_PCIPal_Services {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Ismael Yubero
    Company:        Accenture
    Description:    
    
    IN:       
    OUT:      

    History: 
    <Date>                  <Author>                <Change Description>
    25/01/2019              Ismael Yuebro           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

public String source {get; set;}
public String bearerToken{get; set;}
public String refreshToken{get; set;}

public class WS_login_response {
        public String access_token;
        public String token_type;
        public Integer expires_in;
        public String refresh_token;
        public String client_id;
        public String tenantName;
    }

public class WS_session_response{

    public String Id;
    public String LinkId;

}


public R2_CLS_PCIPal_Services(ApexPages.StandardController stdController) {

    WS_login_response response = loginPCIPal();
    bearerToken = response.access_token;
    refreshToken = response.refresh_token;
    System.debug('Access_token: ' + response.access_token);
    String sessionId = obtainSessionId(response.access_token);
    System.debug('Session id : ' + sessionId);
    source = Label.PCI_Service_url + sessionId;
}

public  static WS_login_response loginPCIPal() {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Ismael Yubero
    Company:        Accenture
    Description:    Method to login in PCI Pal system
    
    IN:       
    OUT:      

    History: 
    <Date>                     <Author>                <Change Description>
    25/01/2019                 Ismael Yubero            Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    HTTPResponse res;
    try{
        if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');} //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;    

        string wsMethod = 'PCI Pal Services';
        System.debug('Se ejecuta el metodo de PCI PAL');
        if(R1_CS_Endpoints__c.getInstance(wsMethod) == null || !R1_CS_Endpoints__c.getInstance(wsMethod).R1_CHK_Activo__c){
                return null;
                
        }
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint(R1_CS_Endpoints__c.getInstance(wsMethod).R1_TXT_EndPoint__c + 'token');
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        String body = 'client_id='+EncodingUtil.urlEncode(Label.PCIPal_ClientId,'UTF-8')+'&client_secret='+EncodingUtil.urlEncode           (Label.PCIPal_ClientSecret,'UTF-8')+'&tenantname='+EncodingUtil.urlEncode(Label.PCIPal_TenantName,'UTF-8')+'&username='          +EncodingUtil.urlEncode(Label.PCIPal_Username,'UTF-8')+'&grant_type=client_credentials';         
        
        req.setbody(body);

        req.setTimeout(35000);
        
        
        
        system.debug('!!!req: ' + req);
        system.debug('body:' + body);
        
        Http http = new Http();
        

            res = http.send(req);
            WS_login_response responseToken = new WS_login_response();
            responseToken = (WS_login_response)JSON.deserialize(res.getBody(),WS_login_response.class);
            System.debug('!!!res.getBody(): ' + res.getBody());
            System.debug('responseToken.access_token: ' + responseToken.access_token);
            System.debug('responseToken.refresh_token: ' +responseToken.refresh_token);
            // obtainSessionId(responseToken.access_token);
            return responseToken;
        
        

    }catch(Exception exc){

        R1_CLS_LogHelper.generateErrorLog('R2_CLS_PCIPal_Services.loginPCIPal()', '', exc.getmessage()+', '+exc.getLineNumber(), 'PCI Pal Services');
        return null;
    }
}

public static String obtainSessionId(String accessToken) {
        HTTPResponse res;
        try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');} //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;    

        String wsMethod = 'PCI Pal SessionId';
        if(R1_CS_Endpoints__c.getInstance(wsMethod) == null || !R1_CS_Endpoints__c.getInstance(wsMethod).R1_CHK_Activo__c){
                return null;                
        }
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint(R1_CS_Endpoints__c.getInstance(wsMethod).R1_TXT_EndPoint__c);
        req.setMethod('POST');
        req.setHeader('Authorization', 'Bearer ' +  accessToken);
        req.setHeader('Accept', 'application/json');
        req.setHeader('Content-Type', 'application/json');
        
        String body = '{"flowID":"'+Label.PCIPal_FlowId +'"}'; 
        req.setbody(body);
        req.setTimeout(35000);
        
        system.debug('!!!req: ' + req);
        system.debug('Authorization: ' + req.getHeader('Authorization'));
        system.debug('body: ' + body);
        
        Http http = new Http();
        

            System.debug('Va a lanzar el send');
            res = http.send(req);
            WS_session_response responseToken = new WS_session_response();
            responseToken = (WS_session_response)JSON.deserialize(res.getBody(),WS_session_response.class);
            System.debug('!!!res.getBody(): ' + res.getBody());
            System.debug('responseToken: ' + responseToken.Id);
            return responseToken.Id;
            // System.debug('responseToken.access_token: ' + responseToken.access_token);
            // obtainSessionId(responseToken.access_token);
       
        // return null;

        }catch(Exception exc){

            R1_CLS_LogHelper.generateErrorLog('R2_CLS_PCIPal_Services.obtainSessionId()', '', exc.getmessage()+', '+exc.getLineNumber(), 'PCI Pal Services');
            return null;
        }
    }
}