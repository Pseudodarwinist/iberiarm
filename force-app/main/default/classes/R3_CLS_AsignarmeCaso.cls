Global Class R3_CLS_AsignarmeCaso {    

    webservice static void asignarmeCaso(Id caseId)
    {       
        Case caso = [SELECT Id, ownerId,Status FROM Case WHERE Id =: caseId];
        caso.ownerId = UserInfo.getUserId();
        caso.Status = 'Trabajando';
        update caso;

    }
}