/*-------------------------------------------------------------------------------------------------------------------------------------------------------
	Author:         Jaime Ascanta
	Company:        Accenture
	Description:      crea procesos en cola por cada caso

	IN:
	OUT:

	History:
	 <Date>                     <Author>                <Change Description>
	08/11/2017                  Jaime Ascanta            Initial Version
	--------------------------------------------------------------------------------------------------------------------------------------------------------*/
public class R2_QUE_Retrive_Compensation implements Queueable  , Database.AllowsCallouts{

	private List<Case> casList;

	public R2_QUE_Retrive_Compensation(List<Case> casList){
		this.casList = casList;
		System.debug('*** constructor Queueable casList: ' + this.casList);
	}

	public void execute(QueueableContext context) {
		try{

			if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}

				if (!this.casList.isEmpty()){

					System.debug('case: '+ this.casList[0]);

					// llamamos a la integracion, si la compensacion ha sido insertada correctamente nos devuelve una lista con las compensaciones agregadas
					List<R2_Compensation__c> compInsert = R2_CLS_Retrieve_Compensation.caller_retrieveCompensation(this.casList[0]);
					System.debug('*** compensaciones agregadas: ' + compInsert);

					// eliminamos caso procesado
					this.casList.remove(0);

					// si quedan casos en la lista, volvemos a lanzarlo.
					if(!this.casList.isEmpty()) {
						ID jobID = System.enqueueJob(new R2_QUE_Retrive_Compensation(this.casList));
						System.debug('*** Next jobID: ' + jobID);
					}
				}

		}catch(Exception exc){
			R1_CLS_LogHelper.generateErrorLog('R2_QUE_Retrive_Compensation.execute()', '', exc.getmessage()+', '+exc.getLineNumber(), '');
		}
	}
}