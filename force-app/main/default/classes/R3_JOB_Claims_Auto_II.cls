/*---------------------------------------------------------------------------------------------------------------------
    Author:         ICA-LCS
    Company:        LeadClic
    Description:    Job schedule for Claims Automation II

    History:
     <Date>                     <Author>                         <Change Description>
    02/12/2019             		ICA                  				Initial Version
 ----------------------------------------------------------------------------------------------------------------------*/
/**
* @author TCK&LCS
* @date 02/12/2019
* @description Job schedule for Claims Automation II
*/
global with sharing class R3_JOB_Claims_Auto_II implements Schedulable{
    /**
    * @author TCK&LCS
    * @date 02/12/2019 
    * @description Execute method from Schedulable class. Start the batch job for claims auto II
    * @param SchedulableContext SchedulableContext for the execution.
    */	
    global void execute(SchedulableContext sc) {
		R3_BTCH_ClaimsAutomation ClaimsAutoBatch = new R3_BTCH_ClaimsAutomation();
        Database.executeBatch(ClaimsAutoBatch,100);
	}
}