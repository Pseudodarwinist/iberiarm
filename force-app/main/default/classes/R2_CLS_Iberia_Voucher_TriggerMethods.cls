//public with sharing class R2_CLS_Iberia_Voucher_TriggerMethods {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Alvaro Garcia Tapia
    Company:        Accenture
    Description:    Methods for the triggers of Voucher
    IN:         

    OUT:           
    
    History:
    
    <Date>              <Author>                 <Description>
    14/03/2018          Alvaro Garcia Tapia      Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/   
global with sharing class R2_CLS_Iberia_Voucher_TriggerMethods{
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Alvaro Garcia Tapia
    Company:        Accenture
    Description:    Insert vouchers in gestor de bonos
    IN:         

    OUT:           
    
    History:
    
    <Date>              <Author>                 <Description>
    14/03/2018          Alvaro Garcia Tapia      Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/          
    @future (callout=true)
    webservice static void callInsertarBono(String jSONBonos){
        try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');} //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;
            String codigo = insertarBonoIbcom(jSONBonos, true);
            System.debug('!!!codigo: ' + codigo);
            //si el recordType es compensacion en vuelo y se tiene en codigo comercial del bono, se actualiza el valor en el caso padre
            Id rtVoucherCompVuelo = Schema.SObjectType.R2_Voucher__c.getRecordTypeInfosByName().get('Compensacion en vuelo').getRecordTypeId();
            List<R2_Voucher__c> listVoucher = (List<R2_Voucher__c>)JSON.deserialize(jSONBonos, List<R2_Voucher__c>.class);
            List<Case> casoHijo = [SELECT ParentId  FROM Case WHERE Id = :listVoucher[0].R2_VOU_LOO_Case__c LIMIT 1];
            System.debug('!!!casoHijo: ' + casoHijo);
            List<Case> casoPadre = new List<Case>();
            List<Case> casoUpdate_Lst = new List<Case>();
            if(!casoHijo.isEmpty()){
                casoPadre = [SELECT Id, R2_CAS_TXT_voucher_flight_comp__c, RecordType.Name FROM Case WHERE Id = :casoHijo[0].ParentId LIMIT 1];
            }
            if (codigo != '' && codigo != 'KO, Bono no emitido por Gestos de Bonos' && codigo != 'KO, error en la conexión con Gestor de Bonos') {
                casoHijo[0].Status = 'Cerrado';
                casoUpdate_Lst.add(casoHijo[0]);
                System.debug('!!!casoPadre 0 : ' + casoPadre);
                if (!casoPadre.isEmpty() && listVoucher[0].RecordTypeId == rtVoucherCompVuelo) {
                    casoPadre[0].R2_CAS_TXT_voucher_flight_comp__c = codigo;
                    System.debug('!!!casoPadre: ' + casoPadre);
                    casoUpdate_Lst.add(casoPadre[0]);
                    Datetime fechaHora = DateTime.now().addMinutes(1);
                //System.schedule('ReExecuteFlightCompensations_JOB' + DateTime.now(), R1_CLS_Utilities.generateStrProg(fechaHora), new R2_JOB_ReExecuteFlightCompensation(casoPadre[0]));
                    
                }
            }else{
                //LLAMAR JOB REEXECUTE FLIGHT COMPENSATION
                // casoPadre[0].Id
                Datetime fechaHora = DateTime.now().addMinutes(2);
                //System.schedule('ReExecuteFlightCompensations_JOB' + DateTime.now(), R1_CLS_Utilities.generateStrProg(fechaHora), new R2_JOB_ReExecuteFlightCompensation(casoPadre[0]));
            }

            if(!casoUpdate_Lst.isEmpty()) {
                update casoUpdate_Lst;
            }
                  
        }catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_CLS_Iberia_Voucher_TriggerMethods.callInsertarBono()', '', exc.getmessage()+', '+exc.getLineNumber(), 'R2_Voucher__c');
         }
    }
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Alvaro Garcia Tapia
    Company:        Accenture
    Description:    Insert vouchers in gestor de bonos
    IN:         

    OUT:           
    
    History:
    
    <Date>              <Author>                 <Description>
    14/03/2018          Alvaro Garcia Tapia      Initial version
    20/03/2018          Jaime Ascanta            Commented DML operation in catch
--------------------------------------------------------------------------------------------------------------------------------------------------------*/          
    //@future (callout=true)
    public static String insertarBonoIbcom(String jSONBonos, Boolean checkTrigger){
        String codigo = '';
        try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');} //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;
            
            List<R2_Voucher__c> listVoucher = (List<R2_Voucher__c>)JSON.deserialize(jSONBonos, List<R2_Voucher__c>.class); 
            Id rtVoucherIberia = Schema.SObjectType.R2_Voucher__c.getRecordTypeInfosByName().get('BonoIberia.com').getRecordTypeId();
            Id rtVoucherCompVuelo = Schema.SObjectType.R2_Voucher__c.getRecordTypeInfosByName().get('Compensacion en vuelo').getRecordTypeId();
            
            if(listVoucher[0].RecordTypeId == rtVoucherIberia || listVoucher[0].RecordTypeId == rtVoucherCompVuelo){
                
                R2_CLS_BonosIberiacom.WsResponseBono wsResponseBono;// = new R2_CLS_BonosIberiacom.WsResponseBono();
                //Parametros de entrada
                String codigoCampanhaComercial = listVoucher[0].R2_VOU_TXT_Campaign_Commercial_Code__c;
                String maxUsoxBono = '0';
                if (listVoucher[0].R2_VOU_NUM_Maximum_Bonus_Consumption__c!=null){
                    maxUsoxBono = String.valueof(Integer.valueOf(listVoucher[0].R2_VOU_NUM_Maximum_Bonus_Consumption__c));
                }
                String tipoDescuento = listVoucher[0].R2_VOU_PKL_Discount_Type__c;
                //if (listVoucher[0].R2_VOU_PKL_Discount_Type__c.startsWith('0')){
                //  tipoDescuento = '0';
                //}else{
                //  tipoDescuento = '1';
                //}
                /*
                if (listVoucher[0].R2_VOU_PKL_Discount_Type__c!=null){
                    tipoDescuento = String.valueof(Integer.valueOf(listVoucher[0].R2_VOU_PKL_Discount_Type__c));
                }
                */
                String valorDescuento = '0';
                if (listVoucher[0].R2_VOU_FOR_bono_amount_iberiacom__c != null){
                    valorDescuento = String.valueof(Integer.valueOf(listVoucher[0].R2_VOU_FOR_bono_amount_iberiacom__c));
                }
                String moneda = listVoucher[0].R2_VOU_PKL_Coin__c;
                String startDateTime='';
                if (listVoucher[0].R2_VOU_DATH_Effective_Date_From__c != null){
                    startDateTime = String.valueOf(listVoucher[0].R2_VOU_DATH_Effective_Date_From__c);
                }
                String endDateTime='';
                if (listVoucher[0].R2_VOU_DATH_Effective_Date_To__c != null){
                    endDateTime = String.valueOf(listVoucher[0].R2_VOU_DATH_Effective_Date_To__c);
                }

                //Llamada al servicio
                wsResponseBono = wsInsertarBonoIbcom(codigoCampanhaComercial,maxUsoxBono,tipoDescuento,valorDescuento,moneda,startDateTime, endDateTime,0);
                List<R2_Voucher__c> listadoBonos;
                if (checkTrigger){
                    listadoBonos = [SELECT Id, R2_VOU_TXT_Id_Bono__c, R2_VOU_TXT_Campaign_Commercial_Code__c, R2_VOU_TXT_Commercial_Code__c, R2_VOU_NUM_Discount_Value__c, 
                                        R2_VOU_TXT_Campaign_Id__c, R2_VOU_CHK_Allow_Unselected_Operators__c, R2_VOU_CHK_Redeem_Reservation__c, R2_VOU_CHK_User_logged__c, 
                                        R2_VOU_CHK_Sunday_Rule__c, R2_VOU_NUM_Number_Uses_Bonus__c, R2_VOU_NUM_Consumed_Amount__c, R2_VOU_NUM_Maximum_Bonus_Consumption__c, 
                                        R2_VOU_DATH_Effective_Date_From__c, R2_VOU_DATH_Effective_Date_To__c, R2_VOU_TXT_Days_Return__c, R2_VOU_TXT_Days_Departure__c, 
                                        R2_VOU_PKL_Status__c, R2_VOU_PKL_Discount_Type__c, R2_VOU_LOO_Case__c, RecordTypeId, R2_VOU_PKL_Service__c 
                                        FROM R2_Voucher__c WHERE Id =:listVoucher[0].Id];
                }
                else {
                    listadoBonos = new List<R2_Voucher__c>();
                    R2_Voucher__c voucher = new R2_Voucher__c();
                    listadoBonos.add(voucher);
                }
                //List<Case> casoHijo = [SELECT ParentId  FROM Case WHERE Id = :listVoucher[0].R2_VOU_LOO_Case__c LIMIT 1];
                //List<Case> casoPadre = new List<Case>();
                //if(!casoHijo.isEmpty()){
                //    casoPadre = [SELECT Id, R2_CAS_TXT_voucher_flight_comp__c, RecordType.Name FROM Case WHERE Id = :casoHijo[0].ParentId LIMIT 1];
                //}

                if (wsResponseBono != null) {
                    if (wsResponseBono.bonus != null){
                        System.debug(LoggingLevel.Info, 'insertarBonoIbcom : Se ha generado el bono con ID: '+ wsResponseBono.bonus.id);
                        
                        //List<R2_Voucher__c> listadoBonos = [Select id, R2_VOU_TXT_Id_Bono__c, R2_VOU_TXT_Campaign_Commercial_Code__c from R2_Voucher__c where id =:listVoucher[0].id];
                        codigo = wsResponseBono.bonus.commercialCode;
                        //carga de datos respuesta del WS
                        listadoBonos[0].R2_VOU_TXT_Id_Bono__c = wsResponseBono.bonus.id;
                        listadoBonos[0].R2_VOU_TXT_Campaign_Commercial_Code__c = wsResponseBono.bonus.campaignCommercialCode;
                        listadoBonos[0].R2_VOU_TXT_Commercial_Code__c = wsResponseBono.bonus.commercialCode;
                        //listadoBonos[0].R2_VOU_NUM_Discount_Value__c = decimal.valueOf(wsResponseBono.bonus.discountValue);
                        listadoBonos[0].R2_VOU_TXT_Campaign_Id__c = wsResponseBono.bonus.campaignID;
                        listadoBonos[0].R2_VOU_CHK_Allow_Unselected_Operators__c = boolean.valueOf(wsResponseBono.bonus.allowUnselectedOperators);
                        listadoBonos[0].R2_VOU_CHK_Redeem_Reservation__c = boolean.valueOf(wsResponseBono.bonus.reservationRedeemabilityIndicator);
                        listadoBonos[0].R2_VOU_CHK_User_logged__c = boolean.valueOf(wsResponseBono.bonus.logInUserRequiredIndicator);
                        listadoBonos[0].R2_VOU_CHK_Sunday_Rule__c = boolean.valueOf(wsResponseBono.bonus.sundayRuleApplicabilityIndicator);
                        listadoBonos[0].R2_VOU_NUM_Number_Uses_Bonus__c = decimal.valueOf(wsResponseBono.bonus.bonusUsageCount);
                        listadoBonos[0].R2_VOU_NUM_Consumed_Amount__c = decimal.valueOf(wsResponseBono.bonus.amountconsumed);
                        listadoBonos[0].R2_VOU_NUM_Maximum_Bonus_Consumption__c = decimal.valueOf(wsResponseBono.bonus.maximumPermittedUse);
                        if (wsResponseBono.bonus.validity != null){
                            listadoBonos[0].R2_VOU_DATH_Effective_Date_From__c = Datetime.valueOf(wsResponseBono.bonus.validity.startDateTime);
                            listadoBonos[0].R2_VOU_DATH_Effective_Date_To__c = Datetime.valueOf(wsResponseBono.bonus.validity.endDateTime);
                        }
                        
                        if (wsResponseBono.bonus.returnDays != null){
                            listadoBonos[0].R2_VOU_TXT_Days_Return__c='';
                            for (String dia:wsResponseBono.bonus.returnDays.dayCode){
                                listadoBonos[0].R2_VOU_TXT_Days_Return__c = listadoBonos[0].R2_VOU_TXT_Days_Return__c + dia + ';';
                            }
                            listadoBonos[0].R2_VOU_TXT_Days_Return__c.removeEnd(';');
                        }
                        if (wsResponseBono.bonus.departureDays != null){
                            listadoBonos[0].R2_VOU_TXT_Days_Departure__c='';
                            for (String dia:wsResponseBono.bonus.departureDays.dayCode){
                                listadoBonos[0].R2_VOU_TXT_Days_Departure__c = listadoBonos[0].R2_VOU_TXT_Days_Departure__c + dia + ';';
                            }
                            listadoBonos[0].R2_VOU_TXT_Days_Departure__c.removeEnd(';');
                        }               
                        listadoBonos[0].R2_VOU_PKL_Status__c = 'Emitido';
                    }else{
                        listadoBonos[0].R2_VOU_PKL_Status__c = 'Rechazado';
                        codigo = 'KO, Bono no emitido por Gestos de Bonos';
                    }
                    //si es compensacion en vuelo y se ha llamado desde la integracion automatica
                    System.debug('!!!listVoucher[0].RecordTypeId: '+ listVoucher[0].RecordTypeId);
                    System.debug('!!!rtVoucherCompVuelo: '+ rtVoucherCompVuelo);
                    System.debug('!!!listVoucher[0].R2_VOU_PKL_Service__c : '+ listVoucher[0].R2_VOU_PKL_Service__c );
                    //this part is running when comes from Inflight compensation in the automatic way
                    if (listVoucher[0].RecordTypeId == rtVoucherCompVuelo && listVoucher[0].R2_VOU_PKL_Service__c == '1') {
                        System.debug('!!!entra');
                        //listadoBonos[0].R2_VOU_TXT_Campaign_Commercial_Code__c = listVoucher[0].R2_VOU_TXT_Campaign_Commercial_Code__c;
                        listadoBonos[0].R2_VOU_PKL_Discount_Type__c = listVoucher[0].R2_VOU_PKL_Discount_Type__c;
                        listadoBonos[0].R2_VOU_LOO_Case__c = listVoucher[0].R2_VOU_LOO_Case__c;
                        listadoBonos[0].RecordTypeId = listVoucher[0].RecordTypeId;
                        listadoBonos[0].R2_VOU_PKL_Service__c = '1';
                        insert listadoBonos[0];
                    }
                    //when running from trigger
                    else {
                        update listadoBonos[0];
                    }
                    //si el recordType es compensacion en vuelo y se tiene en codigo comercial del bono, se actualiza el valor en el caso padre
                    //if (!casoPadre.isEmpty() && (listadoBonos[0].R2_VOU_TXT_Commercial_Code__c != null || listadoBonos[0].R2_VOU_TXT_Commercial_Code__c != '') && listadoBonos[0].RecordTypeId == rtVoucherCompVuelo) {
                    //    casoPadre[0].Parent.R2_CAS_TXT_voucher_flight_comp__c = codigo;
                    //    update casoPadre;
                    //}
                }
                else {
                    codigo = 'KO, error en la conexión con Gestor de Bonos';
                }  
            }      
        }catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_CLS_Iberia_Voucher_TriggerMethods.insertarBonoIbcom()', '', exc.getmessage()+', '+exc.getLineNumber(), 'R2_Voucher__c');
            // List<R2_Voucher__c> listVoucher = (List<R2_Voucher__c>)JSON.deserialize(jSONBonos, List<R2_Voucher__c>.class);
            // List<R2_Voucher__c> listadoBonos = [Select id, R2_VOU_TXT_Id_Bono__c, R2_VOU_TXT_Campaign_Commercial_Code__c from R2_Voucher__c where id =:listVoucher[0].id];
            // listVoucher[0].R2_VOU_PKL_Status__c = 'Rechazado';
            // update listadoBonos[0];
         }
         return codigo;
    }
    
    
    public static R2_CLS_BonosIberiacom.WsResponseBono wsInsertarBonoIbcom (String codigoComercial, String maxUsoxBono, String tipoDescuento, String valorDescuento, String moneda, String fechaDesde, String fechaHasta, Integer intentos){
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
        System.debug(LoggingLevel.Info, 'wsInsertarBonoIbcom');
        try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');} //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;    
            
            string wsMethod = 'R2_IberiaComBonos'; //R2_VoucherManagement

            if(R1_CS_Endpoints__c.getInstance(wsMethod) == null || !R1_CS_Endpoints__c.getInstance(wsMethod).R1_CHK_Activo__c){
                    return null;
            }
            //R1_CLS_SendCustomerMDM.login();
            String token = R1_CLS_Utilities.getCache('local.sessionCache.token');
            
            if(token ==null){
                System.debug(LoggingLevel.Info, 'wsInsertarBonoIbcom : token null');
                if(intentos<3){
                    System.debug(LoggingLevel.Info, 'wsInsertarBonoIbcom : intento '+intentos);
                    intentos= intentos+1;
                    R1_CLS_SendCustomerMDM.login();
                    return wsInsertarBonoIbcom(codigoComercial, maxUsoxBono, tipoDescuento, valorDescuento, moneda, fechaDesde, fechaHasta, intentos);
                }else{
                    return null;
                }
            }
            
            System.debug('Ha funcionado el login: '+ token);
            System.debug(LoggingLevel.Info, 'Ha funcionado el login: '+ token);
            
            R2_CLS_BonosIberiacom.wpRequestAltaBono wpParamEntrada = new R2_CLS_BonosIberiacom.wpRequestAltaBono();
            
            R2_CLS_BonosIberiacom.validez validezAux = new R2_CLS_BonosIberiacom.validez();
            Boolean existeValidez = false;
            if (fechaDesde != null && fechaDesde != '') {
                validezAux.startDateTime = fechaDesde;
                existeValidez = true;
            }
            if (fechaHasta != null && fechaHasta != '') {
                validezAux.endDateTime = fechaHasta;
                existeValidez = true;
            }
            if (validezAux != null && existeValidez) {
                wpParamEntrada.validity = validezAux;
            }
            if (codigoComercial != null && codigoComercial != '') {
                wpParamEntrada.campaignCommercialCode = codigoComercial;
            }
            if (maxUsoxBono != null && maxUsoxBono != '') {
                wpParamEntrada.maximumPermittedUse = maxUsoxBono;
            }
            if (valorDescuento != null && valorDescuento != '') {
                wpParamEntrada.discountValue = valorDescuento;
            }
            if (tipoDescuento != null && tipoDescuento != '') {
                wpParamEntrada.discountType = tipoDescuento;
            }
            if (moneda != null && moneda != '') {
                wpParamEntrada.currencyCode = moneda;
            }
            
            String body = JSON.serialize(wpParamEntrada);
            System.debug(LoggingLevel.Info, 'wsInsertarBonoIbcom : body del serialize '+body);
            
            HttpRequest req = new HttpRequest();
            String endPoint = R1_CS_Endpoints__c.getInstance(wsMethod).R1_TXT_EndPoint__c;//api/pvm-vcm/rs/v1/vouchers/
            System.debug('EndPoint: ' +  endPoint);
            System.debug(LoggingLevel.Info, 'EndPoint: ' +  endPoint);
            req.setHeader('Authorization', 'Bearer ' + token); 
            req.setEndpoint(endPoint);
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/json');
            TimeOut_Integrations__c timeOut = TimeOut_Integrations__c.getInstance('BonoIberiacom');
            req.setTimeout((Integer)timeOut.setTimeOut__c);
            req.setBody(body);//carga parametros de entrada serializados
            
            Http http = new Http();
            HTTPResponse res;
            res = http.send(req);
            System.debug('Body: '+ res.getBody());
            System.debug('Status: '+ res.getStatusCode());
            if(res.getStatusCode()==200 || res.getStatusCode()==201 || res.getStatusCode()==202 || res.getStatusCode()==203 || res.getStatusCode() == 0){
                R2_CLS_BonosIberiacom.WsResponseBono resp = (R2_CLS_BonosIberiacom.WsResponseBono)JSON.deserialize(res.getBody(),R2_CLS_BonosIberiacom.WsResponseBono.class);
                if(resp == null){
                    return null;
                }
                return resp;
            }else{
                if(intentos<3 && res.getStatusCode() == 401){
                    intentos+=1;
                        R1_CLS_SendCustomerMDM.login();
                        return wsInsertarBonoIbcom(codigoComercial, maxUsoxBono, tipoDescuento, valorDescuento, moneda, fechaDesde, fechaHasta, intentos);
                }else{
                    return null;
                }
            }

        }catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_CLS_Iberia_Voucher_TriggerMethod.wsInsertarBonoIbcom()', '', exc.getmessage()+', '+exc.getLineNumber(), 'Flight');
            return null;
        }
    }
    
    
    
    
    


    
    //Método para enviar mail una vez insertado el bono
    //public static void enviarEmail(List<R2_Voucher__c> listVoucher) {
    //  try{
    //      //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;
    //      if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}
    //                      // Se crea un email para cada destinatario
    //      Id rtVoucherIberia = Schema.SObjectType.R2_Voucher__c.getRecordTypeInfosByName().get('BonoIberia.com').getRecordTypeId();
    //      //List<R2_Voucher__c> bonos = trigger.new;
    //      //R2_Voucher__c bono = bonos[0];
            
    //        //List<R2_Voucher__c> lst_vouc = [SELECT Id, R2_VOU_CHK_Email_Sent__c FROM R2_Voucher__c WHERE Id = : listVoucher[0].Id ];
    //      if(listVoucher[0].RecordTypeId == rtVoucherIberia){
    //             if ((listVoucher[0].R2_VOU_PKL_Status__c == 'Emitido') && (listVoucher[0].R2_VOU_CHK_Email_Sent__c == false)){ //Comprueba que el estado sea emitido y que el boolean de email sea falso
    //            /*Messaging.SingleEmailMessage[] mailsToSend = new Messaging.SingleEmailMessage[1];
    //            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
              
    //            String[] toAddresses = new String[] {correo};
    //            Añadimos el destinatario al email
    //            mail.setToAddresses(toAddresses);
    
                
    //            mail.setSubject('Se ha emitido un ebono');
    //            mail.setPlainTextBody('');
                
    
                    
    //            //mail.setSenderDisplayName('Salesforce Support');
    //            //mail.setBccSender(false);
    //            mail.setUseSignature(false);
    //            mailsToSend[0]=mail;*/
               

    //                 system.debug('envio el correo ');
    //                 lst_vouc[0].R2_VOU_CHK_Email_Sent__c = true;
    //                 lst_vouc[0].R2_VOU_CHK_Workflow__c = true;
    //                 update lst_vouc;
    //             }
     //       }
    //  }catch(Exception exc){

    //      R1_CLS_LogHelper.generateErrorLog('R1_CLS_Utilities.enviarEmail()', '', exc.getmessage()+', '+exc.getLineNumber(), '');
    //  }
    //} 
    
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Alvaro Garcia Tapia
    Company:        Accenture
    Description:    insert the value of the voucher in the parent case
    IN:         

    OUT:           
    
    History:
    
    <Date>              <Author>                 <Description>
    27/06/2018          Alvaro Garcia Tapia      Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/          
    public static void updateCase(List<R2_Voucher__c> news, List<R2_Voucher__c> olds){
        try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');} //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;
            
            if (news[0].R2_VOU_LOO_Case__c != null && news[0].R2_VOU_TXT_Voucher__c != olds[0].R2_VOU_TXT_Voucher__c && news[0].R2_VOU_TXT_Voucher__c != null && news[0].R2_VOU_TXT_Voucher__c != '') {
                Id proactId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Proactivo').getRecordTypeId();
                Id rtIdExp = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Expediente').getRecordTypeId();
                List<Case> currentCase = [SELECT Id, RecordTypeId, ParentId, Parent.RecordTypeId, Parent.R2_CAS_TXT_voucher_flight_comp__c FROM Case WHERE Id = :news[0].R2_VOU_LOO_Case__c];
                if (currentCase[0].RecordTypeId == proactId && currentCase[0].Parent.RecordTypeId == rtIdExp 
                    && (currentCase[0].Parent.R2_CAS_TXT_voucher_flight_comp__c == null || currentCase[0].Parent.R2_CAS_TXT_voucher_flight_comp__c == '')) {
                    List<Group> groupLst = [SELECT id, Name from Group where Type = 'Queue' AND Name = 'Cerrados'];
                    if (!groupLst.isEmpty()) {
                        Case parent = new Case();
                        parent.Id = currentCase[0].ParentId;
                        parent.R2_CAS_TXT_voucher_flight_comp__c = news[0].R2_VOU_TXT_Voucher__c;
                        parent.OwnerId = groupLst[0].Id; 
                        parent.Status = 'Cerrado';
                        update parent;
                    }
                }
            }
        }catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_CLS_Iberia_Voucher_TriggerMethods.updateCase()', '', exc.getmessage()+', '+exc.getLineNumber(), 'R2_Voucher__c');
         }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Alvaro Garcia Tapia
    Company:        Accenture
    Description:    fill the fields of the voucher through the name of the commercial code and the information in the custom setting
    IN:         

    OUT:           
    
    History:
    
    <Date>              <Author>                 <Description>
    18/10/2018          Eva Sanchez Ruiz      Initial version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/          
    public static void fillVoucherIbCom(List<R2_Voucher__c> news){
        try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');} //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;
            
            List<R2_Voucher__c> toUpdate = new List<R2_Voucher__c>();
            Id rtVoucherIberia = Schema.SObjectType.R2_Voucher__c.getRecordTypeInfosByName().get('BonoIberia.com').getRecordTypeId();
            for (R2_Voucher__c vou : news){
                if (vou.RecordTypeId == rtVoucherIberia && vou.R2_VOU_TXT_Campaign_Commercial_Code__c != null && vou.R2_VOU_TXT_Campaign_Commercial_Code__c != '') {
                    toUpdate.add(vou);
                }
            }
            if(!toUpdate.isEmpty()){
                for (R2_Voucher__c vouAux : toUpdate){
                    R2_CS_GestorBonosCampaign__c bono = R2_CS_GestorBonosCampaign__c.getInstance(vouAux.R2_VOU_TXT_Campaign_Commercial_Code__c.toUpperCase());
                    if (bono != null){
                        vouAux.R2_VOU_NUM_Discount_Value__c = bono.R2_NUM_DiscountValue__c;
                        vouAux.R2_VOU_PKL_Coin__c = bono.R2_TXT_Currency__c;
                        vouAux.R2_VOU_NUM_Maximum_Bonus_Consumption__c = 1;
                        vouAux.R2_VOU_PKL_Discount_Type__c = '2';
                        vouAux.R2_VOU_DATH_Effective_Date_From__c = System.now();
                        vouAux.R2_VOU_DATH_Effective_Date_To__c = System.now().addYears(1);
                    }
                    else {
                        vouAux.addError('Ese bono no existe');
                    }
                }
            }
        }catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_CLS_Iberia_Voucher_TriggerMethods.fillVoucherIbCom()', '', exc.getmessage()+', '+exc.getLineNumber(), 'R2_Voucher__c');
         }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
        Author:         Raúl Julián 
        Company:        Accenture
        Description:    fill expediente field with sum vouchers value of the child.
        IN:         

        OUT:           
        
        History:
        
        <Date>              <Author>                 <Description>
        19/02/2019          Raúl Julián                 Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/          
    public static void fillsumvouchers(List<R2_Voucher__c> news,List<R2_Voucher__c> olds){
        try{
            System.debug('fillsumvouchers init');
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');} //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;
            Id rtVoucherIberia = Schema.SObjectType.R2_Voucher__c.getRecordTypeInfosByName().get('BonoIberia.com').getRecordTypeId();
            Map<R2_Voucher__c,Id> vou_case = new Map<R2_Voucher__c,Id>();
            Set<Id> set_case = new Set<Id>();
            
            for(Integer i=0;i<news.size();i++){
                System.debug('Estado news:' + news[i].R2_VOU_PKL_Status__c);
                System.debug('Estado olds:' + olds[i].R2_VOU_PKL_Status__c);
                if(news[i].RecordTypeId == rtVoucherIberia && news[i].R2_VOU_PKL_Status__c =='Emitido' && news[i].R2_VOU_PKL_Status__c!= olds[i].R2_VOU_PKL_Status__c && news[i].R2_VOU_CHK_Email_Sent__c){
                    vou_case.put(news[i],news[i].R2_VOU_LOO_Case__c);
                    set_case.add(news[i].R2_VOU_LOO_Case__c);
                }
            }
            if(!set_case.isEmpty()){
                System.debug('map vou_case:' + vou_case);
                Map<Id,Case> map_childs = new Map<Id,Case>([SELECT CaseNumber,ParentId FROM Case WHERE Id IN :set_case]);
                if(!map_childs.isEmpty()){
                    List<CaseComment> comentarios = new List<CaseComment>();
                    for(R2_Voucher__c vou: vou_case.keyset()){
                        CaseComment comment = new CaseComment();
                        comment.ParentId = map_childs.get(vou_case.get(vou)).ParentId;
                        comment.CommentBody = 'Emitido un bono de tipo IbCom en el hijo: ' + map_childs.get(vou_case.get(vou)).CaseNumber + ' por valor de ' + vou.R2_VOU_FOR_Total_voucher__c + ' €';
                        System.debug(comment);
                        comentarios.add(comment);
                    }
                    if(!comentarios.isEmpty()){
                        insert comentarios;
                    }
                }
            }
        }catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_CLS_Iberia_Voucher_TriggerMethods.fillsumvouchers()', '', exc.getmessage()+', '+exc.getLineNumber(), 'R2_Voucher__c');
         }
    }

}