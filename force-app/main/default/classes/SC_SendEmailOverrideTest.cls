/**
* @name     SC_SendEmailOverrideTest
* @author   Salesforce Team - Najlae    
* @since    2018                    
* @desc     Test class SC_SendEmailOverride 
* @history
*/
@isTest
public class SC_SendEmailOverrideTest {
    static testmethod void DefaultCaseFeedEmailImplementor_SimpleTest(){
        //Create test data here
        Case ca = new Case();
        insert ca;
        Exception failureDuringExecution = null;
        
        String defaultsAsJSON = '[{"targetSObject":{"attributes":{"type":"EmailMessage"},"TextBody":"",'
            + '"FromName":"Test","FromAddress":"test@example.com","HtmlBody":"<html><body></body></html>","BccAddress":"test@example.com",'
            + '"CcAddress":"","ToAddress":"test@example.com","Subject":"Testing"},'
            + '"contextId":"'+ ca.Id
            + '","actionType":"Email",'
            + '"actionName":"Case.Email","fromAddressList":["salesforce@test.com"]}]';
        List<QuickAction.SendEmailQuickActionDefaults> defaultsSettings = 
            (List<QuickAction.SendEmailQuickActionDefaults>)JSON.deserialize(defaultsAsJSON, List<QuickAction.SendEmailQuickActionDefaults>.class);
        
        Test.startTest();
        try { 
            (new SC_SendEmailOverride()).onInitDefaults(defaultsSettings); 
        }catch(Exception failure) { 
            failureDuringExecution = failure;
        }
        
        Test.stopTest();
        
        System.assertEquals(null, failureDuringExecution, 'There was an exception thrown during the test!');
    }
}