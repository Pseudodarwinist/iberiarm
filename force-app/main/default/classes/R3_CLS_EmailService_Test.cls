/*---------------------------------------------------------------------------------------------------------------------
    Author:         TCK - LeadClic
    Description:    R3_CLS_EmailService

    History:
     <Date>                     <Author>                         <Change Description>
    08/08/2019            		TCK                				Initial Version
 ----------------------------------------------------------------------------------------------------------------------*/
/**
* @author TCK&LCS
* @date 23/09/2019
* @description Test class for R3_CLS_EmailService class
*/
@isTest
public class R3_CLS_EmailService_Test {
    @testSetup
	public static void setup(){
        Id recordTypeIdAcc = [SELECT Id FROM recordType WHERE Name like 'Cuenta personal' LIMIT 1].Id;
        Account acc = new Account();
        acc.RecordTypeId = recordTypeIdAcc;
        acc.LastName = 'TestAcc';
        acc.R1_ACC_TXT_Id_Golden_record__c='1234565';
        acc.PersonBirthdate = Date.newInstance(1980, 01, 31);
        acc.R1_ACC_TXT_Primary_Frecuent_Flyer_Card__c = 'IB21639208';
        acc.R1_ACC_PKL_Card_Type__c = '2';
        acc.PersonEmail = 'test@the-cocktail.com';
        acc.R1_ACC_PKL_Comunication_Language__c = '4';
        acc.R1_ACC_CHK_Flag_Iberia_Singular__c = true;
        Account acc1 = new Account();
        acc1.RecordTypeId = recordTypeIdAcc;
        acc1.LastName = 'TestAcc1';
        acc1.R1_ACC_TXT_Id_Golden_record__c='1234565';
        acc1.PersonBirthdate = Date.newInstance(1980, 12, 31);
        acc1.R1_ACC_TXT_Primary_Frecuent_Flyer_Card__c = 'IB343434';
        acc1.R1_ACC_PKL_Card_Type__c = '1';
        acc1.R1_ACC_PKL_Comunication_Language__c = '4';
        acc1.PersonEmail = 'test@the-cocktail.co';
        List<Account> accList = new List<Account>();
        accList.add(acc);
        accList.add(acc1);
        insert accList;

        List<R3_Name_Buzones__c> colasBuzonesList = new List<R3_Name_Buzones__c>();
        R3_Name_Buzones__c colasBuzones =new R3_Name_Buzones__c();
        colasBuzones.Name='CCPO';
        colasBuzones.R3_TXT_Values__c='CCPO_Infinita , CCPO_Oro , CCPO_Platino , CCPO_InfinitaPrime, CCPO_Singular';
        R3_Name_Buzones__c colasBuzones1 =new R3_Name_Buzones__c();
        colasBuzones1.Name='Others';
        colasBuzones1.R3_TXT_Values__c='hotelesconavios , serviberia_ibplusmenores , serviberia_menoresiberia';
        R3_Name_Buzones__c colasBuzones2 =new R3_Name_Buzones__c();
        colasBuzones2.Name='ServIberia';
        colasBuzones2.R3_TXT_Values__c='serviberia_clasica , serviberia_plata , serviberia_baby';

        colasBuzonesList.add(colasBuzones);
        colasBuzonesList.add(colasBuzones1);
        colasBuzonesList.add(colasBuzones2);
        insert colasBuzonesList;

        List<R3_CS_ColasBuzones__c> buzonesList = new List<R3_CS_ColasBuzones__c>();
        R3_CS_ColasBuzones__c buz1 = new R3_CS_ColasBuzones__c();
        buz1.Name = 'CCPO_Oro';
        buz1.Api_Name__c = 'R3_CCPO_Oro';

        R3_CS_ColasBuzones__c buz2 = new R3_CS_ColasBuzones__c();
        buz2.Name = '	Serviberia_Clasica';
        buz2.Api_Name__c = 'R3_Serviberia_Clasica';
        R3_CS_ColasBuzones__c buz3 = new R3_CS_ColasBuzones__c();
        buz3.Name = 'HotelesConAvios';
        buz3.Api_Name__c = 'R3_HotelesAvios';
        R3_CS_ColasBuzones__c buz4 = new R3_CS_ColasBuzones__c();
        buz4.Name = 'CCPO_Singular';
        buz4.Api_Name__c = 'R3_CCPO_Singular';
        buzonesList.add(buz1);
        buzonesList.add(buz2);
        buzonesList.add(buz3);
        buzonesList.add(buz4);
        insert buzonesList;

		R1_Flight__c vuelo = new R1_Flight__c();
        vuelo.Name = 'TestFlight';
        vuelo.R1_FLG_TXT_Flight_number__c='1111';
        vuelo.R1_FLG_DAT_Flight_date_local__c= Date.newInstance(2019, 1, 1);
        vuelo.R1_FLG_DATH_Schedule_depart_time__c = Datetime.newInstance(2019, 1, 1);
		vuelo.R1_FLG_DATH_Onblocks_act__c = Datetime.now().addHours(4);
		vuelo.R1_FLG_DATH_Sched_arrive__c= Datetime.now();
        vuelo.R1_FLG_TXT_Carrier_code__c='IB';
        vuelo.R1_FLG_TXT_Destination__c='BCN';
        vuelo.R1_FLG_TXT_Origin__c='MAD';
        insert vuelo;

    }

    @isTest
	static void handleInboundEmail_test() {
        List<Messaging.Inboundemail.TextAttachment> listTxtAtt = new List<Messaging.Inboundemail.TextAttachment>();
		Messaging.Inboundemail.TextAttachment textAtt = new Messaging.Inboundemail.TextAttachment();
		textAtt.Body = 'Text';
		textAtt.fileName = 'test.txt';
		listTxtAtt.add(textAtt);

		List<Messaging.Inboundemail.BinaryAttachment> listBinAtt = new List<Messaging.Inboundemail.BinaryAttachment>();
		Messaging.Inboundemail.BinaryAttachment binAtt = new Messaging.Inboundemail.BinaryAttachment();
		binAtt.Body = Blob.valueof('Text');
		binAtt.fileName = 'test.pdf';
		listBinAtt.add(binAtt);
        
        List<EmailServicesAddress> buzon1List = new List<EmailServicesAddress>([SELECT DeveloperName,EmailDomainName FROM EmailServicesAddress WHERE DeveloperName = 'CCPO_Oro']);
        List<EmailServicesAddress>  buzon2List = new List<EmailServicesAddress>([SELECT DeveloperName,EmailDomainName FROM EmailServicesAddress WHERE DeveloperName = 'serviberia_clasica']);
        List<EmailServicesAddress> buzon3List = new List<EmailServicesAddress>([SELECT DeveloperName,EmailDomainName FROM EmailServicesAddress WHERE DeveloperName = 'hotelesconavios']);
        List<EmailServicesAddress> buzon4List = new List<EmailServicesAddress>([SELECT DeveloperName,EmailDomainName FROM EmailServicesAddress WHERE DeveloperName = 'CCPO_Singular']);
        Test.startTest();
        EmailServicesAddress buzon1;
        EmailServicesAddress buzon2;
        EmailServicesAddress buzon3;
        EmailServicesAddress buzon4;
        if(!buzon1List.isEmpty()){
            buzon1 = buzon1List[0];
        }
        if(!buzon2List.isEmpty()){
            buzon2 = buzon2List[0];
        }
        if(!buzon3List.isEmpty()){
            buzon3 = buzon3List[0];
        }
        if(!buzon4List.isEmpty()){
            buzon4 = buzon4List[0];
        }
        if(buzon1 != null){
            Messaging.InboundEmail email = new Messaging.InboundEmail();
            email.subject = 'Test subject 343434';
            email.fromAddress = 'test@the-cocktail.com';
            Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
            env.toAddress = buzon1.DeveloperName+'@'+buzon1.EmailDomainName;
            env.fromAddress = 'test@the-cocktail.com';
            R3_CLS_EmailService aux = new R3_CLS_EmailService();
            aux.handleInboundEmail(email, env);

            Messaging.InboundEmail email1 = new Messaging.InboundEmail();
            email1.subject = 'Test subject';
            email1.fromAddress = 'testds@the-cocktail.com';
            Messaging.InboundEnvelope env1 = new Messaging.InboundEnvelope();
            env1.toAddress = buzon1.DeveloperName+'@'+buzon1.EmailDomainName;
            env1.fromAddress = 'testx@the-cocktail.com';
            R3_CLS_EmailService aux1 = new R3_CLS_EmailService();
            aux1.handleInboundEmail(email1, env1);

            Messaging.InboundEmail email6 = new Messaging.InboundEmail();
            email6.subject = 'Test subject';
            email6.fromAddress = 'test@the-cocktail.com';
            Messaging.InboundEnvelope env6 = new Messaging.InboundEnvelope();
            env6.toAddress = buzon1.DeveloperName+'@'+buzon1.EmailDomainName;
            env6.fromAddress = 'test@the-cocktail.com';
            R3_CLS_EmailService aux6 = new R3_CLS_EmailService();
            aux6.handleInboundEmail(email6, env6);
            
        }
        if(buzon2 != null){
            Messaging.InboundEmail email2 = new Messaging.InboundEmail();
            email2.subject = 'Test subject IB343434';
            email2.fromAddress = 'test@the-cocktail.co';
            Messaging.InboundEnvelope env2 = new Messaging.InboundEnvelope();
            env2.toAddress = buzon2.DeveloperName+'@'+buzon2.EmailDomainName;
            env2.fromAddress = 'test@the-cocktail.co';
            R3_CLS_EmailService aux2 = new R3_CLS_EmailService();
            aux2.handleInboundEmail(email2, env2);
            
            Messaging.InboundEmail email7 = new Messaging.InboundEmail();
            email7.subject = 'Test subject';
            email7.fromAddress = 'test@the-cocktail.com';
            Messaging.InboundEnvelope env7 = new Messaging.InboundEnvelope();
            env7.toAddress = buzon2.DeveloperName+'@'+buzon2.EmailDomainName;
            env7.fromAddress = 'test@the-cocktail.com';
            R3_CLS_EmailService aux7 = new R3_CLS_EmailService();
            aux7.handleInboundEmail(email7, env7);
            
            Messaging.InboundEmail email8 = new Messaging.InboundEmail();
            email8.subject = 'Test subject';
            email8.fromAddress = 'test@the-cocktail.co';
            Messaging.InboundEnvelope env8 = new Messaging.InboundEnvelope();
            env8.toAddress = buzon2.DeveloperName+'@'+buzon2.EmailDomainName;
            env8.fromAddress = 'test@the-cocktail.co';
            R3_CLS_EmailService aux8 = new R3_CLS_EmailService();
            aux8.handleInboundEmail(email8, env8);
        }

        if(buzon3 != null){
            Messaging.InboundEmail email3 = new Messaging.InboundEmail();
            email3.subject = 'Test subject IB:343434';
            email3.fromAddress = 'test@the-cocktail.co';
            Messaging.InboundEnvelope env3 = new Messaging.InboundEnvelope();
            env3.toAddress = buzon3.DeveloperName+'@'+buzon3.EmailDomainName;
            env3.fromAddress = 'test@the-cocktail.com';
            R3_CLS_EmailService aux3 = new R3_CLS_EmailService();
            aux3.handleInboundEmail(email3, env3);

            Messaging.InboundEmail email4 = new Messaging.InboundEmail();
            email4.subject = 'Test subject';
            email4.fromAddress = 'notexits@the-cocktail.com';
            email4.textAttachments = listTxtAtt;
            email4.binaryAttachments = listBinAtt;
            Messaging.InboundEnvelope env4 = new Messaging.InboundEnvelope();
            env4.fromAddress = 'notexits@the-cocktail.com';
            env4.toAddress = buzon3.DeveloperName+'@'+buzon3.EmailDomainName;
            R3_CLS_EmailService aux4 = new R3_CLS_EmailService();
            aux4.handleInboundEmail(email4, env4);

            Messaging.InboundEmail email5 = new Messaging.InboundEmail();
            email5.subject = 'Test subject 21639208';
            email5.fromAddress = 'tests1@the-cocktail.co';
            Messaging.InboundEnvelope env5 = new Messaging.InboundEnvelope();
            env5.toAddress = buzon3.DeveloperName+'@'+buzon3.EmailDomainName;
            env5.fromAddress = 'notexitss@the-cocktail.com';
            R3_CLS_EmailService aux5 = new R3_CLS_EmailService();
            aux3.handleInboundEmail(email5, env5);
        }

        if(buzon4 != null){


            Messaging.InboundEmail email6 = new Messaging.InboundEmail();
            email6.subject = 'Test subject IB21639208';
            email6.fromAddress = 'test@the-cocktail.com';
            Messaging.InboundEnvelope env6 = new Messaging.InboundEnvelope();
            env6.toAddress = buzon4.DeveloperName+'@'+buzon4.EmailDomainName;
            env6.fromAddress = 'notexitss@the-cocktail.com';
            R3_CLS_EmailService aux9 = new R3_CLS_EmailService();
            aux9.handleInboundEmail(email6, env6);
            test.stopTest();
            
           
            
        }
    }

    @isTest
	static void emailExistsCase_test() {
        test.startTest();
        Messaging.InboundEmail email5 = new Messaging.InboundEmail();
        email5.subject = 'Test subject 21639208';
        email5.fromAddress = 'tests1@the-cocktail.co';
        email5.plainTextBody = 'ref:asdasd:ref';
        R3_CLS_EmailService.existsCase(email5);
        Messaging.InboundEmail email1 = new Messaging.InboundEmail();
        email1.subject = 'Test subject 21639208';
        email1.fromAddress = 'tests1@the-cocktail.co';
        R3_CLS_EmailService.existsCase(email1);
        test.stopTest();        
    }

    @isTest
	static void queueLevel_test() {
        test.startTest();
        List<Account> accountList = [SELECT id,R1_ACC_PKL_Card_Type__c FROM account];
        R3_CLS_EmailService aux = new R3_CLS_EmailService();
        String a = aux.queueLevel(accountList,'CCPO_Oro');
        test.stopTest();  
    }
}