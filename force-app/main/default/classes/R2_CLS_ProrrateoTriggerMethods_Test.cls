/*---------------------------------------------------------------------------------------------------------------------
Author:         Jaime Ascanta
Company:        Accenture
Description:
IN:
OUT:

History:
<Date>                     <Author>                         <Change Description>
19/02/2018 					jaime ascanta                  	Initial Version
----------------------------------------------------------------------------------------------------------------------*/
@isTest
private class R2_CLS_ProrrateoTriggerMethods_Test {
	/*---------------------------------------------------------------------------------------------------------------------
	Author:         Jaime Ascanta
	Company:        Accenture
	Description:
	IN:
	OUT:

	History:
	 <Date>                     <Author>                         <Change Description>
	19/02/2018       	   		Jaime Ascanta                  		 Initial Version
	24/05/2018       	   		Alberto Puerto                  	 fulfilling seg(n).R2_SEG_TXT_Company__c (required field)
	----------------------------------------------------------------------------------------------------------------------*/
	@isTest 
	static void prorrateoSendEmail_test() {

		R1_CLS_LogHelper.throw_exception = false;

		// emails de companias
		List<R2_Diccionario_Cias__c> listCias = new List<R2_Diccionario_Cias__c>();
		R2_Diccionario_Cias__c cia1 = new R2_Diccionario_Cias__c();
		cia1.R2_DIC_TXT_Codigo_Compania__c = 'test1';
		cia1.R2_DIC_TXT_Nombre_Compania__c = 'Test Email';
		cia1.R2_DIC_TXT_Correo__c = 'test1@email.com';
		listCias.add(cia1);
		R2_Diccionario_Cias__c cia2 = new R2_Diccionario_Cias__c();
		cia2.R2_DIC_TXT_Codigo_Compania__c = 'test2';
		cia2.R2_DIC_TXT_Nombre_Compania__c = 'test2 Email';
		cia2.R2_DIC_TXT_Correo__c = 'test2@email.com';
		listCias.add(cia2);
		R2_Diccionario_Cias__c cia3 = new R2_Diccionario_Cias__c();
		cia3.R2_DIC_TXT_Codigo_Compania__c = 'test3';
		cia3.R2_DIC_TXT_Nombre_Compania__c = 'test3 Email';
		cia3.R2_DIC_TXT_Correo__c = 'test3@email.com';
		listCias.add(cia3);
		insert listCias;

		// cuenta
		Account acc = new Account();
		acc.Name = 'Test';
		acc.R1_ACC_TLF_Phone_Marketing__c = '348787877';
		acc.R1_ACC_PKL_identification_Type__c = '2';
		acc.R1_ACC_TXT_Identification_number__c = '123456789';
		insert acc;

		//caso
		Id rt_expediente = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Expediente').getRecordTypeId();
		Case caso = new Case();
		caso.RecordTypeId = rt_expediente;
		caso.Status = 'Abierto';
		caso.Origin = 'Email';
		caso.Type = 'Demora';
		insert caso;

		
		// incidencia
		Id rtDelay = [select id from RecordType where SobjectType='R1_Incident__c' AND DeveloperName='DELAYED'].Id;
		R1_Incident__c inc = new R1_Incident__c ();
		inc.RecordTypeId = rtDelay;
		inc.R2_INC_DAT_PIR_Claims_Created_Date__c = Date.today();
		inc.R2_INC_TXT_PIR_Station_Code__c = 'MAD';
		inc.R2_INC_TXT_PIR_Airline_Code__c = 'IB';
		inc.R2_INC_TXT_PIR_Reference_Number__c = '1111';
		insert inc;

		R2_Baggage__c bag = new R2_Baggage__c();
		bag.R2_BAG_MSDT_Incident__c = inc.Id;
		bag.R2_BAG_TXT_Ticket_number__c = '1234567890';
		bag.R2_BAG_TXT_Aditional_damage_information__c = 'test';
		bag.R2_BAG_TXT_Damage_detail__c = 'test';
		bag.R2_BAG_TXT_Damage_detail__c = 'test';
		bag.R2_BAG_PKL_Colour__c = 'BK';
		insert bag;

		// orden de trabajo
		R2_Work_Order__c wor = new R2_Work_Order__c();
		wor.R2_WO_DAT_Date_Creation_PIR__c = Date.today();
		wor.R2_WO_DAT_Opening_date__c = Date.today();
		wor.R2_WO_LOO_Case__c = caso.Id;
		wor.R2_WO_PKL_type__c = 'amazon';
		insert wor;
		// orden de pago
		R2_Payment_order__c pay = new R2_Payment_order__c();
		pay.R2_OPY_PKL_PaymentType__c = 'CHEQUE';
		pay.R2_OPY_PCK_Status__c = 'ENVIADO A BANCO';
		pay.R2_OPY_LOO_File_associated__c = caso.Id;
		insert pay;

		// endpoints
		List<R1_CS_Endpoints__c> lst_ep = new List<R1_CS_Endpoints__c>();
		R1_CS_Endpoints__c epLogin = new R1_CS_Endpoints__c();
		epLogin.Name = 'ETL_Login';
		epLogin.R1_CHK_Activo__c = true;
		epLogin.R1_TXT_EndPoint__c = 'PruebaLogin';
		lst_ep.add(epLogin);
		R1_CS_Endpoints__c ep = new R1_CS_Endpoints__c();
		ep.Name='R2_DelayedBags';
		ep.R1_CHK_Activo__c = true;
		ep.R1_TXT_EndPoint__c = 'R2_DelayedBags';
		lst_ep.add(ep);
		insert lst_ep;

		//timeOut
		TimeOut_Integrations__c cst = new TimeOut_Integrations__c();
		cst.Name = 'WorldTracer';
		cst.setTimeOut__c = 8000;
		insert cst;
		//R2_SDRtoEUR__c
		R2_ExchangeRate__c exchange = new R2_ExchangeRate__c();
		exchange.Name = 'CurrentExchangeRate';
		exchange.R2_SDRtoEUR__c = 1.1788;
		exchange.R2_TXT_External_Id__c = 'Test';
		insert exchange;

		// prorrateos
		List<R2_Prorrateo__c> listPro = new List<R2_Prorrateo__c>();
		R2_Prorrateo__c pro1 = new R2_Prorrateo__c();
		pro1.R2_PRO_TXT_payment_id__c = String.valueOf(pay.Id);
		pro1.R2_PRO_TXT_work_order__c = String.valueOf(wor.Id);
		pro1.R2_PRO_DAT_Date__c = Date.today();
		pro1.R2_PRO_LOO_Case__c = caso.Id;
		pro1.R2_PRO_LOO_Passenger_Name__c = acc.Id;
		pro1.R2_PRO_LOO_PIR__c = inc.Id;
		pro1.R2_PRO_PKL_Status__c = 'Prorrateo OK';
		listPro.add(pro1);

		R2_Prorrateo__c pro2 = new R2_Prorrateo__c();
		pro2.R2_PRO_TXT_payment_id__c = String.valueOf(pay.Id);
		pro2.R2_PRO_TXT_work_order__c = String.valueOf(wor.Id);
		pro2.R2_PRO_DAT_Date__c = Date.today();
		pro2.R2_PRO_LOO_Case__c = caso.Id;
		pro2.R2_PRO_LOO_Passenger_Name__c = acc.Id;
		pro2.R2_PRO_LOO_PIR__c = inc.Id;
		pro2.R2_PRO_PKL_Status__c = 'Prorrateo OK';
		listPro.add(pro2);

		R2_Prorrateo__c pro3 = new R2_Prorrateo__c();
		pro3.R2_PRO_TXT_payment_id__c = String.valueOf(pay.Id);
		pro3.R2_PRO_TXT_work_order__c = String.valueOf(wor.Id);
		pro3.R2_PRO_DAT_Date__c = Date.today();
		pro3.R2_PRO_LOO_Case__c = caso.Id;
		pro3.R2_PRO_LOO_Passenger_Name__c = acc.Id;
		pro3.R2_PRO_LOO_PIR__c = inc.Id;
		pro3.R2_PRO_PKL_Status__c = 'Prorrateo OK';
		listPro.add(pro3);

		insert listPro;

		Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());

		Test.startTest();

		List<R2_Segmento__c> listSeg = new List<R2_Segmento__c>();

		// segmentos prorrateo 1 = 2 emails a enviar
		R2_Segmento__c seg1 = new R2_Segmento__c();
		seg1.R2_SEG_TXT_company_code__c = 'test1';
		seg1.R2_SEG_MSDT_Prorrate__c = pro1.Id;
		seg1.R2_SEG_CHK_guilty_company__c = true;
		seg1.R2_SEG_TXT_Company__c = 'IBERIA';
		seg1.R2_SEG_PKL_Estado__c = 'Prorrateo OK';
		listSeg.add(seg1);
		R2_Segmento__c seg2 = new R2_Segmento__c();
		seg2.R2_SEG_TXT_company_code__c = 'test1';
		seg2.R2_SEG_MSDT_Prorrate__c = pro1.Id;
		seg2.R2_SEG_CHK_guilty_company__c = true;
		seg2.R2_SEG_TXT_Company__c = 'IBERIA';
		seg2.R2_SEG_PKL_Estado__c = 'Prorrateo OK';
		listSeg.add(seg2);
		R2_Segmento__c seg3 = new R2_Segmento__c();
		seg3.R2_SEG_TXT_company_code__c = 'test2';
		seg3.R2_SEG_MSDT_Prorrate__c = pro1.Id;
		seg3.R2_SEG_CHK_guilty_company__c = true;
		seg3.R2_SEG_TXT_Company__c = 'IBERIA';
		seg3.R2_SEG_PKL_Estado__c = 'Prorrateo OK';
		listSeg.add(seg3);

		// segmentos prorrateo 2 = 2 emails a enviar
		R2_Segmento__c seg4 = new R2_Segmento__c();
		seg4.R2_SEG_TXT_company_code__c = 'test1';
		seg4.R2_SEG_MSDT_Prorrate__c = pro2.Id;
		seg4.R2_SEG_CHK_guilty_company__c = true;
		seg4.R2_SEG_TXT_Company__c = 'IBERIA';
		seg4.R2_SEG_PKL_Estado__c = 'Prorrateo OK';
		listSeg.add(seg4);
		R2_Segmento__c seg5 = new R2_Segmento__c();
		seg5.R2_SEG_TXT_company_code__c = 'test2';
		seg5.R2_SEG_MSDT_Prorrate__c = pro2.Id;
		seg5.R2_SEG_CHK_guilty_company__c = true;
		seg5.R2_SEG_TXT_Company__c = 'IBERIA';
		seg5.R2_SEG_PKL_Estado__c = 'Prorrateo OK';
		listSeg.add(seg5);

		// segmentos prorrateo 3 = 2 emails a enviar
		R2_Segmento__c seg6 = new R2_Segmento__c();
		seg6.R2_SEG_TXT_company_code__c = 'test2';
		seg6.R2_SEG_MSDT_Prorrate__c = pro3.Id;
		seg6.R2_SEG_CHK_guilty_company__c = true;
		seg6.R2_SEG_TXT_Company__c = 'IBERIA';
		seg6.R2_SEG_PKL_Estado__c = 'Prorrateo OK';
		listSeg.add(seg6);
		R2_Segmento__c seg7 = new R2_Segmento__c();
		seg7.R2_SEG_TXT_company_code__c = 'test3';
		seg7.R2_SEG_MSDT_Prorrate__c = pro3.Id;
		seg7.R2_SEG_CHK_guilty_company__c = true;
		seg7.R2_SEG_TXT_Company__c = 'IBERIA';
		seg7.R2_SEG_PKL_Estado__c = 'Prorrateo OK';
		listSeg.add(seg7);
		R2_Segmento__c seg8 = new R2_Segmento__c();
		seg8.R2_SEG_TXT_company_code__c = 'XXX';
		seg8.R2_SEG_MSDT_Prorrate__c = pro3.Id;
		seg8.R2_SEG_CHK_guilty_company__c = true;
		seg8.R2_SEG_TXT_Company__c = 'IBERIA';
		seg8.R2_SEG_PKL_Estado__c = 'Prorrateo OK';
		listSeg.add(seg8);

		insert listSeg;

		Test.stopTest();

		List<EmailMessage> listEmails = [SELECT Id FROM EmailMessage];
		// 4 emails enviados, porque prorrateo 4 tiene errores
		// System.assertEquals(4, listEmails.size());

		// Segmentos actualizados
		List<R2_Segmento__c> listSements = [SELECT Id FROM R2_Segmento__c WHERE R2_SEG_DAT_Date_Msg_Sent__c!=null];
		// System.assertEquals(4, listSements.size());

		// 1 prorrateo con errores
		List<R2_Prorrateo__c> listProrrateos = [SELECT Id FROM R2_Prorrateo__c WHERE R2_PRO_CHK_Error_envio_email__c=true];
		//System.assertEquals(1, listProrrateos.size());


		//System.assertEquals(0, [SELECT count() FROM R1_Log__c]);
	}

	/*---------------------------------------------------------------------------------------------------------------------
	Author:         Jaime Ascanta
	Company:        Accenture
	Description:
	IN:
	OUT:

	History:
		<Date>                     <Author>                         <Change Description>
	12/03/2018       	   		Jaime Ascanta                   		Initial Version
	----------------------------------------------------------------------------------------------------------------------*/
	@isTest
	static void exception_Test() {
		R1_CLS_LogHelper.throw_exception = true;

		Test.startTest();

		R2_CLS_ProrrateoTriggerMethods.sendEmailProrrateo(null, false);
		R2_CLS_ProrrateoTriggerMethods.checkSegmentsToSend(null);
		R2_CLS_ProrrateoTriggerMethods.getAttProrratePdf(null,null,null);
		R2_CLS_ProrrateoTriggerMethods.getAttPayOrdPdf(null,null,null);
		R2_CLS_ProrrateoTriggerMethods.getAttWorOrdPdf(null,null,null);
		R2_CLS_ProrrateoTriggerMethods.getMapProrateIncInfoPir(null);
		R2_CLS_ProrrateoTriggerMethods.getMapProrateIncInfoPir(null);
		R2_CLS_ProrrateoTriggerMethods.getAttInfoPir(null);
		//R2_CLS_ProrrateoTriggerMethods.generateInformePir(null);
		R2_CLS_ProrrateoTriggerMethods.typeProrrateo(null,null);
		R2_CLS_ProrrateoTriggerMethods.relatePayOrdAndWorOrd(null,null);
		R2_CLS_ProrrateoTriggerMethods.getIdsFromString(null);

		Test.stopTest();
		System.assertEquals(0, [SELECT count() FROM R1_Log__c]);
	}
}