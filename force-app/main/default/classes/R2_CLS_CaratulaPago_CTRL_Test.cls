/*---------------------------------------------------------------------------------------------------------------------
Author:         Jaime Ascanta
Company:        Accenture
Description:
IN:
OUT:

History:
<Date>                     <Author>                         <Change Description>
19/02/2018 					jaime ascanta                  	Initial Version
----------------------------------------------------------------------------------------------------------------------*/

@isTest
private class R2_CLS_CaratulaPago_CTRL_Test {
	/*---------------------------------------------------------------------------------------------------------------------
	Author:         Jaime Ascanta
	Company:        Accenture
	Description:
	IN:
	OUT:

	History:
	 <Date>                     <Author>                         <Change Description>
	19/02/2018       	   		Jaime Ascanta                  		 Initial Version
	24/05/2018       	   		Alberto Puerto                  	 fulfilling seg1.R2_SEG_TXT_Company__c (required field)
	10/10/2018				Victor Garcia Barquilla				Actualizado valores de R2_ExchangeRate__c a dimonca.
	----------------------------------------------------------------------------------------------------------------------*/
	@isTest static void generarComprobantes_Test() {

		R1_CLS_LogHelper.throw_exception = false;

		// endpoints
		List<R1_CS_Endpoints__c> lst_ep = new List<R1_CS_Endpoints__c>();
		R1_CS_Endpoints__c epLogin = new R1_CS_Endpoints__c();
		epLogin.Name = 'ETL_Login';
		epLogin.R1_CHK_Activo__c = true;
		epLogin.R1_TXT_EndPoint__c = 'PruebaLogin';
		lst_ep.add(epLogin);
		R1_CS_Endpoints__c ep = new R1_CS_Endpoints__c();
		ep.Name='R2_DelayedBags';
		ep.R1_CHK_Activo__c = true;
		ep.R1_TXT_EndPoint__c = 'R2_DelayedBags';
		lst_ep.add(ep);
		insert lst_ep;
		//timeOut
		TimeOut_Integrations__c cst = new TimeOut_Integrations__c();
		cst.Name = 'WorldTracer';
		cst.setTimeOut__c = 8000;
		insert cst;
		//R2_SDRtoEUR__c
		R2_ExchangeRate__c exchange = new R2_ExchangeRate__c();
		exchange.Name = 'XDR_960';
		exchange.R2_NUM_Rate_to_EUR__c  = 1.1788;
		exchange.R2_TXT_External_Id__c = 'Test';
		insert exchange;

		R2_Diccionario_Cias__c cia1 = new R2_Diccionario_Cias__c();
		cia1.R2_DIC_TXT_Codigo_Compania__c = 'test1';
		cia1.R2_DIC_TXT_Nombre_Compania__c = 'Test Email';
		cia1.R2_DIC_TXT_Correo__c = 'test1@email.com';
		insert cia1;

		// cuenta
		Account acc = new Account();
		acc.Name = 'Test';
		acc.R1_ACC_TLF_Phone_Marketing__c = '348787877';
		acc.R1_ACC_PKL_identification_Type__c = '02';
		acc.R1_ACC_TXT_Identification_number__c = '123456789';
		//acc.BillingAddress = 'calle 89';
		acc.BillingCity = 'test';
		acc.BillingState = 'test';
		acc.BillingPostalCode = '1234';
		acc.BillingCountry = 'test';
		insert acc;

		//caso
		Id rt_expediente = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Expediente').getRecordTypeId();
		Case caso = new Case();
		caso.AccountId = acc.Id;
		caso.RecordTypeId = rt_expediente;
		caso.Status = 'Abierto';
		caso.Origin = 'Email';
		caso.Type = 'Demora';
		insert caso;

		// incidencia
		Id rtDelay = [select id from RecordType where SobjectType='R1_Incident__c' AND DeveloperName='DELAYED'].Id;
		R1_Incident__c inc = new R1_Incident__c ();
		inc.RecordTypeId = rtDelay;
		inc.R2_INC_TXT_PIR_Station_Code__c = 'MAD';
		inc.R2_INC_TXT_PIR_Airline_Code__c = 'IB';
		inc.R2_INC_TXT_PIR_Reference_Number__c = '1111';
		inc.R2_INC_DAT_PIR_Claim_Date__c = Date.today();
		inc.R2_INC_TXT_FD__c = 'test';
		insert inc;

		// orden de trabajo
		R2_Work_Order__c wor = new R2_Work_Order__c();
		wor.R2_WO_DAT_Date_Creation_PIR__c = Date.today();
		wor.R2_WO_DAT_Opening_date__c = Date.today();
		wor.R2_WO_LOO_Case__c = caso.Id;
		wor.R2_WO_PKL_type__c = 'amazon';

		insert wor;

		// orden de pago
		R2_Payment_order__c pay = new R2_Payment_order__c();
		pay.R2_OPY_PKL_PaymentType__c = 'CHEQUE';
		pay.R2_OPY_PCK_Status__c = 'ENVIADO A BANCO';
		pay.R2_OPY_LOO_File_associated__c = caso.Id;
		pay.R2_OPY_DAT_Date_Issue_Remittance__c = Date.today();
		pay.R2_OPY_TXT_Bank_Account__c = 'test';
		pay.R2_OPY_TXT_External_Local_Account__c = 'test';
		pay.R2_OPY_ATXTL_Others__c = 'test';
		pay.R2_OPY_TXT_SWIFT__c = 'test';
		pay.R2_OPY_TXT_Fed_ABA__c = 'test';
		pay.R2_OPY_TXT_Chips_ABA__c = 'test';
		pay.R2_OPY_TXT_IBAN__c = 'test';
		pay.R2_OPY_TXT_Check_Num__c = 'test';
		pay.R2_OPY_TXT_Bank_Name__c = 'test';
		insert pay;

		// prorrateos
		R2_Prorrateo__c pro1 = new R2_Prorrateo__c();
		pro1.R2_PRO_TXT_payment_id__c = String.valueOf(pay.Id);
		pro1.R2_PRO_TXT_work_order__c = String.valueOf(wor.Id);
		pro1.R2_PRO_DAT_Date__c = Date.today();
		pro1.R2_PRO_LOO_Case__c = caso.Id;
		pro1.R2_PRO_LOO_Passenger_Name__c = acc.Id;
		pro1.R2_PRO_LOO_PIR__c = inc.Id;
		pro1.R2_PRO_PKL_Status__c = 'Prorrateo OK';
		insert pro1;

		R2_Segmento__c seg1 = new R2_Segmento__c();
		seg1.R2_SEG_TXT_company_code__c = 'test1';
		seg1.R2_SEG_MSDT_Prorrate__c = pro1.Id;
		seg1.R2_SEG_CHK_guilty_company__c = true;
		seg1.R2_SEG_TXT_Flight__c = 'IB';
		seg1.R2_SEG_DAT_Date__c = Date.today();
		seg1.R2_SEG_TXT_Company__c = 'IBERIA';
		insert seg1;

		Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());

		Test.startTest();

			PageReference pagePayOrd = Page.R2_CaratulaOrdenPago;
			Test.setCurrentPageReference(pagePayOrd);
			pagePayOrd.getParameters().put('pro', (String) pro1.Id);

			PageReference pageWorOrd = Page.R2_CaratulaOrdenTrabajo;
			Test.setCurrentPageReference(pageWorOrd);
			pageWorOrd.getParameters().put('pro', (String) pro1.Id);

			R2_CLS_CaratulaPago_CTRL controller = new R2_CLS_CaratulaPago_CTRL();

			//Datos orden de pago
			R2_CLS_CaratulaPago_CTRL.ComprobanteWP payData = controller.getComprobantePayOrd();
			
			//System.assertNotEquals(null, payData);
			
			System.debug('*** payData: '+ payData);
			
			/*
			System.assertNotEquals('', payData.datosPago.codigoExpediente);
			System.assertNotEquals('', payData.datosPago.pir);
			System.assertNotEquals('', payData.datosPago.fechaApertura);
			System.assertNotEquals('', payData.datosPago.tipoExpediente);
			System.assertNotEquals('', payData.datosPago.estado);
			System.assertNotEquals('', payData.datosPago.fechaEmision);
			System.assertNotEquals('', payData.datosPago.numVueloUltimaCia);
			System.assertNotEquals('', payData.datosPago.fechaVuelo);
			System.assertNotEquals('', payData.datosPago.fdRouting);
			//Datos pasajero
			System.assertNotEquals('', payData.datosPasajero.nombre);
			System.assertNotEquals('', payData.datosPasajero.tipoDocumento);
			System.assertNotEquals('', payData.datosPasajero.numeroDocumento);
			System.assertNotEquals('', payData.datosPasajero.direccion);
			System.assertNotEquals('', payData.datosPasajero.ciudad);
			System.assertNotEquals('', payData.datosPasajero.localidad);
			System.assertNotEquals('', payData.datosPasajero.distritoPostal);
			System.assertNotEquals('', payData.datosPasajero.pais);
			System.assertNotEquals('', payData.datosPasajero.numeroCuenta);
			System.assertNotEquals('', payData.datosPasajero.cuentaExtranjera);
			System.assertNotEquals('', payData.datosPasajero.otrosDatosBancarios);
			System.assertNotEquals('', payData.datosPasajero.swiftBic);
			System.assertNotEquals('', payData.datosPasajero.fedAba);
			System.assertNotEquals('', payData.datosPasajero.chipsAba);
			System.assertNotEquals('', payData.datosPasajero.iban);
			System.assertNotEquals('', payData.datosPasajero.chequeRef);
			System.assertNotEquals('', payData.datosPasajero.entidadPagadora);
			//importe
			System.assertNotEquals('', payData.importe.euro);
			*/

			//Datos orden de trabajo
			R2_CLS_CaratulaPago_CTRL.ComprobanteWP worData = controller.getComprobanteWorkOrd();
			//System.assertNotEquals(null, worData);
			
		Test.stopTest();

	//	System.assertEquals(0, [SELECT count() FROM R1_Log__c]);
	}

	/*---------------------------------------------------------------------------------------------------------------------
	Author:         Jaime Ascanta
	Company:        Accenture
	Description:
	IN:
	OUT:

	History:
	 <Date>                     <Author>                         <Change Description>
	14/03/2018       	   		Jaime Ascanta                  		 Initial Version
	----------------------------------------------------------------------------------------------------------------------*/
	@isTest static void getIdsFromStr_Test() {

		R1_CLS_LogHelper.throw_exception = false;

		// orden de pago
		R2_Work_Order__c wor = new R2_Work_Order__c();
		wor.R2_WO_DAT_Date_Creation_PIR__c = Date.today();
		wor.R2_WO_DAT_Opening_date__c = Date.today();
		wor.R2_WO_PKL_type__c = 'amazon';
		insert wor;

		R2_Work_Order__c wor2 = new R2_Work_Order__c();
		wor2.R2_WO_DAT_Date_Creation_PIR__c = Date.today();
		wor2.R2_WO_DAT_Opening_date__c = Date.today();
		wor2.R2_WO_PKL_type__c = 'amazon';
		insert wor2;

		Set<id> ids;

		Test.startTest();

		// case 1 - Ok
		ids = R2_CLS_CaratulaPago_CTRL.getIdsFromStr(wor.Id+','+wor2.Id);
		//System.assertEquals(2, ids.size());

		// case 2 - ok (not duplicate)
		ids = R2_CLS_CaratulaPago_CTRL.getIdsFromStr(wor.Id+','+wor.Id);
		//System.assertEquals(1, ids.size());

		// case 3  - error (empty)
		ids = R2_CLS_CaratulaPago_CTRL.getIdsFromStr('');
		//System.assertEquals(0, ids.size());

		// case 4  - error (delimiter not valid)
		ids = R2_CLS_CaratulaPago_CTRL.getIdsFromStr(wor.Id+';'+wor2.Id);
		//System.assertEquals(0, ids.size());

		// case 4  - error (id not valid)
		ids = R2_CLS_CaratulaPago_CTRL.getIdsFromStr('xxxxxxxxxx,yyyyyyyyyy');
		//System.assertEquals(0, ids.size());

		Test.stopTest();

		//System.assertEquals(0, [SELECT count() FROM R1_Log__c]);
	}

	/*---------------------------------------------------------------------------------------------------------------------
	Author:         Jaime Ascanta
	Company:        Accenture
	Description:   
	IN:
	OUT:

	History:
		<Date>                     <Author>                         <Change Description>
	13/03/2018       	   		Jaime Ascanta                   		Initial Version
	----------------------------------------------------------------------------------------------------------------------*/
	@isTest
	static void exception_Test() {
		R1_CLS_LogHelper.throw_exception = true;

		R2_CLS_CaratulaPago_CTRL controller = new R2_CLS_CaratulaPago_CTRL();

		Test.startTest();

			controller.getComprobantePayOrd();
			controller.getComprobanteWorkOrd();
			controller.getPaymentOrder(null);
			controller.getWorkOrder(null);
			controller.getTotalPayOrd(null);
			controller.getTotalWorkOrd(null);
			controller.getProrrateo(null);
			controller.getSegmento(null);
			controller.getCase(null);
			controller.getIncidence(null);
			R2_CLS_CaratulaPago_CTRL.getIdsFromStr(null);
			controller.getLavelPKLDocType(null, null);
			controller.getAccount(null);

		Test.stopTest();
		//System.assertEquals(0, [SELECT count() FROM R1_Log__c]);
	}
}