/*---------------------------------------------------------------------------------------------------------------------
    Author:         Alvaro Garcia Tapia
    Company:        Accenture
    Description:    Apex test para la clase apex 'Social_Customer_Service'
    IN:             
    OUT:            

    History: 
     <Date>                     <Author>                         <Change Description>
    24/09/2018             Alvaro Garcia Tapia                   Initial Version
    ----------------------------------------------------------------------------------------------------------------------*/
@isTest
public class Social_Customer_Service_Test {
	static Map<String, Object> sampleSocialData;
	static Social_Customer_Service handler;
	static {
		handler = new Social_Customer_Service();
		sampleSocialData = getSampleSocialData('1');
	}
	static testMethod void verifyNewRecordCreation() {
		SocialPost post = getSocialPost(sampleSocialData);
		SocialPersona persona = getSocialPersona(sampleSocialData);
		Id recordTypeIdAcc = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Entidad Comunicante').getRecordTypeId();
		Account clienteAnonimo = new Account();
		clienteAnonimo.Name='CLIENTE ANONIMO REDES SOCIALES';
		clienteAnonimo.RecordTypeId = recordTypeIdAcc;
        clienteAnonimo.R1_ACC_TLF_Phone_Marketing__c = '123456789';
        clienteAnonimo.R1_ACC_TXT_Id_Golden_record__c = 'Test';
		insert clienteAnonimo;
		Entitlement ent = new Entitlement();
        ent.Name = 'Casos RRSS';
        ent.AccountId = clienteAnonimo.Id;
        insert ent;
		test.startTest();
		handler.handleInboundSocialPost(post, persona, sampleSocialData);
		test.stopTest();
		SocialPost createdPost = [SELECT Id, PersonaId, ParentId, WhoId FROM SocialPost];
		SocialPersona createdPersona = [SELECT Id, ParentId FROM SocialPersona];
		//Contact createdContact = [SELECT Id FROM Contact];
		//Case createdCase = [SELECT Id, ContactId FROM Case];
		System.assertEquals(createdPost.PersonaId, createdPersona.Id, 'Post is not linked to the Persona.');
		System.assertEquals(createdPost.WhoId, createdPersona.ParentId, 'Post is not linked	to the Contact');
		//System.assertEquals(createdPost.ParentId, createdCase.Id, 'Post is not linked to the Case.');
		//System.assertEquals(createdCase.ContactId, createdContact.Id, 'Contact is not linked to the Case.');
	}
	static testMethod void matchSocialPostRecord() {
		SocialPost existingPost = getSocialPost(getSampleSocialData('2'));
		insert existingPost;
		SocialPost post = getSocialPost(sampleSocialData);
		post.R6PostId = existingPost.R6PostId;
		SocialPersona persona = getSocialPersona(sampleSocialData);
		Id recordTypeIdAcc = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Entidad Comunicante').getRecordTypeId();
		Account clienteAnonimo = new Account();
		clienteAnonimo.Name='CLIENTE ANONIMO REDES SOCIALES';
		clienteAnonimo.RecordTypeId = recordTypeIdAcc;
        clienteAnonimo.R1_ACC_TLF_Phone_Marketing__c = '123456789';
        clienteAnonimo.R1_ACC_TXT_Id_Golden_record__c = 'Test';
		insert clienteAnonimo;
		Entitlement ent = new Entitlement();
        ent.Name = 'Casos RRSS';
        ent.AccountId = clienteAnonimo.Id;
        insert ent;
		test.startTest();
		handler.handleInboundSocialPost(post, persona, sampleSocialData);
		test.stopTest();
		System.assertEquals(1, [SELECT Id FROM SocialPost].size(), 'There should be only 1 post');
	}
	static testMethod void matchSocialPersonaRecord() {
		//Contact existingContact = new Contact(LastName = 'LastName');
		//insert existingContact;
		Id recordTypeIdAcc = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente').getRecordTypeId();
		Account clienteAnonimo = new Account();
		clienteAnonimo.LastName='CLIENTE ANONIMO REDES SOCIALES';
		clienteAnonimo.RecordTypeId = recordTypeIdAcc;
        clienteAnonimo.R1_ACC_TLF_Phone_Marketing__c = '123456789';
        clienteAnonimo.R1_ACC_TXT_Id_Golden_record__c = 'Test';
		insert clienteAnonimo;
		List<Account> cliente = [SELECT Id, PersonContactId FROM Account WHERE Id =: clienteAnonimo.Id LIMIT 1];
		Entitlement ent = new Entitlement();
        ent.Name = 'Casos RRSS';
        ent.AccountId = cliente[0].Id;
        insert ent;
		SocialPersona existingPersona = getSocialPersona(getSampleSocialData('2'));
		existingPersona.ParentId = cliente[0].PersonContactId;
		insert existingPersona;
		SocialPost post = getSocialPost(sampleSocialData);
		SocialPersona persona = getSocialPersona(sampleSocialData);
		persona.ExternalId = existingPersona.ExternalId;
		test.startTest();
		handler.handleInboundSocialPost(post, persona, sampleSocialData);
		test.stopTest();
		SocialPost createdPost = [SELECT Id, PersonaId, ParentId, WhoId FROM SocialPost];
		SocialPersona createdPersona = [SELECT Id, ParentId FROM SocialPersona];
		//Contact createdContact = [SELECT Id FROM Contact];
		//Case createdCase = [SELECT Id, ContactId FROM Case];
		System.assertEquals(createdPost.PersonaId, createdPersona.Id, 'Post is not linked to the Persona.');
		System.assertEquals(createdPost.WhoId, createdPersona.ParentId, 'Post is not linked to the Contact');
		//System.assertEquals(createdPost.ParentId, createdCase.Id, 'Post is not linked to the Case.');
		//System.assertEquals(createdCase.ContactId, createdContact.Id, 'Contact is not linked to the Case.');
	}
	static testMethod void matchCaseRecord() {
		//Contact existingContact = new Contact(LastName = 'LastName');
		//insert existingContact;
		Id recordTypeIdAcc = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente').getRecordTypeId();
		Account clienteAnonimo = new Account();
		clienteAnonimo.LastName='CLIENTE ANONIMO REDES SOCIALES';
		clienteAnonimo.RecordTypeId = recordTypeIdAcc;
        clienteAnonimo.R1_ACC_TLF_Phone_Marketing__c = '123456789';
        clienteAnonimo.R1_ACC_TXT_Id_Golden_record__c = 'Test';
		insert clienteAnonimo;
		List<Account> cliente = [SELECT Id, PersonContactId FROM Account WHERE Id =: clienteAnonimo.Id LIMIT 1];
		Entitlement ent = new Entitlement();
        ent.Name = 'Casos RRSS';
        ent.AccountId = cliente[0].Id;
        insert ent;
		SocialPersona existingPersona = getSocialPersona(getSampleSocialData('2'));
		existingPersona.ParentId = cliente[0].PersonContactId;
		insert existingPersona;
		Case existingCase = new Case(ContactId = cliente[0].PersonContactId, Subject = 'Test Case');
		insert existingCase;
		SocialPost existingPost = getSocialPost(getSampleSocialData('2'));
		existingPost.ParentId = existingCase.Id;
		existingPost.WhoId = cliente[0].PersonContactId;
		existingPost.PersonaId = existingPersona.Id;
		String recipient = 'scs';
		existingPost.recipient = recipient;
		insert existingPost;
		SocialPost post = getSocialPost(sampleSocialData);
		post.responseContextExternalId = existingPost.ExternalPostId;
		post.Recipient = recipient;
		test.startTest();
		handler.handleInboundSocialPost(post, existingPersona, sampleSocialData);
		test.stopTest();
		SocialPost createdPost = [SELECT Id, PersonaId, ParentId, WhoId FROM SocialPost
		WHERE R6PostId = :post.R6PostId];
		System.assertEquals(existingPersona.Id, createdPost.PersonaId, 'Post is not linked to the Persona.');
		System.assertEquals(cliente[0].PersonContactId, createdPost.WhoId, 'Post is not linked to the Contact');
		System.assertEquals(existingCase.Id, createdPost.ParentId, 'Post is not linked to the Case.');
		System.assertEquals(1, [SELECT Id FROM Case].size(), 'There should only be 1 Case.');
	}
	static testMethod void reopenClosedCase() {
		Contact existingContact = new Contact(LastName = 'LastName');
		insert existingContact;
		SocialPersona existingPersona = getSocialPersona(getSampleSocialData('2'));
		existingPersona.ParentId = existingContact.Id;
		insert existingPersona;
		Case existingCase = new Case(ContactId = existingContact.Id, Subject = 'Test Case',	Status = 'Cerrado');
		insert existingCase;
		SocialPost existingPost = getSocialPost(getSampleSocialData('2'));
		existingPost.ParentId = existingCase.Id;
		existingPost.WhoId = existingContact.Id;
		existingPost.PersonaId = existingPersona.Id;
		String recipient = 'scs';
		existingPost.recipient = recipient;
		insert existingPost;
		SocialPost post = getSocialPost(sampleSocialData);
		post.responseContextExternalId = existingPost.ExternalPostId;
		post.Recipient = recipient;
		test.startTest();
		handler.handleInboundSocialPost(post, existingPersona, sampleSocialData);
		test.stopTest();
		SocialPost createdPost = [SELECT Id, PersonaId, ParentId, WhoId FROM SocialPost
		WHERE R6PostId = :post.R6PostId];
		System.assertEquals(existingPersona.Id, createdPost.PersonaId, 'Post is not linkedto the Persona.');
		System.assertEquals(existingContact.Id, createdPost.WhoId, 'Post is not linked to Administer Social Customer Service Apex Tests for the Default Apex Class the Contact');
		System.assertEquals(existingCase.Id, createdPost.ParentId, 'Post is not linked to the Case.');
		System.assertEquals(1, [SELECT Id FROM Case].size(), 'There should only be 1 Case.');
		System.assertEquals(false, [SELECT Id, IsClosed FROM Case WHERE Id = :existingCase.Id].IsClosed, 'Case should be open.');
	}

	static testMethod void chatpost() {
		Id recordTypeIdAcc = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Entidad Comunicante').getRecordTypeId();
		Account clienteAnonimo = new Account();
		clienteAnonimo.Name='CLIENTE ANONIMO REDES SOCIALES';
		clienteAnonimo.RecordTypeId = recordTypeIdAcc;
        clienteAnonimo.R1_ACC_TLF_Phone_Marketing__c = '123456789';
        clienteAnonimo.R1_ACC_TXT_Id_Golden_record__c = 'Test';
		insert clienteAnonimo;
		List<Account> cliente = [SELECT Id, PersonContactId FROM Account WHERE Id =: clienteAnonimo.Id LIMIT 1];
		Entitlement ent = new Entitlement();
        ent.Name = 'Casos RRSS';
        ent.AccountId = cliente[0].Id;
        insert ent;
		Contact existingContact = new Contact(LastName = 'LastName');
		insert existingContact;
		SocialPersona persona = getSocialPersona(getSampleSocialData('2'));
		persona.ParentId = existingContact.Id;
		persona.Name ='Test';
		persona.Provider ='Facebook';
		SocialPost post = getSocialPost(getSampleSocialData('2'));
		post.messageType = 'Direct';
		post.Handle ='Test';
		post.Provider ='Facebook';
		test.startTest();
		handler.handleInboundSocialPost(post, persona, sampleSocialData);
		test.stopTest();
		SocialPost createdPost = [SELECT Id, PersonaId, ParentId, WhoId FROM SocialPost
		WHERE R6PostId = :post.R6PostId];
		System.assertEquals(persona.Id, createdPost.PersonaId, 'Post is not linkedto the Persona.');
		System.assertEquals(1, [SELECT Id FROM Case].size(), 'There should only be 1 Case.');
	}

	static testMethod void update_persona() {
		Id recordTypeIdAcc = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Entidad Comunicante').getRecordTypeId();
		Account clienteAnonimo = new Account();
		clienteAnonimo.Name='CLIENTE ANONIMO REDES SOCIALES';
		clienteAnonimo.RecordTypeId = recordTypeIdAcc;
        clienteAnonimo.R1_ACC_TLF_Phone_Marketing__c = '123456789';
        clienteAnonimo.R1_ACC_TXT_Id_Golden_record__c = 'Test';
		insert clienteAnonimo;
		List<Account> cliente = [SELECT Id, PersonContactId FROM Account WHERE Id =: clienteAnonimo.Id LIMIT 1];
		Entitlement ent = new Entitlement();
        ent.Name = 'Casos RRSS';
        ent.AccountId = cliente[0].Id;
        insert ent;
		SocialPersona persona = getSocialPersona(getSampleSocialData('2'));
		persona.ParentId = clienteAnonimo.Id;
		persona.Name ='Test';
		persona.Provider ='Other';
		insert persona;
		SocialPost post = getSocialPost(getSampleSocialData('2'));
		post.messageType = 'Direct';
		post.Handle ='Test';
		post.Provider ='Other';
		String contenido;
		for(integer i =0; i<=325 ; i++){
			contenido += 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
		}
		post.Content = contenido;
		test.startTest();
		handler.handleInboundSocialPost(post, persona, sampleSocialData);
		test.stopTest();
		SocialPost createdPost = [SELECT Id, PersonaId, ParentId, WhoId FROM SocialPost
		WHERE R6PostId = :post.R6PostId];
		System.assertEquals(persona.Id, createdPost.PersonaId, 'Post is not linkedto the Persona.');
		System.assertEquals(1, [SELECT Id FROM Case].size(), 'There should only be 1 Case.');
	}

	static SocialPost getSocialPost(Map<String, Object> socialData) {
		SocialPost post = new SocialPost();
		post.Name = String.valueOf(socialData.get('source'));
		post.Content = String.valueOf(socialData.get('content'));
		post.Posted = Date.valueOf(String.valueOf(socialData.get('postDate')));
		post.PostUrl = String.valueOf(socialData.get('postUrl'));
		post.Provider = String.valueOf(socialData.get('mediaProvider'));
		post.MessageType = String.valueOf(socialData.get('messageType'));
		post.ExternalPostId = String.valueOf(socialData.get('externalPostId'));
		post.R6PostId = String.valueOf(socialData.get('r6PostId'));
		post.Classification = 'Comunicación Entrante';
		post.PostTags= 'Retraso,RPM,English,Pagare,Test,Test';
		return post;
	}
	static SocialPersona getSocialPersona(Map<String, Object> socialData) {
		SocialPersona persona = new SocialPersona();
		persona.Name = String.valueOf(socialData.get('author'));
		persona.RealName = String.valueOf(socialData.get('realName'));
		persona.Provider = String.valueOf(socialData.get('mediaProvider'));
		persona.MediaProvider = String.valueOf(socialData.get('mediaProvider'));
		persona.ExternalId = String.valueOf(socialData.get('externalUserId'));
		return persona;
	}
	static Map<String, Object> getSampleSocialData(String suffix) {
		Map<String, Object> socialData = new Map<String, Object>();
		socialData.put('r6PostId', 'R6PostId' + suffix);
		socialData.put('r6SourceId', 'R6SourceId' + suffix);
		socialData.put('postTags', null);
		socialData.put('externalPostId', 'ExternalPostId' + suffix);
		socialData.put('content', 'Content' + suffix);
		socialData.put('postDate', '2015-01-12T12:12:12Z');
		socialData.put('mediaType', 'Twitter');
		socialData.put('author', 'Author');
		socialData.put('authorTags', 'Influencer');
		socialData.put('skipCreateCase', false);
		socialData.put('mediaProvider', 'TWITTER');
		socialData.put('externalUserId', 'ExternalUserId');
		socialData.put('postUrl', 'PostUrl' + suffix);
		socialData.put('messageType', 'Tweet');
		socialData.put('source', 'Source' + suffix);
		socialData.put('replyToExternalPostId', null);
		socialData.put('realName', 'Real Name');
		return socialData;
	}
}