@isTest
public class TCK1_CLS_UpgSalaVip_Test {
    
    static testMethod void accesoUpg_Test(){
        Tarifas_UPG__c cs = new Tarifas_UPG__c();
        cs.Name='MADABC';
        cs.Tramo__c='MADABC';
        cs.Precio_Avio_TUR_PE__c= 1;
        cs.Precio_Avio_TUR_BUS__c= 1;
        cs.Precio_Avio_PE_BUS__c= 1;
        //cs.Other fiels values
     	insert cs;  
        
        List<Acceso_Sala_VIP__c> listCs = new List<Acceso_Sala_VIP__c>();
        Acceso_Sala_VIP__c cs2 = new Acceso_Sala_VIP__c();
        cs2.Name='ClaseTUR';
        cs2.ValorTexto__c='A';
        Acceso_Sala_VIP__c cs3 = new Acceso_Sala_VIP__c();
        cs3.Name='Margen_BUS';
        cs3.ValorTexto__c='3';
        Acceso_Sala_VIP__c cs4 = new Acceso_Sala_VIP__c();
        cs4.Name='Margen_PE';
        cs4.ValorTexto__c='3';
        Acceso_Sala_VIP__c cs5 = new Acceso_Sala_VIP__c();
        cs5.Name='Company';
        cs5.ValorTexto__c='IB';
        Acceso_Sala_VIP__c cs6 = new Acceso_Sala_VIP__c();
        cs6.Name='ClasePE';
        cs6.ValorTexto__c='W';
        Acceso_Sala_VIP__c cs7 = new Acceso_Sala_VIP__c();
        cs7.Name='ClaseBUS';
        cs7.ValorTexto__c='J';
        
        listCs.add(cs2);
        listCs.add(cs3);
        listCs.add(cs4);
        listCs.add(cs5);
        listCs.add(cs6);
        listCs.add(cs7);
        insert listCs;       
        Account pasajero = new Account();
        pasajero.R1_ACC_NUM_Number_of_Avios__c = 10000;
        pasajero.Name = 'test';
        insert pasajero;

        R1_byPass__c byPass = new R1_byPass__c();
        byPass.byPass_Sala_Vip__c = TRUE;
        insert byPass;

        insert new ResiberCallsNumber__c(ActualCallNumbers__c = 1,MaxCallNumbers__c = 500,day__c = 40, Name = 'CallNumbers',Active__c= true);


        R1_Flight__c vuelo = new R1_Flight__c();
        vuelo.R1_FLG_TXT_Carrier_code_oper__c = 'IB';
        vuelo.R1_FLG_TXT_Flight_number__c = '9999';
        vuelo.Name= 'test';
        vuelo.R1_FLG_TXT_Carrier_code__c = 'IB';
        vuelo.R1_FLG_DAT_Flight_date_local__c = Date.newInstance(2017, 08, 21);
        vuelo.R1_FLG_TXT_Airport_depart__c = 'MAD';
        vuelo.R1_FLG_TXT_Airport_arrive__c = 'BCN';
        vuelo.Name = 'IB9999_';
        insert vuelo;

        R1_VIP_Lounge_Access__c acceso = new R1_VIP_Lounge_Access__c();
        acceso.R1_VLI_PKL_Sala_Vip__c = 'Autoridades';
        acceso.R1_VLI_TXT_Frecuent_Flyer_number__c = 'IB12345678';
        acceso.R1_VLI_PKL_Vip_lounge_position__c = 'Puesto 1';
        acceso.R1_VLI_TXT_Flight_number__c = '9999';
        acceso.R1_VLI_DAT_Date__c = Date.newInstance(2017, 08, 21);
        acceso.R1_VLI_TXT_Comp_Flight__c = 'compTest';
        acceso.R1_VLI_TXT_Class__c = 'A';
        acceso.R1_VLI_PKL_OneWolrd_code__c = 'codeTest';
        acceso.R1_VLI_TXT_Mkt_Flight__c = '9999';
        acceso.R1_VLI_NUM_Guest_number__c = 3;
        acceso.R1_VLI_TXT_Origin__c = 'MAD';
        acceso.R1_VLI_TXT_Destination__c= 'ABC';
        acceso.R1_VLI_PKL_Access_status__c= 'Aprobado';
        acceso.R1_VLI_LOO_Cliente__c = pasajero.id;
        acceso.R1_VLI_LOO_Flight__c = vuelo.id;
        insert acceso;
        
        List<Mensajes_Sala_VIP__c> listMssg = new List<Mensajes_Sala_VIP__c>();

        Mensajes_Sala_VIP__c Mssg1 = new Mensajes_Sala_VIP__c();
        Mssg1.Name='Mssg PE-BUS';
        Mssg1.MensajesSIP__c	='El pasajero [nameCliente] del vuelo [vuelo] puede acceder a un upgrade a la clase BUS con un coste de [aviosPE] avios.';
        Mensajes_Sala_VIP__c Mssg2 = new Mensajes_Sala_VIP__c();
        Mssg2.Name='Mssg TUR-BUS';
        Mssg2.MensajesSIP__c	='El pasajero [nameCliente] del vuelo [vuelo] puede acceder a un upgrade a la clase BUS con un coste de [aviosPE] avios.';
        Mensajes_Sala_VIP__c Mssg3 = new Mensajes_Sala_VIP__c();
        Mssg3.Name='Mssg TUR-PE';
        Mssg3.MensajesSIP__c	='El pasajero [nameCliente] del vuelo [vuelo] puede acceder a un upgrade a la clase BUS con un coste de [aviosPE] avios.';
        Mensajes_Sala_VIP__c Mssg4 = new Mensajes_Sala_VIP__c();
        Mssg4.Name='Mssg TUR/PE-BUS';
        Mssg4.MensajesSIP__c	='El pasajero [nameClient] del vuelo [vuelo] puede acceder a un upgrade a las clases PE y BUS con un coste de [aviosTUR1] y [aviosTUR2] avios respectivamente.';

        
        listMssg.add(Mssg1);
        listMssg.add(Mssg2);
        listMssg.add(Mssg3);
        listMssg.add(Mssg4);
        insert listMssg;
        

        Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());
        
        TCK1_CLS_UpgSalaVip.accesoUpg(acceso);
    }
    
    static testMethod void accesoUpg_Test2(){
        
        Account pasajero = new Account();
        pasajero.R1_ACC_NUM_Number_of_Avios__c = 10000;
        pasajero.Name = 'test';
        insert pasajero;
        
        R1_byPass__c byPass = new R1_byPass__c();
        byPass.byPass_Sala_Vip__c = TRUE;
        insert byPass;

        R1_Flight__c vuelo = new R1_Flight__c();
        vuelo.R1_FLG_TXT_Carrier_code_oper__c = 'IB';
        vuelo.R1_FLG_TXT_Flight_number__c = '9999';
        vuelo.Name= 'test';
        vuelo.R1_FLG_TXT_Carrier_code__c = 'IB';
        vuelo.R1_FLG_DAT_Flight_date_local__c = Date.newInstance(2017, 08, 21);
        vuelo.R1_FLG_TXT_Airport_depart__c = 'MAD';
        vuelo.R1_FLG_TXT_Airport_arrive__c = 'BCN';
        insert vuelo;
        
        R1_VIP_Lounge_Access__c acceso = new R1_VIP_Lounge_Access__c();
        acceso.R1_VLI_PKL_Sala_Vip__c = 'Autoridades';
        acceso.R1_VLI_TXT_Frecuent_Flyer_number__c = 'IB12345678';
        acceso.R1_VLI_PKL_Vip_lounge_position__c = 'Puesto 1';
        acceso.R1_VLI_TXT_Flight_number__c = '9999';
        acceso.R1_VLI_DAT_Date__c = Date.newInstance(2017, 08, 21);
        acceso.R1_VLI_TXT_Comp_Flight__c = 'compTest';
        acceso.R1_VLI_TXT_Class__c = 'W';
        acceso.R1_VLI_PKL_OneWolrd_code__c = 'codeTest';
        acceso.R1_VLI_TXT_Mkt_Flight__c = '1234';
        acceso.R1_VLI_NUM_Guest_number__c = 3;
        acceso.R1_VLI_TXT_Origin__c = 'MAD';
        acceso.R1_VLI_TXT_Destination__c= 'ABC';
        acceso.R1_VLI_PKL_Access_status__c= 'Aprobado';
        acceso.R1_VLI_LOO_Cliente__c = pasajero.id;
        
        Tarifas_UPG__c cs = new Tarifas_UPG__c();
        cs.Name='MADABC';
        cs.Tramo__c='MADABC';
        cs.Precio_Avio_TUR_PE__c= 1;
        cs.Precio_Avio_TUR_BUS__c= 1;
        cs.Precio_Avio_PE_BUS__c= 1;
        //cs.Other fiels values
     	insert cs;  
        
        List<Acceso_Sala_VIP__c> listCs = new List<Acceso_Sala_VIP__c>();
        Acceso_Sala_VIP__c cs2 = new Acceso_Sala_VIP__c();
        cs2.Name='ClasePE';
        cs2.ValorTexto__c='W';
        Acceso_Sala_VIP__c cs3 = new Acceso_Sala_VIP__c();
        cs3.Name='Margen_BUS';
        cs3.ValorTexto__c='3';
        Acceso_Sala_VIP__c cs4 = new Acceso_Sala_VIP__c();
        cs4.Name='Margen_PE';
        cs4.ValorTexto__c='3';
        Acceso_Sala_VIP__c cs5 = new Acceso_Sala_VIP__c();
        cs5.Name='Company';
        cs5.ValorTexto__c='IB';
        Acceso_Sala_VIP__c cs6 = new Acceso_Sala_VIP__c();
        cs6.Name='ClaseTUR';
        cs6.ValorTexto__c='Y';
        Acceso_Sala_VIP__c cs7 = new Acceso_Sala_VIP__c();
        cs7.Name='ClaseBUS';
        cs7.ValorTexto__c='J';
        
        listCs.add(cs2);
        listCs.add(cs3);
        listCs.add(cs4);
        listCs.add(cs5);
        listCs.add(cs6);
        listCs.add(cs7);
        insert listCs;
        
        List<Mensajes_Sala_VIP__c> listMssg = new List<Mensajes_Sala_VIP__c>();

        Mensajes_Sala_VIP__c Mssg1 = new Mensajes_Sala_VIP__c();
        Mssg1.Name='Mssg PE-BUS';
        Mssg1.MensajesSIP__c	='El pasajero [nameCliente] del vuelo [vuelo] puede acceder a un upgrade a la clase BUS con un coste de [aviosPE] avios.';
        Mensajes_Sala_VIP__c Mssg2 = new Mensajes_Sala_VIP__c();
        Mssg2.Name='Mssg TUR-BUS';
        Mssg2.MensajesSIP__c	='El pasajero [nameCliente] del vuelo [vuelo] puede acceder a un upgrade a la clase BUS con un coste de [aviosPE] avios.';
        Mensajes_Sala_VIP__c Mssg3 = new Mensajes_Sala_VIP__c();
        Mssg3.Name='Mssg TUR-PE';
        Mssg3.MensajesSIP__c	='El pasajero [nameCliente] del vuelo [vuelo] puede acceder a un upgrade a la clase BUS con un coste de [aviosPE] avios.';
        Mensajes_Sala_VIP__c Mssg4 = new Mensajes_Sala_VIP__c();
        Mssg4.Name='Mssg TUR/PE-BUS';
        Mssg4.MensajesSIP__c	='El pasajero [nameClient] del vuelo [vuelo] puede acceder a un upgrade a las clases PE y BUS con un coste de [aviosTUR1] y [aviosTUR2] avios respectivamente.';

        
        listMssg.add(Mssg1);
        listMssg.add(Mssg2);
        listMssg.add(Mssg3);
        listMssg.add(Mssg4);
        insert listMssg;
        insert new ResiberCallsNumber__c(ActualCallNumbers__c = 1,MaxCallNumbers__c = 500,day__c = 40, Name = 'CallNumbers');        
        Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());

        
        TCK1_CLS_UpgSalaVip.accesoUpg(acceso);
    }
    
    static testMethod void accesoUpg_Test3(){
        
        Account pasajero = new Account();
        pasajero.R1_ACC_NUM_Number_of_Avios__c = 4000;
        pasajero.Name = 'test';
        insert pasajero;

        R1_byPass__c byPass = new R1_byPass__c();
        byPass.byPass_Sala_Vip__c = TRUE;
        insert byPass;

        R1_Flight__c vuelo = new R1_Flight__c();
        vuelo.R1_FLG_TXT_Carrier_code_oper__c = 'IB';
        vuelo.R1_FLG_TXT_Flight_number__c = '9999';
        vuelo.Name= 'test';
        vuelo.R1_FLG_TXT_Carrier_code__c = 'IB';
        vuelo.R1_FLG_DAT_Flight_date_local__c = Date.newInstance(2017, 08, 21);
        vuelo.R1_FLG_TXT_Airport_depart__c = 'MAD';
        vuelo.R1_FLG_TXT_Airport_arrive__c = 'BCN';
        insert vuelo;
        
        
        
        R1_VIP_Lounge_Access__c acceso = new R1_VIP_Lounge_Access__c();
        acceso.R1_VLI_PKL_Sala_Vip__c = 'Autoridades';
        acceso.R1_VLI_TXT_Frecuent_Flyer_number__c = 'IB12345678';
        acceso.R1_VLI_PKL_Vip_lounge_position__c = 'Puesto 1';
        acceso.R1_VLI_TXT_Flight_number__c = '9999';
        acceso.R1_VLI_DAT_Date__c = Date.newInstance(2017, 08, 21);
        acceso.R1_VLI_TXT_Comp_Flight__c = 'compTest';
        acceso.R1_VLI_TXT_Class__c = 'A';
        acceso.R1_VLI_PKL_OneWolrd_code__c = 'codeTest';
        acceso.R1_VLI_TXT_Mkt_Flight__c = '1234';
        acceso.R1_VLI_NUM_Guest_number__c = 3;
        acceso.R1_VLI_TXT_Origin__c = 'MAD';
        acceso.R1_VLI_TXT_Destination__c= 'ABC';
        acceso.R1_VLI_PKL_Access_status__c= 'Aprobado';
        acceso.R1_VLI_LOO_Cliente__c = pasajero.id;
        
        Tarifas_UPG__c cs = new Tarifas_UPG__c();
        cs.Name='MADABC';
        cs.Tramo__c='MADABC';
        cs.Precio_Avio_TUR_PE__c= 1;
        cs.Precio_Avio_TUR_BUS__c= 100000;
        cs.Precio_Avio_PE_BUS__c= 1;
        //cs.Other fiels values
     	insert cs;  
        
        List<Acceso_Sala_VIP__c> listCs = new List<Acceso_Sala_VIP__c>();
        Acceso_Sala_VIP__c cs2 = new Acceso_Sala_VIP__c();
        cs2.Name='ClaseTUR';
        cs2.ValorTexto__c='A';
        Acceso_Sala_VIP__c cs3 = new Acceso_Sala_VIP__c();
        cs3.Name='Margen_BUS';
        cs3.ValorTexto__c='3';
        Acceso_Sala_VIP__c cs4 = new Acceso_Sala_VIP__c();
        cs4.Name='Margen_PE';
        cs4.ValorTexto__c='3';
        Acceso_Sala_VIP__c cs5 = new Acceso_Sala_VIP__c();
        cs5.Name='Company';
        cs5.ValorTexto__c='IB';
        Acceso_Sala_VIP__c cs6 = new Acceso_Sala_VIP__c();
        cs6.Name='ClasePE';
        cs6.ValorTexto__c='W';
        Acceso_Sala_VIP__c cs7 = new Acceso_Sala_VIP__c();
        cs7.Name='ClaseBUS';
        cs7.ValorTexto__c='J';
        
        listCs.add(cs2);
        listCs.add(cs3);
        listCs.add(cs4);
        listCs.add(cs5);
        listCs.add(cs6);
        listCs.add(cs7);

        insert listCs;
        
        List<Mensajes_Sala_VIP__c> listMssg = new List<Mensajes_Sala_VIP__c>();

        Mensajes_Sala_VIP__c Mssg1 = new Mensajes_Sala_VIP__c();
        Mssg1.Name='Mssg PE-BUS';
        Mssg1.MensajesSIP__c	='El pasajero [nameCliente] del vuelo [vuelo] puede acceder a un upgrade a la clase BUS con un coste de [aviosPE] avios.';
        Mensajes_Sala_VIP__c Mssg2 = new Mensajes_Sala_VIP__c();
        Mssg2.Name='Mssg TUR-BUS';
        Mssg2.MensajesSIP__c	='El pasajero [nameCliente] del vuelo [vuelo] puede acceder a un upgrade a la clase BUS con un coste de [aviosPE] avios.';
        Mensajes_Sala_VIP__c Mssg3 = new Mensajes_Sala_VIP__c();
        Mssg3.Name='Mssg TUR-PE';
        Mssg3.MensajesSIP__c	='El pasajero [nameCliente] del vuelo [vuelo] puede acceder a un upgrade a la clase BUS con un coste de [aviosPE] avios.';
        Mensajes_Sala_VIP__c Mssg4 = new Mensajes_Sala_VIP__c();
        Mssg4.Name='Mssg TUR/PE-BUS';
        Mssg4.MensajesSIP__c	='El pasajero [nameClient] del vuelo [vuelo] puede acceder a un upgrade a las clases PE y BUS con un coste de [aviosTUR1] y [aviosTUR2] avios respectivamente.';

        
        listMssg.add(Mssg1);
        listMssg.add(Mssg2);
        listMssg.add(Mssg3);
        listMssg.add(Mssg4);
        insert listMssg;
        
        insert new ResiberCallsNumber__c(ActualCallNumbers__c = 1,MaxCallNumbers__c = 500,day__c = 40, Name = 'CallNumbers');
        
        Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());

        TCK1_CLS_UpgSalaVip.accesoUpg(acceso);
    }
    static testMethod void accesoUpg_Test4(){
Tarifas_UPG__c cs = new Tarifas_UPG__c();
        cs.Name='MADABC';
        cs.Tramo__c='MADABC';
        cs.Precio_Avio_TUR_PE__c= 1;
        cs.Precio_Avio_TUR_BUS__c= 1;
        cs.Precio_Avio_PE_BUS__c= 1;
        //cs.Other fiels values
     	insert cs;  
        
        List<Acceso_Sala_VIP__c> listCs = new List<Acceso_Sala_VIP__c>();
        Acceso_Sala_VIP__c cs2 = new Acceso_Sala_VIP__c();
        cs2.Name='ClaseTUR';
        cs2.ValorTexto__c='A';
        Acceso_Sala_VIP__c cs3 = new Acceso_Sala_VIP__c();
        cs3.Name='Margen_BUS';
        cs3.ValorTexto__c='3';
        Acceso_Sala_VIP__c cs4 = new Acceso_Sala_VIP__c();
        cs4.Name='Margen_PE';
        cs4.ValorTexto__c='3';
        Acceso_Sala_VIP__c cs5 = new Acceso_Sala_VIP__c();
        cs5.Name='Company';
        cs5.ValorTexto__c='IB';
        Acceso_Sala_VIP__c cs6 = new Acceso_Sala_VIP__c();
        cs6.Name='ClasePE';
        cs6.ValorTexto__c='W';
        Acceso_Sala_VIP__c cs7 = new Acceso_Sala_VIP__c();
        cs7.Name='ClaseBUS';
        cs7.ValorTexto__c='J';
        
        listCs.add(cs2);
        listCs.add(cs3);
        listCs.add(cs4);
        listCs.add(cs5);
        listCs.add(cs6);
        listCs.add(cs7);
        insert listCs;       
        Account pasajero = new Account();
        pasajero.R1_ACC_NUM_Number_of_Avios__c = 10000;
        pasajero.Name = 'test';
        insert pasajero;

        R1_byPass__c byPass = new R1_byPass__c();
        byPass.byPass_Sala_Vip__c = TRUE;
        insert byPass;

        insert new ResiberCallsNumber__c(ActualCallNumbers__c = 1,MaxCallNumbers__c = 500,day__c = 40, Name = 'CallNumbers',Active__c= true);


        R1_Flight__c vuelo = new R1_Flight__c();
        vuelo.R1_FLG_TXT_Carrier_code_oper__c = 'IB';
        vuelo.R1_FLG_TXT_Flight_number__c = '9999';
        vuelo.Name= 'test';
        vuelo.R1_FLG_TXT_Carrier_code__c = 'IB';
        vuelo.R1_FLG_DAT_Flight_date_local__c = Date.newInstance(2017, 08, 21);
        vuelo.R1_FLG_TXT_Airport_depart__c = 'MAD';
        vuelo.R1_FLG_TXT_Airport_arrive__c = 'BCN';
        vuelo.Name = 'IB9999_';
        insert vuelo;

        R1_VIP_Lounge_Access__c acceso = new R1_VIP_Lounge_Access__c();
        acceso.R1_VLI_PKL_Sala_Vip__c = 'Autoridades';
        acceso.R1_VLI_TXT_Frecuent_Flyer_number__c = 'IB12345678';
        acceso.R1_VLI_PKL_Vip_lounge_position__c = 'Puesto 1';
        acceso.R1_VLI_TXT_Flight_number__c = '9999';
        acceso.R1_VLI_DAT_Date__c = Date.newInstance(2017, 08, 21);
        acceso.R1_VLI_TXT_Comp_Flight__c = 'compTest';
        acceso.R1_VLI_TXT_Class__c = 'W';
        acceso.R1_VLI_PKL_OneWolrd_code__c = 'codeTest';
        acceso.R1_VLI_TXT_Mkt_Flight__c = '9999';
        acceso.R1_VLI_NUM_Guest_number__c = 3;
        acceso.R1_VLI_TXT_Origin__c = 'MAD';
        acceso.R1_VLI_TXT_Destination__c= 'ABC';
        acceso.R1_VLI_PKL_Access_status__c= 'Aprobado';
        acceso.R1_VLI_LOO_Cliente__c = pasajero.id;
        acceso.R1_VLI_LOO_Flight__c = vuelo.id;
        insert acceso;
        
        List<Mensajes_Sala_VIP__c> listMssg = new List<Mensajes_Sala_VIP__c>();

        Mensajes_Sala_VIP__c Mssg1 = new Mensajes_Sala_VIP__c();
        Mssg1.Name='Mssg PE-BUS';
        Mssg1.MensajesSIP__c	='El pasajero [nameCliente] del vuelo [vuelo] puede acceder a un upgrade a la clase BUS con un coste de [aviosPE] avios.';
        Mensajes_Sala_VIP__c Mssg2 = new Mensajes_Sala_VIP__c();
        Mssg2.Name='Mssg TUR-BUS';
        Mssg2.MensajesSIP__c	='El pasajero [nameCliente] del vuelo [vuelo] puede acceder a un upgrade a la clase BUS con un coste de [aviosPE] avios.';
        Mensajes_Sala_VIP__c Mssg3 = new Mensajes_Sala_VIP__c();
        Mssg3.Name='Mssg TUR-PE';
        Mssg3.MensajesSIP__c	='El pasajero [nameCliente] del vuelo [vuelo] puede acceder a un upgrade a la clase BUS con un coste de [aviosPE] avios.';
        Mensajes_Sala_VIP__c Mssg4 = new Mensajes_Sala_VIP__c();
        Mssg4.Name='Mssg TUR/PE-BUS';
        Mssg4.MensajesSIP__c	='El pasajero [nameClient] del vuelo [vuelo] puede acceder a un upgrade a las clases PE y BUS con un coste de [aviosTUR1] y [aviosTUR2] avios respectivamente.';

        
        listMssg.add(Mssg1);
        listMssg.add(Mssg2);
        listMssg.add(Mssg3);
        listMssg.add(Mssg4);
        insert listMssg;
        

        Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());
        
        TCK1_CLS_UpgSalaVip.accesoUpg(acceso);
    }   
}