@isTest
private class R2_CLS_Compensation_Web_Form_CTRL_Test {
	



	/*---------------------------------------------------------------------------------------------------------------------
		Author:         Victor Garcia Barquilla
		Company:        Accenture
		Description:
		IN:
		OUT:

		History:
		<Date>                  <Author>                         <Change Description>
		29/10/2018             	Victor Garcia Barquilla          	Initial Version
        30/10/2018              jaime Ascanta                       update coverage test.
	----------------------------------------------------------------------------------------------------------------------*/
	@isTest
	static void submitForm_test() {

        R1_CLS_LogHelper.throw_exception = false;
		Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());

		Test.startTest();
		Id rtIndem = Schema.SObjectType.R2_Compensation__c.getRecordTypeInfosByName().get('Indemnización/Compensación').getRecordTypeId();
		Id rTPasaje = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Pasaje').getRecordTypeId();

		List<Case> listCaseDb = [SELECT Id,AccountID,R1_CAS_LOO_Flight__c,ParentId FROM Case WHERE RecordTypeId=:rTPasaje];

		R2_Compensation__c pago = new R2_Compensation__c();
		pago.RecordTypeId = rtIndem;
		pago.R2_COM_PKL_Status__c = 'Inicial';
		pago.R2_COM_LOO_Case__c = listCaseDb[0].Id;
		pago.R2_COM_LOO_Account__c = listCaseDb[0].AccountID;
		pago.R2_COM_LOO_Flight__c = listCaseDb[0].R1_CAS_LOO_Flight__c;
		pago.R2_COM_PKL_Payment_method__c =  'Transferencia';
		pago.R2_COM_PKL_Type__c = 'Indemnización';
		pago.R2_COM_PKL_Currency_Claimed__c = 'EUR';
		pago.R2_COM_DIV_Total_Amount__c = 100;
		pago.R2_COM_NUM_Total_Amount_local__c = 100;
		pago.R2_COM_TXT_Passenger_Reason__c = 'Test';
		pago.R2_COM_TXT_Frecuent_Flyer__c = '1234567890';
		insert pago;

		R2_Monitor__c m = new R2_Monitor__c();
		m.R2_MON_TXT_Key__c = '99999999999999999999999999';
		m.R2_MON_TXT_Content__c = String.valueOf(pago.Id);
		m.R2_MON_PKL_Action__c = 'Email Alert';
		m.R2_MON_EMA_Email__c = 'email@email.com';
		m.R2_MON_DIV_Amount__c = pago.R2_COM_NUM_Total_Amount_local__c;
		m.R2_MON_TXT_Currency__c = pago.R2_COM_PKL_Currency_Claimed__c;
		m.R2_MON_PKL_Type__c = 'Formulario web pagos';
		m.R2_MON_LOO_File_associated__c = listCaseDb[0].ParentId;
		insert m;


		PageReference pageRef = Page.R2_VF_Compensation_Web_Form;
		Test.setCurrentPage(pageRef);
		ApexPages.currentPage().getParameters().put('key', '99999999999999999999999999');
		ApexPages.currentPage().getParameters().put('lang', 'es');


		R2_CLS_Compensation_Web_Form_CTRL ctrl = new R2_CLS_Compensation_Web_Form_CTRL();
		ctrl.docPersonName = 'Test';
		ctrl.typeDoc = '04';
		ctrl.numDoc = '999999999';
		ctrl.iban = 'ES3200491500002010138058';
		ctrl.swift = '12345678901';
		ctrl.bankName = 'test';
		ctrl.bankCountry = 'ES';
		ctrl.bankLocation = 'Madrid';
		ctrl.bankAddr = 'Test';

		ctrl.submitForm();

		Test.stopTest();

        // payment order created ok?
		List<R2_Payment_order__c> listPayOrd = [SELECT Id, R2_OPY_PCK_Status__c,R2_OPY_TXT_Budget_Center__c,R2_OPY_TXT_Charge_account__c FROM R2_Payment_order__c];
		System.assertEquals(1, listPayOrd.size());
		//System.assertEquals('Ready', listPayOrd[0].R2_OPY_PCK_Status__c);
        System.assertEquals('IBAPM0HQT2', listPayOrd[0].R2_OPY_TXT_Budget_Center__c);
        System.assertEquals('2330920500', listPayOrd[0].R2_OPY_TXT_Charge_account__c);

        // compensation status update?
        List<R2_Compensation__c> listCom = [SELECT ID FROM R2_Compensation__c WHERE R2_COM_PKL_Status__c='Enviado' AND R2_COM_LOO_Payment_Code__c=:listPayOrd[0].Id ];
        System.assertEquals(1, listCom.size());

        // key in monitos is deleted?
        List<R2_Monitor__c> lstMont = [SELECT Id FROM R2_Monitor__c WHERE R2_MON_TXT_Key__c='99999999999999999999999999'];
        System.assertEquals(0, lstMont.size());

		System.assertEquals(0, [SELECT count() FROM R1_Log__c]);
	}

	@isTest
    static void exception_Test() {
        R1_CLS_LogHelper.throw_exception = true;
        Test.startTest();
			PageReference pageRef = Page.R2_VF_Compensation_Web_Form;
			Test.setCurrentPage(pageRef);
			ApexPages.currentPage().getParameters().put('key', '99999999999999999999999999');
			ApexPages.currentPage().getParameters().put('lang', 'es');
			
            R2_CLS_Compensation_Web_Form_CTRL ctrl = new R2_CLS_Compensation_Web_Form_CTRL();
            ctrl.submitForm();
            ctrl.setDefaultFormData();
            ctrl.validateFormFields();
            ctrl.checkAccess();
            ctrl.getCase(null);
            ctrl.getFlight(null);
            ctrl.getAccount(null);
            ctrl.getCompensations(null);
            ctrl.makePaymentOrder();
            ctrl.insertPaymentOrder(null);
            ctrl.paymentValidationSAP(null);
        Test.stopTest();
    }

    @testSetup
    static void setuptDataTest() {

		R1_CLS_LogHelper.throw_exception = false;

        // Custom Settings
        List<R1_CS_Endpoints__c> lst_ep=new List<R1_CS_Endpoints__c>();
        R1_CS_Endpoints__c ep = new R1_CS_Endpoints__c();
        ep.Name='ETL_Login';
        ep.R1_CHK_Activo__c = true;
        ep.R1_TXT_EndPoint__c='ETL_Login';
        lst_ep.add(ep);
        
        R1_CS_Endpoints__c ep2 = new R1_CS_Endpoints__c();
        ep2.Name = 'Pagos';
        ep2.R1_CHK_Activo__c = true;
        ep2.R1_TXT_EndPoint__c = 'Pagos';
        lst_ep.add(ep2);
        insert lst_ep;

		PagosSAP__c csPay = new PagosSAP__c();
		csPay.Name = 'ESPAÑA EUR T';
		csPay.Centro_Emisor_SAP__c = 'CP06530000';
		csPay.Codigo_Pais_SAP__c = 'ES';
		csPay.Pais_SAP__c = 'ESPAÑA';
		csPay.Active_SAP__c = true;
		csPay.Metodo_de_Pago_SAP__c = 'T';
		csPay.Moneda_SAP__c = 'EUR';
		csPay.Tipo_SAP__c = 'SEPA';
		insert csPay;

		R2_CS_Centros_Cuentas__c csCC = new R2_CS_Centros_Cuentas__c();
		csCC.Name = '001';
		csCC.R2_TXT_Record_Type__c = 'Pasaje';
		csCC.R2_TXT_Type__c = 'Retraso';
		csCC.R2_TXT_Subtype__c = 'Avería';
		csCC.R2_TXT_Manage__c = 'Pago';
		csCC.R2_CHK_Level__c = false;
		csCC.R2_TXT_Centro__c = 'IBAPM0HQT2';
		csCC.R2_TXT_Cuenta__c = '2330920500';
		csCC.R2_TXT_Payment_Method__c = 'SAP';
		insert csCC;

		List<R2_CS_Translation_Payments_Web_Form__c> listCsLang = new List<R2_CS_Translation_Payments_Web_Form__c>();
		R2_CS_Translation_Payments_Web_Form__c csLang = new R2_CS_Translation_Payments_Web_Form__c();
		csLang.Name = 'EN';
		csLang.language__c = 'English';
		listCsLang.add(csLang);
		R2_CS_Translation_Payments_Web_Form__c csLang2 = new R2_CS_Translation_Payments_Web_Form__c();
		csLang2.Name = 'ES';
		csLang2.language__c = 'Español';
		listCsLang.add(csLang2);
		insert listCsLang;

        // Datos de prueba
        Id rTExp = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Expediente').getRecordTypeId();
        Id rTPasaje = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Pasaje').getRecordTypeId();
        Id rtIndem = Schema.SObjectType.R2_Compensation__c.getRecordTypeInfosByName().get('Indemnización/Compensación').getRecordTypeId();
        Id rtAcc = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente').getRecordTypeId();
        
        Account acc = new Account();
        acc.RecordTypeId = rtAcc;
        acc.FirstName = 'Test';
        acc.LastName = 'ClienteTest';
        acc.PersonEmail = 'test@test.com';
        acc.R1_ACC_PKL_Gender_description__c = 'M';
        acc.R1_ACC_TLF_Phone_Marketing__c = '123456789';
        acc.R1_ACC_PKL_identification_Type__c = '02';
        acc.R1_ACC_TXT_Identification_number__c = '123456789';
        insert acc;

        R1_Flight__c vuelo = new R1_Flight__c();
        vuelo.Name = 'VueloTest';
        vuelo.R1_FLG_TXT_Origin__c = 'OrigenTest';
        vuelo.R1_FLG_TXT_Destination__c = 'DestinoTest';
        vuelo.R1_FLG_TXT_Carrier_code__c = 'IB';
        vuelo.R1_FLG_TXT_Carrier_code_oper__c = 'IB';
        vuelo.R1_FLG_TXT_AC_owner__c = 'IB';
        vuelo.R1_FLG_DAT_Flight_date_local__c = Date.newInstance(2017, 08, 21);
        vuelo.R1_FLG_TXT_Flight_number__c = '2342';
        insert vuelo;

        Case casoPa = new Case();
        casoPa.RecordTypeId = rTExp;
        casoPa.AccountID = acc.Id;
        casoPa.R1_CAS_LOO_Flight__c = vuelo.Id;
        casoPa.Status = 'Abierto';
        casoPa.Origin = 'Llamada';
        casoPa.Type = 'Retraso';
		casoPa.R1_CAS_PKL_Subtype__c = 'Avería';
        casoPa.Subject = 'SubjectTest';
        casoPa.R1_CAS_PKL_Idioma__c = 'es';
		//insert casoPa;

        Case casoH = new Case();
        casoH.RecordTypeId = rTPasaje;
        casoH.ParentId = casoPa.Id;
        casoH.AccountID = acc.Id;
        casoH.R1_CAS_LOO_Flight__c = vuelo.Id;
        casoH.Status = 'Abierto';
        casoH.Origin = 'Llamada';
        casoH.Type = 'Retraso';
		casoH.R1_CAS_PKL_Subtype__c = 'Avería';
        casoH.Subject = 'SubjectTest';
        casoH.R1_CAS_PKL_Idioma__c = 'es';
		casoH.R2_CAS_PKL_Manage__c = 'Pago';
        insert casoH;
    }
}