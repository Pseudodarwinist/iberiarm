public with sharing class ArchivedEmailSearchController {
    public final String accountId {get; set;}
    public final String contactId {get; set;}

    public ArchivedEmailSearchController() {
        this.accountId = ApexPages.currentPage().getParameters().get('accountId');
        this.contactId = ApexPages.currentPage().getParameters().get('contactId');
    }
}