public with sharing class EmailDatatableWrapper {
    
    @TestVisible private static List<archive_email_tracking__x> mockedEmailTrackings = null;
    @TestVisible private static Datetime testDatetime = null;
    @TestVisible private static Boolean archivedEmailTrackingCanBeRead = null;
    @TestVisible private static Boolean accountCanBeRead = null;

    @AuraEnabled
    public String filename {get; set;}
    @AuraEnabled
    public String contactKey {get; set;}
    @AuraEnabled
    public String eventTime {get; set;}
    @AuraEnabled
    public String name {get; set;}
    @AuraEnabled
    public String surname {get; set;}
    @AuraEnabled
    public String email {get; set;}
    @AuraEnabled
    public String subject {get; set;}
    @AuraEnabled
    public String flightDate {get; set;}
    @AuraEnabled
    public String goldenRecord {get; set;}
    @AuraEnabled
    public String comMkt {get; set;}
    @AuraEnabled
    public String iberiaPlus {get; set;}
    @AuraEnabled
    public String pnrAmadeus {get; set;}
    @AuraEnabled
    public String pnrResiber {get; set;}
    @AuraEnabled
    public String langMkt {get; set;}
    @AuraEnabled
    public String clientType {get; set;}
    @AuraEnabled
    public String tkt {get; set;}
    @AuraEnabled
    public String processName {get; set;}
    @AuraEnabled
    public String externalURLpdf {get; set;}
    @AuraEnabled
    public String externalURLhtml {get; set;}
    @AuraEnabled
    public String externalURLeml {get; set;}
    @AuraEnabled
    public String flightId {get; set;}
    @AuraEnabled
    public Boolean trackingDataNotPresent {get; set;}
    @AuraEnabled
    public Boolean contactDataNotPresent {get; set;}

    public EmailDatatableWrapper(archive_v_email__x archivedEmail) {

        // Map fields to wrapper
        mapFields(archivedEmail);

        // Prepare fields for button visibility in list
        prepareButtonsVisibility();

        // AWS links for eml and html files
        prepareAWSExternalLinks();
    }


    private void mapFields(archive_v_email__x archivedEmail) {

        filename = archivedEmail.filename__c;
        name = archivedEmail.nombre_st__c;
        surname = archivedEmail.primer_apellido_st__c;
        email = archivedEmail.email__c;
        flightDate = Util.convertDateToHumanReadableWithNoTime(archivedEmail.flight_date__c);
        goldenRecord = archivedEmail.id_golden_record__c;
        comMkt = archivedEmail.mercado_comunicaciones_st__c;
        iberiaPlus = archivedEmail.num_tarjeta_ibp_st__c;
        pnrAmadeus = archivedEmail.pnr_amadeus__c;
        pnrResiber = archivedEmail.pnr_resiber__c;
        langMkt = archivedEmail.seg_communication_language__c;
        clientType = archivedEmail.tipo_cliente_bl__c;
        tkt = archivedEmail.tkt__c;
        processName = archivedEmail.processname__c;
        subject = archivedEmail.subjectline__c;
        eventTime = Util.convertDateToHumanReadable(archivedEmail.eventdate__c);
        contactKey = archivedEmail.contactkey__c;
        flightId = archivedEmail.flight_id__c;
    }

    private void prepareButtonsVisibility() {

        contactDataNotPresent = true;
        trackingDataNotPresent = true;

        if(archivedEmailTrackingCanBeRead != null ? archivedEmailTrackingCanBeRead : filename != null) {
            trackingDataNotPresent = false;
        }

        if (accountCanBeRead != null ? accountCanBeRead : Schema.sObjectType.Account.isAccessible()) {
            if (contactKey != null) {
                Account [] clients = [SELECT Id FROM Account WHERE ID =:contactKey];
                if (clients.size() > 0) {
                    contactDataNotPresent = false;
               }
            }
        }
    }

    private void prepareAWSExternalLinks() {
        if (filename != null && processName != null) {

            String folder = 'archive/' + processName + '/' ;
            String[] splitted = filename.split('\\_');

            if (splitted.size() >= 5) {
                folder += splitted[1] + '/';
                folder += splitted[2] + '/';
                folder += splitted[3] + '/';
            }

            folder += filename;

            Datetime now = DateTime.now();
            externalURLpdf = AWSLinkGenerator.getSignedURL(folder + '.pdf', testDatetime != null? testDatetime : now);
            externalURLhtml = AWSLinkGenerator.getSignedURL(folder + '.html', testDatetime != null? testDatetime : now);
            externalURLeml = AWSLinkGenerator.getSignedURL(folder + '.zip', testDatetime != null? testDatetime : now);
        }
    }
}