public with sharing class R2_CLS_Retrieve_Compensation {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Ismael Yubero Moreno
    Company:        Accenture
    Description:

    IN:
   OUT:

    History:
    <Date>                     <Author>                <Change Description>
    07/11/2017                 Ismael Yubero Moreno    Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public class GetCompensationWebRequest{
        public String ticketNumber;
        public String pnr;   
        public String journeyDestination;
        public String residenceCountryType;
        public String flightNumber;
        public String flightDate;
    } 
    public class GetCompensationWebResponse{
        public String clientType;
        public String singularIndicator;
        public String clientValue;
        public String frustrationValue;
        public String incidentCodes;
        public String notificationMinutes;
        public String delayTime;
        public String carrierCode;
        public String flightNumber;
        public String departureAirport;//both service, claim automation and compensacion en vuelo
        public String arrivalAirport;//both service, claim automation and compensacion en vuelo
        public String cabinClass;//both service, claim automation and compensacion en vuelo
        public String traveledDistance;
        public String missedConnection;
        public String notAutomateIndicator;
        public String indNotAutomate;
        public String indemnization;
        public String aviosCompensation;
        public String vouchersCompensation;
        public String delayType;
        public String departureCountry;//both service, claim automation and compensacion en vuelo
        public String arrivalCountry;//both service, claim automation and compensacion en vuelo
        public String paxInFlightIndicator;//only Compensacion en vuelo
        public String haulType;//only Compensacion en vuelo
        public String boardingNumber;
        public String seatRow;
        public String seatLetter;
        public String incidentSegmentOrigin;
        public String incidentSegmentDestination;
        public String incidentDescription;
        public List<Errors> errors;
    }

    public class Errors{
        public String code;
        public String reason;
        public String timeStamp;
    }

    public static String formatoFecha(Date fechaVuelo){
        try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');} //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;           
            System.debug('fecha antigua: '+ fechaVuelo);
            // String fecha;
            String fecha= String.valueOf(fechaVuelo.year());
            fecha += '-';
            system.debug('!!! fecha: ' +  fechaVuelo);
            if(fechaVuelo.month()<10){
                system.debug('!!! mes if: ' +  fechaVuelo.month());
                fecha += '0'+String.valueOf(fechaVuelo.month());
            }else{
                fecha += String.valueOf(fechaVuelo.month());
            }
            fecha += '-';
            if(fechaVuelo.day()<10){
                system.debug('!!! dia if: ' +  fechaVuelo.day());
                fecha+= '0'+ String.valueOf(fechaVuelo.day());
            }else{
                fecha+= String.valueOf(fechaVuelo.day());
            }
            
            System.debug('Esta es la fecha con el formato bueno: ' + fecha);
            
            
            return fecha;
        }catch(Exception exc){
                R1_CLS_LogHelper.generateErrorLog('R2_CLS_Flight_TriggerMethods__c.formatoFecha', '', exc.getmessage()+', '+exc.getLineNumber(), 'R1_Flight__c');
                return null;

        }      
        
    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Ismael Yubero Moreno
    Company:        Accenture
    Description:    Retrieves Delayed Bag
    IN:

    OUT:

    History:

    <Date>              <Author>                    <Description>
    07/11/2017          Ismael Yubero Moreno        Initial version
    07/11/2017          Ricardo Pereira Ramirez     Generate Payment
    23/01/2018                  Jaime Ascanta                           Fixed null points errors in response fields
    26/01/2018                  Jaime Ascanta                           Refactored code and fix bugs.
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static List<R2_Compensation__c> caller_retrieveCompensation(Case caseComp){

        try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');} //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;

            System.debug('*** entra en caller_retrieveCompensation()');
            // request wp
            GetCompensationWebRequest requestWRP =  new GetCompensationWebRequest();
            requestWRP.pnr = caseComp.Parent.R1_CAS_TXT_PNR__c;
            requestWRP.ticketNumber = caseComp.Parent.R2_CAS_TXT_TKT_Ticket__c;
            requestWRP.journeyDestination = String.isBlank(caseComp.Parent.R1_CAS_LOO_Flight__r.R1_FLG_TXT_Ap_arrive_orig__c) ? 
                caseComp.Parent.R2_CAS_FOR_Destination__c : caseComp.Parent.R1_CAS_LOO_Flight__r.R1_FLG_TXT_Ap_arrive_orig__c;
            String noper = caseComp.Parent.R1_CAS_LOO_Flight__r.R1_FLG_TXT_Flight_no_oper__c;
            for(Integer i =0;i<4 && noper.length()<4;i++){
                noper ='0'+noper;
            }
            requestWRP.flightNumber =caseComp.Parent.R1_CAS_LOO_Flight__r.R1_FLG_TXT_Carrier_code_oper__c + noper;
            requestWRP.flightDate = formatoFecha(caseComp.Parent.R1_CAS_LOO_Flight__r.R1_FLG_DAT_Flight_date_local__c);
            requestWRP.residenceCountryType = caseComp.Parent.R2_CAS_PKL_Country__c=='DE' ||  caseComp.Parent.R2_CAS_PKL_Country__c =='AT' ? 'SPECIAL1' : 'STANDARD';
            System.debug('!!!!!!!!!!!request: ' + requestWRP);
            GetCompensationWebResponse resp = retrieveCompensation(requestWRP, 0);
            System.debug('*** resp: ' + resp);

            // si la respuesta no es valida, terminamos ejecucion.
            //if(resp==null || resp.paxInFlightIndicator == 'KO') return null;
            if(resp.errors != null || resp==null){
                GetCompensationWebRequest requestWRP2 = requestWRP;
                requestWRP2.PNR =Label.CA_DummyPNR;
                GetCompensationWebResponse resp2 = retrieveCompensation(requestWRP2,0);
                if(resp2.errors !=null || resp2==null){
                    caseComp.R2_CAS_CHK_CI_Record_not_found__c = true;
                    R2_CLS_Retrive_ClaimantInfo.ClaimantInfoWebRequestWP req = new R2_CLS_Retrive_ClaimantInfo.ClaimantInfoWebRequestWP();
                    req.pnr = requestWRP.pnr;
                    req.ticketNumber = requestWRP.ticketNumber;
                    R2_CLS_Retrive_ClaimantInfo.ClaimantInfoWebResponseWP res = R2_CLS_Retrive_ClaimantInfo.retriveClaimantInfo(req, 0);
                    if(res == null){
                        caseComp.R2_CAS_CHK_PNR_Not_found_in_IFAR__c = true;
                    }
                    update(caseComp);
                    return null;
                }
                resp = resp2;
            }
            Id rtPagoAvios = Schema.SObjectType.R2_Compensation__c.getRecordTypeInfosByName().get('Avios').getRecordTypeId();
            Id rtPagoComInd = Schema.SObjectType.R2_Compensation__c.getRecordTypeInfosByName().get('Indemnización/Compensación').getRecordTypeId();
            Id rtPas = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Pasaje').getRecordTypeId();
            List<R2_Compensation__c> compList = new List<R2_Compensation__c>();
            
            if((resp.indemnization != null && resp.indemnization != '' && resp.indemnization != 'No_Info')&& Integer.valueOf(resp.indemnization)!=0){
                R2_Compensation__c compInd = createCompensation(caseComp);
                if(compInd!=null){
                    String tipo;
                    List<R2_Payment_order__c> posPO = new List<R2_Payment_order__c>();
                    List<R2_Compensation__c> posComp = new List<R2_Compensation__c>();
                    if(resp.delayType != null){
                        if(resp.delayType == 'CNL'){
                            tipo = 'Cancelación';
                        }else{
                            tipo = 'Retraso';
                        }
                    }
                    List<Case> posChilds = [SELECT Id,ParentId FROM Case WHERE RecordTypeId=:rtPas AND Parent.Type=:tipo 
                                                                AND Parent.R2_CAS_TXT_TKT_Ticket__c=:caseComp.Parent.R2_CAS_TXT_TKT_Ticket__c 
                                                                AND Parent.R1_CAS_TXT_PNR__c=:caseComp.Parent.R1_CAS_TXT_PNR__c
                                                                AND R2_CAS_TXT_Seg_origin_incident__c=:resp.incidentSegmentOrigin
                                                                AND R2_CAS_TXT_Seg_destination_incident__c=:resp.incidentSegmentDestination];
                    System.debug('posChilds::::: ' + posChilds );

                    if(!poschilds.isEmpty()){
                        Set<String> setpadres = new Set<String>();
                        for(Case hijo : posChilds){
                            setpadres.add(hijo.ParentId);
                        }
                        posPO = [SELECT Id,R2_OPY_LOO_File_associated__c FROM R2_Payment_order__c WHERE R2_OPY_LOO_File_associated__c IN :setpadres];
                        posComp = [SELECT Id, R2_COM_LOO_Case__r.ParentId FROM R2_Compensation__c WHERE R2_COM_LOO_Case__c IN : posChilds];
                    }
                    System.debug('posPo::::: ' + posPO);
                    if(posPo.isEmpty() && posComp.isEmpty()){
                        compInd.R2_COM_LOO_Case__c = caseComp.Id;
                        compInd.R2_COM_LOO_Client__c = caseComp.AccountId;
                        compInd.RecordTypeId = rtPagoComInd;
                        compInd.R2_COM_PKL_Status__c = 'Pendiente de emisión';
                        compInd.R2_COM_PKL_Payment_method__c =  'Transferencia';
                        compInd.R2_COM_PKL_Type__c = 'Indemnización';
                        compInd.R2_COM_CUR_Amount__c =  Double.valueOf(resp.indemnization);
                        compInd.R2_COM_PKL_Currency_Claimed__c = 'EUR';
                        compInd.R2_COM_NUM_Total_Amount_local__c =Double.valueOf(resp.indemnization);
                        caseComp.R2_CAS_PKL_Manage__c='Pago';
                        fullfillBudgetaryAndChargeAccount(caseComp,compInd);
                        insert compInd;
                        if(caseComp.Parent.R1_CAS_PKL_Idioma__c=='es' || caseComp.Parent.R1_CAS_PKL_Idioma__c=='it' || caseComp.Parent.R1_CAS_PKL_Idioma__c=='pt' || caseComp.Parent.R1_CAS_PKL_Idioma__c=='fr'
                            || caseComp.Parent.R1_CAS_PKL_Idioma__c=='en' || caseComp.Parent.R1_CAS_PKL_Idioma__c=='de'){
                            if(caseComp.Parent.R2_CAS_EMA_PASS_Email_Passenger__c!=null && caseComp.Parent.R2_CAS_EMA_PASS_Email_Passenger__c!=''){
                                createMonitorPeticionPago(compInd,caseComp.Parent.R2_CAS_EMA_PASS_Email_Passenger__c,caseComp.ParentId);
                            }else{
                                createMonitorPeticionPago(compInd,caseComp.Parent.R2_CAS_EMA_Email__c,caseComp.ParentId);
                            }
                        }
                        compList.add(compInd);
                    }
                    else{
                        List<Case> caso = [SELECT Id,R2_CAS_EMA_Email__c, Account.PersonContactId, Type, R2_CAS_FOR_carrier_code_oper__c, R2_CAS_FOR_UK__c, R1_CAS_PKL_Idioma__c FROM Case WHERE Id = :caseComp.ParentId];
                        if(R2_CLS_SendEmailClaimsAuto.sendEmailsClaims(caso,false,true)){
                            caso[0].R2_CAS_LOO_Original_Case__c = posPo.isEmpty() ? posComp[0].R2_COM_LOO_Case__r.ParentId : posPo[0].R2_OPY_LOO_File_associated__c ;
                            caso[0].R2_CAS_PKL_Manage__c = 'No pago';
                            caso[0].Status ='Cerrado';
                            caso[0].R2_CAS_PKL_Exp_Pago_Claims_Auto__c= 'No';
                            Database.DMLOptions dmo =new Database.DMLOptions();
                                dmo.AssignmentRuleHeader.UseDefaultRule= true;
                            Database.update(caso[0],dmo);
                        }
                    }
                }
            }
            if((resp.aviosCompensation != null && resp.aviosCompensation != '' && resp.aviosCompensation != 'No_Info') && Integer.valueOf(resp.aviosCompensation)!=0){
                R2_Compensation__c compAvios = createCompensation(caseComp);
                if(compAvios!=null){
                    compAvios.R2_COM_LOO_Case__c = caseComp.Id;
                    compAvios.R2_COM_LOO_Client__c = caseComp.AccountId;
                    compAvios.RecordTypeId = rtPagoAvios;
                    compAvios.R2_COM_PKL_Payment_method__c =  'Avios';
                    compAvios.R2_COM_PKL_Type__c = 'Compensación';
                    compAvios.R2_COM_DIV_Total_Amount__c = Double.valueOf(resp.aviosCompensation);
                    compAvios.R2_COM_PKL_Currency_Claimed__c = 'IBP';
                    insert compAvios;
                    compList.add(compAvios);
                }
            }
            
            System.debug('[[[Despues de la comprobacion de si pago o no pago');
            

            // clientType
            if(resp.clientType!=null) {
                caseComp.R2_CAS_TXT_AUT_Client_type__c = resp.clientType;
            }
            // singularIndicator
            if(resp.singularIndicator!=null) {
                caseComp.R1_CAS_CHK_AUT_Iberia_singular__c = resp.singularIndicator=='Si' ? true : false;
            }
            // clientValue
            if(resp.clientValue!=null) {
                caseComp.R2_CAS_TXT_AUT_Client_value__c = resp.clientValue;
            }
            // frustrationValue
            if(resp.frustrationValue!=null) {
                caseComp.R2_CAS_TXT_AUT_Frustration_value__c = resp.frustrationValue;
            }
            // incidentCodes
            if(resp.incidentCodes!=null) {
                caseComp.R2_CAS_NUM_AUT_Incident_code__c = resp.incidentCodes;
            }
            // notificationMinutes
            if(resp.notificationMinutes!=null) {
                try{
                    caseComp.R2_CAS_NUM_AUT_Advance_notice_time__c = Decimal.valueOf(resp.notificationMinutes);
                }catch(TypeException exc){
                    caseComp.R2_CAS_NUM_AUT_Advance_notice_time__c = 0;
                }
            }
            // delayTime
            if(resp.delayTime!=null) {
                try{
                    caseComp.R2_CAS_NUM_AUT_Delay_time__c = Decimal.valueOf(resp.delayTime);
                }catch(TypeException exc){
                    caseComp.R2_CAS_NUM_AUT_Delay_time__c = 0;
                }
            }
            // carrierCode
            if(resp.carrierCode!=null) {
                caseComp.R2_CAS_TXT_AUT_Operating_company__c = resp.carrierCode;
            }
            // flightNumber
            if(resp.flightNumber!=null) {
                caseComp.R2_CAS_TXT_AUT_Main_Incident_Fligth__c = resp.flightNumber;
            }
            // departureAirport
            if(resp.departureAirport!=null) {
                caseComp.R2_CAS_TXT_AUT_Airport_depart__c = resp.departureAirport;
            }
            // arrivalAirport
            if(resp.arrivalAirport!=null) {
                caseComp.R2_CAS_TXT_AUT_Airport_arrive__c = resp.arrivalAirport;
            }
            // cabinClass
            if(resp.cabinClass!=null) {
                caseComp.R2_CAS_TXT_AUT_Class__c = resp.cabinClass;
            }
            // traveledDistance
            if(resp.traveledDistance!=null) {
                try{
                    caseComp.R2_CAS_NUM_AUT_Distance__c = Decimal.valueOf(resp.traveledDistance);
                }catch(TypeException exc){
                }
            }
            // missedConnection
            if(resp.missedConnection!=null) {
                caseComp.R2_CAS_CHK_AUT_missed_connection__c = resp.missedConnection=='Si' ? true : false;
            }

            if(resp.paxInFlightIndicator == 'KO'){
                caseComp.R2_CAS_CHK_Passenger_Not_Flown__c = true;
            }

            // Ind_Not_Automate
            if(resp.indNotAutomate!=null) {
                caseComp.R2_CAS_CHK_Not_Automate_indicator__c = resp.indNotAutomate=='Si' ? true : false;
            }

            if(resp.notAutomateIndicator!=null) {
                caseComp.R2_CAS_CHK_Not_Automate_indicator_2__c = resp.notAutomateIndicator=='Si' ? true : false;
            }
            if(resp.boardingNumber != null){
                caseComp.R2_CAS_TXT_Boarding_number__c = resp.boardingNumber;
            }

            if(resp.seatRow != null){
                caseComp.R2_CAS_TXT_Seat_row__c = resp.seatRow;
            }

             if(resp.seatLetter != null){
                caseComp.R2_CAS_TXT_Seat_letter__c = resp.seatLetter;
            }

             if(resp.incidentSegmentOrigin != null){
                caseComp.R2_CAS_TXT_Seg_origin_incident__c = resp.incidentSegmentOrigin;
            }

            if(resp.incidentSegmentDestination != null){
                caseComp.R2_CAS_TXT_Seg_destination_incident__c = resp.incidentSegmentDestination;
            }

            if(resp.incidentDescription != null){
                caseComp.R2_CAS_ATXT_Description__c = resp.incidentDescription;
            }
            // indemnization
            if(resp.indemnization!=null) {
                try{
                    caseComp.R2_CAS_DIV_AUT_Compensation__c = Decimal.valueOf(resp.indemnization);
                }catch(TypeException exc){
                    caseComp.R2_CAS_DIV_AUT_Compensation__c = 0;
                }
            }else{
                caseComp.R2_CAS_DIV_AUT_Compensation__c = 0;
            }
            // aviosCompensation
            if(resp.aviosCompensation!=null) {
                try{
                    caseComp.R2_CAS_NUM_AUT_Compensation_in_Avios__c = Decimal.valueOf(resp.aviosCompensation);
                }catch(TypeException exc){
                    caseComp.R2_CAS_NUM_AUT_Compensation_in_Avios__c = 0;
                }
            }else{
                caseComp.R2_CAS_NUM_AUT_Compensation_in_Avios__c = 0;
            }
            // vouchersCompensation
            if(resp.vouchersCompensation!=null) {
                try{
                    caseComp.R2_CAS_DIV_AUT_Compensation_Vouchers__c = Decimal.valueOf(resp.vouchersCompensation);
               }catch(TypeException exc){
                    caseComp.R2_CAS_DIV_AUT_Compensation_Vouchers__c = 0;
                }
            }else{
                caseComp.R2_CAS_DIV_AUT_Compensation_Vouchers__c = 0;
            }

            if(resp.delayType != null){
                if(resp.delayType == 'CNL'){
                    caseComp.Type = 'Cancelación';
                }else{
                    caseComp.Type = 'Retraso';
                }
            }

            update caseComp;

            return compList;

        }
        catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_CLS_Retrieve_Compensation.caller_retrieveCompensation', '', exc.getmessage()+', '+exc.getLineNumber(), 'R2_Baggage__c');
            return null;
        }
    }

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Ismael Yubero Moreno
    Company:        Accenture
    Description:   genera una compensacion con valores comunes.

    IN:
   OUT:

    History:
    <Date>                     <Author>                <Change Description>
    18/12/2017                 jaime Ascanta                    Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    private static R2_Compensation__c createCompensation(Case caseComp){
        try{
            
                R2_Compensation__c compe =  new R2_Compensation__c();
                compe.R2_COM_LOO_Case__c = caseComp.Id;
                compe.R2_COM_LOO_Account__c = caseComp.AccountId;
                compe.R2_COM_LOO_Flight__c = caseComp.R1_CAS_LOO_Flight__c;
                compe.R2_COM_LOO_Incidence__c = caseComp.R1_CAS_LOO_Incidences__c;
                compe.R2_COM_TXT_Passenger_Reason__c = caseComp.Type + ' ' + caseComp.R1_CAS_PKL_Subtype__c;
                compe.R2_COM_TXT_Charge_account__c = caseComp.R1_CAS_TXT_Charge_account__c;
                compe.R2_COM_TXT_Budgetary_center__c = caseComp.R1_CAS_TXT_Budgetary_center__c;
                String aux_ff = caseComp.R2_CAS_TXT_Primary_Frecuent_Flyer_Card__c!=null ? caseComp.R2_CAS_TXT_Primary_Frecuent_Flyer_Card__c : '';
                aux_ff = aux_ff.toUpperCase();
                if(aux_ff.contains('IB')) aux_ff = aux_ff.remove('IB');
                compe.R2_COM_TXT_Frecuent_Flyer__c = aux_ff!='' ? aux_ff : null;
                return compe;
            
        }catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_Retrive_Compensation.createCompensation()', '', exc.getmessage()+', '+exc.getLineNumber(), 'R2_Compensation__c');
            return null;
        }

    }
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Ismael Yubero Moreno
    Company:        Accenture
    Description:    Retrieves Compensations

    IN:
    OUT:

    History:
    <Date>                     <Author>                <Change Description>
    07/11/2017                 Ismael Yubero Moreno    Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    public static GetCompensationWebResponse retrieveCompensation (GetCompensationWebRequest requestWRP, Integer intentos){

        try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');} //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;

            System.debug('*** entra en la integracion retrieveCompensation()');

            String wsMethod = 'R2_Retrive_Compensation';

            if(R1_CS_Endpoints__c.getInstance(wsMethod) == null || !R1_CS_Endpoints__c.getInstance(wsMethod).R1_CHK_Activo__c){
                    return null;
            }

            String token = R1_CLS_Utilities.getCache('local.sessionCache.token');
            if(token ==null){
                if(intentos<3){
                    intentos= intentos+1;
                    R1_CLS_SendCustomerMDM.login();
                    System.debug('Ha funcionado el login');
                    return retrieveCompensation(requestWRP, intentos);
                }else{
                    R1_CLS_LogHelper.generateErrorLog('R2_CLS_Retrieve_Compensation.retrieveCompensation()', '', 'No se puede conectar con Intelligence Integration', 'Compensations');
                    return null;
                }
            }


            HttpRequest req = new HttpRequest();
            String endPoint = R1_CS_Endpoints__c.getInstance(wsMethod).R1_TXT_EndPoint__c;
            String bodyReq = JSON.serialize(requestWRP);
            System.debug('requestWRP:' + requestWRP);
            System.debug('bodyReq: ' + bodyReq);
            req.setHeader('Authorization', 'Bearer ' + token);
            req.setEndpoint(endPoint);
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/json');
            req.setTimeout(8000);
            req.setBody(bodyReq);

            System.debug(req);

            System.debug('*** req body: ' + bodyReq);

            Http http = new Http();
            HTTPResponse res;


            res = http.send(req);

            System.debug('*** response status code: '+res.getStatusCode());
            System.debug('*** response body: '+res.getBody());

            if(res.getStatusCode()==200 || res.getStatusCode()==201 || res.getStatusCode()==202 || res.getStatusCode()==203){

                System.debug('*** antes de deserialize: ');
                GetCompensationWebResponse resp = (GetCompensationWebResponse)JSON.deserialize(res.getBody(),GetCompensationWebResponse.class);

                System.debug('wp to return: '+resp);
                return resp;

            }
            //added for Compensation in Flight to know when a client is not found in CI
            else if(res.getStatusCode() == 400){
                GetCompensationWebResponse resp = (GetCompensationWebResponse)JSON.deserialize(res.getBody(),GetCompensationWebResponse.class);
                if(resp != null){
                    if(!resp.errors.isEmpty()){
                        for(Errors error : resp.errors){
                            if (error.reason == 'No Record Found' && error.code =='SVR_COMP_00002'){
                                resp.paxInFlightIndicator = 'KO';
                            }
                        }
                    }
                }
                
                return resp;
            //  //Si pago
            //  //String result = '{"clientType":"CLASICA","singularIndicator":"Si","clientValue":"5","frustrationValue":"5","incidentCodes":"IB_IC_ID","notificationMinutes":"120","delayTime":"16.0","carrierCode":"IB","flightNumber":"IB_3949_2018-02-05","departureAirport":"SVQ","arrivalAirport":"BRU","cabinClass":"0","traveledDistance":"1240.539","missedConnection":"Si","ind_Not_Automate":"No","indemnization":"100","aviosCompensation":"150","vouchersCompensation":"50"}';
            //  //No pago
            //  String result = '{"clientType":"CLASICA","singularIndicator":"Si","clientValue":"5","frustrationValue":"5","incidentCodes":"IB_IC_ID","notificationMinutes":"120","delayTime":"16.0","carrierCode":"IB","flightNumber":"IB_3949_2018-02-05","departureAirport":"SVQ","arrivalAirport":"BRU","cabinClass":"0","traveledDistance":"1240.539","missedConnection":"Si","ind_Not_Automate":"No","indemnization":"No_Info","aviosCompensation":"No_Info","vouchersCompensation":"No_Info"}';
            //  GetCompensationWebResponse resp = (GetCompensationWebResponse)JSON.deserialize( result ,GetCompensationWebResponse.class);

            //  return resp;
            }
            else{

                if(intentos<3 && res.getStatusCode() == 401){
                    intentos+=1;
                        R1_CLS_SendCustomerMDM.login();
                        return retrieveCompensation(requestWRP, intentos);
                }else{
                    //R1_CLS_LogHelper.generateErrorLog('R2_Retrive_Compensation.retrieveCompensation()', '', res.getBody() , 'Compensations');
                    return null;
                }
            }

        }catch(CalloutException exc){
            if(intentos<3){
                    intentos+=1;
                    return retrieveCompensation(requestWRP, intentos);
            }else{
                System.debug('*** retrieveCompensation() CalloutException  : ' + exc.getmessage());
                return null;
            }

        }catch(Exception exc){

            R1_CLS_LogHelper.generateErrorLog('R2_Retrive_Compensation.retrieveCompensation()', '', exc.getmessage()+', '+exc.getLineNumber(), 'Compensations');
            return null;
        }
    }


    public static void createMonitorPeticionPago(R2_Compensation__c pago,String emailCliente,Id padre){
        System.debug('createMonitorPeticionPago');
        System.debug('pago:' + pago);
       Savepoint sp = Database.setSavepoint();
       try{
			if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}
            Boolean processSuccess = false;

            //Check if the lstPagosRequestDelivery and listaIdsCompesations have values
            if( pago.R2_COM_LOO_Account__c!=null && (emailCliente!=null&&emailCliente!='') ){

                //String that is going to get all the ids of the compensations
                List<String> lstIds = new List<String>();
                //Loop trough all the request payments and add the ids to listaIdsCompesations
                Double totalAmountLocal = Double.valueOf(pago.R2_COM_NUM_Total_Amount_local__c);

                String key = R1_CLS_Utilities.generateHash('SHA-512', pago.Id);

                if( totalAmountLocal>0 && key!=null){
                    //System.debug('Ready to create monitor');
                    //CREATE THE MONITOR WITH THE INFORMATION REQUIRED TO TRIGGER THE PROCCESS BUILDER ASSSIGNED TO THIS PROCESS
                    List<R2_Monitor__c> duplicado = [SELECT Id FROM R2_Monitor__c WHERE R2_MON_LOO_File_associated__c=:padre LIMIT 1];
                    if(duplicado.isEmpty()){
                        R2_Monitor__c monitor = new R2_Monitor__c();
                        monitor.R2_MON_TXT_Key__c = key;
                        monitor.R2_MON_EMA_Email__c = emailCliente;
                        monitor.R2_MON_LOO_File_associated__c = padre;
                        monitor.R2_MON_PKL_Type__c = 'Formulario web pagos';  
                        monitor.R2_MON_PKL_Action__c  = 'Email Alert';
                        monitor.R2_MON_DIV_Amount__c = totalAmountLocal;
                        monitor.R2_MON_TXT_Currency__c = pago.R2_COM_PKL_Currency_Claimed__c;
                        monitor.R2_MON_TXT_Content__c  = pago.Id;
                        System.debug('MONITOR: ' + monitor);

                        //TRY TO INSERT THE MONITOR AND GIVE FEEDBACK TO THE USER DEPENDING ON THE RESULT OF THE OPERATION
                        Database.SaveResult saveResult = Database.insert(monitor, true);
                        System.debug('saveresult:' + saveresult);
                        if(saveResult.isSuccess()){
                            if(CompensacionController.sendEmail(saveResult.getId())){
                                System.debug('Email enviado correctamente.');
                                processSuccess = true;
                                Id padreid =[SELECT Id,ParentId FROM case WHERE Id=:pago.R2_COM_LOO_Case__c].ParentId;
                                if(padreid!=null){
                                    Case caso = new case();
                                    caso.Id=padreid;
                                    caso.R2_CAS_PKL_Exp_Pago_Claims_Auto__c='Si';
                                    caso.R2_CAS_PKL_Manage__c='Pago';
                                    caso.Status='Cerrado';
                                    Database.DMLOptions dmo =new Database.DMLOptions();
                                    dmo.AssignmentRuleHeader.UseDefaultRule= true;
                                    Database.update(caso,dmo);
                                }
                            }
                        }
                    }
                }      
            }

            if(!processSuccess){
                Database.rollback(sp);
            }

        }catch(exception exc){
            Database.rollback(sp);
            R1_CLS_LogHelper.generateErrorLog('CompensacionController.createMonitorPeticionPago()', '', exc.getmessage()+', '+exc.getLineNumber(), '');
        }
   }

    public static R2_Compensation__c fullfillBudgetaryAndChargeAccount(Case caso,R2_Compensation__c pago){
        try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}
            Id idPasaje = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Pasaje').getRecordTypeId();
            Id rtIdEquipaje = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Equipaje').getRecordTypeId();
            String labelpago = Label.R2_LBL_Escoger_Proceso_Pago_SAP_OF;
            System.debug('El proceso va a elegir ' + labelpago );
            if(labelpago == 'OF'){
                labelpago = 'SDP';
            }
            Map<String,String> map_cc = CompensacionController.calculaCentroCuenta(labelpago,caso);
            if(map_cc !=null){
                pago.R2_COM_TXT_Charge_account__c	= map_cc.get('centro');
                pago.R2_COM_TXT_Budgetary_center__c = map_cc.get('cuenta');
            }
            System.debug('pago:' + pago);
            return pago;

        }catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_CLS_CaseTriggerMethods.fullfillBudgetaryAndChargeAccount()', '', exc.getmessage()+', '+exc.getLineNumber(), 'Case');
            return null;
        }
    }
}