public class R2_CLS_SendEmailClaimsAuto {

    public static Boolean sendEmailsClaims(List<Case> listCases,Boolean rebote,Boolean pagoduplicado){
        try{
            Boolean result = false;
            Map<String,Id> maptemplates = new Map<String,Id>();
            for(EmailTemplate template: [SELECT Id, DeveloperName FROM EmailTemplate WHERE Name Like '%AUTOMATICA%' AND  (NOT Name LIKE '%MACRO%')]){
                maptemplates.put(formatNameEmatpl(template.DeveloperName) , template.Id);
            }
            OrgWideEmailAddress orgWideAddress = [SELECT id,Address,Displayname FROM OrgWideEmailAddress WHERE Address ='servicerecoveryteamnoreply@iberia.es'];
            Map<Id, Messaging.SingleEmailMessage> mapCasEmail = new Map<Id, Messaging.SingleEmailMessage>();
            for (Case cas: listCases) {
                String templateName = makeTemplateName(cas,rebote, pagoduplicado);

                if (maptemplates.containsKey(templateName) && cas.R2_CAS_EMA_Email__c!=null) {
                    Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                    email.setOrgWideEmailAddressId(orgWideAddress.Id);
                    email.setTemplateId(maptemplates.get(templateName));
                    email.setTargetObjectId(cas.Account.PersonContactId);
                    email.setWhatId(cas.Id);
                    email.setTreatTargetObjectAsRecipient(false);
                    email.setSaveAsActivity(false);
                    email.setToAddresses(new String[]{cas.R2_CAS_EMA_Email__c});
                    mapCasEmail.put(cas.Id, email);
                }else{
                    System.debug('Template for Cas: ' + cas.Id +' not found.');
                }
            }

            System.debug('mapCasEmail to send:' + mapCasEmail );
            
            if(!mapCasEmail.isEmpty()){
                Messaging.SendEmailResult[] results = Messaging.sendEmail(mapCasEmail.values(), true);
                if (results[0].success) {
                    System.debug('The emails was sent successfully.');
                    try {
                        List<EmailMessage> listEmailMsg = new List<EmailMessage>();
                        for (Case cas : listCases) {
                            if (mapCasEmail.containsKey(cas.Id)) {
                                    EmailMessage e = new EmailMessage();
                                    e.Incoming = false;
                                    e.Status = '3';
                                    e.MessageDate = Datetime.now();
                                    e.ToAddress = String.join(mapCasEmail.get(cas.Id).getToAddresses() ,',');
                                    e.Subject = mapCasEmail.get(cas.Id).getSubject();
                                    e.FromAddress = orgWideAddress.Address;
                                    e.FromName = orgwideaddress.Displayname;
                                    e.HtmlBody = mapCasEmail.get(cas.Id).getHtmlBody();
                                    e.TextBody = mapCasEmail.get(cas.Id).getPlainTextBody();
                                    e.ParentId = cas.Id;
                                    listEmailMsg.add(e);
                            }
                        }
                        if (!listEmailMsg.isEmpty() ) Insert listEmailMsg;
                        result = true;
                    } catch (Exception exc) {
                        R1_CLS_LogHelper.generateErrorLog('R2_CLS_SendEmailClaimsAuto.sendEmailsClaims()', '', exc.getmessage()+', '+exc.getLineNumber(), '');
                    }
                } else {
                    System.debug('The emails failed to send: ' + results[0].errors[0].message);
                }
            }

            return result;

        }catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_CLS_SendEmailClaimsAuto.sendEmailsClaims()', '', exc.getmessage()+', '+exc.getLineNumber(), '');
            return false;
        }
    }

    public static String makeTemplateName(Case cas,Boolean rebote, Boolean pagoduplicado){
        try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}
            String  key = '';       
            key += cas.Type!=null && cas.Type!='' ? cas.Type + '_AUTOMATICA' : '';
            if(rebote){
                key += cas.R2_CAS_NUM_Number_of_rebounds__c!=null ? '_'+ cas.R2_CAS_NUM_Number_of_rebounds__c +'_Rebote' : '';
            }else if(pagoduplicado){
                key += '_NO_PAGO';
            }
            if(cas.R1_CAS_PKL_Idioma__c!=null && cas.R1_CAS_PKL_Idioma__c!='' && cas.R1_CAS_PKL_Idioma__c !='ja' && cas.R1_CAS_PKL_Idioma__c!='zh'){
                key += cas.R1_CAS_PKL_Idioma__c!=null && cas.R1_CAS_PKL_Idioma__c!='' ? '_' + cas.R1_CAS_PKL_Idioma__c : '';
            }
            else if(cas.R1_CAS_PKL_Idioma__c =='ja'){
                key+= '_JP';
            }
            else if(cas.R1_CAS_PKL_Idioma__c =='zh'){
                key+= '_CN';
            }
            if(cas.R2_CAS_FOR_carrier_code_oper__c!=null && cas.R2_CAS_FOR_carrier_code_oper__c!=''){
                if(cas.R2_CAS_FOR_carrier_code_oper__c !='IB' && cas.R2_CAS_FOR_carrier_code_oper__c!='I2' && cas.R2_CAS_FOR_carrier_code_oper__c!='I0' && cas.R2_CAS_FOR_carrier_code_oper__c!='LV'){
                    key+='_IB';
                }
                else{
                    key+='_'+cas.R2_CAS_FOR_carrier_code_oper__c;
                }
            }
            key += cas.R2_CAS_FOR_UK__c==true? ' UK': '';
            key = formatNameEmatpl(key);

            System.debug('template name key: ' + key);

            return key;

        }catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_CLS_SendEmailClaimsAuto.makeTemplateName()', '', exc.getmessage()+', '+exc.getLineNumber(), '');
            return null;
        }
    }

    public static String formatNameEmatpl(String textInput){
        try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}
            String result = textInput;
            System.debug('Antes de modificar: ' + result);
            result = result.toUpperCase();
            result = result.trim();
            result = result.deleteWhitespace();
            result = result.replaceAll('[ÀÁÂÄ]','A');
            result = result.replaceAll('[ÈÉÊË]','E');
            result = result.replaceAll('[ÌÍÎÏ]','I');
            result = result.replaceAll('[ÒÓÔÖ]','O');
            result = result.replaceAll('[ÙÚÛÜ]','U');
            System.debug('Después de modificar: ' + result);
            return result;
        }catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_CLS_SendEmailClaimsAuto.formatNameEmatpl()', '', exc.getmessage()+', '+exc.getLineNumber(), '');
            return null;
        }
    }
}