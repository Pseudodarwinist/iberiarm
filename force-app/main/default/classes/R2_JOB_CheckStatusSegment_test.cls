/*---------------------------------------------------------------------------------------------------------------------
	Author:         Jaime Ascanta
	Company:        Accenture
	Description:    Apex test para la clase apex 'R2_SCH_Retrive_Compensation'
	IN:
	OUT:

	History:
	 <Date>                     <Author>                         <Change Description>
	08/11/2017       	   				Jaime Ascanta                   Initial Version
----------------------------------------------------------------------------------------------------------------------*/
@isTest
private class R2_JOB_CheckStatusSegment_test {


	/*---------------------------------------------------------------------------------------------------------------------
	Author:         Jaime Ascanta
	Company:        Accenture
	Description:    Metodo test para la funcion execute()
	IN:
	OUT:

	History:
	 <Date>                     <Author>                         <Change Description>
	 08/11/2017       	   				Jaime Ascanta                   Initial Version
	----------------------------------------------------------------------------------------------------------------------*/
	@isTest
	static void execute_Test() {

		R1_CLS_LogHelper.throw_exception = false;

		Test.startTest();

			String CRON_EXP = '0 0 0 3 9 ? 2022';
			String CRON_EXP_IN_DATETIME = '2022-09-03 00:00:00';
			String CRON_JOB_NAME = 'R2_JOB_CheckStatusSegment';

			// Schedule the test job
			String jobId = System.schedule(CRON_JOB_NAME, CRON_EXP, new R2_JOB_CheckStatusSegment());
			// Get the information from the CronTrigger API object
			CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime, CronJobDetailId FROM CronTrigger WHERE Id = :jobId];
			//verica que se agrego la tarea
			System.assertNotEquals(null, ct);
			//verificamos nombre tarea
			CronJobDetail cjd = [SELECT Name FROM CronJobDetail WHERE Id= :ct.CronJobDetailId];
			System.assertEquals(CRON_JOB_NAME, cjd.Name);
			// verifica la expresion
			System.assertEquals(CRON_EXP, ct.CronExpression);
			// verifica la froxima fecha de ejecucion
		    System.assertEquals(CRON_EXP_IN_DATETIME, String.valueOf(ct.NextFireTime));
			// verifica que el trabajo no se ha ejecutado
		    System.assertEquals(0, ct.TimesTriggered);
		Test.stopTest();
		
		System.assertEquals(0, [SELECT count() FROM R1_Log__c]);
	}


	@testSetup
	static void setupTest(){

		R2_Prorrateo__c pro = new R2_Prorrateo__c();
		pro.R2_PRO_PKL_Status__c = 'Prorrateo Ok';
		insert pro;

		List<R2_Segmento__c> listSeg = new List<R2_Segmento__c>();

		R2_Segmento__c seg = new R2_Segmento__c();
		seg.R2_SEG_PKL_Estado__c = 'Prorrateo OK';
		seg.R2_SEG_MSDT_Prorrate__c = pro.Id;
		//seg.R2_SEG_PKL_Segment_Status__c = 'Aceptado';
		seg.R2_SEG_TXT_Company__c = 'Iberia';
		seg.R2_SEG_DAT_Date_Msg_Sent__c = Date.today().addDays(-62);
		listSeg.add(seg);

		R2_Segmento__c seg2 = new R2_Segmento__c();
		seg2.R2_SEG_PKL_Estado__c = 'Prorrateo OK';
		seg2.R2_SEG_MSDT_Prorrate__c = pro.Id;
		seg2.R2_SEG_PKL_Segment_Status__c = 'Rechazado';
		seg2.R2_SEG_TXT_Company__c = 'Iberia';
		seg2.R2_SEG_DAT_Date_Msg_Sent__c = Date.today().addDays(-61);
		listSeg.add(seg2);

		R2_Segmento__c seg3 = new R2_Segmento__c();
		seg3.R2_SEG_PKL_Estado__c = 'Prorrateo OK';
		seg3.R2_SEG_MSDT_Prorrate__c = pro.Id;
		seg3.R2_SEG_PKL_Segment_Status__c = 'Aceptado';
		seg3.R2_SEG_TXT_Company__c = 'Iberia';
		seg3.R2_SEG_DAT_Date_Msg_Sent__c = Date.today().addDays(-10);
		listSeg.add(seg3);

		insert listSeg;

		System.assertEquals(0, [SELECT count() FROM R1_Log__c]);
	}
}