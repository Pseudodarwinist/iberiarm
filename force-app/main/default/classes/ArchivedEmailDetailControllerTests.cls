@IsTest
private with sharing class ArchivedEmailDetailControllerTests {
    
    
    @IsTest
    static void validateInstantiation() {

        Test.startTest();

        String id = 'ABC1234';
        ApexPages.currentPage().getParameters().put('id', id);

        ArchivedEmailDetailController controller = new ArchivedEmailDetailController();
        String controllerId = controller.id;
        System.assert(controllerId.equals(id), 'Parameter ID is not correct');

        Test.stopTest();  
    }
}