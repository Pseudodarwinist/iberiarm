public class R2_CLS_EmailSocialPost_ctrl {
	public Id caseId {get;set;}

    public List<SocialPost> getSocialPosts()
    {
        List<SocialPost> socialposts;
        socialposts = [SELECT Handle, ParentId, Name, Posted,Content FROM SocialPost WHERE ParentId =: caseId ORDER BY Posted];
        return socialposts;
    }
    
}