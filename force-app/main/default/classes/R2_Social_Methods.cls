global class R2_Social_Methods {
	/*-----------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Raúl Julián González
    Company:        Accenture
    Description:    Update the Owner of the case for Social_Customer_Service

    IN:     
    OUT:    

    History:
    <Date>                      <Author>                         <Change Description>
    11/09/2018               Raúl Julián González                 Initial Version
    -----------------------------------------------------------------------------------------*/

    webservice static String assignNewCaseForPost(String postid){
    	System.debug('assignNewCaseForPost');
    	try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');} //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;

    		SocialPost post = [Select id, Personaid, Parentid, Provider, Classification, Name, Sentiment, PostPriority, 
    								R2_SP_CHK_Influencer__c, PostTags
    							from SocialPost where id=:postid];
    		List<SocialPersona> persona = [Select id, Parentid from SocialPersona where id=:post.personaid];
    		Case caso = new Case();
    		caso.Id = post.ParentId;
    		caso.Status = 'Cerrado';
    		update caso;
    		Id rtIdComEnt = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Comunicaciones entrantes').getRecordTypeId();
			if(post.Classification != 'General' && String.isNotBlank(post.Classification) && !persona.isEmpty()){// post.Classification != null && post.Classification !=''){
				String caseSubject = '';
				Case newCase = new Case(R2_CAS_PKL_Client_RRSS_Initial_Feeling__c = post.Sentiment, R2_CAS_PKL_Client_RRSS_Final_Feeling__c = post.Sentiment, 
										Priority = post.PostPriority);
				newCase.RecordTypeId= rtIdComEnt; 
				if(post.R2_SP_CHK_Influencer__c==true){

					newCase.R2_CAS_CHK_Influencer__c=true;
				}
				//almacena en un set las etiquetas introducidas en el post
				List<String> tags = new List<String>();
				tags.addAll(getPostTags(post));
				if(!tags.isEmpty()){
					Map<String,String> fieldIdioma_Map = createLanguagePicklistValues();
					Set<String> category_Set = createCategoryPicklistValues();
					Set<String> type_Set = createTypePicklistValues();
					System.debug('!!!fieldIdioma_Map: ' + fieldIdioma_Map);
					System.debug('!!!category_Set: ' + category_Set);
					System.debug('!!!type_Set: ' + type_Set);
					for (String tag : tags) {
						if(!fieldIdioma_Map.containsKey(tag) && !category_Set.contains(tag) && !type_Set.contains(tag)){
							if(caseSubject != ''){
								caseSubject += ', ';
							}
							caseSubject += tag;
						}else{
							if (fieldIdioma_Map.containsKey(tag)) {
							newCase.R1_CAS_PKL_Idioma__c = fieldIdioma_Map.get(tag);
							}
							if (category_Set.contains(tag)) {
									newCase.R2_CAS_PKL_Vip_Type__c = tag;
							}
							if (type_Set.contains(tag)) {
									newCase.Type= tag;
							}
						}
					}
					if(caseSubject != ''){
						newCase.Subject = caseSubject;
					}else{
						newCase.Subject = post.Name;
					}
				}	
				Group tinkle = [Select id from Group Where Type='Queue' and Name='Tinkle'];
				newCase.OwnerId = tinkle.Id;
				//newCase.R1_CAS_PKL_Idioma__c = tags.get(tags.size() -1);	
				
				if (persona[0].ParentId != null) {
					if (persona[0].ParentId.getSObjectType() == Contact.sObjectType) {
						Account acc = [SELECT Id, PersonContactId FROM Account WHERE PersonContactId = :persona[0].ParentId LIMIT 1];
						newCase.AccountId = acc.Id;
						newCase.ContactId = acc.PersonContactId;
					} else if (persona[0].ParentId.getSObjectType() == Account.sObjectType) {
						Account acc = [SELECT Id, PersonContactId FROM Account WHERE Id = :persona[0].ParentId LIMIT 1];
						newCase.AccountId = acc.Id;
						newCase.ContactId = acc.PersonContactId;
					}
				}
				if (post != null && post.Provider != null) {
					newCase.Origin='Redes sociales';
					newCase.R2_CAS_TXT_Social_Network__c = post.Provider;
				}
				Entitlement ent = [Select id from Entitlement where name='Casos RRSS' limit 1];
				newCase.EntitlementId = ent.id;
				insert newCase;
				post.parentId = newcase.id;
				update post;
				return 'OK';
			}
    		return 'KO';
    	}
    	catch(Exception exc){
               R1_CLS_LogHelper.generateErrorLog('R2_Social_Methods.assignNewCaseForPost()', '', exc.getmessage()+', '+exc.getLineNumber(), 'Case');
                return 'ERROR';
        }
    }

    private static Set<String> getPostTags(SocialPost post){
		System.debug('!!!getPostTags, post: ' + post);
		Set<String> postTags = new Set<String>();
		if(post.postTags != null)
			postTags.addAll(post.postTags.split(',', 0));
		return postTags;
	}

	private static Map<String,String> createLanguagePicklistValues() {
		Map<String,String> fieldIdioma_Map = new Map<String,String>();
		fieldIdioma_Map.put('German', 'de');
		fieldIdioma_Map.put('Chinese', 'zh');
		fieldIdioma_Map.put('Spanish', 'es');
		fieldIdioma_Map.put('French', 'fr');
		fieldIdioma_Map.put('English', 'en');
		fieldIdioma_Map.put('Italian', 'it');
		fieldIdioma_Map.put('Japanese', 'ja');
		fieldIdioma_Map.put('Portuguese', 'pt');
		fieldIdioma_Map.put('Others', 'Otros');
		return fieldIdioma_Map;
	}

	private static Set<String> createCategoryPicklistValues() {
		Schema.DescribeFieldResult fieldCategoria = Case.R2_CAS_PKL_Vip_Type__c.getDescribe();
		List<Schema.PicklistEntry> fieldCategoria_Lst = fieldCategoria.getPicklistValues();
		Set<String> category_Set = new Set<String>();
		for (Schema.PicklistEntry categ : fieldCategoria_Lst) {
			category_Set.add(categ.getValue());
		}
		return category_Set;
	}

	private static Set<String> createTypePicklistValues() {
		Schema.DescribeFieldResult fieldType = Case.Type.getDescribe();
		List<Schema.PicklistEntry> fieldType_Lst = fieldType.getPicklistValues();
		Set<String> type_Set = new Set<String>();
		for (Schema.PicklistEntry typ : fieldType_Lst) {
			type_Set.add(typ.getValue());
		}
		return type_Set;
	}

	/*-----------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Raúl Julián González
    Company:        Accenture
    Description:    Create a new Case Consulta an associate as a child of the case introduce by parameters

    IN:     
    OUT:    

    History:
    <Date>                      <Author>                         <Change Description>
    14/09/2018               Raúl Julián González                 Initial Version
    -----------------------------------------------------------------------------------------*/
	webservice static String assignConsultaToDepart(String casoId,String tipoDpto){
		System.debug('assignConsultaToDepart');
        try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');} //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;
        	Id rtIdCon = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Consulta').getRecordTypeId();
        	Id rTAsistencia = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Asistencia').getRecordTypeId();
        	Id rTIberiaCom = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Iberia.com').getRecordTypeId();
            String camposCaso = R1_CLS_Utilities.dynamicQuery('Case');
            String query = camposCaso + ' from Case where id =\'' + casoId+'\'';
            System.debug (query);
            List<Case> lstCasos = Database.query(query);
            if(!lstCasos.isEmpty()){
            	List<Case> toUpdate = new List<Case>();
            	if (lstCasos[0].Status == 'Cerrado') {
            		return 'Cerrado';
            	}
            	else {
            		Database.DMLOptions dmo =new Database.DMLOptions();
                    dmo.AssignmentRuleHeader.UseDefaultRule= true;
            		if (lstCasos[0].Status != 'Abierto a la espera de info de Iberia') {
			            Case casoEntrante = new Case();
			            casoEntrante.Id = casoId;
			            casoEntrante.Status = 'Abierto a la espera de info de Iberia';
			            casoEntrante.setOptions(dmo);
			            toUpdate.add(casoEntrante);

			        }
	            	Case consult = lstCasos[0].clone(false,false,false,false);
	            	if (tipoDpto == 'CCPO') {
	            		consult.RecordTypeId = rTAsistencia;
	            		consult.R1_CAS_PCK_Group__c = 'Grupo Asistencia';
	            	}
	            	else if (tipoDpto == 'Claims') { 
	            		consult.RecordTypeId = rtIdCon;
	            	}
	            	else if (tipoDpto == 'Digital Support') { 
	            		consult.RecordTypeId = rTIberiaCom;
	            	}
	            	else if (tipoDpto == 'Serviberia') { 
						consult.RecordTypeId = rTAsistencia;
						consult.R1_CAS_PCK_Group__c = Label.R3_LB_GrupoServiberia;
	            	}
	            	else if (tipoDpto == 'Iberia Plus'){
	            		consult.RecordTypeId = rTAsistencia;
	            		consult.OwnerId = [SELECT id FROM Group WHERE Name ='Grupo Iberia Plus Clientes'].Id;
	            	}
	            	consult.EntitlementId = null;
	            	consult.ParentId = lstCasos[0].id;
	            	consult.R2_CAS_TXT_Social_Type__c = tipoDpto;
	            	consult.Status = 'Abierto';
	            	if(String.isBlank(consult.R2_CAS_PKL_Vip_Type__c)){
	            		consult.R2_CAS_PKL_Vip_Type__c = 'Estándar';
	            	}
                    consult.setOptions(dmo);
                    toUpdate.add(consult);
	            }
	            if(!toUpdate.isEmpty()){
	            	Schema.SObjectField idCaso = Case.Fields.Id;
                	Database.upsert(toUpdate,idCaso,true);
                	return 'OK';
                }
                else{
                	return 'KO';
                }
			}
			return 'KO';
        }
        catch(Exception exc){
               R1_CLS_LogHelper.generateErrorLog('R2_Social_Methods.assignConsultaToDepart()', '', exc.getmessage()+', '+exc.getLineNumber(), 'Case');
                return 'KO';
        }
	}

	webservice static String createExpRRSS(String casoId){
		System.debug('createExpRRSS');
        try{

            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');} //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;
        	Group tinkle = [SELECT id FROM Group WHERE Type='Queue' and Name='Tinkle'];
        	Group redes = [SELECT Id FROM group WHERE Type='Queue' and Name ='SYR Redes'];
        	Id rtIdExp = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Expediente').getRecordTypeId();
            String camposCaso = R1_CLS_Utilities.dynamicQuery('Case');
            String query = camposCaso + ' from Case where id =\'' + casoId+'\'';
            System.debug (query);
            List<Case> lstCasos = Database.query(query);
            if(!lstCasos.isEmpty()){
            	if (lstCasos[0].Status == 'Cerrado' || lstCasos[0].OwnerId == tinkle.Id ) {
            		return 'KO';
            	}
            	else {
	            	Case exp = lstCasos[0].clone(false,false,false,false);
	            	exp.RecordTypeId = rtIdExp;
	            	exp.ParentId= null;
	            	exp.R2_CAS_PKL_Vip_Type__c = 'Redes';
                    insert exp;
                    lstCasos[0].ParentId = exp.Id;
                    update lstCasos;
                    return String.valueOf(exp.Id);
	            }
			}
			return 'KO';
        }
        catch(Exception exc){
               R1_CLS_LogHelper.generateErrorLog('R2_Social_Methods.createExpRRSS()', '', exc.getmessage()+', '+exc.getLineNumber(), 'Case');
                return 'KO';
        }
	}

/*-----------------------------------------------------------------------------------------------------------------------------------------------------
Author:         Eva Sanchez Ruiz
Company:        Accenture
Description:    Create a new button to know who closed the case.

IN:     
OUT:    

History:
<Date>                      <Author>                         <Change Description>
27/09/2018               Eva Sanchez Ruiz                       Initial Version
-----------------------------------------------------------------------------------------*/
	webservice static String botonCerrarCaso(String casoId, String estado){
		System.debug('assignConsultaToDepart');
		try{
	        if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');} //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;
	        System.debug('Estado:  ' + estado);

	        Map<String,String> mapColas = new Map<String,String>();
	        List<Group> lstColas = new List<Group>();
	        lstColas = [SELECT id,Name from Group where Type = 'Queue'];
	        for(Group cola : lstColas){
	            mapColas.put(cola.Name,cola.id);
	        }

        	if (estado != 'Cerrado') {
        		
        		for (Case cas : [SELECT Id, Status, OwnerId FROM Case WHERE parentID =:casoId]){

        			if(!(cas.OwnerId == mapColas.get('Tinkle') || cas.OwnerId == mapColas.get('Cerrados') || cas.Status == 'Cerrado')){

			        	return 'KOHijos';
			        			
			        }
		        }
		        List<CaseMilestone> cmsToUpdate = [SELECT Id, completionDate FROM CaseMilestone 
													WHERE caseId =: casoId 
													AND MilestoneType.Name = 'Tiempo de resolucion'
													AND completionDate = null LIMIT 1];
            	System.debug('!!!completeMilestone, cmsToUpdate before' + cmsToUpdate);									
		        if (!cmsToUpdate.isEmpty()){
		        	cmsToUpdate[0].completionDate = Datetime.now();
		        	System.debug('!!!completeMilestone, cmsToUpdate after' + cmsToUpdate);		
		        	update cmsToUpdate;
				}
        		Case casUpdate = new Case();
        		casUpdate.Id = casoId;
        		casUpdate.Status = 'Cerrado';
        		casUpdate.OwnerId =  mapColas.get('Tinkle');
        		casUpdate.R2_CAS_TXT_Closed_by__c = UserInfo.getName();
        		update casUpdate;
    			
        		return 'OK';
        	}else{

	        return 'KOCerrado';
        	}
	        
 //consulte x reporte
		}
		catch(Exception exc){
			R1_CLS_LogHelper.generateErrorLog('R2_Social_Methods.assignConsultaToDepart()', '', exc.getmessage()+', '+exc.getLineNumber(), 'Case');
			return 'KOReporte';
		}
	}
}