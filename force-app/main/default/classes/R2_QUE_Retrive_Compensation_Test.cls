/*---------------------------------------------------------------------------------------------------------------------
	Author:         Jaime Ascanta
	Company:        Accenture
	Description:    Apex test para la clase apex 'R2_QUE_Retrive_Compensation'
	IN:
	OUT:

	History:
	 <Date>                     <Author>                         <Change Description>
	08/11/2017       	   				Jaime Ascanta                   Initial Version
----------------------------------------------------------------------------------------------------------------------*/
@isTest
private class R2_QUE_Retrive_Compensation_Test {

	/*---------------------------------------------------------------------------------------------------------------------
	Author:         Jaime Ascanta
	Company:        Accenture
	Description:    Metodo test para la funcion execute()
	IN:
	OUT:

	History:
	 <Date>                     <Author>                         <Change Description>
	 08/11/2017       	   			Jaime Ascanta                   Initial Version
	----------------------------------------------------------------------------------------------------------------------*/
	@isTest static void execute_Test() {

		R1_CLS_LogHelper.throw_exception = false;

		Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());

		// cuenta
		Id accRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente').getRecordTypeId();
		Account acc = new Account();
		acc.RecordTypeId = accRT;
		acc.LastName = 'ClienteTest';
		acc.PersonEmail = 'test@test.com';
		acc.R1_ACC_PKL_Gender_description__c = 'M';
		acc.R1_ACC_TLF_Phone_Marketing__c = '123456789';
		acc.R1_ACC_PKL_identification_Type__c = '02';
		acc.R1_ACC_TXT_Identification_number__c = '123456789';
		insert acc;

		// vuelo
		R1_Flight__c vuelo = new R1_Flight__c();
		vuelo.Name = 'VueloTest';
		vuelo.R1_FLG_TXT_Origin__c = 'origen';
		vuelo.R1_FLG_TXT_Destination__c = 'destino';
		vuelo.R1_FLG_TXT_Airport_depart__c = 'FRA';
		vuelo.R1_FLG_TXT_Airport_arrive__c = 'MAD';
		vuelo.R1_FLG_TXT_Carrier_code__c = 'IB';
		vuelo.R1_FLG_DAT_Flight_date_local__c = Date.newInstance(2017, 12, 25);
		vuelo.R1_FLG_TXT_Flight_number__c = '1111';
		vuelo.R2_FLG_TXT_Stop_automation__c = false;
		insert vuelo;


		Id rtExpediente = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Expediente').getRecordTypeId();
		Case casoPa = new Case();
		casoPa.RecordTypeId = rtExpediente;
		casoPa.Status = 'Abierto';
		casoPa.Origin = 'Email';
		casoPa.Type = 'Retraso';
		insert casoPa;

		// caso hijo
		Id rtPasaje = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Pasaje').getRecordTypeId();
		Case cas = new Case();
		cas.RecordTypeId = rtPasaje;
		cas.AccountId = acc.Id;
		cas.ParentId = casoPa.Id;
		cas.R1_CAS_LOO_Flight__c = vuelo.Id;
		cas.Status = 'Abierto';
		cas.Origin = 'Email';
		cas.Type = 'Retraso';
		cas.R1_CAS_TXT_PNR__c = 'H00E7';
		cas.R2_CAS_TXT_TKT_Ticket__c = '0752368205835';
		cas.R2_CAS_PKL_Country__c = 'DE';
		cas.R2_CAS_TXT_Primary_Frecuent_Flyer_Card__c = '12345678';
		cas.R2_CAS_CHK_AUT_Do_not_automate_flag__c = false;
		System.debug(rtPasaje);
		System.debug(cas);
		System.debug(cas.Id);
		insert cas;


		// endpoint
		List<R1_CS_Endpoints__c> lst_ep = new List<R1_CS_Endpoints__c>();
		R1_CS_Endpoints__c ep=new R1_CS_Endpoints__c();
		ep.Name='R2_Retrive_Compensation';
		ep.R1_CHK_Activo__c=true;
		ep.R1_TXT_EndPoint__c='retriveCompensationAvios';
		lst_ep.add(ep);
		R1_CS_Endpoints__c ep2 = new R1_CS_Endpoints__c();
		ep2.Name = 'ETL_Login';
		ep2.R1_CHK_Activo__c = true;
		ep2.R1_TXT_EndPoint__c = 'PruebaLogin';
		lst_ep.add(ep2);
		insert lst_ep;

		//Id rtPasaje = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Pasaje').getRecordTypeId();

		List<Case> casList = [SELECT Id, ParentId, R1_CAS_TXT_PNR__c, R2_CAS_TXT_TKT_Ticket__c, 
									R1_CAS_LOO_Flight__c,R1_CAS_LOO_Incidences__c,AccountId, Type, 
									R1_CAS_PKL_Subtype__c, R1_CAS_TXT_Charge_account__c, R1_CAS_TXT_Budgetary_center__c, 
									R2_CAS_FOR_Destination__c, R2_CAS_CHK_AUT_Do_not_automate_flag__c, R2_CAS_PKL_Country__c,
									R2_CAS_TXT_Primary_Frecuent_Flyer_Card__c 
							FROM Case WHERE RecordTypeId =: rtPasaje
							LIMIT 1];

		Test.startTest();
			System.enqueueJob(new R2_QUE_Retrive_Compensation(casList));
		Test.stopTest();

		// enqueueJob se ejecuta cuando termina el test, ¿Se ha insertado correctamente la compansacion?
		//List<R2_Compensation__c> compList = [SELECT id, R2_COM_DIV_Total_Amount__c FROM R2_Compensation__c WHERE R2_COM_LOO_Case__c=:casList[0].Id];
		//System.assertEquals(true, !compList.isEmpty() );
		//System.assertNotEquals(0, compList[0].R2_COM_DIV_Total_Amount__c );

		// Se ha actualizado el caso padre ?
		//casList = [SELECT Id, R2_CAS_PKL_Exp_Pago_Claims_Auto__c FROM Case WHERE Id=:casList[0].ParentId ];
		//System.assertEquals('Si', casList[0].R2_CAS_PKL_Exp_Pago_Claims_Auto__c);

		//System.assertEquals(0, [SELECT count() FROM R1_Log__c]);

	}

/*---------------------------------------------------------------------------------------------------------------------
	Author:         Victor Garcia Barquilla
	Company:        Accenture
	Description:    Metodo test para la funcion execute()
	IN:
	OUT:

	History:
	 <Date>                     <Author>                         <Change Description>
	 15/10/2018					Victor Garcia Barquilla				Initial version
	----------------------------------------------------------------------------------------------------------------------*/
	@isTest static void execute_Test2() {

		R1_CLS_LogHelper.throw_exception = false;

		Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());

		Id accRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente').getRecordTypeId();
		Account acc = new Account();
		acc.RecordTypeId = accRT;
		acc.LastName = 'ClienteTest';
		acc.PersonEmail = 'test@test.com';
		acc.R1_ACC_PKL_Gender_description__c = 'M';
		acc.R1_ACC_TLF_Phone_Marketing__c = '123456789';
		acc.R1_ACC_PKL_identification_Type__c = '02';
		acc.R1_ACC_TXT_Identification_number__c = '123456789';
		insert acc;

		// vuelo
		R1_Flight__c vuelo = new R1_Flight__c();
		vuelo.Name = 'VueloTest';
		vuelo.R1_FLG_TXT_Origin__c = 'origen';
		vuelo.R1_FLG_TXT_Destination__c = 'destino';
		vuelo.R1_FLG_TXT_Airport_depart__c = 'FRA';
		vuelo.R1_FLG_TXT_Airport_arrive__c = 'MAD';
		vuelo.R1_FLG_TXT_Carrier_code__c = 'IB';
		vuelo.R1_FLG_DAT_Flight_date_local__c = Date.newInstance(2017, 12, 25);
		vuelo.R1_FLG_TXT_Flight_number__c = '1111';
		vuelo.R2_FLG_TXT_Stop_automation__c = false;
		insert vuelo;

		Id rtExpediente = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Expediente').getRecordTypeId();
		Case casoPa = new Case();
		casoPa.RecordTypeId = rtExpediente;
		casoPa.AccountId = acc.Id;
		casoPa.R1_CAS_LOO_Flight__c = vuelo.Id;
		casoPa.Status = 'Abierto';
		casoPa.Origin = 'Email';
		casoPa.Type = 'Retraso';
		casoPa.R1_CAS_TXT_PNR__c = 'H00E7';
		casoPa.R2_CAS_TXT_TKT_Ticket__c = '0752368205835';
		casoPa.R2_CAS_PKL_Country__c = 'DE';
		casoPa.R2_CAS_TXT_Primary_Frecuent_Flyer_Card__c = '12345678';
		casoPa.R2_CAS_CHK_AUT_Do_not_automate_flag__c = false;
		casoPa.R1_CAS_PKL_Subtype__c = '10a)pricing';
		casoPa.R1_CAS_TXT_Charge_account__c = 'test';
		casoPa.R1_CAS_TXT_Budgetary_center__c = 'test';
		System.debug(casoPa);
		System.debug(casoPa.Id);
		insert casoPa;

		// caso hijo
		Id rtPasaje = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Pasaje').getRecordTypeId();
		Case cas = new Case();
		cas.AccountId = acc.Id;
		cas.R1_CAS_LOO_Flight__c = vuelo.Id;
		cas.Type = 'Retraso';
		cas.RecordTypeId = rtPasaje;
		cas.ParentId = casoPa.Id;
		cas.R1_CAS_PKL_Subtype__c = '10a)pricing';
		cas.R1_CAS_TXT_Charge_account__c = 'test';
		cas.R1_CAS_TXT_Budgetary_center__c = 'test';
		cas.R2_CAS_TXT_Primary_Frecuent_Flyer_Card__c = '12345678';
		System.debug(rtPasaje);
		System.debug(cas);
		System.debug(cas.Id);
		insert cas;

		R1_Incident__c incidencia = new R1_Incident__c();
        incidencia.R1_INC_LOO_Case__c = cas.Id;
        incidencia.R2_INC_LOO_Case__c = casoPa.Id;
        insert incidencia;

		List<R1_CS_Endpoints__c> lst_ep = new List<R1_CS_Endpoints__c>();
		R1_CS_Endpoints__c ep=new R1_CS_Endpoints__c();
		ep.Name='R2_Retrive_Compensation';
		ep.R1_CHK_Activo__c=true;
		ep.R1_TXT_EndPoint__c='retriveCompensationAvios2';
		lst_ep.add(ep);
		R1_CS_Endpoints__c ep2 = new R1_CS_Endpoints__c();
		ep2.Name = 'ETL_Login';
		ep2.R1_CHK_Activo__c = true;
		ep2.R1_TXT_EndPoint__c = 'PruebaLogin';
		lst_ep.add(ep2);
		insert lst_ep;

		System.debug(cas.Parent.R1_CAS_TXT_PNR__c);
		
		System.debug(rtPasaje);
		List<Case> casList = [SELECT Id, ParentId, Parent.R1_CAS_TXT_PNR__c, Parent.R2_CAS_TXT_TKT_Ticket__c, 
									Parent.R1_CAS_LOO_Flight__c, R1_CAS_LOO_Flight__c, Parent.AccountId, AccountId, Parent.Type, Type, 
									Parent.R1_CAS_PKL_Subtype__c, R1_CAS_PKL_Subtype__c, Parent.R1_CAS_TXT_Charge_account__c, R1_CAS_TXT_Charge_account__c,  Parent.R1_CAS_TXT_Budgetary_center__c, R1_CAS_TXT_Budgetary_center__c, 
									Parent.R2_CAS_FOR_Destination__c, Parent.R2_CAS_CHK_AUT_Do_not_automate_flag__c, Parent.R2_CAS_PKL_Country__c,
									Parent.R2_CAS_TXT_Primary_Frecuent_Flyer_Card__c, R2_CAS_TXT_Primary_Frecuent_Flyer_Card__c,  R1_CAS_LOO_Incidences__c 
							FROM Case WHERE RecordTypeId =: rtPasaje
							];

		System.debug(casList);

		Test.startTest();
			System.enqueueJob(new R2_QUE_Retrive_Compensation(casList));
		Test.stopTest();
		
		//List<R2_Compensation__c> compList = [SELECT id, R2_COM_DIV_Total_Amount__c FROM R2_Compensation__c WHERE R2_COM_LOO_Case__c=:casList[0].Id];
		
		//casList = [SELECT Id, R2_CAS_PKL_Exp_Pago_Claims_Auto__c FROM Case WHERE Id=:casList[0].ParentId ];

	}

/*---------------------------------------------------------------------------------------------------------------------
	Author:         Victor Garcia Barquilla
	Company:        Accenture
	Description:    Metodo test para la funcion execute()
	IN:
	OUT:

	History:
	<Date>                     <Author>                         <Change Description>
	17/10/2018				Victor Garcia Barquilla				Initial version
	----------------------------------------------------------------------------------------------------------------------*/
	@isTest static void execute_Test3() {

		R1_CLS_LogHelper.throw_exception = false;

		Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());

		Id accRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente').getRecordTypeId();
		Account acc = new Account();
		acc.RecordTypeId = accRT;
		acc.LastName = 'ClienteTest';
		acc.PersonEmail = 'test@test.com';
		acc.R1_ACC_PKL_Gender_description__c = 'M';
		acc.R1_ACC_TLF_Phone_Marketing__c = '123456789';
		acc.R1_ACC_PKL_identification_Type__c = '02';
		acc.R1_ACC_TXT_Identification_number__c = '123456789';
		insert acc;

		// vuelo
		R1_Flight__c vuelo = new R1_Flight__c();
		vuelo.Name = 'VueloTest';
		vuelo.R1_FLG_TXT_Origin__c = 'origen';
		vuelo.R1_FLG_TXT_Destination__c = 'destino';
		vuelo.R1_FLG_TXT_Airport_depart__c = 'FRA';
		vuelo.R1_FLG_TXT_Airport_arrive__c = 'MAD';
		vuelo.R1_FLG_TXT_Carrier_code__c = 'IB';
		vuelo.R1_FLG_DAT_Flight_date_local__c = Date.newInstance(2017, 12, 25);
		vuelo.R1_FLG_TXT_Flight_number__c = '1111';
		vuelo.R2_FLG_TXT_Stop_automation__c = false;
		insert vuelo;

		Id rtExpediente = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Expediente').getRecordTypeId();
		Case casoPa = new Case();
		casoPa.RecordTypeId = rtExpediente;
		casoPa.AccountId = acc.Id;
		casoPa.R1_CAS_LOO_Flight__c = vuelo.Id;
		casoPa.Status = 'Abierto';
		casoPa.Origin = 'Email';
		casoPa.Type = 'Retraso';
		casoPa.R1_CAS_TXT_PNR__c = 'H00E7';
		casoPa.R2_CAS_TXT_TKT_Ticket__c = '0752368205835';
		casoPa.R2_CAS_PKL_Country__c = 'DE';
		casoPa.R2_CAS_TXT_Primary_Frecuent_Flyer_Card__c = '12345678';
		casoPa.R2_CAS_CHK_AUT_Do_not_automate_flag__c = false;
		casoPa.R1_CAS_PKL_Subtype__c = '10a)pricing';
		casoPa.R1_CAS_TXT_Charge_account__c = 'test';
		casoPa.R1_CAS_TXT_Budgetary_center__c = 'test';
		System.debug(casoPa);
		System.debug(casoPa.Id);
		insert casoPa;

		// caso hijo
		Id rtPasaje = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Pasaje').getRecordTypeId();
		Case cas = new Case();
		cas.AccountId = acc.Id;
		cas.R1_CAS_LOO_Flight__c = vuelo.Id;
		cas.Type = 'Retraso';
		cas.RecordTypeId = rtPasaje;
		cas.ParentId = casoPa.Id;
		cas.R1_CAS_PKL_Subtype__c = '10a)pricing';
		cas.R1_CAS_TXT_Charge_account__c = 'test';
		cas.R1_CAS_TXT_Budgetary_center__c = 'test';
		cas.R2_CAS_TXT_Primary_Frecuent_Flyer_Card__c = '12345678';
		System.debug(rtPasaje);
		System.debug(cas);
		System.debug(cas.Id);
		insert cas;

		R1_Incident__c incidencia = new R1_Incident__c();
        incidencia.R1_INC_LOO_Case__c = cas.Id;
        incidencia.R2_INC_LOO_Case__c = casoPa.Id;
        insert incidencia;

		List<R1_CS_Endpoints__c> lst_ep = new List<R1_CS_Endpoints__c>();
		R1_CS_Endpoints__c ep=new R1_CS_Endpoints__c();
		ep.Name='R2_Retrive_Compensation';
		ep.R1_CHK_Activo__c=true;
		ep.R1_TXT_EndPoint__c='retriveCompensationAvios3';
		lst_ep.add(ep);
		R1_CS_Endpoints__c ep2 = new R1_CS_Endpoints__c();
		ep2.Name = 'ETL_Login';
		ep2.R1_CHK_Activo__c = true;
		ep2.R1_TXT_EndPoint__c = 'PruebaLogin';
		lst_ep.add(ep2);
		insert lst_ep;

		System.debug(cas.Parent.R1_CAS_TXT_PNR__c);
		
		System.debug(rtPasaje);
		List<Case> casList = [SELECT Id, ParentId, Parent.R1_CAS_TXT_PNR__c, Parent.R2_CAS_TXT_TKT_Ticket__c, 
									Parent.R1_CAS_LOO_Flight__c, R1_CAS_LOO_Flight__c, Parent.AccountId, AccountId, Parent.Type, Type, 
									Parent.R1_CAS_PKL_Subtype__c, R1_CAS_PKL_Subtype__c, Parent.R1_CAS_TXT_Charge_account__c, R1_CAS_TXT_Charge_account__c,  Parent.R1_CAS_TXT_Budgetary_center__c, R1_CAS_TXT_Budgetary_center__c, 
									Parent.R2_CAS_FOR_Destination__c, Parent.R2_CAS_CHK_AUT_Do_not_automate_flag__c, Parent.R2_CAS_PKL_Country__c,
									Parent.R2_CAS_TXT_Primary_Frecuent_Flyer_Card__c, R2_CAS_TXT_Primary_Frecuent_Flyer_Card__c,  R1_CAS_LOO_Incidences__c 
							FROM Case WHERE RecordTypeId =: rtPasaje
							];

		System.debug(casList);

		Test.startTest();
			System.enqueueJob(new R2_QUE_Retrive_Compensation(casList));
		Test.stopTest();
		
		//List<R2_Compensation__c> compList = [SELECT id, R2_COM_DIV_Total_Amount__c FROM R2_Compensation__c WHERE R2_COM_LOO_Case__c=:casList[0].Id];
		
		//casList = [SELECT Id, R2_CAS_PKL_Exp_Pago_Claims_Auto__c FROM Case WHERE Id=:casList[0].ParentId ];

	}

	/*---------------------------------------------------------------------------------------------------------------------
	Author:         Victor Garcia Barquilla
	Company:        Accenture
	Description:    Metodo test para la funcion execute()
	IN:
	OUT:

	History:
	<Date>                     <Author>                         <Change Description>
	17/10/2018				Victor Garcia Barquilla				Initial version
	----------------------------------------------------------------------------------------------------------------------*/
	@isTest static void execute_Test4() {

		R1_CLS_LogHelper.throw_exception = false;

		Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());

		Id accRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente').getRecordTypeId();
		Account acc = new Account();
		acc.RecordTypeId = accRT;
		acc.LastName = 'ClienteTest';
		acc.PersonEmail = 'test@test.com';
		acc.R1_ACC_PKL_Gender_description__c = 'M';
		acc.R1_ACC_TLF_Phone_Marketing__c = '123456789';
		acc.R1_ACC_PKL_identification_Type__c = '02';
		acc.R1_ACC_TXT_Identification_number__c = '123456789';
		insert acc;

		// vuelo
		R1_Flight__c vuelo = new R1_Flight__c();
		vuelo.Name = 'VueloTest';
		vuelo.R1_FLG_TXT_Origin__c = 'origen';
		vuelo.R1_FLG_TXT_Destination__c = 'destino';
		vuelo.R1_FLG_TXT_Airport_depart__c = 'FRA';
		vuelo.R1_FLG_TXT_Airport_arrive__c = 'MAD';
		vuelo.R1_FLG_TXT_Carrier_code__c = 'IB';
		vuelo.R1_FLG_DAT_Flight_date_local__c = Date.newInstance(2017, 12, 25);
		vuelo.R1_FLG_TXT_Flight_number__c = '1111';
		vuelo.R2_FLG_TXT_Stop_automation__c = false;
		insert vuelo;

		Id rtExpediente = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Expediente').getRecordTypeId();
		Case casoPa = new Case();
		casoPa.RecordTypeId = rtExpediente;
		casoPa.AccountId = acc.Id;
		casoPa.R1_CAS_LOO_Flight__c = vuelo.Id;
		casoPa.Status = 'Abierto';
		casoPa.Origin = 'Email';
		casoPa.Type = 'Retraso';
		casoPa.R1_CAS_TXT_PNR__c = 'H00E7';
		casoPa.R2_CAS_TXT_TKT_Ticket__c = '0752368205835';
		casoPa.R2_CAS_PKL_Country__c = 'DE';
		casoPa.R2_CAS_TXT_Primary_Frecuent_Flyer_Card__c = '12345678';
		casoPa.R2_CAS_CHK_AUT_Do_not_automate_flag__c = false;
		casoPa.R1_CAS_PKL_Subtype__c = '10a)pricing';
		casoPa.R1_CAS_TXT_Charge_account__c = 'test';
		casoPa.R1_CAS_TXT_Budgetary_center__c = 'test';
		System.debug(casoPa);
		System.debug(casoPa.Id);
		insert casoPa;

		// caso hijo
		Id rtPasaje = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Pasaje').getRecordTypeId();
		Case cas = new Case();
		cas.AccountId = acc.Id;
		cas.R1_CAS_LOO_Flight__c = vuelo.Id;
		cas.Type = 'Retraso';
		cas.RecordTypeId = rtPasaje;
		cas.ParentId = casoPa.Id;
		cas.R1_CAS_PKL_Subtype__c = '10a)pricing';
		cas.R1_CAS_TXT_Charge_account__c = 'test';
		cas.R1_CAS_TXT_Budgetary_center__c = 'test';
		cas.R2_CAS_TXT_Primary_Frecuent_Flyer_Card__c = '12345678';
		System.debug(rtPasaje);
		System.debug(cas);
		System.debug(cas.Id);
		insert cas;

		R1_Incident__c incidencia = new R1_Incident__c();
        incidencia.R1_INC_LOO_Case__c = cas.Id;
        incidencia.R2_INC_LOO_Case__c = casoPa.Id;
        insert incidencia;

		List<R1_CS_Endpoints__c> lst_ep = new List<R1_CS_Endpoints__c>();
		R1_CS_Endpoints__c ep=new R1_CS_Endpoints__c();
		ep.Name='R2_Retrive_Compensation';
		ep.R1_CHK_Activo__c=true;
		ep.R1_TXT_EndPoint__c='retriveCompensationAvios4';
		lst_ep.add(ep);
		R1_CS_Endpoints__c ep2 = new R1_CS_Endpoints__c();
		ep2.Name = 'ETL_Login';
		ep2.R1_CHK_Activo__c = true;
		ep2.R1_TXT_EndPoint__c = 'PruebaLogin';
		lst_ep.add(ep2);
		insert lst_ep;

		System.debug(cas.Parent.R1_CAS_TXT_PNR__c);
		
		System.debug(rtPasaje);
		List<Case> casList = [SELECT Id, ParentId, Parent.R1_CAS_TXT_PNR__c, Parent.R2_CAS_TXT_TKT_Ticket__c, 
									Parent.R1_CAS_LOO_Flight__c, R1_CAS_LOO_Flight__c, Parent.AccountId, AccountId, Parent.Type, Type, 
									Parent.R1_CAS_PKL_Subtype__c, R1_CAS_PKL_Subtype__c, Parent.R1_CAS_TXT_Charge_account__c, R1_CAS_TXT_Charge_account__c,  Parent.R1_CAS_TXT_Budgetary_center__c, R1_CAS_TXT_Budgetary_center__c, 
									Parent.R2_CAS_FOR_Destination__c, Parent.R2_CAS_CHK_AUT_Do_not_automate_flag__c, Parent.R2_CAS_PKL_Country__c,
									Parent.R2_CAS_TXT_Primary_Frecuent_Flyer_Card__c, R2_CAS_TXT_Primary_Frecuent_Flyer_Card__c,  R1_CAS_LOO_Incidences__c 
							FROM Case WHERE RecordTypeId =: rtPasaje
							];

		System.debug(casList);

		Test.startTest();
			System.enqueueJob(new R2_QUE_Retrive_Compensation(casList));
		Test.stopTest();
		
		//List<R2_Compensation__c> compList = [SELECT id, R2_COM_DIV_Total_Amount__c FROM R2_Compensation__c WHERE R2_COM_LOO_Case__c=:casList[0].Id];
		
		//casList = [SELECT Id, R2_CAS_PKL_Exp_Pago_Claims_Auto__c FROM Case WHERE Id=:casList[0].ParentId ];

	}

	/*---------------------------------------------------------------------------------------------------------------------
	Author:         Victor Garcia Barquilla
	Company:        Accenture
	Description:    Metodo test para la funcion execute()
	IN:
	OUT:

	History:
	<Date>                     <Author>                         <Change Description>
	17/10/2018				Victor Garcia Barquilla				Initial version
	----------------------------------------------------------------------------------------------------------------------*/
	@isTest static void execute_Test5() {

		R1_CLS_LogHelper.throw_exception = false;

		Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());

		Id accRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente').getRecordTypeId();
		Account acc = new Account();
		acc.RecordTypeId = accRT;
		acc.LastName = 'ClienteTest';
		acc.PersonEmail = 'test@test.com';
		acc.R1_ACC_PKL_Gender_description__c = 'M';
		acc.R1_ACC_TLF_Phone_Marketing__c = '123456789';
		acc.R1_ACC_PKL_identification_Type__c = '02';
		acc.R1_ACC_TXT_Identification_number__c = '123456789';
		insert acc;

		// vuelo
		R1_Flight__c vuelo = new R1_Flight__c();
		vuelo.Name = 'VueloTest';
		vuelo.R1_FLG_TXT_Origin__c = 'origen';
		vuelo.R1_FLG_TXT_Destination__c = 'destino';
		vuelo.R1_FLG_TXT_Airport_depart__c = 'FRA';
		vuelo.R1_FLG_TXT_Airport_arrive__c = 'MAD';
		vuelo.R1_FLG_TXT_Carrier_code__c = 'IB';
		vuelo.R1_FLG_DAT_Flight_date_local__c = Date.newInstance(2017, 12, 25);
		vuelo.R1_FLG_TXT_Flight_number__c = '1111';
		vuelo.R2_FLG_TXT_Stop_automation__c = false;
		insert vuelo;

		Id rtExpediente = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Expediente').getRecordTypeId();
		Case casoPa = new Case();
		casoPa.RecordTypeId = rtExpediente;
		casoPa.AccountId = acc.Id;
		casoPa.R1_CAS_LOO_Flight__c = vuelo.Id;
		casoPa.Status = 'Abierto';
		casoPa.Origin = 'Email';
		casoPa.Type = 'Retraso';
		casoPa.R1_CAS_TXT_PNR__c = 'H00E7';
		casoPa.R2_CAS_TXT_TKT_Ticket__c = '0752368205835';
		casoPa.R2_CAS_PKL_Country__c = 'DE';
		casoPa.R2_CAS_TXT_Primary_Frecuent_Flyer_Card__c = '12345678';
		casoPa.R2_CAS_CHK_AUT_Do_not_automate_flag__c = false;
		casoPa.R1_CAS_PKL_Subtype__c = '10a)pricing';
		casoPa.R1_CAS_TXT_Charge_account__c = 'test';
		casoPa.R1_CAS_TXT_Budgetary_center__c = 'test';
		System.debug(casoPa);
		System.debug(casoPa.Id);
		insert casoPa;

		// caso hijo
		Id rtPasaje = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Pasaje').getRecordTypeId();
		Case cas = new Case();
		cas.AccountId = acc.Id;
		cas.R1_CAS_LOO_Flight__c = vuelo.Id;
		cas.Type = 'Retraso';
		cas.RecordTypeId = rtPasaje;
		cas.ParentId = casoPa.Id;
		cas.R1_CAS_PKL_Subtype__c = '10a)pricing';
		cas.R1_CAS_TXT_Charge_account__c = 'test';
		cas.R1_CAS_TXT_Budgetary_center__c = 'test';
		cas.R2_CAS_TXT_Primary_Frecuent_Flyer_Card__c = '12345678';
		System.debug(rtPasaje);
		System.debug(cas);
		System.debug(cas.Id);
		insert cas;

		R1_Incident__c incidencia = new R1_Incident__c();
        incidencia.R1_INC_LOO_Case__c = cas.Id;
        incidencia.R2_INC_LOO_Case__c = casoPa.Id;
        insert incidencia;

		List<R1_CS_Endpoints__c> lst_ep = new List<R1_CS_Endpoints__c>();
		R1_CS_Endpoints__c ep=new R1_CS_Endpoints__c();
		ep.Name='R2_Retrive_Compensation';
		ep.R1_CHK_Activo__c=true;
		ep.R1_TXT_EndPoint__c='retriveCompensationAvios5';
		lst_ep.add(ep);
		R1_CS_Endpoints__c ep2 = new R1_CS_Endpoints__c();
		ep2.Name = 'ETL_Login';
		ep2.R1_CHK_Activo__c = true;
		ep2.R1_TXT_EndPoint__c = 'PruebaLogin';
		lst_ep.add(ep2);
		insert lst_ep;

		System.debug(cas.Parent.R1_CAS_TXT_PNR__c);
		
		System.debug(rtPasaje);
		List<Case> casList = [SELECT Id, ParentId, Parent.R1_CAS_TXT_PNR__c, Parent.R2_CAS_TXT_TKT_Ticket__c, 
									Parent.R1_CAS_LOO_Flight__c, R1_CAS_LOO_Flight__c, Parent.AccountId, AccountId, Parent.Type, Type, 
									Parent.R1_CAS_PKL_Subtype__c, R1_CAS_PKL_Subtype__c, Parent.R1_CAS_TXT_Charge_account__c, R1_CAS_TXT_Charge_account__c,  Parent.R1_CAS_TXT_Budgetary_center__c, R1_CAS_TXT_Budgetary_center__c, 
									Parent.R2_CAS_FOR_Destination__c, Parent.R2_CAS_CHK_AUT_Do_not_automate_flag__c, Parent.R2_CAS_PKL_Country__c,
									Parent.R2_CAS_TXT_Primary_Frecuent_Flyer_Card__c, R2_CAS_TXT_Primary_Frecuent_Flyer_Card__c,  R1_CAS_LOO_Incidences__c 
							FROM Case WHERE RecordTypeId =: rtPasaje
							];

		System.debug(casList);

		Test.startTest();
			System.enqueueJob(new R2_QUE_Retrive_Compensation(casList));
		Test.stopTest();
		
		//List<R2_Compensation__c> compList = [SELECT id, R2_COM_DIV_Total_Amount__c FROM R2_Compensation__c WHERE R2_COM_LOO_Case__c=:casList[0].Id];
		
		//casList = [SELECT Id, R2_CAS_PKL_Exp_Pago_Claims_Auto__c FROM Case WHERE Id=:casList[0].ParentId ];

	}

/*---------------------------------------------------------------------------------------------------------------------
	Author:         Victor Garcia Barquilla
	Company:        Accenture
	Description:    Metodo test para la funcion execute()
	IN:
	OUT:

	History:
	<Date>                     <Author>                         <Change Description>
	17/10/2018				Victor Garcia Barquilla				Initial version
	----------------------------------------------------------------------------------------------------------------------*/
	
	@isTest
	static void exception_Test() {
		R1_CLS_LogHelper.throw_exception = true;

		Test.startTest();

			R2_CLS_Retrieve_Compensation.retrieveCompensation(null, null);

		Test.stopTest();
		System.assertEquals(0, [SELECT count() FROM R1_Log__c]);
	}

}