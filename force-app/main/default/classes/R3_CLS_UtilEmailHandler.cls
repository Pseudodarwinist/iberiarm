/*---------------------------------------------------------------------------------------------------------------------
   Author:         TCK
   Company:        The Cocktail
   Description:    Email Service Class.

   History:
    <Date>                     <Author>                         <Change Description>
   30/09/2019                     TCK                           Initial Version
----------------------------------------------------------------------------------------------------------------------*/
public class R3_CLS_UtilEmailHandler{

    /*@author TCK&LCS
    * @date 09/09/2019
    * @description method to find the right template for a non-created case
    */
    public EmailTemplate findTemplate(){
        EmailTemplate et = new EmailTemplate();
        et = [SELECT Id,Subject,HtmlValue, Body , name FROM EmailTemplate WHERE name =: Label.ko_mixta];                  
        return et;
    }

    public EmailTemplate findTemplateSingular(){
        EmailTemplate et = new EmailTemplate();
        et = [SELECT Id,Subject,HtmlValue, Body , name FROM EmailTemplate WHERE name =: Label.Singular_error];                  
        return et;
    }

    /*@author TCK&LCS
    * @date 09/09/2019
    * @description method to find the right account for serviberia
    */    
    public List<Account> findCuentaByMail(String emailRemitente){
        List<Account> cuentaList = new List<Account>();
        try{
            cuentaList = [SELECT Id,R1_ACC_PKL_Comunication_Language__c,R1_ACC_PKL_Card_Type__c,PersonEmail FROM account WHERE PersonEmail=: emailRemitente];
            return cuentaList;
        } catch(Exception ex){
        
        }    
        
        return null;
    }

    /*@author TCK&LCS
    * @date 09/09/2019
    * @description method to find the right account for CCPO
    */
    public Account findCuentaByFF(String ff){
        Account cuenta = new Account();
        if(ff != ''){
            try{
                cuenta = [SELECT PersonEmail,Id,R1_ACC_PKL_Comunication_Language__c,R1_ACC_PKL_Card_Type__c,R1_ACC_CHK_Flag_Iberia_Singular__c FROM account WHERE R1_ACC_TXT_Primary_Frecuent_Flyer_Card__c=: ff LIMIT 1];
                return cuenta;
            } catch(Exception ex){
                cuenta.Id = null;
            }
        }
        return null;
    }

    /*@author TCK&LCS
    * @date 09/09/2019
    * @description method to verify if the FF fulfill the conditions 
    */
    public String checkString (String word){
        Pattern pat = Pattern.compile('([0-9]+)');
        Matcher matcher = pat.matcher(word);
        if(matcher.find()){
            word = matcher.group(1);
        }
        if((word.isNumeric()==true) & (word.length()>=4) & (word.length()<=14)) {
            boolean check = ccValidation(word);
            if(ccValidation(word)==true){
                if(word.startswith('0')){
                    word = word.replaceFirst('^0+(?!$)', '');
                }
                String primerFF = 'IB' + word;
                return primerFF;
            }
        }
        return '';
    }

    /*@author TCK&LCS
    * @date 09/09/2019
    * @description method verify if Luhn algorithm verifies for the FF 
    */
    public boolean ccValidation(String ccNumber){
        Integer sum = 0;
        Integer len = ccNumber.length();
        for(Integer i=len-1;i>=0;i--){
            Integer num = Integer.ValueOf(ccNumber.substring(i,i+1));
            if ( math.mod(i , 2) == math.mod(len, 2) )
            {
                Integer n = num * 2;
                sum += (n / 10) + ( math.mod(n, 10));
            }
            else{
                sum += num;
            }
        }
        return ( math.mod( sum, 10) == 0 );
    }

    /*@author TCK&LCS
    * @date 09/09/2019
    * @description method to send the autoreply  email to the client 
    */
    public void sendEmail(String emailRemitente, EmailTemplate et){
        Messaging.reserveSingleEmailCapacity(1);
        OrgwideEmailAddress[] orgwideaddress = [select id, displayname from orgwideEmailAddress where Address = 'noreply@iberia.com'];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[]{emailRemitente};
        mail.setOrgWideEmailAddressId(orgwideaddress[0].id);
        mail.setToAddresses(toAddresses);
        mail.setUseSignature(false);
        mail.setSaveAsActivity(false);
        //mail.setSenderDisplayName('MMPT');
        mail.setTargetObjectId(UserInfo.getUserId());
        mail.setTemplateId(et.Id);
        mail.setTreatTargetObjectAsRecipient(false);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
        

    }

    /*@author TCK&LCS
    * @date 09/09/2019
    * @description method to create the case im SFDC
    */

    public case createCaso(String subject, String description, String emailToConfirm, String emailRemitente, Account cuenta, String ff){
        
        Case c = new Case();
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Comunicaciones entrantes').getRecordTypeId();
        c.R3_CAS_EMA_SenderEmail__c  = emailRemitente;
        c.Origin = 'email';
        c.RecordTypeID=caseRecordTypeId;
        c.Subject= subject;
        c.Description=description;
        c.R2_CAS_EMA_Email__c = emailToConfirm;

        c.R2_CAS_TXT_Primary_Frecuent_Flyer_Card__c	= ff;
        if(cuenta != null && cuenta.Id != null){
            c.AccountId=cuenta.Id;
        }
        return c;
    }

    /*@author TCK&LCS
    * @date 09/09/2019
    * @description method to attach documents to the case in SFDC
    */
    public List<Attachment> insertAttatchments(EmailMessage emailmsg, Messaging.inboundEmail email){
        //Processing Attachments
        List<Attachment> lstAttachments = new List<Attachment>();

        if(email.TextAttachments != Null && email.TextAttachments.size() > 0){
            for(Messaging.InboundEmail.TextAttachment textAttachment: email.TextAttachments){
                Attachment attachment = new Attachment();
                attachment.name = textAttachment.fileName;
                attachment.body = blob.valueof(textAttachment.body);
                attachment.parentId = emailmsg.Id;

                lstAttachments.add(attachment);
            }
        }
        //Binary Attachment
        if(email.BinaryAttachments != Null && email.BinaryAttachments.size() > 0){
            for(Messaging.InboundEmail.binaryAttachment binaryAttachment: email.BinaryAttachments){
                Attachment attachment = new Attachment();
                attachment.name = binaryAttachment.filename;
                attachment.body = binaryAttachment.body;
                attachment.parentId = emailmsg.Id;

                lstAttachments.add(attachment);
            }
        }
        if(!lstAttachments.isEmpty()){
            try{
                insert lstAttachments;
                return lstAttachments;
            }catch(DmlException e){
                R1_CLS_LogHelper.generateErrorLog('R3_CLS_UtilEmailHandler.insertAttatchments()', '', e.getmessage() + ', ' + e.getLineNumber(), 'insert Attachments');
            }
        }
        return null;
    }

    /**
    * @author TCK&LCS
    * @date 05/09/2019
    * @description get FF from subject
    * @param String subject
    * @return String
    */  
    public String returnFF(String subject){
        List<String> parts = subject.split(' ');
        String ff = '';
        for(String aux:parts){
            if(aux.startswith('IB:') || aux.startswith('FF:')){
                ff = checkString(aux.substring(3,aux.length()));
                if(ff != ''){
                    break;
                } 
            }else if(aux.startswith('IB') || aux.startswith('FF')){
                ff = checkString(aux.substring(2,aux.length()));
                if(ff != ''){
                    break;
                }             
            }else{
                ff = checkString(aux);
                if(ff != ''){
                    break;
                }
            }
        }
        Account acc = findCuentaByFF(ff);
        if(acc!= null){
            return ff;
        }else{
            return '';
        }
    }

    /**
    * @author TCK&LCS
    * @date 05/09/2019
    * @description get FF from subject
    * @param String subject
    * @return String
    */  
    public void updateCaso(Case casoIn, Messaging.InboundEnvelope env ){
        List<Account> cuentaList = [SELECT id,R1_ACC_PKL_Card_Type__c,R1_ACC_CHK_Flag_Iberia_Singular__c from Account Where Id =: casoIn.AccountId];
        if(cuentaList != null & !cuentaList.isEmpty()){
            TCK1_CON_ContactIberiaExtension aux = new TCK1_CON_ContactIberiaExtension();
            Group colaId = aux.getAssign(cuentaList[0].R1_ACC_PKL_Card_Type__c,cuentaList[0].R1_ACC_CHK_Flag_Iberia_Singular__c, env.fromAddress);
            if(colaId.id != null){
                casoIn.OwnerId = colaId.Id;
                casoIn.R2_CAS_DATH_Rebounds_Last_Modify__c = system.now();
                casoIn.R2_CAS_NUM_Number_of_rebounds__c = casoIn.R2_CAS_NUM_Number_of_rebounds__c +1;
            }
            casoIn.Status = 'Reabierto';
            update casoIn;
        }
    }
}