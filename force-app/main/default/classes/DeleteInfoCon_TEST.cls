@isTest
private class DeleteInfoCon_TEST {
  
  static testMethod void deleteInfoCon() {
      R1_CLS_LogHelper.throw_exception = false;
      Account acc1 = R1_CLS_Utilities.helper_Account_constructor('1');
      acc1.R1_ACC_TXT_Primary_Frecuent_Flyer_Card__c = 'IB1';
      insert acc1;
      R1_Contact_Data__c cd = new R1_Contact_Data__c(R1_CD_PKL_IBPlus_References__c='00',R1_CD_MSDT_Client__c=acc1.Id);
      insert cd;
      system.debug('>>>'+[SELECT ID,R1_CD_PKL_IBPlus_References__c FROM R1_Contact_Data__c WHERE R1_CD_PKL_IBPlus_References__c='00']);
      system.assertequals(1,[SELECT Count() FROM R1_Contact_Data__c]);
      Test.startTest();
      Database.executeBatch(new deleteInfoCon());
      Test.stopTest();
      system.assertequals(0,[SELECT Count() FROM R1_Contact_Data__c]);
  }
  
}