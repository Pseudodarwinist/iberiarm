@isTest
public class R2_CLS_Rebound_Status_Test {
    static testMethod void myUnitTest() {
        Id accRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente').getRecordTypeId();
    Account acc = new Account();
    acc.RecordTypeId = accRT;
    acc.LastName = 'ClienteTest';
    acc.PersonEmail = 'test@test.com';
    acc.R1_ACC_PKL_Gender_description__c = 'M';
    acc.R1_ACC_TLF_Phone_Marketing__c = '123456789';
    acc.R1_ACC_PKL_identification_Type__c = '02';
    acc.R1_ACC_TXT_Identification_number__c = '123456789';
    insert acc;

    // vuelo
    R1_Flight__c vuelo = new R1_Flight__c();
    vuelo.Name = 'VueloTest';
    vuelo.R1_FLG_TXT_Origin__c = 'origen';
    vuelo.R1_FLG_TXT_Destination__c = 'destino';
    vuelo.R1_FLG_TXT_Airport_depart__c = 'FRA';
    vuelo.R1_FLG_TXT_Airport_arrive__c = 'MAD';
    vuelo.R1_FLG_TXT_Carrier_code__c = 'IB';
    vuelo.R1_FLG_DAT_Flight_date_local__c = Date.newInstance(2017, 12, 25);
    vuelo.R1_FLG_TXT_Flight_number__c = '1111';
    vuelo.R2_FLG_TXT_Stop_automation__c = false;
    insert vuelo;    
        Id rtExpediente = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Expediente').getRecordTypeId();
        Case casoPa = new Case();
        casoPa.RecordTypeId = rtExpediente;
        casoPa.Status = 'Cerrado';
        casoPa.Origin = 'Email';
        casoPa.Type = 'Retraso';
        casoPa.R2_CAS_NUM_Number_of_rebounds__c  = 1;
        casoPa.R2_CAS_DATH_Rebounds_Last_Modify__c  = Datetime.now().addDays(-4);
        insert casoPa;
    
        // caso hijo
            Id rtPasaje = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Pasaje').getRecordTypeId();
            Case cas = new Case();
            cas.RecordTypeId = rtPasaje;
            cas.AccountId = acc.Id;
            cas.ParentId = casoPa.Id;
            cas.R1_CAS_LOO_Flight__c = vuelo.Id;
            cas.Status = 'Abierto';
            cas.Origin = 'Email';
            cas.Type = 'Retraso';
            cas.R1_CAS_TXT_PNR__c = 'H00E7';
            cas.R2_CAS_TXT_TKT_Ticket__c = '0752368205835';
            cas.R2_CAS_PKL_Country__c = 'DE';
            cas.R2_CAS_TXT_Primary_Frecuent_Flyer_Card__c = '12345678';
            cas.R2_CAS_CHK_AUT_Do_not_automate_flag__c = false;
            cas.R2_CAS_DIV_AUT_Compensation__c  = 0;
            cas.R2_CAS_DIV_AUT_Compensation_Vouchers__c  = 0;
            cas.R2_CAS_NUM_AUT_Compensation_in_Avios__c  = 0;
            insert cas;
        
            Test.startTest();
            Datetime dt = Datetime.now().addMinutes(1);
            String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
            String jobId = System.schedule('Sample_Heading', CRON_EXP, new R2_CLS_Rebound_Status () );   
            Test.stopTest();      
    }

}