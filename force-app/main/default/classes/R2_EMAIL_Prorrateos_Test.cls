/*---------------------------------------------------------------------------------------------------------------------
Author:         Jaime Ascanta
Company:        Accenture
Description:
IN:
OUT:

History:
<Date>                     <Author>                         <Change Description>
12/03/2018 					jaime ascanta                  	Initial Version
----------------------------------------------------------------------------------------------------------------------*/
@isTest
private class R2_EMAIL_Prorrateos_Test {

	/*---------------------------------------------------------------------------------------------------------------------
	Author:         Jaime Ascanta
	Company:        Accenture
	Description:
	IN:
	OUT:

	History:
	 <Date>                     <Author>                         <Change Description>
	12/03/2018       	   		Jaime Ascanta                  		 Initial Version
	24/05/2018       	   		Alberto Puerto                  	 fulfilling seg1.R2_SEG_TXT_Company__c (required field)
	----------------------------------------------------------------------------------------------------------------------*/
	@isTest
	static void seveEmailOfSegement_test() {
		R1_CLS_LogHelper.throw_exception = false;

		List<R2_Segmento__c> listSeg = [SELECT Id, R2_SEG_FOR_Thread_Id__c,R2_SEG_CHK_Message_Received__c FROM R2_Segmento__c];

		Messaging.InboundEmail email = new Messaging.InboundEmail();
		Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
		email.subject = 'Test subject '+listSeg[0].R2_SEG_FOR_Thread_Id__c+' test';
		env.fromAddress = 'test@test.com';

		Test.startTest();

			R2_EMAIL_Prorrateos emailInBound = new R2_EMAIL_Prorrateos();
			emailInBound.handleInboundEmail(email, env);

		Test.stopTest();

		List<EmailMessage> listEmails = [SELECT Id FROM EmailMessage WHERE R2_EMA_LOO_Segmento__c=:listSeg[0].Id];
		System.assertEquals(1, listEmails.size());

		List<R2_Segmento__c> listSegUpdate = [SELECT Id, R2_SEG_FOR_Thread_Id__c, R2_SEG_CHK_Message_Received__c FROM R2_Segmento__c WHERE Id=:listSeg[0].Id];
		System.assertEquals(true, listSegUpdate[0].R2_SEG_CHK_Message_Received__c);

		//System.assertEquals(0, [SELECT count() FROM R1_Log__c]);

	}

	/*---------------------------------------------------------------------------------------------------------------------
	Author:         Jaime Ascanta
	Company:        Accenture
	Description:
	IN:
	OUT:

	History:
	 <Date>                     <Author>                         <Change Description>
	12/03/2018       	   		Jaime Ascanta                  		 Initial Version
	----------------------------------------------------------------------------------------------------------------------*/
	@isTest
	static void seveEmailOfSegement_error_test() {
		R1_CLS_LogHelper.throw_exception = false;

		Messaging.InboundEmail email = new Messaging.InboundEmail();
		Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
		email.subject = 'Test subject [sfref:1234567890:sfref] test';
		env.fromAddress = 'test@test.com';

		Test.startTest();
			R2_EMAIL_Prorrateos emailInBound = new R2_EMAIL_Prorrateos();
			emailInBound.handleInboundEmail(email, env);
		Test.stopTest();

		List<EmailMessage> listEmails = [SELECT Id FROM EmailMessage];
		System.assertEquals(0, listEmails.size());

		//System.assertEquals(0, [SELECT count() FROM R1_Log__c]);
	}


	/*---------------------------------------------------------------------------------------------------------------------
	Author:         Jaime Ascanta
	Company:        Accenture
	Description:
	IN:
	OUT:

	History:
	 <Date>                     <Author>                         <Change Description>
	04/06/2018       	   		Jaime Ascanta                  		 Initial Version
	10/10/2018				Victor Garcia Barquilla				En setup: actualizado valores de R2_ExchangeRate__c a dimonca.
	----------------------------------------------------------------------------------------------------------------------*/
	@isTest
	static void seveEmailToCaseCustom_test() {
		R1_CLS_LogHelper.throw_exception = false;
		String subjet = 'Test subject email to case custom';
		String fromAddress = 'testetc@test.com';

		List<Messaging.Inboundemail.TextAttachment> listTxtAtt = new List<Messaging.Inboundemail.TextAttachment>();
		Messaging.Inboundemail.TextAttachment textAtt = new Messaging.Inboundemail.TextAttachment();
		textAtt.Body = 'Text';
		textAtt.fileName = 'test.txt';
		listTxtAtt.add(textAtt);

		List<Messaging.Inboundemail.BinaryAttachment> listBinAtt = new List<Messaging.Inboundemail.BinaryAttachment>();
		Messaging.Inboundemail.BinaryAttachment binAtt = new Messaging.Inboundemail.BinaryAttachment();
		binAtt.Body = Blob.valueof('Text');
		binAtt.fileName = 'test.pdf';
		listBinAtt.add(binAtt);

		Messaging.InboundEmail email = new Messaging.InboundEmail();
		Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
		email.subject = subjet;
		email.textAttachments = listTxtAtt;
		email.binaryAttachments = listBinAtt;
		env.fromAddress = fromAddress;

		Test.startTest();
			R2_EMAIL_Prorrateos emailInBound = new R2_EMAIL_Prorrateos();
			emailInBound.handleInboundEmail(email, env);
		Test.stopTest();

		Id rtId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Prorrateo').getRecordTypeId();
		Id ownrId = [SELECT id from group where Type = 'Queue' and Name='SYR Prorrateos' Limit 1].Id;
		List<Case> listCase = [SELECT Id,Subject FROM Case WHERE RecordTypeId=:rtId AND OwnerId=:ownrId];
		System.assertEquals(1, listCase.size());
		System.assertEquals(subjet, listCase[0].Subject);

		List<Attachment> listAtt = [SELECT Id FROM Attachment WHERE ParentId=:listCase[0].Id];
		System.assertEquals(2, listAtt.size());

		List<EmailMessage> listEmails = [SELECT Id FROM EmailMessage WHERE ParentId=:listCase[0].Id];
		System.assertEquals(1, listEmails.size());

		//System.assertEquals(0, [SELECT count() FROM R1_Log__c]);
	}

	@testSetup
	static void setup(){
		R1_CLS_LogHelper.throw_exception = false;
		//R2_SDRtoEUR__c
		R2_ExchangeRate__c sdr = new R2_ExchangeRate__c();
		sdr.Name = 'XDR_960';
		sdr.R2_NUM_Rate_to_EUR__c = 1.1788;
		sdr.R2_TXT_External_Id__c = 'Test';
		insert sdr;

		//RT del caso expendiente
        Id rt_expediente = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Expediente').getRecordTypeId();
        Case caso = new Case();
        caso.RecordTypeId = rt_expediente;
        caso.Status = 'Abierto';
        caso.Origin = 'Email';
        caso.Type = 'Retraso';
		insert caso;

		R2_Prorrateo__c pro = new R2_Prorrateo__c();
		pro.R2_PRO_LOO_Case__c = caso.Id;
		insert pro;

		R2_Segmento__c seg = new R2_Segmento__c();
		seg.R2_SEG_MSDT_Prorrate__c = pro.Id;
		seg.R2_SEG_TXT_company_code__c = 'IB';
		seg.R2_SEG_NUM_SDRtoEUR__c = 10;
		seg.R2_SEG_CHK_Message_Received__c = false;
		seg.R2_SEG_TXT_Company__c = 'IBERIA';
		seg.R2_SEG_PKL_Estado__c = 'Prorrateo OK';
		insert seg;

		//System.assertEquals(0, [SELECT count() FROM R1_Log__c]);
	}

	@isTest
	static void exception_test() {
		R1_CLS_LogHelper.throw_exception = true;
		Test.startTest();
			R2_EMAIL_Prorrateos emailInBound = new R2_EMAIL_Prorrateos();
			emailInBound.handleInboundEmail(null, null);
			emailInBound.seveEmailOfSegement(null);
			emailInBound.seveEmailToCaseCustom(null);
			emailInBound.getAttachments(null,null);
			emailInBound.makeInboundEmailToSave(null);
		Test.stopTest();
		//System.assertEquals(0, [SELECT count() FROM R1_Log__c]);
	}
}