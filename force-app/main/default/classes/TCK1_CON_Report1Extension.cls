/******************************************************
* Extensión para la page PDFReport1                   *               
*                                                     *  
* The Cocktail                                        *
*                                                     *
* Version1:01/08/2019                                 *
*                                                     *
*******************************************************/
/**
* @author TCK&LCS
* @date 01/08/2019
* @description PDFReport1 controller extension class. A new PDF version is created. Since second version, approval from the user is requested with an alert. Claims profile only can create the first version.
*/
public with sharing class TCK1_CON_Report1Extension {
    public string codDesTim  {get;set;} 
    public string docsAportados {get;set;} 
    public string logo {get;set;} 
    public string existReport {get;set;}

	/**
    * @author TCK&LCS
    * @date 01/08/2019
    * @description Class constructor controller. Populates the Visualforce with the appearance of PDF version.
    * @param ApexPages.standardController Report 1 Standard Controller.
    */
    public TCK1_CON_Report1Extension(ApexPages.standardController stdCtr){
        String reportId = ApexPages.currentPage().getParameters().get('id');
        List<ContentDocumentLink> cdl = new List<ContentDocumentLink>();
        cdl = [SELECT Id,ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: reportId];
        List<String> cdlIds = new List<String>();
        List<ContentDocument> cont = new List<ContentDocument>();
        List<String> contDoc = new List<String>();
        for(ContentDocumentLink aux:cdl){
            cdlIds.add(aux.Id);
            contDoc.add(aux.ContentDocumentId);
        }
        cont = [SELECT Id FROM ContentDocument WHERE Id IN:contDoc AND Title LIKE 'Report 1%'];
        system.debug('cont:: '+cont);
        if(cont.isEmpty()){
            existReport =  'false';
        }else{
            existReport =  'true';
        }
        TCK1_Report1__c qryReport1 = [ Select TCK1_RP1_LOO_Vuelo__r.R1_FLG_TXT_Flight_number__c,TCK1_RP1_TEX_Causas_Retraso__c,TCK1_RP1_Log_Book_Adjunto__c,TCK1_RP1_Metar_Adjunto__c,TCK1_RP1_Notam_Adjunto__c,TCK1_RP1_SAM_Adjunto__c,TCK1_RP1_LOO_Vuelo__c,TCK1_RP1_LOO_Vuelo__r.R1_FLG_TXT_Carrier_code_oper__c, TCK1_RP1_LOO_Vuelo__r.R1_FLG_DATH_Sched_depart_orig__c, TCK1_RP1_LOO_Vuelo__r.R1_FLG_DATH_Airborne_Act__c from TCK1_Report1__c WHERE id =:reportId LIMIT 1][0];  
        system.debug('Compañia operadora: '+qryReport1.TCK1_RP1_LOO_Vuelo__r.R1_FLG_TXT_Carrier_code_oper__c);
        logo = qryReport1.TCK1_RP1_LOO_Vuelo__r.R1_FLG_TXT_Carrier_code_oper__c;
        if(logo == 'IB'){
            if(integer.valueof(qryReport1.TCK1_RP1_LOO_Vuelo__r.R1_FLG_TXT_Flight_number__c) >= 2600 && integer.valueof(qryReport1.TCK1_RP1_LOO_Vuelo__r.R1_FLG_TXT_Flight_number__c) <= 2999){
                logo = 'LV';
            }else if(integer.valueof(qryReport1.TCK1_RP1_LOO_Vuelo__r.R1_FLG_TXT_Flight_number__c) >= 3600 && integer.valueof(qryReport1.TCK1_RP1_LOO_Vuelo__r.R1_FLG_TXT_Flight_number__c) <= 3999){
                logo = 'I2';
            }else if(integer.valueof(qryReport1.TCK1_RP1_LOO_Vuelo__r.R1_FLG_TXT_Flight_number__c) >= 8000 && integer.valueof(qryReport1.TCK1_RP1_LOO_Vuelo__r.R1_FLG_TXT_Flight_number__c) <= 8999){
                logo = 'YW';
            }
        }
        List<String> listaQry = new List<String>();
        system.debug('qryReport1: '+qryReport1.TCK1_RP1_TEX_Causas_Retraso__c);
        if(qryReport1.TCK1_RP1_TEX_Causas_Retraso__c != null){
            listaQry = qryReport1.TCK1_RP1_TEX_Causas_Retraso__c.split('\n');
        }
        system.debug('listaQry: '+listaQry);
        codDesTim = '<br/>';
        for(String aux:listaQry){
            system.debug('aux:'+aux);
            aux = aux.replace(',','    |     ');
            codDesTim = codDesTim + aux +' mins <br/> ';
        }
        codDesTim = codDesTim.removeEnd(' | ');
        List<ContentDocumentLink> contentIdList = new List<ContentDocumentLink> ([SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: reportId]);
        List<String> listContentDocumentIds = new List<String>();
        for(ContentDocumentLink aux:contentIdList){
            listContentDocumentIds.add(aux.ContentDocumentId);
        }
        List<ContentDocument> filesName = new List<ContentDocument> ([SELECT Title FROM ContentDocument WHERE Id IN:listContentDocumentIds]);
        docsAportados = '';
        for(ContentDocument aux:filesName){
            if(!aux.Title.startsWith('Report 1'))
                docsAportados = docsAportados+'<br />' + aux.Title;
        }
    }

	/**
    * @author TCK&LCS
    * @date 01/08/2019
    * @description Generates the PDF report based on Report 1 parameters
    */
    @RemoteAction
    public static void makePDFReport1(){
        String reportId = ApexPages.currentPage().getParameters().get('id');
        integer versionReport1 = getReport1(reportId);
        PageReference ref = new PageReference('/apex/PDFReport1?id=' + reportId);
        ContentVersion cont = new ContentVersion();
        cont.Title = 'Report 1 v.'+versionReport1;
        cont.PathOnClient = 'report1v'+versionReport1+'.pdf';
		if(Test.isRunningTest()){
            cont.VersionData = blob.valueOf('Test');
        }else{
        	cont.VersionData = ref.getContentAsPDF();
		}
        cont.Origin = 'H';
        insert cont;
        ContentDocument cd = new ContentDocument();
        cd = [SELECT Id FROM ContentDocument WHERE LatestPublishedVersionId =: cont.id LIMIT 1];
        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.ContentDocumentId = cd.Id;
        cdl.LinkedEntityId = reportId;
        cdl.ShareType = 'I';
        insert cdl;
    }

	/**
    * @author TCK&LCS
    * @date 01/08/2019
    * @description Get the number of PDF versions created
	* @param String The recordId of Report 1 to be generated as PDF
	* @return Integer Return the number + 1 of PDF versions created
    */
    public static Integer getReport1(String recordId){
        List<ContentDocumentLink> cdl = new List<ContentDocumentLink>();
        cdl = [SELECT Id,ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: recordId];
        List<String> cdlIds = new List<String>();
        List<ContentDocument> cont = new List<ContentDocument>();
        List<String> contDoc = new List<String>();
        for(ContentDocumentLink aux:cdl){
            cdlIds.add(aux.Id);
            contDoc.add(aux.ContentDocumentId);
        }
        cont = [SELECT Id FROM ContentDocument WHERE Id IN:contDoc AND Title LIKE 'Report 1%'];
        return cont.size()+1;
    }
}