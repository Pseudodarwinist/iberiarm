@istest
public with sharing class R2_CLS_Fast_Track_Test {
	static testMethod void create_access() {
		R1_CLS_LogHelper.throw_exception = false;
		

		String passengerName = 'GUIRALCANOS/JOSEPA';
		String pnr = 'KDZX9';
		String origin = 'MAD';
		String oneWolrdCode = 'SAPPHIRE';
		String destination = 'PTY';
		String operationCompany = 'IB';
		String flight = '6339';
		String flightDate = '2018-09-25';
		String classe = 'I';
		String vipPermissions = 'null';
		String idAdIndicator = 'null';
		String seatNumber = '004H';
		String boardingNumber = '0131';
		String marketingCode = 'IB';
		String status = 'Accepted';
		String etkdNumber = '0752377200037';
		String ffNumber = 'IB12059788';
		String vipLounge = 'Test 1';
		String vipLoungePosition = 'Puesto 1';
		String specialMeals = 'null';
		String specialServices = 'null';
		String specialPAX = 'null';
		String boardingGate = 'S';
		String departureTime = '1155';
		String ancillaries = 'null';
		String minor = 'null';
		String flightMarketing = '8765';
		String inboundAirline = 'IB';
		String inboundFlight = '8999';
		String inboundClass = 'I';
		String barcodeLectorType = 'Fast Track';
		String operationType = null;
		String inboundOrigin = null;
		String recordId = 'IB_6339_GUIRALCANOS/JOSEPA_20180920';

		Salas_VIP__c sala =  new Salas_VIP__c();
		sala.Name = 'Test 1 Puesto 1';
		sala.Nombre_del_Equipo__c ='Test';
		sala.Puesto_de_Trabajo__c = 'Puesto 1';
		sala.Sala_Vip__c ='Test 1';
		sala.Status__c = True;
		sala.UserId__c = 'Test';
		insert sala;

		Account cliente =  new Account();
		cliente.Name = 'Nombre TCS';
		//cliente.PersonEmail = 'test@test.com';
		cliente.R1_ACC_TLF_Phone_Marketing__c = '6786546786';
		cliente.R1_ACC_TXT_Primary_Frecuent_Flyer_Card__c='IB12059788';
		insert cliente;

		test.startTest();
		R2_CLS_Fast_Track.createAccess(passengerName, pnr,	origin, oneWolrdCode, destination, operationCompany, flight,flightDate, classe, vipPermissions, idAdIndicator,vipLounge, seatNumber,boardingNumber, marketingCode,flightMarketing, status, etkdNumber, ffNumber, vipLoungePosition, specialMeals, specialServices, specialPAX, boardingGate, departureTime,  minor, inboundAirline, inboundFlight, inboundClass,inboundOrigin,  operationType, recordId);
		test.stopTest();

	}

	static testMethod void create_accessActualizar() {
		R1_CLS_LogHelper.throw_exception = false;
		

		String passengerName = 'GUIRALCANOS/JOSEPA';
		String pnr = 'KDZX9';
		String origin = 'MAD';
		String oneWolrdCode = 'SAPPHIRE';
		String destination = 'PTY';
		String operationCompany = 'IB';
		String flight = '6339';
		String flightDate = '2018-06-02';
		String classe = 'I';
		String vipPermissions = 'null';
		String idAdIndicator = 'null';
		String seatNumber = '004H';
		String boardingNumber = '0131';
		String marketingCode = 'IB';
		String status = 'Accepted';
		String etkdNumber = '0752377200037';
		String ffNumber = 'IB12059788';
		String vipLounge = 'Test 1';
		String vipLoungePosition = 'Puesto 1';
		String specialMeals = 'null';
		String specialServices = 'null';
		String specialPAX = 'null';
		String boardingGate = 'S';
		String departureTime = '1155';
		String ancillaries = 'null';
		String minor = 'null';
		String inboundAirline = 'IB';
		String inboundFlight = '8999';
		String flightMarketing = '8765';
		String inboundOrigin = null;
		String inboundClass = 'I';
		String barcodeLectorType = 'Fast Track';
		String operationType = 'Actualizar';
		String recordId = 'IB_6339_GUIRALCANOS/JOSEPA_20180920';

		Salas_VIP__c sala =  new Salas_VIP__c();
		sala.Name = 'Test 1 Puesto 1';
		sala.Nombre_del_Equipo__c ='Test';
		sala.Puesto_de_Trabajo__c = 'Puesto 1';
		sala.Sala_Vip__c ='Test 1';
		sala.Status__c = True;
		sala.UserId__c = 'Test';
		insert sala;

		test.startTest();
		R2_CLS_Fast_Track.createAccess(passengerName, pnr,	origin, oneWolrdCode, destination, operationCompany, flight,flightDate, classe, vipPermissions, idAdIndicator,vipLounge, seatNumber,boardingNumber, marketingCode,flightMarketing, status, etkdNumber, ffNumber, vipLoungePosition, specialMeals, specialServices, specialPAX, boardingGate, departureTime,  minor, inboundAirline, inboundFlight, inboundClass,inboundOrigin,  operationType, recordId);
		test.stopTest();

	}

	static testMethod void create_accessRechazarAcceso() {
		R1_CLS_LogHelper.throw_exception = false;
		

		String passengerName = 'GUIRALCANOS/JOSEPA';
		String pnr = 'KDZX9';
		String origin = 'MAD';
		String oneWolrdCode = 'SAPPHIRE';
		String destination = 'PTY';
		String operationCompany = 'IB';
		String flight = '6339';
		String flightDate = '2018-09-24';
		String classe = 'I';
		String vipPermissions = null;
		String idAdIndicator = null;
		String seatNumber = '004H';
		String boardingNumber = '0131';
		String marketingCode = 'IB';
		String status = 'Accepted';
		String etkdNumber = '0752377200037';
		String ffNumber = 'IB12059788';
		String vipLounge = 'Test 1';
		String vipLoungePosition = 'Puesto 1';
		String specialMeals = null;
		String specialServices = null;
		String specialPAX = null;
		String boardingGate = 'S';
		String departureTime = '1155';
		String ancillaries = null;
		String minor = null;
		String inboundAirline = 'IB';
		String inboundFlight = '8999';
		String flightMarketing = '8765';
		String inboundOrigin = null;
		String inboundClass = 'I';
		String barcodeLectorType = 'Fast Track';
		String operationType = null;
		String recordId = 'IB_6339_GUIRALCANOS/JOSEPA_20180920';

		R1_VIP_Lounge_Access__c acceso = new R1_VIP_Lounge_Access__c();
		acceso.R1_VLI_TXT_passenger_name__c = passengerName;
		        
		       acceso.R1_VLI_TXT_PNR__c = pnr;
		        
		       acceso.R1_VLI_TXT_Origin__c = origin;		        
		       acceso.R1_VLI_TXT_OneWolrd_code__c = oneWolrdCode;		        
		       acceso.R1_VLI_TXT_Destination__c = destination;		        
		       acceso.R1_VLI_TXT_Comp_Flight__c = operationCompany;		        
		       acceso.R1_VLI_TXT_Flight_number__c = flight;		        
		        //Resivar este para poner la fecha con el formato correcto
		       acceso.R1_VLI_DAT_Date__c = Date.valueOf(flightDate);		        
		       acceso.R1_VLI_TXT_Class__c = classe;		        
		       acceso.R1_VLI_TXT_vip_lounge_permition__c = vipPermissions;		        
		       acceso.R1_VLI_TXT_ID_AD_indicator__c = idAdIndicator;		        
		       acceso.R1_VLI_TXT_Seat_number__c = seatNumber;		        
		       acceso.R1_VLI_TXT_Bn__c = boardingNumber;		        
		       acceso.R1_VLI_TXT_Comp_Mkt_flight__c = marketingCode;		        
		       acceso.R1_VLI_TXT_passenger_status__c = status;		        
		       acceso.R1_VLI_TXT_ETKD__c = etkdNumber;		        
		       acceso.R1_VLI_TXT_Frecuent_Flyer_number__c = ffNumber;		        
		       acceso.R1_VLI_PKL_Sala_Vip__c = vipLounge;		        
		       acceso.R1_VLI_PKL_Vip_lounge_position__c = vipLoungePosition;		        
		       acceso.R1_VLI_TXT_Meal__c = specialMeals;		        
		       acceso.R1_VLI_TXT_Special_Services__c = specialServices;		        
		       acceso.R1_VLI_TXT_Special_PAX__c = specialPAX;		        
		       acceso.R1_VLI_TXT_boarding_gate_cki__c = boardingGate;		        
		       acceso.R1_VLI_NUM_departure_tieme_cki__c = departureTime;		       
		       acceso.R1_VLI_TXT_Ancillaries__c = ancillaries;		        
		       acceso.R1_VIP_CHK_Minor__c = minor;		        
		       acceso.R1_VLI_TXT_Inbound__c = inboundAirline;		        
		       acceso.R1_VLI_TXT_Inbound_flight__c = inboundFlight;		        
		       acceso.R1_VLI_TXT_Inbound_Class__c = inboundClass;		        
		       acceso.R2_VLI_Barcode_lector_type__c = barcodeLectorType;		        
		       acceso.R2_VLI_Operation_type__c = operationType;		        
		       acceso.R2_VLI_Record_Id__c = recordId;	

		       insert acceso;	        

		operationType = 'Actualizar';
		Salas_VIP__c sala =  new Salas_VIP__c();
		sala.Name = 'Test 1 Puesto 1';
		sala.Nombre_del_Equipo__c ='Test';
		sala.Puesto_de_Trabajo__c = 'Puesto 1';
		sala.Sala_Vip__c ='Test 1';
		sala.Status__c = True;
		sala.UserId__c = 'Test';
		insert sala;

		test.startTest();
		R2_CLS_Fast_Track.createAccess(passengerName, pnr,	origin, oneWolrdCode, destination, operationCompany, flight,flightDate, classe, vipPermissions, idAdIndicator,vipLounge, seatNumber,boardingNumber, marketingCode,flightMarketing, status, etkdNumber, ffNumber, vipLoungePosition, specialMeals, specialServices, specialPAX, boardingGate, departureTime,  minor, inboundAirline, inboundFlight, inboundClass,inboundOrigin,  operationType, recordId);
		test.stopTest();

	}

	static testMethod void create_accessRechazarAccesoPuestoInactivo() {
		R1_CLS_LogHelper.throw_exception = false;
		

		String passengerName = 'GUIRALCANOS/JOSEPA';
		String pnr = 'KDZX9';
		String origin = 'MAD';
		String oneWolrdCode = 'SAPPHIRE';
		String destination = 'PTY';
		String operationCompany = 'IB';
		String flight = '6339';
		String flightDate = '2018-09-25';
		String classe = 'I';
		String vipPermissions = null;
		String idAdIndicator = null;
		String seatNumber = '004H';
		String boardingNumber = '0131';
		String marketingCode = 'IB';
		String status = 'Accepted';
		String etkdNumber = '0752377200037';
		String ffNumber = 'IB12059788';
		String vipLounge = 'Test 1';
		String vipLoungePosition = 'Puesto 1';
		String specialMeals = null;
		String specialServices = null;
		String specialPAX = null;
		String boardingGate = 'S';
		String departureTime = '1155';
		String flightMarketing = '8765';
		String inboundOrigin = null;
		String ancillaries = null;
		String minor = null;
		String inboundAirline = 'IB';
		String inboundFlight = '8999';
		String inboundClass = 'I';
		String barcodeLectorType = 'Fast Track';
		String operationType = null;
		String recordId = 'IB_6339_GUIRALCANOS/JOSEPA_20180920';

		R1_VIP_Lounge_Access__c acceso = new R1_VIP_Lounge_Access__c();
		acceso.R1_VLI_TXT_passenger_name__c = passengerName;
		        
		       acceso.R1_VLI_TXT_PNR__c = pnr;
		        
		       acceso.R1_VLI_TXT_Origin__c = origin;		        
		       acceso.R1_VLI_TXT_OneWolrd_code__c = oneWolrdCode;		        
		       acceso.R1_VLI_TXT_Destination__c = destination;		        
		       acceso.R1_VLI_TXT_Comp_Flight__c = operationCompany;		        
		       acceso.R1_VLI_TXT_Flight_number__c = flight;		        
		        //Resivar este para poner la fecha con el formato correcto
		       acceso.R1_VLI_DAT_Date__c = Date.valueOf(flightDate);		        
		       acceso.R1_VLI_TXT_Class__c = classe;		        
		       acceso.R1_VLI_TXT_vip_lounge_permition__c = vipPermissions;		        
		       acceso.R1_VLI_TXT_ID_AD_indicator__c = idAdIndicator;		        
		       acceso.R1_VLI_TXT_Seat_number__c = seatNumber;		        
		       acceso.R1_VLI_TXT_Bn__c = boardingNumber;		        
		       acceso.R1_VLI_TXT_Comp_Mkt_flight__c = marketingCode;		        
		       acceso.R1_VLI_TXT_passenger_status__c = status;		        
		       acceso.R1_VLI_TXT_ETKD__c = etkdNumber;		        
		       acceso.R1_VLI_TXT_Frecuent_Flyer_number__c = ffNumber;		        
		       acceso.R1_VLI_PKL_Sala_Vip__c = vipLounge;		        
		       acceso.R1_VLI_PKL_Vip_lounge_position__c = vipLoungePosition;		        
		       acceso.R1_VLI_TXT_Meal__c = specialMeals;		        
		       acceso.R1_VLI_TXT_Special_Services__c = specialServices;		        
		       acceso.R1_VLI_TXT_Special_PAX__c = specialPAX;		        
		       acceso.R1_VLI_TXT_boarding_gate_cki__c = boardingGate;		        
		       acceso.R1_VLI_NUM_departure_tieme_cki__c = departureTime;		       
		       acceso.R1_VLI_TXT_Ancillaries__c = ancillaries;		        
		       acceso.R1_VIP_CHK_Minor__c = minor;		        
		       acceso.R1_VLI_TXT_Inbound__c = inboundAirline;		        
		       acceso.R1_VLI_TXT_Inbound_flight__c = inboundFlight;		        
		       acceso.R1_VLI_TXT_Inbound_Class__c = inboundClass;		        
		       acceso.R2_VLI_Barcode_lector_type__c = barcodeLectorType;		        
		       acceso.R2_VLI_Operation_type__c = operationType;		        
		       acceso.R2_VLI_Record_Id__c = recordId;	

		       insert acceso;	        

		Salas_VIP__c sala =  new Salas_VIP__c();
		sala.Name = 'Test 1 Puesto 1';
		sala.Nombre_del_Equipo__c ='Test';
		sala.Puesto_de_Trabajo__c = 'Puesto 1';
		sala.Sala_Vip__c ='Test 1';
		sala.Status__c = False;
		sala.UserId__c = 'Test';
		insert sala;

		test.startTest();
		R2_CLS_Fast_Track.createAccess(passengerName, pnr,	origin, oneWolrdCode, destination, operationCompany, flight,flightDate, classe, vipPermissions, idAdIndicator,vipLounge, seatNumber,boardingNumber, marketingCode,flightMarketing, status, etkdNumber, ffNumber, vipLoungePosition, specialMeals, specialServices, specialPAX, boardingGate, departureTime,  minor, inboundAirline, inboundFlight, inboundClass,inboundOrigin,  operationType, recordId);
		test.stopTest();

	}

	static testMethod void create_accessRechazarPuestoInexistente() {
		R1_CLS_LogHelper.throw_exception = false;
		

		String passengerName = 'GUIRALCANOS/JOSEPA';
		String pnr = 'KDZX9';
		String origin = 'MAD';
		String oneWolrdCode = 'SAPPHIRE';
		String destination = 'PTY';
		String operationCompany = 'IB';
		String flight = '6339';
		String flightDate = '2018-09-25';
		String classe = 'I';
		String vipPermissions = null;
		String idAdIndicator = null;
		String seatNumber = '004H';
		String boardingNumber = '0131';
		String marketingCode = 'IB';
		String status = 'Accepted';
		String etkdNumber = '0752377200037';
		String ffNumber = 'IB12059788';
		String vipLounge = 'Test 1';
		String vipLoungePosition = 'Puesto 2';
		String specialMeals = null;
		String specialServices = null;
		String specialPAX = null;
		String boardingGate = 'S';
		String departureTime = '1155';
		String ancillaries = null;
		String flightMarketing = '8765';
		String inboundOrigin = null;
		String minor = null;
		String inboundAirline = 'IB';
		String inboundFlight = '8999';
		String inboundClass = 'I';
		String barcodeLectorType = 'Fast Track';
		String operationType = null;
		String recordId = 'IB_6339_GUIRALCANOS/JOSEPA_20180920';

		R1_VIP_Lounge_Access__c acceso = new R1_VIP_Lounge_Access__c();
		acceso.R1_VLI_TXT_passenger_name__c = passengerName;
		        
		       acceso.R1_VLI_TXT_PNR__c = pnr;
		        
		       acceso.R1_VLI_TXT_Origin__c = origin;		        
		       acceso.R1_VLI_TXT_OneWolrd_code__c = oneWolrdCode;		        
		       acceso.R1_VLI_TXT_Destination__c = destination;		        
		       acceso.R1_VLI_TXT_Comp_Flight__c = operationCompany;		        
		       acceso.R1_VLI_TXT_Flight_number__c = flight;		        
		        //Resivar este para poner la fecha con el formato correcto
		       acceso.R1_VLI_DAT_Date__c = Date.valueOf(flightDate);		        
		       acceso.R1_VLI_TXT_Class__c = classe;		        
		       acceso.R1_VLI_TXT_vip_lounge_permition__c = vipPermissions;		        
		       acceso.R1_VLI_TXT_ID_AD_indicator__c = idAdIndicator;		        
		       acceso.R1_VLI_TXT_Seat_number__c = seatNumber;		        
		       acceso.R1_VLI_TXT_Bn__c = boardingNumber;		        
		       acceso.R1_VLI_TXT_Comp_Mkt_flight__c = marketingCode;		        
		       acceso.R1_VLI_TXT_passenger_status__c = status;		        
		       acceso.R1_VLI_TXT_ETKD__c = etkdNumber;		        
		       acceso.R1_VLI_TXT_Frecuent_Flyer_number__c = ffNumber;		        
		       acceso.R1_VLI_PKL_Sala_Vip__c = vipLounge;		        
		       acceso.R1_VLI_PKL_Vip_lounge_position__c = vipLoungePosition;		        
		       acceso.R1_VLI_TXT_Meal__c = specialMeals;		        
		       acceso.R1_VLI_TXT_Special_Services__c = specialServices;		        
		       acceso.R1_VLI_TXT_Special_PAX__c = specialPAX;		        
		       acceso.R1_VLI_TXT_boarding_gate_cki__c = boardingGate;		        
		       acceso.R1_VLI_NUM_departure_tieme_cki__c = departureTime;		       
		       acceso.R1_VLI_TXT_Ancillaries__c = ancillaries;		        
		       acceso.R1_VIP_CHK_Minor__c = minor;		        
		       acceso.R1_VLI_TXT_Inbound__c = inboundAirline;		        
		       acceso.R1_VLI_TXT_Inbound_flight__c = inboundFlight;		        
		       acceso.R1_VLI_TXT_Inbound_Class__c = inboundClass;		        
		       acceso.R2_VLI_Barcode_lector_type__c = barcodeLectorType;		        
		       acceso.R2_VLI_Operation_type__c = operationType;		        
		       acceso.R2_VLI_Record_Id__c = recordId;	

		       insert acceso;	        

		Salas_VIP__c sala =  new Salas_VIP__c();
		sala.Name = 'Test 1 Puesto 1';
		sala.Nombre_del_Equipo__c ='Test';
		sala.Puesto_de_Trabajo__c = 'Puesto 1';
		sala.Sala_Vip__c ='Test 1';
		sala.Status__c = False;
		sala.UserId__c = 'Test';
		insert sala;

		test.startTest();
		R2_CLS_Fast_Track.createAccess(passengerName, pnr,	origin, oneWolrdCode, destination, operationCompany, flight,flightDate, classe, vipPermissions, idAdIndicator,vipLounge, seatNumber,boardingNumber, marketingCode,flightMarketing, status, etkdNumber, ffNumber, vipLoungePosition, specialMeals, specialServices, specialPAX, boardingGate, departureTime,  minor, inboundAirline, inboundFlight, inboundClass,inboundOrigin,  operationType, recordId);
		test.stopTest();

	}
}