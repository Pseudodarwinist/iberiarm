/*---------------------------------------------------------------------------------------------------------------------
    Author:         Alvaro Garcia Tapia
    Company:        Accenture
    Description:    Apex test para la clase apex 'R2_CLS_SocialPersonaTriggerMethods'
    IN:             
    OUT:            

    History: 
     <Date>                     <Author>                         <Change Description>
    24/09/2018             Alvaro Garcia Tapia                   Initial Version
    ----------------------------------------------------------------------------------------------------------------------*/
@isTest
private class R2_CLS_SocialPersonaTriggerMethods_Test {
    /*---------------------------------------------------------------------------------------------------------------------
    Author:         Alvaro Garcia Tapia
    Company:        Accenture
    Description:    Metodo test para la funcion udpateAccountInCases()
    IN:             
    OUT:            

    History: 
     <Date>                     <Author>                         <Change Description>
    24/09/2018             Alvaro Garcia Tapia                   Initial Version
    ----------------------------------------------------------------------------------------------------------------------*/
    static testMethod void udpateAccountInCasesTest(){
        R1_CLS_LogHelper.throw_exception = false;  
      
        List<Account> lstClientes = new List<Account>();
        
        Id recordTypeIdAcc = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cuenta personal').getRecordTypeId();
        Id recordTypeIdCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Asistencia').getRecordTypeId();

        List<Account> lst_Acc = new List<Account>();
        Account acc = new Account();
        acc.RecordTypeId = recordTypeIdAcc;
        acc.LastName = 'Test';
        acc.R1_ACC_TLF_Phone_Marketing__c = '123456789';
        acc.R1_ACC_TXT_Id_Golden_record__c = 'Test';
        lst_Acc.add(acc);

        Account acc2 = new Account();
        acc2.RecordTypeId = recordTypeIdAcc;
        acc2.LastName = 'Test2';
        acc2.R1_ACC_TLF_Phone_Marketing__c = '123456788';
        acc2.R1_ACC_TXT_Id_Golden_record__c = 'Test2';
        lst_Acc.add(acc2);
        insert lst_Acc;

        SocialPersona persona = new SocialPersona();
        persona.ParentId = acc.Id;
        persona.Name = 'personaName';
        persona.Provider = 'Twitter';
        insert persona;

        Case caso = new Case();
        caso.AccountId = acc.Id;
        insert caso;

        SocialPost post = new SocialPost();
        post.Name = 'postName';
        post.PersonaId = persona.Id;
        post.Sentiment = 'Neutral';
        post.Classification = 'Comunicación Entrante';
        post.R2_SP_CHK_Influencer__c = true;
        post.PostTags = 'Spanish,Prensa,Acceso';
        post.Provider = 'Twitter';
        post.ParentId = caso.Id;
        insert post;

        Test.startTest();
            
            persona.ParentId = acc2.Id;
            update persona;

        Test.stopTest();
        
        //System.assertEquals(0, [SELECT count() FROM R1_Log__c]);

    }

/*---------------------------------------------------------------------------------------------------------------------
    Author:         Alvaro Garcia Tapia
    Company:        Accenture
    Description:    Metodo test para cubrir los catch()
    IN:
    OUT:

    History:
     <Date>                     <Author>                         <Change Description>
    24/09/2018             Alvaro Garcia Tapia                     Initial Version
    ----------------------------------------------------------------------------------------------------------------------*/
    static testMethod void exception_Test(){
        R1_CLS_LogHelper.throw_exception = true;

        R2_CLS_SocialPersonaTriggerMethods.updateAccountInCases(null, null);
    }
}