public with sharing class R2_CLS_CaratulaPago_CTRL {
		public class DatosPagoWP{
		public String codigoExpediente {get; set;}
		public String pir {get; set;}
		public String tipoExpediente {get; set;}
		public String estado {get; set;}
		public String ordenTrabajo {get; set;}
		public String prestarioServicio {get; set;}
		public String numVueloUltimaCia {get; set;}
		public String fechaVuelo {get; set;}
		public String fdRouting {get; set;}
		public String fechaApertura {get; set;}
		public String fechaEmision {get; set;}
		public String fechaEnvio {get; set;}
		public String prestacionOt {get; set;}
		public String pc {get; set;}
		public String facturacionServicio {get; set;}
		public String cif {get; set;}
		public String conceptoFacturacion {get; set;}
		public String numeroPedido {get; set;}
	}
	public class DatosPasajeroWP{
		public String nombre {get; set;}
		public String tipoDocumento {get; set;}
		public String numeroDocumento {get; set;}

		public String direccion {get; set;}
		public String ciudad {get; set;}
		public String localidad {get; set;}
		public String distritoPostal {get; set;}
		public String pais {get; set;}

		public String numeroCuenta {get; set;}
		public String cuentaExtranjera {get; set;}
		public String otrosDatosBancarios {get; set;}
		public String swiftBic {get; set;}
		public String fedAba {get; set;}
		public String chipsAba {get; set;}
		public String iban {get; set;}
		public String chequeRef {get; set;}
		public String entidadPagadora {get; set;}
	}

	public class ImporteWP{
		public String euro {get; set;}
		public String monedaPropia {get; set;}
	}

	public class ComprobanteWP{
		public DatosPagoWP datosPago {get; set;}
		public DatosPasajeroWP datosPasajero {get; set;}
		public ImporteWP importe {get; set;}
	}

	public String render {get; set;}

	private String idPro;

	private R2_Prorrateo__c prorrateo;
	private R2_Segmento__c segmento;

	private Case expediente;
	private R1_Incident__c pir;

	public R2_CLS_CaratulaPago_CTRL() {
		idPro = (String) ApexPages.currentPage().getParameters().get('pro');
		render = ApexPages.currentPage().getParameters().get('render')!='' && ApexPages.currentPage().getParameters().get('render')!=null ? (String) ApexPages.currentPage().getParameters().get('render') : 'pdf';

		// prorrateo
		prorrateo = idPro!=null && idPro!='' ? getProrrateo(idPro) : null;
		// Segmento
		segmento = prorrateo!=null ? getSegmento(prorrateo.Id) : null;		
		// expediente
		expediente = prorrateo!=null && prorrateo.R2_PRO_LOO_Case__c!=null ? getCase(prorrateo.R2_PRO_LOO_Case__c) : null;
		// pir
		pir = prorrateo!=null && prorrateo.R2_PRO_LOO_PIR__c!=null ? getIncidence(prorrateo.R2_PRO_LOO_PIR__c) : null;
		
	}

	public ComprobanteWP getComprobantePayOrd(){
		try{
			if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}

				// orden pago
				R2_Payment_order__c paymentOrder = prorrateo!=null && prorrateo.R2_PRO_TXT_payment_id__c!=null ? getPaymentOrder(prorrateo.R2_PRO_TXT_payment_id__c) : null;

				// Datos Pago
				DatosPagoWP datosPago = new DatosPagoWP();
				datosPago.codigoExpediente = expediente!=null && expediente.R1_CAS_FOR_Case_Number__c!=null ? expediente.R1_CAS_FOR_Case_Number__c : '';
				datosPago.pir = pir!=null && pir.R2_INC_FOR_PIR__c!=null ? pir.R2_INC_FOR_PIR__c : '';
				datosPago.fechaApertura = pir!=null && pir.R2_INC_DAT_PIR_Claims_Created_Date__c!=null ? Date.valueOf(pir.R2_INC_DAT_PIR_Claims_Created_Date__c).format() : '';
				datosPago.tipoExpediente = expediente!=null && expediente.Type!=null ? translateType(expediente.Type) : '';
				datosPago.estado = paymentOrder!=null && paymentOrder.R2_OPY_PCK_Status__c!=null ? paymentOrder.R2_OPY_PCK_Status__c : '';
				datosPago.fechaEmision = paymentOrder!=null && paymentOrder.R2_OPY_DAT_Date_Issue_Remittance__c!=null ? Date.valueOf(paymentOrder.R2_OPY_DAT_Date_Issue_Remittance__c).format() : '';
				datosPago.fechaEnvio = '';
				datosPago.ordenTrabajo = '';
				datosPago.prestacionOt = '';
				datosPago.prestarioServicio = '';
				datosPago.numVueloUltimaCia = segmento!=null && segmento.R2_SEG_TXT_Flight__c!=null ? segmento.R2_SEG_TXT_Flight__c : '';
				datosPago.fechaVuelo = segmento!=null && segmento.R2_SEG_DAT_Date__c!=null ? Date.valueOf(segmento.R2_SEG_DAT_Date__c).format() : '';
				//datosPago.fdRouting = pir!=null && pir.R2_INC_TXT_FD__c!=null ? pir.R2_INC_TXT_FD__c : '';
				datosPago.fdRouting = pir!=null && pir.R2_INC_TXT_PIR_flight_Number__c!=null ? pir.R2_INC_TXT_PIR_flight_Number__c : '';
				Account acc = paymentOrder!=null && paymentOrder.R2_OPY_LOO_File_associated__r.AccountId!=null ? getAccount(paymentOrder.R2_OPY_LOO_File_associated__r.AccountId) : null;
				// Datos Pasajero
				
				DatosPasajeroWP datosPasajero = new DatosPasajeroWP();
				datosPasajero.nombre = acc!=null && acc.Name!=null ? acc.Name : '';
				datosPasajero.tipoDocumento = paymentOrder!=null && paymentOrder.R2_OPY_PKL_Document_Type__c!=null ? getLavelPKLDocType(paymentOrder.R2_OPY_PKL_Document_Type__c,'R2_Payment_order__c') : '';
				datosPasajero.numeroDocumento = paymentOrder!=null && paymentOrder.R2_OPY_TXT_NIF__c!=null ? paymentOrder.R2_OPY_TXT_NIF__c : '';
				datosPasajero.direccion = paymentOrder!=null && paymentOrder.R2_OPY_TXT_Address1__c!=null ? String.valueOf(paymentOrder.R2_OPY_TXT_Address1__c) : '';
				datosPasajero.ciudad = paymentOrder!=null && paymentOrder.R2_OPY_TXT_City__c!=null ? paymentOrder.R2_OPY_TXT_City__c : '';
				datosPasajero.localidad = paymentOrder!=null && paymentOrder.R2_OPY_TXT_City__c!=null ? paymentOrder.R2_OPY_TXT_City__c : '';
				datosPasajero.distritoPostal = paymentOrder!=null && paymentOrder.R2_OPY_TXT_Postal_Code__c!=null ? paymentOrder.R2_OPY_TXT_Postal_Code__c : '';
				datosPasajero.pais = paymentOrder!=null && paymentOrder.R2_OPY_TXT_Bank_Country__c!=null ? paymentOrder.R2_OPY_TXT_Bank_Country__c : '';

				datosPasajero.numeroCuenta = paymentOrder!=null && paymentOrder.R2_OPY_TXT_Bank_Account__c!=null ? paymentOrder.R2_OPY_TXT_Bank_Account__c : '';
				datosPasajero.cuentaExtranjera = paymentOrder!=null && paymentOrder.R2_OPY_TXT_External_Local_Account__c!=null ? paymentOrder.R2_OPY_TXT_External_Local_Account__c : '';
				datosPasajero.otrosDatosBancarios = paymentOrder!=null && paymentOrder.R2_OPY_ATXTL_Others__c!=null ? paymentOrder.R2_OPY_ATXTL_Others__c : '';
				datosPasajero.swiftBic = paymentOrder!=null && paymentOrder.R2_OPY_TXT_SWIFT__c!=null ? paymentOrder.R2_OPY_TXT_SWIFT__c : '';
				datosPasajero.fedAba = paymentOrder!=null && paymentOrder.R2_OPY_TXT_Fed_ABA__c!=null ? paymentOrder.R2_OPY_TXT_Fed_ABA__c : '';
				datosPasajero.chipsAba = paymentOrder!=null && paymentOrder.R2_OPY_TXT_Chips_ABA__c!=null ? paymentOrder.R2_OPY_TXT_Chips_ABA__c : '';
				datosPasajero.iban = paymentOrder!=null && paymentOrder.R2_OPY_TXT_IBAN__c!=null ? getencrypted(paymentOrder.R2_OPY_TXT_IBAN__c) : '';
				datosPasajero.chequeRef = paymentOrder!=null && paymentOrder.R2_OPY_TXT_Check_Num__c!=null ? paymentOrder.R2_OPY_TXT_Check_Num__c : '';
				datosPasajero.entidadPagadora = paymentOrder!=null && paymentOrder.R2_OPY_TXT_Bank_Name__c!=null ? paymentOrder.R2_OPY_TXT_Bank_Name__c : '';

				// Importe
				ImporteWP importe = new ImporteWP();
				importe.euro = prorrateo!=null ? getTotalPayOrd(prorrateo.R2_PRO_TXT_payment_id__c) : '';
				importe.monedaPropia = '';

				ComprobanteWP comprobante = new ComprobanteWP();
				comprobante.datosPago = datosPago;
				comprobante.datosPasajero = datosPasajero;
				comprobante.importe = importe;

				return comprobante;

		}catch(Exception exc){
			System.debug('Type: '+ exc.getTypeName()+ ' Trace: '+ exc.getStackTraceString());
        	// R1_CLS_LogHelper.generateErrorLog('PdfOrdenPagoController.getComprobantePayOrd()', '', exc.getmessage()+', '+exc.getLineNumber(), '');
			return null;
        }
	}

	public ComprobanteWP getComprobanteWorkOrd(){
		try{
			if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}

				R2_Work_Order__c workOrder = prorrateo!=null && prorrateo.R2_PRO_TXT_work_order__c!=null ? getWorkOrder(prorrateo.R2_PRO_TXT_work_order__c) : null;

				// Datos Pago
				DatosPagoWP datosPago = new DatosPagoWP();
				datosPago.codigoExpediente = expediente!=null && expediente.R1_CAS_FOR_Case_Number__c!=null ? expediente.R1_CAS_FOR_Case_Number__c : '';
				datosPago.pir = pir!=null && pir.R2_INC_FOR_PIR__c!=null ? pir.R2_INC_FOR_PIR__c : '';
				datosPago.fechaApertura = pir!=null && pir.R2_INC_DAT_PIR_Claims_Created_Date__c!=null ? Date.valueOf(pir.R2_INC_DAT_PIR_Claims_Created_Date__c).format() : '';
				datosPago.tipoExpediente = expediente!=null && expediente.Type!=null ? expediente.Type : '';
				datosPago.estado = workOrder!=null && workOrder.R2_WO_PKL_status__c!=null ? workOrder.R2_WO_PKL_status__c : '';
				datosPago.fechaEmision = workOrder!=null && workOrder.R2_WO_DAT_Opening_date__c!=null ? Date.valueOf(workOrder.R2_WO_DAT_Opening_date__c).format() : '';
				datosPago.fechaEnvio = '';
				datosPago.ordenTrabajo = workOrder!=null && workOrder.Name!=null ? workOrder.Name : '';
				datosPago.prestacionOt = workOrder!=null && workOrder.R2_WO_TXT_OT__c!=null ? workOrder.R2_WO_TXT_OT__c : '';
				datosPago.prestarioServicio = workOrder!=null && workOrder.R2_WO_PKL_type__c!=null ? workOrder.R2_WO_PKL_type__c : '';
				datosPago.numVueloUltimaCia = segmento!=null && segmento.R2_SEG_TXT_Flight__c!=null ? segmento.R2_SEG_TXT_Flight__c : '';
				datosPago.fechaVuelo = segmento!=null && segmento.R2_SEG_DAT_Date__c!=null ? Date.valueOf(segmento.R2_SEG_DAT_Date__c).format() : '';
				//datosPago.fdRouting = pir!=null && pir.R2_INC_TXT_FD__c!=null ? pir.R2_INC_TXT_FD__c : '';
				datosPago.fdRouting = pir!=null && pir.R2_INC_TXT_PIR_flight_Number__c!=null ? pir.R2_INC_TXT_PIR_flight_Number__c : '';
				//
				datosPago.pc = '';
				datosPago.facturacionServicio = ''; 
				datosPago.cif = '';
				datosPago.conceptoFacturacion = '';
				datosPago.numeroPedido = '';


				Account acc = workOrder!=null && workOrder.R2_WO_LOO_Case__r.AccountId!=null ? getAccount(workOrder.R2_WO_LOO_Case__r.AccountId) : null;
				// Datos Pasajero
				DatosPasajeroWP datosPasajero = new DatosPasajeroWP();
				datosPasajero.nombre = acc!=null && acc.Name!=null ? acc.Name : '';
				datosPasajero.tipoDocumento = workOrder!=null && workOrder.R2_WO_TXT_Document_Type__c!=null ?workOrder.R2_WO_TXT_Document_Type__c : '';
				datosPasajero.numeroDocumento = workOrder!=null && workOrder.R2_WO_TXT_Document_Number__c!=null ? workOrder.R2_WO_TXT_Document_Number__c : '';
				datosPasajero.direccion = workOrder!=null && workOrder.R2_WO_TXT_delivery_address__c!=null ? workOrder.R2_WO_TXT_delivery_address__c: '';
				datosPasajero.ciudad = workOrder!=null && workOrder.R2_WO_TXT_delivery_city__c!=null ? workOrder.R2_WO_TXT_delivery_city__c : '';
				datosPasajero.localidad = workOrder!=null && workOrder.R2_WO_TXT_delivery_city__c!=null ? workOrder.R2_WO_TXT_delivery_city__c : '';
				datosPasajero.distritoPostal = workOrder!=null && workOrder.R2_WO_TXT_delivery_Postal_Code__c!=null ? workOrder.R2_WO_TXT_delivery_Postal_Code__c : '';
				datosPasajero.pais = workOrder!=null && workOrder.R2_WO_TXT_delivery_Country__c!=null ? workOrder.R2_WO_TXT_delivery_Country__c : '';

				// Importe
				ImporteWP importe = new ImporteWP();
				importe.euro = prorrateo!=null ? getTotalWorkOrd(prorrateo.R2_PRO_TXT_work_order__c) : '';
				importe.monedaPropia = '';

				ComprobanteWP comprobante = new ComprobanteWP();
				comprobante.datosPago = datosPago;
				comprobante.datosPasajero = datosPasajero;
				comprobante.importe = importe;

				return comprobante;

		}catch(Exception exc){
			System.debug('Type: '+ exc.getTypeName()+ ' Trace: '+ exc.getStackTraceString());
        	//R1_CLS_LogHelper.generateErrorLog('PdfOrdenPagoController.getComprobanteWorkOrd()', '', exc.getmessage()+', '+exc.getLineNumber(), '');
			return null;
        }
	}

	@TestVisible
	private R2_Payment_order__c getPaymentOrder(String val){
		try{

		if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}

		R2_Payment_order__c ordenPago = null;
		Set<Id> setIds = getIdsFromStr(val);
		if(setIds!=null && !setIds.isEmpty()){
			List<R2_Payment_order__c> listPayOrd = [SELECT Id, R2_OPY_PCK_Status__c, R2_OPY_LOO_File_associated__c, 
													R2_OPY_TXT_Bank_Account__c, R2_OPY_TXT_External_Local_Account__c, 
													R2_OPY_ATXTL_Others__c, R2_OPY_TXT_SWIFT__c, R2_OPY_TXT_Fed_ABA__c, 
													R2_OPY_TXT_Chips_ABA__c, R2_OPY_TXT_IBAN__c, R2_OPY_TXT_Check_Num__c, 
													R2_OPY_TXT_Bank_Name__c, R2_OPY_DIV_Amount__c, R2_OPY_TXT_Address1__c, 
													R2_OPY_TXT_City__c,R2_OPY_FOR_Country__c,R2_OPY_TXT_Postal_Code__c,
													R2_OPY_TXT_NIF_Name__c, R2_OPY_PKL_Document_Type__c, 
													R2_OPY_TXT_NIF__c, R2_OPY_LOO_CaseAccount__c,R2_OPY_DAT_Date_Issue_Remittance__c,
													R2_OPY_LOO_File_associated__r.AccountId,R2_OPY_TXT_Bank_Country__c
												FROM R2_Payment_order__c 
												WHERE Id IN:setIds ORDER BY R2_OPY_DAT_Date_Issue_Remittance__c DESC LIMIT 1];
			ordenPago = listPayOrd.size()>0 ? listPayOrd[0] : null;
		}

		return ordenPago;
		}catch(Exception exc){
			System.debug('Type: '+ exc.getTypeName()+ ' Trace: '+ exc.getStackTraceString());
        	return null;
        }
	}

	@TestVisible
	private R2_Work_Order__c getWorkOrder(String val){
		try{
			if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}

			R2_Work_Order__c workOrder = null;
			Set<Id> setIds = getIdsFromStr(val);
			if(setIds!=null && !setIds.isEmpty()){
				List<R2_Work_Order__c> listWorOrd = [SELECT Id,R2_WO_LOO_Case__c,R2_WO_TXT_PIR__c, R2_WO_PKL_status__c,
														R2_WO_DAT_Opening_date__c,R2_WO_TXT_OT__c,Name,R2_WO_TXT_Last_Name__c,
														R2_WO_TXT_Description_Borrower__c,R2_WO_TXT_Name__c,
														R2_WO_TXT_Document_Type__c,R2_WO_TXT_Document_Number__c,
														R2_WO_NUM_Amount_tot__c,R2_WO_TXT_delivery_address__c,R2_WO_TXT_delivery_city__c,
														R2_WO_TXT_delivery_Country__c,R2_WO_TXT_delivery_Postal_Code__c,R2_WO_TXT_TN_Bag__c,
														R2_WO_PKL_type__c, R2_WO_LOO_Case__r.AccountId   
													FROM R2_Work_Order__c 
													WHERE Id IN:setIds ORDER BY R2_WO_DAT_Opening_date__c DESC LIMIT 1];
				workOrder = listWorOrd.size()>0 ? listWorOrd[0] : null;
			}

			return workOrder;
		}catch(Exception exc){
			System.debug('Type: '+ exc.getTypeName()+ ' Trace: '+ exc.getStackTraceString());
        	return null;
        }
	}

	@TestVisible
	private String getTotalPayOrd(String val){
		try{
			if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}
			String total = '';
			Set<Id> setIds = getIdsFromStr(val);
			if(setIds!=null && !setIds.isEmpty()){
				List<AggregateResult> res = [SELECT SUM(R2_OPY_DIV_Amount__c) total FROM R2_Payment_order__c WHERE Id IN:setIds];
				total = res!=null && res.size()>0 ? String.valueOf(res[0].get('total')) : '';
			}
			return total;
		}catch(Exception exc){
			System.debug('Type: '+ exc.getTypeName()+ ' Trace: '+ exc.getStackTraceString());
        	return '';
        }
	}

	@TestVisible
	private String getTotalWorkOrd(String val){
		try{
			if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}
			String total = '';
			Set<Id> setIds = getIdsFromStr(val);
			if(setIds!=null && !setIds.isEmpty()){
				List<AggregateResult> res = [SELECT SUM(R2_WO_NUM_Amount_tot__c) total FROM R2_Work_Order__c WHERE Id IN:setIds];
				total = res!=null && res.size()>0 ? String.valueOf(res[0].get('total')) : '';
			}
			return total;
		}catch(Exception exc){
			System.debug('Type: '+ exc.getTypeName()+ ' Trace: '+ exc.getStackTraceString());
        	return '';
        }
	}

	@TestVisible
	private R2_Prorrateo__c getProrrateo(String proId){
		try{
			if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}
			R2_Prorrateo__c prorrateo = null;

			List<R2_Prorrateo__c> listPro = [SELECT Id, R2_PRO_DAT_Date__c, R2_PRO_LOO_PIR__c,
												R2_PRO_TXT_payment_id__c, R2_PRO_TXT_work_order__c,
												R2_PRO_LOO_Case__c 
										FROM R2_Prorrateo__c 
										WHERE Id=:proId];

			prorrateo = listPro.size()>0 ? listPro[0] : null;

			return prorrateo;
		}catch(Exception exc){
			System.debug('Type: '+ exc.getTypeName()+ ' Trace: '+ exc.getStackTraceString());
        	return null;
        }
	}

	@TestVisible
	private R2_Segmento__c getSegmento(String proId){
		try{
			if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}
			R2_Segmento__c segmento = null;
			List<R2_Segmento__c> listSeg = [SELECT Id, R2_SEG_TXT_Flight__c, R2_SEG_DAT_Date__c, R2_SEG_TXT_segment_order__c 
										FROM R2_Segmento__c 
										WHERE R2_SEG_MSDT_Prorrate__c=:proId 
										ORDER By R2_SEG_TXT_segment_order__c DESC 
										LIMIT 1];
			segmento = listSeg.size()>0 ? listSeg[0] : null;
			return segmento;
		}catch(Exception exc){
			System.debug('Type: '+ exc.getTypeName()+ ' Trace: '+ exc.getStackTraceString());
        	return null;
        }
	}

	@TestVisible
	private Case getCase(String idCase){
		try{
			if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}
			Case cas = null;
			List<Case> listCase = [SELECT Id, CaseNumber, R2_CAS_LOO_PIR__c, R1_CAS_FOR_Case_Number__c, Type, Status, R2_CAS_TXT_PASS_Name_Passenger__c,R2_CAS_PKL_Type_document__c,R2_CAS_TXT_Identification_number__c,R2_CAS_TXT_PASS_LastName_Passenger__c FROM Case WHERE Id=:idCase];
			cas = listCase.size()>0 ? listCase[0] : null;
			return cas;
		}catch(Exception exc){
			System.debug('Type: '+ exc.getTypeName()+ ' Trace: '+ exc.getStackTraceString());
        	return null;
        }
	}

	@TestVisible
	private R1_Incident__c getIncidence(String idInc){
		try{
			if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}
			R1_Incident__c inc = new R1_Incident__c();
			List<R1_Incident__c> listInc = [SELECT Id, R2_INC_FOR_PIR__c, R2_INC_TXT_FD__c,R2_INC_DAT_PIR_Claim_Date__c, R2_INC_DAT_PIR_Claims_Created_Date__c, R2_INC_TXT_PIR_flight_Number__c FROM R1_Incident__c WHERE Id=:idInc];
			inc = listInc.size()>0 ? listInc[0] : null;
			return inc;
		}catch(Exception exc){
			System.debug('Type: '+ exc.getTypeName()+ ' Trace: '+ exc.getStackTraceString());
        	return null;
        }
	}

	@TestVisible
	private static Set<Id> getIdsFromStr(String value){
		try{
			if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}
			Set<Id> setIds = new Set<Id>();
			if(value!=null && value!=''){
				String idsSrt = value.deleteWhitespace();
				List<String> listIds = idsSrt.split(',');
				for(String str :  listIds){
					try{ 
						setIds.add( Id.valueOf(str) );
					}catch(Exception exc){}
				}
			}
			return setIds;
		}catch(Exception exc){
			System.debug('Type: '+ exc.getTypeName()+ ' Trace: '+ exc.getStackTraceString());
        	return null;
        }
    }

	@TestVisible
	private String getLavelPKLDocType(String value, String objName){
		try{
			if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}
			
			String result = '';
			Schema.DescribeFieldResult describe = null;
			if(objName=='R2_Payment_order__c'){
				describe = R2_Payment_order__c.R2_OPY_PKL_Document_Type__c.getDescribe();
			}else if(objName=='Case'){
				describe = Case.R2_CAS_PKL_Type_document__c.getDescribe();
			}
			if(describe!=null){
				List<Schema.PicklistEntry> listValues = describe.getPicklistValues();
				for( Schema.PicklistEntry f :  listValues){
					if (f.getValue()==value) result = f.getLabel();
				}
			}

			return result;
		}catch(Exception exc){
			System.debug('Type: '+ exc.getTypeName()+ ' Trace: '+ exc.getStackTraceString());
        	return '';
        }
	}

	@TestVisible
	private Account getAccount(String idAcc){
		try{
			if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}
			Account acc = null;
			List<Account> listAcc = [SELECT Id, Name, R1_ACC_PKL_identification_Type__c,R1_ACC_TXT_Identification_number__c,
									BillingAddress,BillingCity,BillingCountry,BillingPostalCode,BillingState
									FROM Account WHERE Id=:idAcc];
			acc = listAcc.size()>0 ? listAcc[0] : null;
			return acc;
		}catch(Exception exc){
			System.debug('Type: '+ exc.getTypeName()+ ' Trace: '+ exc.getStackTraceString());
        	return null;
        }
	}
	
	@TestVisible
	private String getencrypted(String iban){
		Integer tamano = iban.length();
		if(tamano >10){
			String asteriscos = '';
			for(Integer i=0; i< tamano -10 ; i++){
				asteriscos+='*';
			}
			System.debug('IBAN ' + iban);
			System.debug('encriptado ' + iban.substring(0,6) + asteriscos + iban.substring((tamano -4) ,tamano));
			return iban.substring(0,6) + asteriscos + iban.substring((tamano -4) ,tamano);
		}
		return iban;
	}

	@TestVisible
	private String translateType(String type){
		//Demora- Delay; Extravío – Loss; Deterioro – Damage; Falta de contenido – Pilferage
		if(type =='Demora'){
			return 'Delay';
		}else if (type == 'Extravío'){
			return 'Loss';
		}else if (type == 'Deterioro'){
			return 'Damage';
		}else if (type == 'Falta de contenido'){
			return 'Pilferage';
		}
		return type;
	}
}