@isTest
public class R2_SurveyTriggerMethods_Test {
	
    @isTest
    public static void assignCase_Test(){
        R1_CLS_LogHelper.throw_exception = false;
        Id rt_case =  Schema.SObjectType.Case.getRecordTypeInfosByName().get('Comunicaciones entrantes').getRecordTypeId();
        Case caso = new Case();
        caso.Status = 'Cerrado';
        caso.RecordTypeId = rt_case;
        caso.Type = 'Sin información';
        caso.Origin = 'Llamada';
        insert caso;
        Task tarea = new Task();
        tarea.thinkConnect__UCID__c='1234567890';
        tarea.WhatId = caso.Id;
        insert tarea;
        R2_Survey__c encuesta = new R2_Survey__c();
        encuesta.R2_SRV_TXT_Call_Id__c ='1234567890';
        encuesta.R2_SRV_TXT_Question_1__c ='TEST?';
        encuesta.R2_SRV_TXT_Answer_1__c = '1';
        Test.startTest();
        insert encuesta;
        Test.stopTest();
    }
    
    @IsTest
    public static void exception_Test(){
        R1_CLS_LogHelper.throw_exception = true;
        R2_SurveyTriggerMethods.assignCase(null);
    }
}