public class R2_SurveyTriggerMethods {

    /*-----------------------------------------------------------------------------------------------------------------------------------------------------
    Author:          Julián González
    Company:        Accenture
    Description:    Relate Survey with Case searching by CallId
                                                                
    OUT:

    History:
    <Date>                      <Author>                         <Change Description>
    05/11/2018                Julián González                 Initial Version
    -----------------------------------------------------------------------------------------*/
    public static void assignCase(List<R2_Survey__c> news){
        try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}
            Id rtIdComEnt = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Comunicaciones entrantes').getRecordTypeId();
            List<R2_Survey__c> lst_surveys = new List<R2_Survey__c>();
            Set<String> set_surveysId = new Set<String>();
            Map<String,String> mapCallCase = new Map<String,String>();
            for(R2_Survey__c encuesta : news){
                System.debug('call id encuesta : ' + encuesta.R2_SRV_TXT_Call_Id__c);
                lst_surveys.add(encuesta);
                set_surveysId.add(encuesta.R2_SRV_TXT_Call_Id__c);
            }
            System.debug('Set:' + set_surveysId);
            if(!lst_surveys.isEmpty()){
                List<Task> lst_task = [SELECT Id,thinkConnect__UCID__c, WhatId FROM Task WHERE thinkConnect__UCID__c IN :set_surveysId];
                if(!lst_task.IsEmpty()){
                    for(Task tarea : lst_task){
                        System.debug('UCID tarea: ' + tarea.thinkConnect__UCID__c);
                        mapCallCase.put(tarea.thinkConnect__UCID__c, tarea.WhatId);
                    }
                    for(R2_Survey__c encuesta : lst_surveys){
                        if(mapCallCase.containsKey(encuesta.R2_SRV_TXT_Call_Id__c)){
                            System.debug('Relacionados');
                            encuesta.R2_SRV_LOO_Case__c = mapCallCase.get(encuesta.R2_SRV_TXT_Call_Id__c);
                        }
                    }
                }
            }
        }catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_SurveyTriggerMethods.assignCase()', '', exc.getmessage()+', '+exc.getLineNumber(), 'R2_Survey__c');
        }
    }
}