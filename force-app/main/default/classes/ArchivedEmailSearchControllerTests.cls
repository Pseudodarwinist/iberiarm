@IsTest
private with sharing class ArchivedEmailSearchControllerTests {
    
    @IsTest
    static void validateInstantiation() {

        Test.startTest();

        String accountIdParam = 'ABC1234';
        String contactIdParam = 'DEF4567';
        ApexPages.currentPage().getParameters().put('accountId', accountIdParam);
        ApexPages.currentPage().getParameters().put('contactId', contactIdParam);

        ArchivedEmailSearchController controller = new ArchivedEmailSearchController();
        String accountId = controller.accountId;
        String contactId = controller.contactId;

        System.assert(accountIdParam.equals(accountId), 'Account ID is not correct');
        System.assert(contactIdParam.equals(contactId), 'Contact ID is not correct');

        Test.stopTest();  
    }
}