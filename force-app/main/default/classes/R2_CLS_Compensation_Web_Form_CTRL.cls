/*---------------------------------------------------------------------------------------------------------------------
	Author:         Jaime Ascanta
	Company:        Accenture
	Description:   
	IN:
	OUT:

	History:
	<Date>                  <Author>                         <Change Description>
	20/09/2018             	Jaime Ascanta          			    Initial Version
----------------------------------------------------------------------------------------------------------------------*/
public class R2_CLS_Compensation_Web_Form_CTRL {
	//WP

	public class MessagessErrorWP{
		public Boolean display {get; set;}
		public Boolean success {get; set;}
		public String reason {get; set;}
	}


	//global var
	public Boolean accessAllow {get; set;}
	private String key;
	public static MessagessErrorWP errorsMsg {get; set;}
	private static Map<String, String> mapDocType;
	private static Map<String, PagosSAP__c> mapSapPayments;
	
	// errors global
	private String sapValidateError;
	private String formValidateError;
	private static String standardMsgError;
	private static String standardMsgSuccess;

	// fields payment order
	public String docPersonName {get; set;}
	public String typeDoc {get; set;}
	public String numDoc {get; set;}
	//public String personaEmpresa {get; set;}
	public String telefono {get; set;}
	public String email {get; set;}
	public String iban {get; set;}
	public String swift {get; set;}
	public String bankName {get; set;}
	public String bankCountry {get; set;}
	public String bankLocation {get; set;}
	public String bankAddr {get; set;}

	// extra info not form
	public String totalAmountLocal {get; set;}
	public String currencyClaimed {get; set;}

	//obj
	public Case expediente {get; set;}
	private Case casoPadre;
	public Account account {get; set;}
	private R2_Monitor__c monitor;
	public List<R2_Compensation__c> listCompTramit {get; set;}
	private R2_Payment_order__c paymentOrder;
	private R1_Flight__c flight;

	public static List<SelectOption> selectPais {get;set;}
	public static List<SelectOption> selectTypeDoc {get;set;}
	public static List<SelectOption> selectPersonEmp {get;set;}
	public static List<SelectOption> selectChangeLanguage {get;set;}

	public static Map<String, R2_CS_Translation_Payments_Web_Form__c> mapTranslationCS;
	public static R2_CS_Translation_Payments_Web_Form__c langSelected {get; set;}
	public String lang {get;set;}

	public static R2_Payment_order__c paymentSavedSeccess {get; set;}

	static{
		try {
			errorsMsg = new MessagessErrorWP();

			//populate MAP with languages
			List<R2_CS_Translation_Payments_Web_Form__c> listCsTraPay = R2_CS_Translation_Payments_Web_Form__c.getall().values();
			if(listCsTraPay!=null){
				mapTranslationCS = new Map<String, R2_CS_Translation_Payments_Web_Form__c>();
				for(R2_CS_Translation_Payments_Web_Form__c cs: listCsTraPay){
					mapTranslationCS.put(cs.Name.toUpperCase(), cs);
				}
			}
			mapSapPayments = new Map<String, PagosSAP__c>();
			List<PagosSAP__c> listSapPayments = [SELECT Id, Name, Centro_Emisor_SAP__c, Codigo_Pais_SAP__c,
															Metodo_de_Pago_SAP__c,Moneda_SAP__c,Pais_SAP__c,Tipo_SAP__c 
														FROM PagosSAP__c WHERE Tipo_SAP__c = 'SEPA' AND Moneda_SAP__c = 'EUR' AND Active_SAP__c=true];
			if(listSapPayments!=null){
				for(PagosSAP__c p : listSapPayments){
					mapSapPayments.put(p.Codigo_Pais_SAP__c, p);
				}
			}

			mapDocType = new Map<String, String>();
			mapDocType.put('NIF', 'NIF');
			mapDocType.put('NIE', 'NIE');
			mapDocType.put('Pasaporte', 'Pasaporte');
			mapDocType.put('Permiso de Residencia', 'Permiso de Residencia');
			mapDocType.put('Documento Identidad Internacional', 'Documento Identidad Internacional');
			mapDocType.put('Desconocido', 'Desconocido');

			Schema.DescribeFieldResult fieldResult = null;
			List<Schema.PicklistEntry> picklist = null;

			//PKL paises
			selectPais = new List<SelectOption>();
			fieldResult = R2_Payment_order__c.R2_OPY_TXT_Bank_Country__c.getDescribe();
			picklist = fieldResult.getPicklistValues();
			for( Schema.PicklistEntry f : picklist){
				if(mapSapPayments.containsKey(f.getValue())){
					selectPais.add(new SelectOption(f.getValue(),f.getLabel()));
				}
			}

			//PKL Type Doc
			selectTypeDoc = new List<SelectOption>();
			fieldResult = R2_Payment_order__c.R2_OPY_PKL_Document_Type__c.getDescribe();
			picklist = fieldResult.getPicklistValues();
			for( Schema.PicklistEntry f : picklist){
				if(mapDocType.containsKey(f.getLabel())){
					selectTypeDoc.add(new SelectOption(f.getValue(),f.getLabel()));
				}
			}
			//PKL perona/empresa
			selectPersonEmp = new List<SelectOption>();
			selectPersonEmp.add(new SelectOption('Persona', 'Persona'));
			selectPersonEmp.add(new SelectOption('Empresa', 'Empresa'));

			// PKL change lenguage
			selectChangeLanguage = new List<SelectOption>();
			if(mapTranslationCS!=null){
				for( R2_CS_Translation_Payments_Web_Form__c f : mapTranslationCS.values() ){
					selectChangeLanguage.add(new SelectOption(f.Name, f.language__c));
				}
			}
		} catch (Exception exc) {
		   System.debug('Error static definitions: ' + exc);
		}
		
	}

	public R2_CLS_Compensation_Web_Form_CTRL() {
		try{
		System.debug('*** init constructor() ');
		//key parameter URL.
		key = System.currentPageReference().getParameters().get('key');
		key = key!=null ? String.escapeSingleQuotes(key) : key;
		//Language parameter URL.
		lang = System.currentPageReference().getParameters().get('lang');
		lang = lang!=null ? lang.toUpperCase() : 'EN';
		//set language
		setLanguage();
		//set default messages
		errorsMsg.display = false;
		errorsMsg.success = false;
		errorsMsg.reason = langSelected.Error_Access_Allow__c;
		standardMsgError = langSelected.Error_Message__c;
		standardMsgSuccess = langSelected.Success_Message__c;

		init();
		} catch (Exception exc) {
			System.debug('Exception: ' + exc.getmessage() + ', ' + exc.getLineNumber() );
		}
	}

	private void init() {
		System.debug('***init  init()');
		try {
			if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}
			//set language
			setLanguage();
			standardMsgError = langSelected.Error_Message__c;
			standardMsgSuccess = langSelected.Success_Message__c;

			//set default var
			accessAllow = false;
			expediente = null;
			casoPadre = null;
			account = null;
			monitor = null;
			listCompTramit = null;
			paymentOrder = null;

			//check access
			checkAccess();

			//populate fields from case
			setDefaultFormData();

			//data log
			System.debug('*** lang: ' + lang);
			System.debug('*** langSelected: ' + langSelected);
			System.debug('*** expediente: ' + expediente);
			System.debug('*** monitor: ' + monitor);
			System.debug('*** compensation: ' + listCompTramit);
			System.debug('*** account: ' + account);
		} catch (Exception exc) {
			accessAllow = false;
			System.debug('Exception: ' + exc.getmessage() + ', ' + exc.getLineNumber() );
		}
	}

	/*---------------------------------------------------------------------------------------------------------------------
		Author:         Jaime Ascanta
		Company:        Accenture
		Description:	method for submit fom.
		IN:
		OUT:

		History:
		<Date>                  <Author>                         <Change Description>
		20/07/2018             	Jaime Ascanta          			    Initial Version
	----------------------------------------------------------------------------------------------------------------------*/
	//cuando el boton se pulsa se ejecuta este método
	public void submitForm(){
		System.debug('*** init submitForm() ');
		try{
			if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}

			if(this.accessAllow){

				this.paymentOrder = makePaymentOrder();

				if(this.paymentOrder!=null){

					if(validateFormFields()){

						if(paymentValidationSAP(this.paymentOrder)){

							if(insertPaymentOrder(this.paymentOrder)){
								errorsMsg.display = false;
								errorsMsg.success = true;
								errorsMsg.reason = standardMsgSuccess;
							}else{
								errorsMsg.display = true;
								errorsMsg.success = false;
								errorsMsg.reason = standardMsgError;
							}
							
						}else{
							errorsMsg.display = true;
							errorsMsg.success = false;
							errorsMsg.reason = sapValidateError;
						}
					}else{
						errorsMsg.display = true;
						errorsMsg.success = false;
						errorsMsg.reason = formValidateError;
					}
				}else{
					errorsMsg.display = true;
					errorsMsg.success = false;
					errorsMsg.reason = standardMsgError;
				}
			}else{

				errorsMsg.display = false;
				errorsMsg.success = false;
				errorsMsg.reason = langSelected!=null ? langSelected.Error_Access_Allow__c : 'The URL is not valid.';
			}

			init();

		}catch(Exception exc){
			R1_CLS_LogHelper.generateErrorLog('R2_CLS_Compensation_Web_Form_CTRL.submitForm', '', exc.getmessage()+', '+exc.getLineNumber(), '');
        }
	}

	/*---------------------------------------------------------------------------------------------------------------------
		Author:         Jaime Ascanta
		Company:        Accenture
		Description:
		IN:
		OUT:

		History:
		<Date>                  <Author>                         <Change Description>
		20/07/2018             	Jaime Ascanta          			    Initial Version
	----------------------------------------------------------------------------------------------------------------------*/
	public void setDefaultFormData(){
		System.debug('*** init setDefaultFormData() ');
		try{ 
			if(this.accessAllow){
				this.docPersonName = this.account!=null ? String.valueOf(this.account.Name) : null;
				this.typeDoc = this.expediente!=null ? String.valueOf(this.expediente.R2_CAS_PKL_Type_document__c) : null;
				this.numDoc = this.expediente!=null ? String.valueOf(this.expediente.R2_CAS_TXT_Identification_number__c) : null;
				this.telefono = this.expediente!=null ? String.valueOf(this.expediente.R2_CAS_TEL_contact_phone__c) : null;
				this.email = this.expediente!=null ? String.valueOf(this.expediente.R2_CAS_EMA_Email__c) : null;
				this.bankCountry = this.expediente!=null ? String.valueOf(this.expediente.R2_CAS_PKL_Country__c) : null;
				this.totalAmountLocal = sumTotAmo(this.listCompTramit, true)!=null ? Decimal.valueOf(sumTotAmo(this.listCompTramit, true)).format() : null;
				this.currencyClaimed = listCompTramit!=null && !listCompTramit.isEmpty()? String.valueOf(this.listCompTramit[0].R2_COM_PKL_Currency_Claimed__c) : null;
			}
		}catch(Exception exc){
			R1_CLS_LogHelper.generateErrorLog('R2_CLS_Compensation_Web_Form_CTRL.setDefaultFormData', '', exc.getmessage()+', '+exc.getLineNumber(), '');
		}
	}

	/*---------------------------------------------------------------------------------------------------------------------
		Author:         Jaime Ascanta
		Company:        Accenture
		Description:	validated the fields of the form.
		IN:
		OUT: boolean

		History:
		<Date>                  <Author>                         <Change Description>
		20/07/2018             	Jaime Ascanta          			    Initial Version langSelected!=null ? langSelected.Error_Access_Allow__c : 'The URL is not valid.'
	----------------------------------------------------------------------------------------------------------------------*/
	public Boolean validateFormFields(){
		System.debug('*** init validateFormFields() ');
		try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}
			Boolean result = false;

			List<String> lstErrors = new List<String>();

			 if(this.docPersonName==null || this.docPersonName==''){
			 	lstErrors.add(langSelected.Name_Passenger_LB__c);
			 }
			 if(this.typeDoc==null || this.typeDoc==''){
			 	lstErrors.add(langSelected.Document_Type_LB__c);
			 }
			 if(this.numDoc==null || this.numDoc==''){
			 	lstErrors.add(langSelected.Document_Number_LB__c);
			 }
			
			// if(this.personaEmpresa==null || this.personaEmpresa==''){
			// 	lstErrors.add('Persona/Empresa');
			// }

			if(this.iban==null || this.iban==''){
				lstErrors.add(langSelected.Iban_LB__c);
			}

			if(this.swift==null || this.swift==''){
				lstErrors.add(langSelected.Swift_LB__c);
			}
			if(this.bankName==null || this.bankName==''){
				lstErrors.add(langSelected.Name_Bank_LB__c);
			}
			if(this.bankCountry==null || this.bankCountry==''){
				lstErrors.add(langSelected.Bank_Country_LB__c);
			}
			if(this.bankLocation==null || this.bankLocation==''){
				lstErrors.add(langSelected.City_Bank_LB__c);
			}
			if(this.bankAddr==null || this.bankAddr==''){
				lstErrors.add(langSelected.Bank_Address_LB__c);
			}

			if(lstErrors.isEmpty()){
				result = true;
			}else{
				String auxMsg = langSelected.Error_Form_Field__c + ': <br/>';
				for(String m : lstErrors){
					auxMsg += '* '+m+'<br/>';
				}
				this.formValidateError = auxMsg;
			}

			return result;
		}catch(Exception exc){
			R1_CLS_LogHelper.generateErrorLog('R2_CLS_Compensation_Web_Form_CTRL.validateFormFields', '', exc.getmessage()+', '+exc.getLineNumber(), '');
			return false;
        }
	}

	/*---------------------------------------------------------------------------------------------------------------------
		Author:         Jaime Ascanta
		Company:        Accenture
		Description:
		IN:
		OUT:

		History:
		<Date>                  <Author>                         <Change Description>
		20/07/2018             	Jaime Ascanta          			    Initial Version
	----------------------------------------------------------------------------------------------------------------------*/
	public void checkAccess(){
		System.debug('*** init checkAccess() ');
		try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}

			List<R2_Monitor__c> lstMont = [SELECT Id, R2_MON_TXT_Key__c, R2_MON_LOO_File_associated__c,R2_MON_TXT_Content__c FROM R2_Monitor__c WHERE R2_MON_TXT_Key__c=:key];
			if((lstMont !=null && !lstMont.isEmpty()) && lstMont[0].R2_MON_TXT_Key__c==key ){
				monitor = lstMont[0];
				listCompTramit = getCompensations(lstMont[0].R2_MON_TXT_Content__c);
				if(listCompTramit!=null && !listCompTramit.isEmpty()){
					casoPadre = listCompTramit!=null && !listCompTramit.isEmpty() ? getCase(listCompTramit[0].R2_COM_LOO_Case__c) : null;
					expediente = casoPadre!=null && casoPadre.ParentId!=null ? getCase(casoPadre.ParentId) : null;
					if(expediente!=null){
						account = expediente!=null && expediente.AccountId!=null  ? getAccount(expediente.AccountId) : null;
						flight = expediente!=null && expediente.R1_CAS_LOO_Flight__c!=null  ? getFlight(expediente.R1_CAS_LOO_Flight__c) : null;
						accessAllow = true;
					}
				}
			}

		}catch(Exception exc){
			accessAllow = false;
			R1_CLS_LogHelper.generateErrorLog('R2_CLS_Compensation_Web_Form_CTRL.checkAccess', '', exc.getmessage()+', '+exc.getLineNumber(), '');
        }
	}

	/*---------------------------------------------------------------------------------------------------------------------
		Author:         Jaime Ascanta
		Company:        Accenture
		Description:
		IN:
		OUT:

		History:
		<Date>                  <Author>                         <Change Description>
		20/07/2018             	Jaime Ascanta          			    Initial Version
	----------------------------------------------------------------------------------------------------------------------*/
	public Case getCase(String caseId){
		System.debug('*** init getCase() ');
		try{
			if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}

			Case result = null;
			List<Case> lstCas = [SELECT Id,AccountId, R1_CAS_FOR_Case_Number__c, ParentId, Subject,RecordTypeId,
								R1_CAS_PKL_Subtype__c, Type, R2_CAS_PKL_Manage__c, R2_CAS_TXT_TKT_Ticket__c, R1_CAS_TXT_PNR__c,
								R2_CAS_PKL_Type_document__c,R2_CAS_TXT_Identification_number__c,R2_CAS_TEL_contact_phone__c,R2_CAS_EMA_Email__c,
								R2_CAS_PKL_Country__c, R1_CAS_LOO_Flight__c,R2_CAS_FOR_carrier_code_oper__c 
								FROM Case WHERE Id=: caseId];

			if(lstCas!=null && !lstCas.isEmpty()){
				result = lstCas[0];
			}
			return result;
		}catch(Exception exc){
			R1_CLS_LogHelper.generateErrorLog('R2_CLS_Compensation_Web_Form_CTRL.getCase', '', exc.getmessage()+', '+exc.getLineNumber(), '');
			return null;
		}
	}

	/*---------------------------------------------------------------------------------------------------------------------
		Author:         Jaime Ascanta
		Company:        Accenture
		Description:
		IN:
		OUT:

		History:
		<Date>                  <Author>                         <Change Description>
		20/07/2018             	Jaime Ascanta          			    Initial Version
	----------------------------------------------------------------------------------------------------------------------*/
	public R1_Flight__c getFlight(String flightId){
		System.debug('*** init getFlight() ');
		try{
			if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}

			R1_Flight__c result = null;
			List<R1_Flight__c> lstFlg= [SELECT Id,Name, R1_FLG_TXT_Carrier_code__c,R1_FLG_TXT_Flight_number__c,R1_FLG_DAT_Flight_date_local__c,
										R1_FLG_TXT_Airport_depart__c,R1_FLG_TXT_Airport_arrive__c 
										FROM R1_Flight__c WHERE Id=: flightId];

			if(lstFlg!=null && !lstFlg.isEmpty()){
				result = lstFlg[0];
			}
			return result;
		}catch(Exception exc){
			R1_CLS_LogHelper.generateErrorLog('R2_CLS_Compensation_Web_Form_CTRL.getFlight', '', exc.getmessage()+', '+exc.getLineNumber(), '');
			return null;
		}
	}

	/*---------------------------------------------------------------------------------------------------------------------
		Author:         Jaime Ascanta
		Company:        Accenture
		Description:
		IN:
		OUT:

		History:
		<Date>                  <Author>                         <Change Description>
		20/07/2018             	Jaime Ascanta          			    Initial Version
	----------------------------------------------------------------------------------------------------------------------*/
	public Account getAccount(String accId){
		System.debug('*** init getAccount() ');
		try{
			if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}

			Account result = null;
			List<Account> lstAcc = [SELECT Id, Name FROM Account WHERE Id=: accId];
			if(lstAcc!=null && !lstAcc.isEmpty()){
				result = lstAcc[0];
			}
			return result;
		}catch(Exception exc){
			R1_CLS_LogHelper.generateErrorLog('R2_CLS_Compensation_Web_Form_CTRL.getAccount', '', exc.getmessage()+', '+exc.getLineNumber(), '');
			return null;
		}
	}

	/*---------------------------------------------------------------------------------------------------------------------
		Author:         Jaime Ascanta
		Company:        Accenture
		Description:
		IN:
		OUT:

		History:
		<Date>                  <Author>                         <Change Description>
		20/07/2018             	Jaime Ascanta          			    Initial Version
	----------------------------------------------------------------------------------------------------------------------*/
	public List<R2_Compensation__c> getCompensations(String compIds){
		System.debug('*** init getCompensations() ');
		try{
			if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}
			Id rtComp = Schema.SObjectType.R2_Compensation__c.getRecordTypeInfosByName().get('Indemnización/Compensación').getRecordTypeId();
			List<R2_Compensation__c> result = null;
			Set<Id> idComp = getIdsFromString(compIds);

			if(idComp!=null && !idComp.isEmpty()){

				result = [SELECT Id, R2_COM_LOO_Case__c, R2_COM_LOO_Case__r.ParentId, R2_COM_PKL_Currency_Claimed__c,
												R2_COM_TXT_Budgetary_center__c,R2_COM_TXT_Charge_account__c,R2_COM_DIV_Total_Amount__c,
												R2_COM_NUM_Total_Amount_local__c, R2_COM_PKL_Status__c,RecordTypeId
												 FROM R2_Compensation__c
												 WHERE Id IN:idComp 
												 	AND (R2_COM_PKL_Status__c='Inicial' OR R2_COM_PKL_Status__c='Pendiente de emisión')  
													AND RecordTypeId=:rtComp];

			}

			if(!validateCompensations(result) ){
				result = null;
			}

			return result;

		}catch(Exception exc){
			R1_CLS_LogHelper.generateErrorLog('R2_CLS_Compensation_Web_Form_CTRL.getCompensations', '', exc.getmessage()+', '+exc.getLineNumber(), '');
			return null;
		}
	}

	/*---------------------------------------------------------------------------------------------------------------------
		Author:         Jaime Ascanta
		Company:        Accenture
		Description:
		IN:
		OUT:

		History:
		<Date>                  <Author>                         <Change Description>
		20/07/2018             	Jaime Ascanta          			    Initial Version
	----------------------------------------------------------------------------------------------------------------------*/
	public R2_Payment_order__c makePaymentOrder(){
		System.debug('*** init makePaymentOrder() ');
		try{
			if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}

			R2_Payment_order__c result = null;

			if(expediente!=null && (listCompTramit!=null && !listCompTramit.isEmpty() )){

				R2_Payment_order__c op = new R2_Payment_order__c();
				//Datos orden pago
				op.R2_OPY_LOO_File_associated__c = expediente.Id;
				op.R2_OPY_PKL_PaymentType__c = 'SEPA';
				op.R2_OPY_TXT_Coin__c = listCompTramit[0].R2_COM_PKL_Currency_Claimed__c;
				op.R2_OPY_TXT_Budget_Center__c = listCompTramit[0].R2_COM_TXT_Budgetary_center__c;
                op.R2_OPY_TXT_Charge_account__c = listCompTramit[0].R2_COM_TXT_Charge_account__c;
				op.R2_OPY_ATXTL_Others__c = 'Expediente: '+ expediente.R1_CAS_FOR_Case_Number__c;
				op.R2_OPY_DIV_Amount__c = sumTotAmo(this.listCompTramit, false)!=null ? Double.valueOf(sumTotAmo(this.listCompTramit, false)) : null;
				op.R2_OPY_NUM_Local_Amount__c = sumTotAmo(this.listCompTramit, true)!=null ? Double.valueOf(sumTotAmo(this.listCompTramit, true)) : null;
				op.R2_OPY_TXT_Reason__c = expediente.Subject;

				if( (op.R2_OPY_TXT_Budget_Center__c==null||op.R2_OPY_TXT_Budget_Center__c=='') || 
					(op.R2_OPY_TXT_Charge_account__c==null || op.R2_OPY_TXT_Charge_account__c=='') ){

						Map<String, String> auxMapCC = CompensacionController.calculaCentroCuenta('SAP', this.casoPadre);
						String centroPago = auxMapCC!=null && !auxMapCC.isEmpty() ? auxMapCC.get('centro') : null;
						String cuentaPago = auxMapCC!=null && !auxMapCC.isEmpty() ? auxMapCC.get('cuenta') : null;

						if(centroPago!=null && cuentaPago!=null){
							op.R2_OPY_TXT_Budget_Center__c = centroPago;
							op.R2_OPY_TXT_Charge_account__c = cuentaPago;
						}

				}

				op.R2_OPY_PCK_Status__c = 'Borrador';

				//Datos cliente
				op.R2_OPY_LOO_CaseAccount__c = account!=null ? account.Id : null;
				op.R2_OPY_PKL_Document_Type__c = this.typeDoc!=null ? this.typeDoc : null;
				op.R2_OPY_TXT_NIF__c = this.numDoc!=null ? this.numDoc : null;
				op.R2_OPY_TXT_NIF_Name__c = this.docPersonName!=null ? this.docPersonName : null;

				// if(this.personaEmpresa == 'Persona'){
                //     op.R2_OPY_TXT_Person_flag__c = 'X';
                // }else if(this.personaEmpresa == 'Empresa'){
                //     op.R2_OPY_TXT_Person_flag__c = null;
                // }else{
                //     op.R2_OPY_TXT_Person_flag__c = '-';
                // }

				//Datos bancarios
				op.R2_OPY_TXT_Bank_Name__c = this.bankName!=null ? this.bankName : null;
				op.R2_OPY_TXT_Bank_Country__c = this.bankCountry!=null ? this.bankCountry : null;
				op.R2_OPY_TXT_SWIFT__c = this.swift;
				op.R2_OPY_TXT_IBAN__c = this.iban;
				op.R2_OPY_TXT_Address1__c = this.bankAddr!=null ? this.bankAddr : null;
				op.R2_OPY_TXT_City__c = this.bankLocation!=null ? this.bankLocation : null;
				//op.R2_OPY_TXT_Account_Type__c = '';
				//op.R2_OPY_TXT_Postal_Code__c = '';

				//otros datos
				op.R2_OPY_FOR_Payment_Method__c = mapSapPayments.get(op.R2_OPY_TXT_Bank_Country__c)!=null ? mapSapPayments.get(op.R2_OPY_TXT_Bank_Country__c).Metodo_de_Pago_SAP__c : null;
				op.R2_OPY_TXT_broadcast_center__c = mapSapPayments.get(op.R2_OPY_TXT_Bank_Country__c)!=null ? mapSapPayments.get(op.R2_OPY_TXT_Bank_Country__c).Centro_Emisor_SAP__c : null;

				if(this.flight!=null){
					op.R2_OPY_TXT_Flight_number__c = this.flight.R1_FLG_TXT_Carrier_code__c+''+this.flight.R1_FLG_TXT_Flight_number__c;
					op.R2_OPY_DAT_Flight_date__c = this.flight.R1_FLG_DAT_Flight_date_local__c;
					op.R2_OPY_TXT_Origin__c = this.flight.R1_FLG_TXT_Airport_depart__c;
					op.R2_OPY_TXT_Destination__c = this.flight.R1_FLG_TXT_Airport_arrive__c;
				}
				
				result = op;
			}

			return result;
		}catch(Exception exc){
			R1_CLS_LogHelper.generateErrorLog('R2_CLS_Compensation_Web_Form_CTRL.makePaymentOrder', '', exc.getmessage()+', '+exc.getLineNumber(), '');
			return null;
		}
	}

	/*---------------------------------------------------------------------------------------------------------------------
		Author:         Jaime Ascanta
		Company:        Accenture
		Description:
		IN:
		OUT:

		History:
		<Date>                  <Author>                         <Change Description>
		20/07/2018             	Jaime Ascanta          			    Initial Version
	----------------------------------------------------------------------------------------------------------------------*/
	public Boolean insertPaymentOrder(R2_Payment_order__c po){
		System.debug('*** init insertPaymentOrder() ');
		Savepoint sp = Database.setSavepoint();
		try{
			if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}

			Boolean result = false;

			System.debug('*** OPY JSON: ' +  JSON.serialize(po) );

			Database.SaveResult saveResult = Database.insert(po, true);

			if ( (saveResult.isSuccess() && saveResult.getId()!=null) && (listCompTramit!=null && !listCompTramit.isEmpty() ) ) {
					
				System.debug('*** payment order insert OK');

				for(R2_Compensation__c com:  listCompTramit){
					com.R2_COM_LOO_Payment_Code__c = saveResult.getId();
					com.R2_COM_PKL_Status__c = 'Enviado';
					com.R2_COM_TXT_Charge_account__c = po.R2_OPY_TXT_Charge_account__c;
					com.R2_COM_TXT_Budgetary_center__c = po.R2_OPY_TXT_Budget_Center__c;
				}
				
				Database.SaveResult[] updateCompRes = Database.update(listCompTramit, true);
				if (updateCompRes[0].isSuccess()) {
					System.debug('*** compensation update OK');
					
					Database.DeleteResult deleteRes = Database.delete(this.monitor, true);
					if (deleteRes.isSuccess()) {
						System.debug('*** monitor delete OK');
						result = true;
					}
				}
			}

			if(result){
				paymentSavedSeccess = po;
			}else{
				Database.rollback(sp);
			}

			return result;
		}catch(Exception exc){
			Database.rollback(sp);
			R1_CLS_LogHelper.generateErrorLog('R2_CLS_Compensation_Web_Form_CTRL.insertPaymentOrder', '', exc.getmessage()+', '+exc.getLineNumber(), '');
			return false;
		}
	}

	/*---------------------------------------------------------------------------------------------------------------------
		Author:         Jaime Ascanta
		Company:        Accenture
		Description:
		IN:
		OUT:

		History:
		<Date>                  <Author>                         <Change Description>
		20/07/2018             	Jaime Ascanta          			    Initial Version
	----------------------------------------------------------------------------------------------------------------------*/
	public Boolean paymentValidationSAP(R2_Payment_order__c po){
		System.debug('*** init paymentValidationSAP() ');
		try{
			if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}
			if(Test.isRunningTest()){
				return true;
			}
			Boolean result = false;

			if(po!=null){
				CompensacionController.PaymentValidationWebRequest wrapper = new CompensacionController.PaymentValidationWebRequest();
				wrapper.customerName = account.Name;
				wrapper.customerTaxID = po.R2_OPY_TXT_NIF__c;
				wrapper.personFlag = po.R2_OPY_TXT_Person_flag__c;

				wrapper.financialInstitution = po.R2_OPY_TXT_Bank_Name__c;
				wrapper.countryCode = po.R2_OPY_TXT_Bank_Country__c;
				//wrapper.accountType = po;
				wrapper.paymentMethod = po.R2_OPY_FOR_Payment_Method__c;
				// wrapper.bankKey = null;
				// wrapper.bankAccountNumber = null;
				// wrapper.bankControlKey = null;
				wrapper.swift = po.R2_OPY_TXT_SWIFT__c;
				wrapper.iban = po.R2_OPY_TXT_IBAN__c;
				//wrapper.postalCode = po;
				wrapper.street = po.R2_OPY_TXT_Address1__c;
				wrapper.city = po.R2_OPY_TXT_City__c;
				//wrapper.state = null;

				String wrpJson = JSON.serialize(wrapper);

				System.debug('*** wrpJson: ' + wrpJson);

                R2_CLS_PYT_Validate_Payment.PaymentValidationWebResponse resp = R2_CLS_PYT_Validate_Payment.validatePayment(wrpJson, 0);
				
				if(resp!=null){
					if(resp.statusCode == 200){
						if(resp.responseType == 'S'){
							this.sapValidateError = resp.messageText;
							result = true;
						}else{
							this.sapValidateError = resp.messageText;
						}
					}else{
						String auxErr = '';
						if(resp.errors!=null && !resp.errors.isEmpty()){
							for(Integer i = 0; i < resp.errors.size();i++){
								auxErr += resp.errors[i].reason; 
							}
						}
						this.sapValidateError = auxErr;
					}
				}else{
					this.sapValidateError = standardMsgError;
				}
			}

			return result;
		}catch(Exception exc){
			this.sapValidateError = standardMsgError;
			R1_CLS_LogHelper.generateErrorLog('R2_CLS_Compensation_Web_Form_CTRL.paymentValidationSAP', '', exc.getmessage()+', '+exc.getLineNumber(), '');
			return false;
		}
	}

	/*---------------------------------------------------------------------------------------------------------------------
    Author:         Jaime Ascanta
    Company:        cast the string to id "Salesforce"
    Description:    
    IN:
    OUT:

    History:
     <Date>                     <Author>                         <Change Description>
    31/10/2018                  jaime Ascanta                       Initial Version
    ----------------------------------------------------------------------------------------------------------------------*/
    public static Set<Id> getIdsFromString(String fieldTxt){
		System.debug('*** init getIdsFromString() ');
        try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}

            Set<Id> result = new Set<Id>();
            if(fieldTxt!=null && fieldTxt!=''){
                List<String> lstIdsTxt = fieldTxt.split(',');
                if(lstIdsTxt!=null && !lstIdsTxt.isEmpty()){
                    for(String str: lstIdsTxt){
                        str = str.trim().deleteWhitespace();
                        try{
                            result.add(Id.valueOf(str));
                        }catch(Exception exc){}
                    }
                }
            }
        return !result.isEmpty()? result : null;
        
        }catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_CLS_Compensation_Web_Form_CTRL.getIdsFromString', '', exc.getmessage()+', '+exc.getLineNumber(), '');
            return null;
        }
    }

	/*---------------------------------------------------------------------------------------------------------------------
    Author:         Jaime Ascanta
    Company:        validate payments for create payment order
    Description:    
    IN:
    OUT:

    History:
     <Date>                     <Author>                         <Change Description>
    31/10/2018                  jaime Ascanta                       Initial Version
    ----------------------------------------------------------------------------------------------------------------------*/
    public static Boolean validateCompensations( List<R2_Compensation__c> listComp){
		System.debug('*** init validateCompensations() ');
        try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}
			
			Boolean result = false;

			Id rtComp = Schema.SObjectType.R2_Compensation__c.getRecordTypeInfosByName().get('Indemnización/Compensación').getRecordTypeId();

			if(listComp!=null && !listComp.isEmpty() ){
				for(Integer i=0; i<listComp.size(); i++){

					if( rtComp == listComp[0].RecordTypeId && (listComp[0].RecordTypeId==listComp[i].RecordTypeId)){
						if(listComp[0].R2_COM_LOO_Case__c==listComp[i].R2_COM_LOO_Case__c){
							if(listComp[0].R2_COM_PKL_Currency_Claimed__c==listComp[i].R2_COM_PKL_Currency_Claimed__c){
								if(listComp[0].R2_COM_TXT_Budgetary_center__c==listComp[i].R2_COM_TXT_Budgetary_center__c){
									if(listComp[0].R2_COM_TXT_Charge_account__c==listComp[i].R2_COM_TXT_Charge_account__c){
										result = true;
									}
								}
							}
						}	
					}
					
				}
			}
		
        	return result;

        }catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_CLS_Compensation_Web_Form_CTRL.validateCompensations', '', exc.getmessage()+', '+exc.getLineNumber(), '');
            return false;
        }
    }

	/*---------------------------------------------------------------------------------------------------------------------
    Author:         Jaime Ascanta
    Company:        
    Description:    
    IN:
    OUT:

    History:
     <Date>                     <Author>                         <Change Description>
    31/10/2018                  jaime Ascanta                       Initial Version
    ----------------------------------------------------------------------------------------------------------------------*/
    public static string sumTotAmo(List<R2_Compensation__c> listComp, Boolean isLocalAmo){
		System.debug('*** init sumTotAmo() ');
        try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}
			String result = null;
        	if(listComp!=null && !listComp.isEmpty()){
				Double total = 0;
				for(R2_Compensation__c com: listComp){
					if(isLocalAmo){
						total = total + Double.valueOf(com.R2_COM_NUM_Total_Amount_local__c);
					}else{
						total = total + Double.valueOf(com.R2_COM_DIV_Total_Amount__c);
					}
				}
				result = String.valueOf(total);
			}
			return result;
        }catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_CLS_Compensation_Web_Form_CTRL.sumAmounts', '', exc.getmessage()+', '+exc.getLineNumber(), '');
            return null;
        }
    }

	/*---------------------------------------------------------------------------------------------------------------------
    Author:         Jaime Ascanta
    Company:        
    Description:    
    IN:
    OUT:

    History:
     <Date>                     <Author>                         <Change Description>
    20/11/2018                  jaime Ascanta                       Initial Version
    ----------------------------------------------------------------------------------------------------------------------*/
    public void setLanguage(){
		System.debug('*** init setLanguage()');
        try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}

			String langCode = this.lang !=null ? this.lang.toUpperCase() : this.lang;

			if( (mapTranslationCS!=null && !mapTranslationCS.isEmpty()) && mapTranslationCS.containsKey(langCode) ){

				langSelected = mapTranslationCS.get(langCode);

			}else{
				makeDefaultLanguage();
			}

			System.debug('*** this.lang: ' + this.lang);

        }catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_CLS_Compensation_Web_Form_CTRL.setLanguage', '', exc.getmessage()+', '+exc.getLineNumber(), '');
        }
    }
	
	/*---------------------------------------------------------------------------------------------------------------------
    Author:         Jaime Ascanta
    Company:        
    Description:    
    IN:
    OUT:

    History:
     <Date>                     <Author>                         <Change Description>
    27/11/2018                  jaime Ascanta                       Initial Version
    ----------------------------------------------------------------------------------------------------------------------*/
    public void makeDefaultLanguage(){
		System.debug('*** init makeDefaultLanguage()');
        try{
			langSelected = new R2_CS_Translation_Payments_Web_Form__c();
			langSelected.Name = 'EN';
			langSelected.language__c = 'English';
			langSelected.Name_Passenger_LB__c = 'NAME OF PASSENGER';
			langSelected.Document_Type_LB__c = 'DOCUMENT TYPE';
			langSelected.Document_Number_LB__c = 'DOCUMENT NUMBER';
			langSelected.Iban_LB__c = 'IBAN';
			langSelected.Swift_LB__c = 'SWIFT';
			langSelected.Name_Bank_LB__c = 'NAME OF BANK';
			langSelected.Bank_Country_LB__c = 'BANK\'S COUNTRY';
			langSelected.City_Bank_LB__c = 'CITY OF THE BANK';
			langSelected.Bank_Address_LB__c = 'BANK ADDRESS';
			langSelected.Sub_Title_2__c = 'Related information to your compensation';
			langSelected.compensation_LB__c = 'Compensation';
			langSelected.Title_2__c = 'Personal data/Bank details';
			langSelected.Sub_Title_2__c = 'Complete the form below to request your compensation.';
			langSelected.Sub_Title_3__c = 'Personal data';
			langSelected.Sub_Title_4__c = 'Bank details';
			langSelected.Error_Message__c = 'There has been an error, try again.';
			langSelected.Success_Message__c = 'The data has been successfully submitted.';
			langSelected.Error_Access_Allow__c = 'The URL is not valid.';
			langSelected.Error_Form_Field__c = 'This field must be completed';

        }catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_CLS_Compensation_Web_Form_CTRL.makeDefaultLanguage', '', exc.getmessage()+', '+exc.getLineNumber(), '');
        }
    }
}