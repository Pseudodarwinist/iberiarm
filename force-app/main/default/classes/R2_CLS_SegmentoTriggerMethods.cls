/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Jaime Ascanta
    Company:        Accenture
    Description:    
    IN:         

    OUT:           
    
    History:
    
    <Date>              <Author>                 <Description>
    25/02/2018          Jaime Ascanta     		Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/   
global class R2_CLS_SegmentoTriggerMethods {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Jaime Ascanta
    Company:        Accenture
    Description:    
    IN:         

    OUT:           
    
    History:
    
    <Date>              <Author>                 <Description>
    25/02/2018          Jaime Ascanta    		 Initial version
    05/03/2018			Alvaro Garcia Tapia		 Add try catch
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/   
	public static void checkProrateStatus(List<R2_Segmento__c> listSeg){
		 try{

            System.debug('*** init checkProrateStatus() ');

            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');} //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;         

			Set<Id> setProid = new Set<Id>();
			for(R2_Segmento__c seg: listSeg){
				if(seg.R2_SEG_MSDT_Prorrate__c!=null)  setProid.add(seg.R2_SEG_MSDT_Prorrate__c);
			}

			// extraigo prorrateos si estan ok
			List<R2_Prorrateo__c> listPro = [SELECT Id, R2_PRO_PKL_Status__c FROM R2_Prorrateo__c 
											WHERE Id IN:setProid AND R2_PRO_PKL_Status__c='Prorrateo OK'];
			
            System.debug('*** listPro: '  + listPro);

			if(!listPro.isEmpty()){

                System.debug('*** send to prorrateoIsOk() ');
                
				R2_CLS_ProrrateoTriggerMethods.prorrateoIsOk(listPro);
			}
		}catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_CLS_SegmentoTriggerMethods.checkProrateStatus()', '', exc.getmessage()+', '+exc.getLineNumber(), 'R2_Segmento__c');
        }
	}

	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Jaime Ascanta
    Company:        Accenture
    Description:    
    IN:         

    OUT:           
    
    History:
    
    <Date>              <Author>                 <Description>
    05/03/2018          Alvaro Garcia Tapia	   	 Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/   
	public static void fillSDRToEUR(List<R2_Segmento__c> listSeg){
		 try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');} //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;         

            //Decimal aux = R2_ExchangeRate__c.getInstance('CurrentExchangeRate').R2_SDRtoEUR__c; //comentado, the old version (previous dimonca)
            Decimal aux = R2_ExchangeRate__c.getInstance('XDR_960').R2_NUM_Rate_to_EUR__c;
			for (R2_Segmento__c seg : listSeg) {
				seg.R2_SEG_NUM_SDRtoEUR__c = aux;
			}

		}catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_CLS_SegmentoTriggerMethods.fillSDRToEUR()', '', exc.getmessage()+', '+exc.getLineNumber(), 'R2_Segmento__c');
        }
	}

    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Jaime Ascanta
    Company:        Accenture
    Description:    establece si la compania asociada al segmento es nacional o internacional.
    IN:         

    OUT:           
    
    History:
    
    <Date>              <Author>                 <Description>
    19/03/2018          Jaime Ascanta   	   	 Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/   
	public static void setTaxType(List<R2_Segmento__c> listSeg){
		 try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}
            Map<String, R2_Diccionario_Cias__c> mapCias = new Map<String, R2_Diccionario_Cias__c>();

            List<R2_Diccionario_Cias__c> listCias = [SELECT Id,R2_DIC_TXT_Codigo_Compania__c,R2_DIC_CHK_Nacional__c FROM R2_Diccionario_Cias__c];
            if(!listCias.isEmpty()){
                for(R2_Diccionario_Cias__c cia : listCias){
                    mapCias.put(cia.R2_DIC_TXT_Codigo_Compania__c, cia);
                }
            }

            for(R2_Segmento__c seg: listSeg){
                if(mapCias.containsKey(seg.R2_SEG_TXT_company_code__c)){
                    Boolean isNational = mapCias.get(seg.R2_SEG_TXT_company_code__c).R2_DIC_CHK_Nacional__c;
                    seg.R2_SEG_PKL_Tax_Type__c = isNational ? 'R3' : 'M1';
                }
            }

		}catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_CLS_SegmentoTriggerMethods.setTaxType()', '', exc.getmessage()+', '+exc.getLineNumber(), 'R2_Segmento__c');
        }
	}


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Jaime Ascanta
    Company:        Accenture
    Description:    get att of the email messages
    IN:         

    OUT:           
    
    History:
    
    <Date>              <Author>                 <Description>
    18/01/2019          Jaime Ascanta   	   	 Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/   
	webservice static Boolean relatedAttOfEmaToSeg(String segId){
        Savepoint sp = Database.setSavepoint();
		 try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}
            Boolean result = false;

            Map<ID, EmailMessage> mapEmailMessage = new Map<ID, EmailMessage>
            ([SELECT Id, R2_EMA_LOO_Segmento__c FROM EmailMessage WHERE R2_EMA_LOO_Segmento__c=:segId]);

            Map<ID, Attachment> mapAtt = new Map<ID, Attachment>
            ([SELECT id, Name, ParentId, IsPrivate, Body, Description FROM Attachment WHERE ParentId=:mapEmailMessage.keySet()]);

            List<Attachment> lisNewsAtt = new List<Attachment>();
            for (EmailMessage ema : mapEmailMessage.values()) {
                for (Attachment att : mapAtt.values()) {
                    if (ema.Id == att.ParentId) {
                        Attachment newAtt = att.clone();
                        newAtt.ParentId = ema.R2_EMA_LOO_Segmento__c;
                        lisNewsAtt.add(newAtt);
                    }
                }
            }

            if (!lisNewsAtt.isEmpty()) {
                Database.SaveResult[] insertRes = Database.insert(lisNewsAtt, true);
                if (insertRes[0].isSuccess()) {
                    Database.DeleteResult[] delRes = Database.delete(mapAtt.values(), false);
                    if (delRes[0].isSuccess()) {
                        System.debug('*** ema att process ok');
                        result = true;
                    }
                }
            }else{
                result = true;
            }

            if(!result){
                Database.rollback(sp);
            }

            return result;
		}catch(Exception exc){
            Database.rollback(sp);
            R1_CLS_LogHelper.generateErrorLog('R2_CLS_SegmentoTriggerMethods.relatedAttOfEmaToSeg()', '', exc.getmessage()+', '+exc.getLineNumber(), 'R2_Segmento__c');
            return false;
        }
	}

}