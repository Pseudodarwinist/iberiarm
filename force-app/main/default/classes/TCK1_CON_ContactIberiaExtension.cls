/*---------------------------------------------------------------------------------------------------------------------
   Author:         TCK
   Company:        The Cocktail
   Description:    Email Service Class.

   History:
    <Date>                     <Author>                         <Change Description>
   29/09/2019                      TCK                           Initial Version
----------------------------------------------------------------------------------------------------------------------*/
public class TCK1_CON_ContactIberiaExtension {

    public Case caso { get; set; }
    public Blob document {get; set;}
    public Blob document2 {get; set;}
    public Blob document3 {get; set;}
    public Blob document4 {get; set;}
    public String contentType {get; set;}
    public String fileName {get; set;}
    public String fileName2 {get; set;}
    public String fileName3 {get; set;}
    public String fileName4 {get; set;}
    public String language {get;set;}
    public List<SelectOption> optionsToReturnShow {get;set;}
    public Boolean isCCPOmail {get;set;}
    public Boolean emailExist {get;set;}
    public String targetLevel {get;set;}
    public Map<String,List<String>> objResults {get;set;}
    public TCK1_CON_ContactIberiaExtension() {
        caso = new Case();
    }

    /**
    * @author TCK&LCS
    * @date 03/09/2019
    * @description Save case
    * @return PageReference pageRef
    */  
    public PageReference save() {
        PageReference pageRef;
        String FFlayercaso = 'IB'+returnFF(caso);
        String emailObtained= returnEmail(caso);
        caso.Origin = 'web';
        Group colaID;
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Comunicaciones entrantes').getRecordTypeId();
        caso.RecordTypeID=caseRecordTypeId;
        if(caso.R2_CAS_TXT_Primary_Frecuent_Flyer_Card__c != null){
            FFlayercaso = FFlayercaso.substring(2,FFlayercaso.length());
            if(FFlayercaso.startswith('0')){
                FFlayercaso = FFlayercaso.replaceFirst('^0+(?!$)', '');
            }
            FFlayercaso = 'IB'+FFlayercaso;

            try{
                list<Account> cuenta = [SELECT R1_ACC_CHK_Flag_Iberia_Singular__c,PersonEmail,R1_ACC_PKL_Card_Type__c,Id,R2_ACC_FOR_Id__c,FirstName,LastName,PersonMailingPostalCode,PersonMailingCity,PersonMailingCountry FROM account WHERE R1_ACC_TXT_Primary_Frecuent_Flyer_Card__c=: FFlayercaso LIMIT 1];              
                if(cuenta.size() == 1){
                    colaId = getAssign(cuenta[0].R1_ACC_PKL_Card_Type__c,cuenta[0].R1_ACC_CHK_Flag_Iberia_Singular__c,'');
                    if(colaID.id != null){
                        caso.OwnerId = colaID.Id;
                    }
                    
                    if(caso.R2_CAS_EMA_Email__c != null){
                    caso.R3_CAS_EMA_SenderEmail__c  = caso.R2_CAS_EMA_Email__c;
                    }
                    caso.description = 'Email del formulario: ' + caso.R2_CAS_EMA_Email__c + '\n' +'Subject: ' +caso.Subject + '\n'+'Date:  ' + datetime.now().addHours(1)  + '\n' + '\n\n'+ caso.description;
                    caso.R2_CAS_EMA_Email__c = cuenta[0].PersonEmail;
                    caso.AccountId=cuenta[0].Id;
                    caso.R2_CAS_TXT_Name__c=cuenta[0].FirstName;
                    caso.R2_CAS_TXT_LastName__c=cuenta[0].LastName;
                    caso.R2_CAS_TXT_Codigo_Postal_del_Pasajero__c=cuenta[0].PersonMailingPostalCode;
                    caso.R2_CAS_TXT_Ciudad_Postal_del_Pasajero__c=cuenta[0].PersonMailingCity;
                    caso.R2_CAS_TXT_Pais_Postal_del_Pasajero__c=cuenta[0].PersonMailingCountry;
                    insert caso;
                    saveAttachemnt(caso.Id);
                    pageRef = new PageReference('/apex/R3_VFP_ContactIberiaOk');
                    return pageRef;
                }else{
                    pageRef = new PageReference('/apex/R3_VFP_ContactIberiaKo');
                    return pageRef;
                }
            }catch(System.DMLException e){
                ApexPages.addMessages(e);
                R1_CLS_LogHelper.generateErrorLog('TCK1_CON_ContactIberiaExtension.save()', '', e.getmessage() + ', ' + e.getLineNumber(), 'Save from formulario');
                R3_CLS_UtilEmailHandler utilHandler = new R3_CLS_UtilEmailHandler();
                pageRef = new PageReference('/apex/R3_VFP_ContactIberiaKo');
                return pageRef;
            }
        }else{
            try{
                if(caso.R2_CAS_EMA_Email__c != null){
                    system.debug('No ponen FF --> buscamos por Email');
                    list<Account> cuenta = [SELECT R1_ACC_CHK_Flag_Iberia_Singular__c,R1_ACC_PKL_Card_Type__c,Id,R2_ACC_FOR_Id__c,FirstName,LastName,PersonMailingPostalCode,PersonMailingCity,PersonMailingCountry FROM account WHERE PersonEmail=: emailObtained];
                    if(cuenta.size() == 1){
                        colaId = getAssign(cuenta[0].R1_ACC_PKL_Card_Type__c,cuenta[0].R1_ACC_CHK_Flag_Iberia_Singular__c,'');
                        if(colaID.id != null){
                            caso.OwnerId = colaID.Id;
                        }
                        caso.description = 'Email del formulario: ' + caso.R2_CAS_EMA_Email__c + '\n' +'Subject: ' +caso.Subject + '\n'+'Date:  ' + datetime.now().addHours(1)  + '\n' + '\n\n'+ caso.description;
                        caso.R3_CAS_EMA_SenderEmail__c  = caso.R2_CAS_EMA_Email__c;
                        caso.AccountId=cuenta[0].Id;
                        caso.R2_CAS_TXT_Name__c=cuenta[0].FirstName;
                        caso.R2_CAS_TXT_LastName__c=cuenta[0].LastName;
                        caso.R2_CAS_TXT_Codigo_Postal_del_Pasajero__c=cuenta[0].PersonMailingPostalCode;
                        caso.R2_CAS_TXT_Ciudad_Postal_del_Pasajero__c=cuenta[0].PersonMailingCity;
                        caso.R2_CAS_TXT_Pais_Postal_del_Pasajero__c=cuenta[0].PersonMailingCountry;
                        insert caso;
                        saveAttachemnt(caso.Id);
                        pageRef = new PageReference('/apex/R3_VFP_ContactIberiaOk');
                    }else if(cuenta.size() > 1){
                        String levelQueue= queueLevel(cuenta);
                        colaId = getAssign(levelQueue,cuenta[0].R1_ACC_CHK_Flag_Iberia_Singular__c,'');
                        if(colaID.id != null){
                            caso.OwnerId = colaID.Id;
                        }
                        insert caso;
                        saveAttachemnt(caso.Id);
                        pageRef = new PageReference('/apex/R3_VFP_ContactIberiaOk');
                    }else{
                        system.debug('No encuentra usuario por email --> devolvemos KO');
                        pageRef = new PageReference('/apex/R3_VFP_ContactIberiaKo');
                    }
                }else{
                    pageRef = new PageReference('/apex/R3_VFP_ContactIberiaKo');
                }
            }catch(System.DMLException e){
                ApexPages.addMessages(e);
                R1_CLS_LogHelper.generateErrorLog('TCK1_CON_ContactIberiaExtension.save()', '', e.getmessage() + ', ' + e.getLineNumber(), 'Save from formulario');
                R3_CLS_UtilEmailHandler utilHandler = new R3_CLS_UtilEmailHandler();
                pageRef = new PageReference('/apex/R3_VFP_ContactIberiaKo');
                return pageRef;
            }
        }
        return pageRef;
    }

    
    /**
    * @author TCK&LCS
    * @date 03/09/2019
    * @description getTypes for the form.
    * @return List<SelectOption> optionsToReturn
    */  
    public List<SelectOption> getTypes() {
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult describeResult = Case.R1_CAS_PKL_TypeCE__c.getDescribe();
        List<Schema.PicklistEntry> entries = describeResult.getPicklistValues();
        List<SelectOption> optionsToReturn = new List<SelectOption>();
        List <R3_CS_TypesForAgents__c> typesForAgents = R3_CS_TypesForAgents__c.getall().values();
        List<String> typesForAgentsString = new List<String>();
        for(R3_CS_TypesForAgents__c aux:typesForAgents){
            typesForAgentsString.add(aux.Name);
        }
        
        for (Schema.PicklistEntry pEntry : entries) {
            if (pEntry.isActive() && !typesForAgentsString.contains(pEntry.getValue())) {
                optionsToReturn.add(new SelectOption(pEntry.getValue(), pEntry.getLabel()));
            }
        }
        Case c= new Case();
        return optionsToReturn;
    }

    
    /**
    * @author TCK&LCS
    * @date 03/09/2019
    * @description get subtypes
    * @return List<SelectOption> optionsToReturn
    */  
    public List<SelectOption> getSubtypes() {
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult describeResult = Case.R1_CAS_PKL_typeCE__c.getDescribe();
        List<Schema.PicklistEntry> entries = describeResult.getPicklistValues();
        List<SelectOption> optionsToReturn = new List<SelectOption>();
        for (Schema.PicklistEntry pEntry : entries) {
            if (pEntry.isActive()) {
                optionsToReturn.add(new SelectOption(pEntry.getValue(), pEntry.getLabel()));
            }
        }
        return optionsToReturn;
    }

    /**
    * @author TCK&LCS
    * @date 05/09/2019
    * @description Save attachment
    * @param Id caseId
    */  
    public void saveAttachemnt(Id caseId){
        List<Attachment> attList = new List<Attachment>();
        if(document != null){
            Attachment attach = new Attachment(Body= document, ParentID = caseId, Name= filename);
            attList.add(attach);
        }

        if(document2 != null){
            Attachment attach2 = new Attachment(Body= document2, ParentID = caseId, Name= filename2);
            attList.add(attach2);
        }

        if(document3 != null){
            Attachment attach3 = new Attachment(Body= document3, ParentID = caseId, Name= filename3);
            attList.add(attach3);
        }
        if(document4 != null){
            Attachment attach4 = new Attachment(Body= document4, ParentID = caseId, Name= filename4);
            attList.add(attach4);
        }
        try{
            insert attList;
        }catch(System.DMLException e){
            ApexPages.addMessages(e);
            R1_CLS_LogHelper.generateErrorLog('TCK1_CON_ContactIberiaExtension.saveAttachment()', '', e.getmessage() + ', ' + e.getLineNumber(), 'Save Attachment');
        }
    }

   //This method is used to find the FF that the client introduces in the site. We will use it in order to link the case in SF to its corresponding client in SF 
    public String returnFF (case caso){
        String subject = String.valueOf(caso.R2_CAS_TXT_Primary_Frecuent_Flyer_Card__c);
        return subject;
        }

   //This method is used to find the email that the client introduces in the site. We will use it in order to link the case in SF to its corresponding client in SF 
    public String returnEmail (case caso){
        String emailObtained = String.valueOf(caso.R2_CAS_EMA_Email__c);
       // system.debug('email: '+subject);
        return emailObtained;
    }

    public PageReference changeEnglish(){
        language = 'EN';
        return null;
    }
    
    public PageReference changeSpanish(){
        language = 'ES';
        return null;
    }
    
    /**
    * @author TCK&LCS
    * @date 05/09/2019
    * @description know QueueLevel
    * @param list<account> listAccount
    * @return String toReturn
    */  
    public String queueLevel(list<account> listAccount){
        String aux = '';
        String toReturn= '';
        for(Account acc:listAccount){
            if(aux == ''){
                aux = acc.R1_ACC_PKL_Card_Type__c;
            }
            if(acc.R1_ACC_PKL_Card_Type__c != aux ){
                toReturn = '2';
                break;
            }
            toReturn = aux;
        }
        return toReturn;
    }

    /**
    * @author TCK&LCS
    * @date 05/09/2019
    * @description 
    * @return PageReference
    */      
    public PageReference retrieveMailInfo() {

        List<Account> acc = [SELECT Id,R1_ACC_PKL_Card_Type__c FROM Account WHERE PersonEmail =: caso.R2_CAS_EMA_Email__c];
        isCCPOmail = true; //le pide a todos el FF, para cambiarlo habria que poner false 
        emailExist = false;
        try{
            if(acc.size() == 0){
                isCCPOmail = true;
            }
            for(Account aux:acc){
                if(aux.R1_ACC_PKL_Card_Type__c != '2' ){//&& aux.R1_ACC_PKL_Card_Type__c != '5' Este es el de Plata que nos pidieron que pidiera el FF tambien
                    //isCCPOmail = true; ahora le pide FF a todos
                }
                emailExist = true;
            }
            return null;
        }catch(System.DMLException e){
            ApexPages.addMessages(e);
            return null;
        }
    }

    /**
    * @author TCK&LCS
    * @date 05/09/2019
    * @description get Queue
    * @param tring DeveloperName
    * @return Group colaId
    */  
    public Group getQueue (String DeveloperName) {      
        Group colaID = [Select ID from Group where Type = 'Queue' and DeveloperName =: DeveloperName Limit 1];
        return colaId;
    }


    /**
    * @author TCK&LCS
    * @date 05/09/2019
    * @description get Queue to Assing
    * @param String cardType
    * @return Group colaId
    */  
    public Group getAssign(String cardType, Boolean singular, String addressTo){
        system.debug('getAssign: '+cardType+', singular: '+singular+', addressTo: '+addressTo);
        Group colaId;
        R3_CS_ColasBuzones__c plata = R3_CS_ColasBuzones__c.getValues('Serviberia_Plata');
        R3_CS_ColasBuzones__c oro = R3_CS_ColasBuzones__c.getValues('CCPO_Oro');
        R3_CS_ColasBuzones__c platino = R3_CS_ColasBuzones__c.getValues('CCPO_Platino');
        R3_CS_ColasBuzones__c clasica = R3_CS_ColasBuzones__c.getValues('Serviberia_Clasica');
        R3_CS_ColasBuzones__c baby = R3_CS_ColasBuzones__c.getValues('CCPO_Baby');
        R3_CS_ColasBuzones__c ibPlusMenores = R3_CS_ColasBuzones__c.getValues('Serviberia_IbPlusMenores');
        R3_CS_ColasBuzones__c menoresIb = R3_CS_ColasBuzones__c.getValues('Serviberia_MenoresIberia');
        R3_CS_ColasBuzones__c hotelesAvios = R3_CS_ColasBuzones__c.getValues('HotelesConAvios');
        R3_CS_ColasBuzones__c infinita = R3_CS_ColasBuzones__c.getValues('CCPO_Infinita');
        R3_CS_ColasBuzones__c infinitaPrime = R3_CS_ColasBuzones__c.getValues('CCPO_InfinitaPrime');
        R3_CS_ColasBuzones__c singularIberia = R3_CS_ColasBuzones__c.getValues('CCPO_Singular');
        if(singular && addressTo != '' && Label.R3_LBL_SingularIberia.contains(addressTo)){
            colaID = getQueue(singularIberia.Api_Name__c);
        }else if('1' == cardType || 'CCPO_Oro'== cardType ){
            colaID = getQueue(oro.Api_Name__c);
        }else if('2' == cardType){
            colaID = getQueue(clasica.Api_Name__c);
        }else if('serviberia_clasica' == cardType){
            colaID = getQueue(clasica.Api_Name__c);
        }else if('4' == cardType ||'CCPO_Platino' == cardType){
            colaID = getQueue(platino.Api_Name__c);
        }else if('5' == cardType){
            colaID = getQueue(plata.Api_Name__c);
        }else if('serviberia_plata' == cardType){
            colaID = getQueue(plata.Api_Name__c);
        }else if('CCPO_Baby' == cardType){
            colaID = getQueue(baby.Api_Name__c);
        }else if('Serviberia_IbPlusMenores' == cardType){
            colaID = getQueue(ibPlusMenores.Api_Name__c);
        }else if('Serviberia_MenoresIberia' == cardType){
            colaID = getQueue(menoresIb.Api_Name__c);
        }else if('hotelesconavios' == cardType){
            colaID = getQueue(hotelesAvios.Api_Name__c);
        }else if('CCPO_InfinitaPrime'== cardType){
            colaID = getQueue(infinitaPrime.Api_Name__c);
        }else if('CCPO_Infinita'== cardType){
            colaID = getQueue(infinita.Api_Name__c);
        }else if('CCPO_Singular'== cardType){
            colaID = getQueue(singularIberia.Api_Name__c);
        }else{
            system.debug('No se encuentra cola-> No tiene que pasar');
            colaID = getQueue(clasica.Api_Name__c);
        }
        return colaId;
    }

    public void getSubtypesDependent() {
        Schema.DescribeFieldResult fieldResult = Case.R1_CAS_PKL_TypeCE__c.getDescribe();
        List<Schema.PicklistEntry> values = fieldResult.getPicklistValues();
        Map<String,String> MapValueLabel = new Map<String,String>();
        String publicLabel;
        for( Schema.PicklistEntry v : values){
            MapValueLabel.put(v.getValue(),v.getLabel()); 
        }
        String typeSelected = Apexpages.currentPage().getParameters().get('valorSel');
        typeSelected = MapValueLabel.get(typeSelected);

        Case c = new Case();
        Map<String, List<String>> auxMap =  getDependentMap(c,'R1_CAS_PKL_TypeCE__c','R1_CAS_PKL_SubtypeCE__c','');
        List<String> listaSubTypes = auxMap.get(typeSelected);
        List<SelectOption> options = new List<SelectOption>(); 
        Schema.DescribeFieldResult describeResult = Case.R1_CAS_PKL_SubtypeCE__c.getDescribe();
        List<Schema.PicklistEntry> entries = describeResult.getPicklistValues();
        List<SelectOption> optionsToReturn = new List<SelectOption>();
        String fieldsOk = '';
        for(String listString:listaSubTypes){
            fieldsOk = fieldsOk+','+listString+',';
        }

        for (Schema.PicklistEntry pEntry : entries) {
            if (pEntry.isActive() && (fieldsOk.contains(','+pEntry.getValue()+',') || fieldsOk.contains(','+pEntry.getLabel()+','))) {
                optionsToReturn.add(new SelectOption(pEntry.getValue(), pEntry.getLabel()));
            }
        }
        optionsToReturnShow =optionsToReturn;
    }

    public static Map<String, List<String>> getDependentMap(sObject objDetail, string contrfieldApiName,string depfieldApiName, string depfieldApiName2) {
        String controllingField = contrfieldApiName.toLowerCase();
        String dependentField = depfieldApiName.toLowerCase();

        Map<String,List<String>> objResults = new Map<String,List<String>>();

        String objType = objDetail.getSObjectType().getDescribe().getName();
        if (objType==null){
            return objResults; 
        }
        Map<String, Schema.SObjectField> objFieldMap = objDetail.getSObjectType().getDescribe().fields.getMap();

        if (!objFieldMap.containsKey(controllingField) || !objFieldMap.containsKey(dependentField)){
            return objResults;     
        }

        Schema.SObjectField theField = objFieldMap.get(dependentField);
        Schema.SObjectField ctrlField = objFieldMap.get(controllingField);

        List<Schema.PicklistEntry> contrEntries = ctrlField.getDescribe().getPicklistValues();
        List<PicklistEntryWrapper> depEntries = wrapPicklistEntries(theField.getDescribe().getPicklistValues());
        List<String> controllingValues = new List<String>();

        for (Schema.PicklistEntry ple : contrEntries) {
            String label = ple.getLabel();
            objResults.put(label, new List<String>());
            controllingValues.add(label);
        }

        for (PicklistEntryWrapper plew : depEntries) {
            String label = plew.label;
            String validForBits = base64ToBits(plew.validFor);
            for (Integer i = 0; i < validForBits.length(); i++) {
                String bit = validForBits.mid(i, 1);
                if (bit == '1') {
                    objResults.get(controllingValues.get(i)).add(label);
                }
            }
        }
        return objResults;

    }


    public static String decimalToBinary(Integer val) {
        String bits = ''; 
        while (val > 0) {
            Integer remainder = Math.mod(val, 2);
            val = Integer.valueOf(Math.floor(val / 2));
            bits = String.valueOf(remainder) + bits;
        }
        return bits;
    }

    public static String base64ToBits(String validFor) {
        if (String.isEmpty(validFor)) return '';

        String validForBits = '';

        for (Integer i = 0; i < validFor.length(); i++) {
            String thisChar = validFor.mid(i, 1);
            Integer val = base64Chars.indexOf(thisChar);
            String bits = decimalToBinary(val).leftPad(6, '0');
            validForBits += bits;
        }

        return validForBits;
    }

    private static final String base64Chars = '' +
        'ABCDEFGHIJKLMNOPQRSTUVWXYZ' +
        'abcdefghijklmnopqrstuvwxyz' +
        '0123456789+/';


    private static List<PicklistEntryWrapper> wrapPicklistEntries(List<Schema.PicklistEntry> PLEs) {
        return (List<PicklistEntryWrapper>)
            JSON.deserialize(JSON.serialize(PLEs), List<PicklistEntryWrapper>.class);
    }

    public class PicklistEntryWrapper{
        public String active {get;set;}
        public String defaultValue {get;set;}
        public String label {get;set;}
        public String value {get;set;}
        public String validFor {get;set;}
        public PicklistEntryWrapper(){            
        }
    }

    public void valueupdate(){
        String x=Apexpages.currentPage().getParameters().get('valFromVf');
        Decimal test;
        test = decimal.valueof(x);

    }


}