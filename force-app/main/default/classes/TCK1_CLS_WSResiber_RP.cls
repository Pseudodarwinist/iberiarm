public class TCK1_CLS_WSResiber_RP {
    public LegData legData;

	public class LegData {
		public String carrierCode;
		public String flightNumber;
		public String departureDateTime;
		public String origin;
		public String destination;
		public List<LegCabinData> legCabinData;
	}

	public class LegCabinData {
		public String cabinCode;
		public String actualCapacity;
		public String authorizedCapacity;
		public String seatsSold;
		public String seatsAvailable;
	}

}