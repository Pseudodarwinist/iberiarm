/*---------------------------------------------------------------------------------------------------------------------
    Author:         ICA-LCS
    Company:        LeadClic
    Description:   R3_Claims_Automation_Rules__c trigger handler

    History:
     <Date>                     <Author>                         <Change Description>
    26/11/2019              		ICA                  				Initial Version
 ----------------------------------------------------------------------------------------------------------------------*/
/**
* @author LCS
* @date 26/11/2019
* @description R3_Claims_Automation_Rules__c trigger handler
*/
public with sharing class R3_CLS_ClaimsAutoRulesHandler {

    public static void beforeInsert(List<R3_Claims_Automation_Rules__c> triggerNew) {
		validateEmailTemplates(triggerNew);
    }

	public static void beforeUpdate(List<R3_Claims_Automation_Rules__c> triggerNew,Map<Id,R3_Claims_Automation_Rules__c> triggerNewMap){
		validateEmailTemplates(triggerNew);
    }

	/**
    * @author LCS
    * @date 27/11/2019 
    * @description Validates whether the rules have a valid and active emailtemplate. Fires an exception if the email template is not valid.
    * @param List<R3_Claims_Automation_Rules__c> triggerNew List incoming values to evaluate.
	* @return List<R3_Claims_Automation_Rules__c> Invalid rules.
    */	
	public static List<R3_Claims_Automation_Rules__c> validateEmailTemplates(List<R3_Claims_Automation_Rules__c> ruleList){
		List<R3_Claims_Automation_Rules__c> retorno = new List<R3_Claims_Automation_Rules__c>();
		Map<String,EmailTemplate> mapEmailTemplateMapping = new Map<String,EmailTemplate>();
		Set<String> setTemplateName = new Set<String>();

		for(R3_Claims_Automation_Rules__c r: ruleList){
			setTemplateName.add(r.R3_CAR_TXT_TemplateName__c);
		}
		for(EmailTemplate em:[Select Id,Name, FolderName from EmailTemplate where Name In: setTemplateName AND IsActive = true]){
			mapEmailTemplateMapping.put(em.Name,em);
		}
		
		for(R3_Claims_Automation_Rules__c r: ruleList){
			if(!mapEmailTemplateMapping.containsKey(r.R3_CAR_TXT_TemplateName__c) || !r.R3_CAR_TXT_Email_Templates_Folder__c.equals(mapEmailTemplateMapping.get(r.R3_CAR_TXT_TemplateName__c).FolderName)){
				if(Trigger.isExecuting && Schema.R3_Claims_Automation_Rules__c.getSObjectType()==(trigger.isDelete ? trigger.old.getSObjectType() : trigger.new.getSObjectType())){
					r.addError(Label.ErrorInsertClaimsAutoRule.replace('+NOMBRE+',r.R3_CAR_TXT_TemplateName__c).replace('+CARPETA+',r.R3_CAR_TXT_Email_Templates_Folder__c));
				}
				retorno.add(r);
			}
		}

		return retorno;
	}
}