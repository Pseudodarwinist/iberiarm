global class R2_JOB_ReExecuteFlightCompensation implements Schedulable {
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:       	ROBERTO RACANCOJ
    Company:       	Accenture
    Description: 	Job to active the process that calls a method to Reexecute the flight compensation that failed previously
    
    IN:				
	OUT:			

    History: 
     <Date>                     <Author>                <Change Description>
    21/01/2019                ROBERTO RACANCOJ             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    Case casoJob;
    public R2_JOB_ReExecuteFlightCompensation(Case childCase){
        casoJob = childCase;
    }


    global void execute(SchedulableContext sc) {
		try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');} //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;    
			
			System.abortJob(sc.getTriggerId());
		}
		catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_JOB_ReExecuteFlightCompensation.execute', 'Error System.abortJob(sc.getTriggerId());', exc.getmessage()+', '+exc.getLineNumber(), '');
        }    	

		callExecuteFlightCompensations(casoJob);

	}


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:       	ROBERTO RACANCOJ
    Company:       	Accenture
    Description: 	Job to active the process that calls a method to Reexecute the flight compensation that failed previously
    
    IN:				
	OUT:			

    History: 
     <Date>                     <Author>                <Change Description>
    21/01/2019                ROBERTO RACANCOJ             Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    public static void callExecuteFlightCompensations (Case caso) {
        try{
			if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');} //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;    

			System.debug('ESTOY DENTRO DE EL JOOOOOOB!!!!!!!!!'+' id caso '+caso.Id);
			//CASE caso = [SELECT Id,CaseNumber FROM CASE WHERE Id =:casoId AND R2_CAS_TXT_voucher_flight_comp__c ='KO, error en la conexión con Gestor de Bonos'];
			
            R2_CLS_CaseTriggerMethods.checkCompensationFlight(null,false,caso.Id);
			//R2_CLS_ProrrateoTriggerMethods.sendEmailProrrateo(casosId_Set, true);

		}catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_JOB_ReExecuteFlightCompensation.callExecuteFlightCompensations', '', exc.getmessage()+', '+exc.getLineNumber(), 'R2_CASTRIGGER_FLIGHT_COMPENSATION__c');
        }  
    }

}