public with sharing class R2_CLS_CaratulaProrrateo_CTRL {

    public class ProrrateoWP{
        public String to {get; set;}
        public String date_now { get; set;}
        public String our_ref { get; set;}
        public String passenger_name { get; set;}
        public String file_reference { get; set;}
        public String date_created { get; set;}
        public String claim_type { get; set;}
        public String ticket_number { get; set;}
        public String baggage_tag_number { get; set;}
        public String please_remit {get; set;}
        public String moneda {get; set;}
        public String total_milles {get; set;}
        public String total_amount {get; set;}
    }
    public class SegmentoWP{
        public String airline { get; set;}
        public String flight { get; set;}
        public String fecha { get; set;}
        public String carrier { get; set;}
        public String desde { get; set;}
        public String hasta { get; set;}
        public String statute_miles { get; set;}
        public String percentage { get; set;}
        public String moneda { get; set;}
        public String amount { get; set;}
    }

    public String render { get; set;}

    private String idPro;
    private String idCia;
    private String idSeg;

    private R2_Prorrateo__c prorrateo;
    private R2_Segmento__c segmento;
    private R2_Diccionario_Cias__c compania;

    public R2_CLS_CaratulaProrrateo_CTRL (){
            idPro = (String) ApexPages.currentPage().getParameters().get('id');
            idCia = (String) ApexPages.currentPage().getParameters().get('cia');
            idSeg = (String) ApexPages.currentPage().getParameters().get('seg');
            render = ApexPages.currentPage().getParameters().get('render')!='' && ApexPages.currentPage().getParameters().get('render')!=null ? (String) ApexPages.currentPage().getParameters().get('render') : 'pdf';

            // prorrateo
		    prorrateo = idPro!=null && idPro!='' ? getProrrateo(idPro) : null;
            // Segmento
            segmento = idSeg!=null && idSeg!='' ? getSegmento(idSeg) : null;
            // comapani CIA
            compania = idCia!=null && idCia!='' ? getCia(idCia) : null; 
    }

    public ProrrateoWP getProrrateoData(){
        try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}

            ProrrateoWP proWp = new ProrrateoWP();

            proWp.to = compania!=null && compania.R2_DIC_TXT_Nombre_Compania__c!=null ? compania.R2_DIC_TXT_Nombre_Compania__c : '';
            proWp.date_now = Date.today().format();
            proWp.our_ref = prorrateo!=null && prorrateo.R2_PRO_TXT_Our_Ref__c!=null ? prorrateo.R2_PRO_TXT_Our_Ref__c : '';
            proWp.passenger_name = prorrateo!=null && prorrateo.R2_PRO_LOO_Passenger_Name__r.Name != null ? prorrateo.R2_PRO_LOO_Passenger_Name__r.Name : '';
            proWp.file_reference = prorrateo!=null && prorrateo.R2_PRO_FOR_PIR__c!=null ? prorrateo.R2_PRO_FOR_PIR__c : '';
            proWp.date_created = prorrateo!=null && prorrateo.R2_PRO_FOR_pir_date__c!=null ? Date.valueOf(prorrateo.R2_PRO_FOR_pir_date__c).format(): '';
            proWp.claim_type = prorrateo!=null && prorrateo.R2_PRO_PKL_Claim_Type__c!=null ? prorrateo.R2_PRO_PKL_Claim_Type__c : '';
            proWp.ticket_number = prorrateo!=null && prorrateo.R2_PRO_NUM_Ticket_Number__c!=null ? prorrateo.R2_PRO_NUM_Ticket_Number__c : '';
            //proWp.baggage_tag_number = prorrateo!=null && prorrateo.R2_PRO_TXT_Baggage_Tag_Number__c!=null ? prorrateo.R2_PRO_TXT_Baggage_Tag_Number__c : '';
            proWp.baggage_tag_number = prorrateo!=null && prorrateo.R2_PRO_LOO_PIR__c!=null ? getBagsTagNumbers(prorrateo.R2_PRO_LOO_PIR__c) : '';
            proWp.total_milles = prorrateo!=null && prorrateo.R2_PRO_NUM_Total_Miles__c!=null ? String.valueOf(prorrateo.R2_PRO_NUM_Total_Miles__c) : '' ;
            proWp.total_amount = prorrateo!=null && prorrateo.R2_PRO_NUM_total_amount__c!=null ? String.valueOf(prorrateo.R2_PRO_NUM_total_amount__c) : '' ;
            proWp.please_remit = segmento!=null && segmento.R2_SEG_FOR_max_pro__c!=null ? String.valueOf(segmento.R2_SEG_FOR_max_pro__c) : '';
            proWp.moneda = segmento!=null && segmento.R2_SEG_CUR_Currency__c!=null ? segmento.R2_SEG_CUR_Currency__c : '';

            return proWp;
            
        }catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_CLS_CaratulaProrrateo_CTRL.getProrrateoData()', '', exc.getmessage()+', '+exc.getLineNumber(), '');
            return null;
        }
    }

    public List<SegmentoWP> getSegmentsData(){
        try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}

            List<SegmentoWP> listResult = new List<SegmentoWP>();

            List<R2_Segmento__c> listSegment = prorrateo!=null ? getSegmentos(prorrateo.Id) : null;
            
            if(listSegment!=null && !listSegment.isEmpty()){
                for(R2_Segmento__c seg : listSegment){
                    SegmentoWP wpr = new SegmentoWP();
                    wpr.airline = seg.R2_SEG_TXT_Company__c!=null ? seg.R2_SEG_TXT_Company__c: '';
                    wpr.flight = seg.R2_SEG_TXT_Flight__c!=null ? seg.R2_SEG_TXT_Flight__c : '';
                    wpr.fecha = seg.R2_SEG_DAT_Date__c!=null? formatearFecha(seg.R2_SEG_DAT_Date__c) : '';
                    wpr.carrier = seg.R2_SEG_TXT_company_code__c!=null ? seg.R2_SEG_TXT_company_code__c : '';
                    wpr.desde = seg.R2_SEG_TXT_From__c!=null ? seg.R2_SEG_TXT_From__c : '';
                    wpr.hasta = seg.R2_SEG_TXT_To__c!=null ? seg.R2_SEG_TXT_To__c : '';
                    wpr.statute_miles = seg.R2_SEG_NUM_Statute_Miles__c!=null ? String.valueOf(seg.R2_SEG_NUM_Statute_Miles__c) : '';
                    wpr.percentage = seg.R2_SEG_NUM_Percentage__c!=null ? String.valueOf(seg.R2_SEG_NUM_Percentage__c) + ' %' : '';
                    wpr.moneda = seg.R2_SEG_CUR_Currency__c!=null ? String.valueOf(seg.R2_SEG_CUR_Currency__c) : '';
                    wpr.amount = seg.R2_SEG_NUM_Amount__c!=null ? String.valueOf(seg.R2_SEG_NUM_Amount__c) : '';
                    listResult.add(wpr);
                }
            }

            return listResult;

        }catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_CLS_CaratulaProrrateo_CTRL.getSegments()', '', exc.getmessage()+', '+exc.getLineNumber(), '');
            return null;
        }
    }

    @TestVisible
	private R2_Prorrateo__c getProrrateo(String proId){
		try{
			if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}
			R2_Prorrateo__c prorrateo = null;

			List<R2_Prorrateo__c> listPro = [SELECT Id, R2_PRO_LOO_Passenger_Name__r.Name,  R2_PRO_LOO_Case__c, 
                                                    R2_PRO_TXT_Guilty_Company__c, R2_PRO_PKL_Status__c, R2_PRO_DAT_Date__c, 
                                                    R2_PRO_FOR_Pir_Number__c, R2_PRO_LOO_PIR__c, R2_PRO_LOO_Passenger_Name__c, 
                                                    R2_PRO_TXT_Baggage_Tag_Number__c, R2_PRO_NUM_Ticket_Number__c, 
                                                    R2_PRO_LOO_pay_order__c, R2_PRO_LOO_work_order__c, R2_PRO_TXT_Our_Ref__c, 
                                                    R2_PRO_PKL_Claim_Type__c, R2_PRO_NUM_Total_Miles__c,R2_PRO_NUM_total_amount__c,
                                                    R2_PRO_FOR_pir_date__c,R2_PRO_FOR_PIR__c
										    FROM R2_Prorrateo__c 
										    WHERE Id=:proId];

			prorrateo = listPro.size()>0 ? listPro[0] : null;

			return prorrateo;
		}catch(Exception exc){
			System.debug('Type: '+ exc.getTypeName()+ ' Trace: '+ exc.getStackTraceString());
        	return null;
        }
	}

    @TestVisible
	private R2_Segmento__c getSegmento(String idSeg){
		try{
			if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}
			R2_Segmento__c segmento = null;
			List<R2_Segmento__c> listSeg = [SELECT Id, R2_SEG_NUM_Total_Acumulado__c, R2_SEG_CUR_Currency__c, R2_SEG_FOR_max_pro__c 
                                            FROM R2_Segmento__c WHERE Id=:idSeg];
			segmento = listSeg.size()>0 ? listSeg[0] : null;
			return segmento;
		}catch(Exception exc){
			System.debug('Type: '+ exc.getTypeName()+ ' Trace: '+ exc.getStackTraceString());
        	return null;
        }
	}

    @TestVisible
	private List<R2_Segmento__c> getSegmentos(String idPro){
		try{
			if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}
		    
            List<R2_Segmento__c> listSeg = [SELECT Id, R2_SEG_TXT_Flight__c,R2_SEG_TXT_company_code__c,
                                                    R2_SEG_TXT_Company__c, R2_SEG_DAT_Date__c, R2_SEG_TXT_From__c,
                                                    R2_SEG_TXT_To__c,R2_SEG_NUM_Statute_Miles__c, R2_SEG_NUM_Percentage__c, 
                                                    R2_SEG_CUR_Currency__c,R2_SEG_NUM_Amount__c 
                                            FROM R2_Segmento__c 
                                            WHERE R2_SEG_MSDT_Prorrate__c=:idPro ORDER BY R2_SEG_TXT_segment_order__c ASC];
			return listSeg;
		}catch(Exception exc){
			System.debug('Type: '+ exc.getTypeName()+ ' Trace: '+ exc.getStackTraceString());
        	return null;
        }
	}

    @TestVisible
	private R2_Diccionario_Cias__c getCia(String idCia){
		try{
			if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}
			R2_Diccionario_Cias__c cia = null;
			List<R2_Diccionario_Cias__c> listCia = [SELECT Id, R2_DIC_TXT_Nombre_Compania__c 
                                                    FROM R2_Diccionario_Cias__c 
                                                    WHERE Id=:idCia];
			cia = listCia.size()>0 ? listCia[0] : null;
			return cia;
		}catch(Exception exc){
			System.debug('Type: '+ exc.getTypeName()+ ' Trace: '+ exc.getStackTraceString());
        	return null;
        }
	}

    @TestVisible
    private String getBagsTagNumbers(String pirId){
        try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}
            String result = '';
            List<R2_Baggage__c> listBag = getBaggages(pirId);
            List<String> listBagNum = new List<String>();
            for(R2_Baggage__c bag : listBag){
                listBagNum.add(bag.R2_BAG_TXT_Ticket_number__c);
            }
            if(!listBagNum.isEmpty()){
                result = String.join(listBagNum, '/');
            }
            return result;
        }catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_CLS_CaratulaProrrateo_CTRL.getBagsTagNumbers()', '', exc.getmessage()+', '+exc.getLineNumber(), '');
            return '';
        }
    }

    @TestVisible
    private List<R2_Baggage__c> getBaggages(String pirId){
        try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}
            List<R2_Baggage__c> listBag = [SELECT Id,R2_BAG_TXT_Ticket_number__c FROM R2_Baggage__c WHERE R2_BAG_MSDT_Incident__c=:pirId];
            return listBag;
        }catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_CLS_CaratulaProrrateo_CTRL.getBaggages()', '', exc.getmessage()+', '+exc.getLineNumber(), '');
            return null;
        }
    }

    @TestVisible
    private String formatearFecha(Date fecha){
        try{
        if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}

        // System.debug('Entro en formatear');
        string dia;
        if(fecha.day()<10){
                        dia= '0'+ String.valueOf(fecha.day());
                }else{
                        dia= String.valueOf(fecha.day());
                }
                string mes;
                if(fecha.month()==1){
                    mes = 'JAN';
                }
                else if(fecha.month()==2){
                    mes = 'FEB';
                }
                else if(fecha.month()==3){
                    mes = 'MAR';
                }
                else if(fecha.month()==4){
                    mes = 'APR';
                }
                else if(fecha.month()==5){
                    mes = 'MAY';
                }
                else if(fecha.month()==6){
                    mes = 'JUN';
                }
                else if(fecha.month()==7){
                    mes = 'JUL';
                }
                else if(fecha.month()==8){
                    mes = 'AUG';
                }
                else if(fecha.month()==9){
                    mes = 'SEP';
                }
                else if(fecha.month()==10){
                    mes = 'OCT';
                }
                else if(fecha.month()==11){
                    mes = 'NOV';
                }
                else if(fecha.month()==12){
                    mes = 'DEC';
                }

                string ano = String.valueOf(fecha.year()).subString(2,4);
                System.debug(dia+mes+ano);
                return dia+mes+ano;
        }catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_CLS_CaratulaProrrateo_CTRL.formatearFecha()', '', exc.getmessage()+', '+exc.getLineNumber(), '');
            return null;
        }
    }


    

}