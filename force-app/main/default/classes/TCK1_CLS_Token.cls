/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:         Victor Manuel Gonzalez
Company:        TCK
Description:    Connect Token

IN:       
OUT:      

History: 
<Date>                            <Author>                      <Change Description>
01/08/2019                 Victor Manuel Gonzalez                  Initial Version
--------------------------------------------------------------------------------------------------------------------------------------------------------*/

public class TCK1_CLS_Token{

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
Author:         Victor Manuel Gonzalez
Company:        TCK
Description:    Solicitud del Token

History: 
<Date>                            <Author>                      <Change Description>
01/08/2019                 Victor Manuel Gonzalez                Creacion del metodo
--------------------------------------------------------------------------------------------------------------------------------------------------------*/


    public static String loginToken() {

        HttpRequest req = new HttpRequest();
        try{
            
            TCK1_MTD_WSInfo__mdt[] tokenInfo = [SELECT Label, Client_API__c, Client_Encode__c, WSR_TEX_ContentType__c ,WSR_TEX_EndPoint__c, WSR_TXT_Method__c, WSR_NUM_TimeOut__c 
                                                FROM TCK1_MTD_WSInfo__mdt WHERE MasterLabel = 'Token'];
            if (tokenInfo.isEmpty()){
                throw new R1_CLS_LogHelper.R1_Exception('No se han definido metadatos para el callout de Token');
            }
            System.debug('Ha cogido el endpoint del Custom_mdt' + tokenInfo[0].WSR_TEX_EndPoint__c);

            req.setEndpoint(tokenInfo[0].WSR_TEX_EndPoint__c);
            req.setHeader('Accept-Encoding','gzip,deflate');
            String autorizationRQ = 'Basic '+ tokenInfo[0].Client_Encode__c +'==';
            req.setHeader('Authorization', autorizationRQ);            
            req.setMethod(tokenInfo[0].WSR_TXT_Method__c);
            req.setHeader('Content-Type', tokenInfo[0].WSR_TEX_ContentType__c);
            req.setHeader('api_key', tokenInfo[0].Client_API__c);            
            String body = 'grant_type=client_credentials';
            req.setbody(body);

            system.debug('req: ' + req);
            
            Http http = new Http();
            HTTPResponse res = new HTTPResponse();
                  
            res = http.send(req);
            system.debug('Respuesta del WS token: '+res);
            R1_CLS_SendCustomerMDM.WS_login_response responseToken = new R1_CLS_SendCustomerMDM.WS_login_response();
            responseToken = (R1_CLS_SendCustomerMDM.WS_login_response)JSON.deserialize(res.getBody(),R1_CLS_SendCustomerMDM.WS_login_response.class);
            return responseToken.access_token;
                
            }
        catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R1_CLS_SendCustomerMDM.login()', '', exc.getmessage()+', '+exc.getLineNumber(), 'Account');
            return null;
        }
    }
}