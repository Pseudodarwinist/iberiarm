@isTest
private class R2_CLS_ATT_AttachmentTriggerMethods_Test {
	
	@isTest static void asignarAttachmentAPadre() {

			R1_Flight__c vuelo = new R1_Flight__c();
            vuelo.Name = 'VueloTest';
            vuelo.R1_FLG_TXT_Origin__c = 'OrigenTest';
            vuelo.R1_FLG_TXT_Destination__c = 'DestinoTest';
            vuelo.R1_FLG_TXT_Carrier_code__c = 'IB';
            vuelo.R1_FLG_DAT_Flight_date_local__c = Date.newInstance(2017, 08, 21);
            vuelo.R1_FLG_TXT_Flight_number__c = '1234';
            insert vuelo;

            Account newAcc = new Account();
            newAcc.LastName = 'CuentaNueva';
        	newAcc.FirstName = 'Abc';
        	newAcc.PersonBirthdate = date.newInstance(1998, 10, 21);
        	newAcc.R1_ACC_EMA_Marketing_Email__c = 'acc@emailmark.com';
        	newAcc.R1_ACC_TXT_Email_Operative__c = 'acc@emailope.com';
        	newAcc.R1_ACC_TLF_Phone_Marketing__c = '+912141241';
        	newAcc.R1_ACC_TLF_Phone_Operative__c = '244242424';
        	newAcc.PersonEmail = 'test@test.com';
        	newAcc.R1_ACC_TXT_Primary_Frecuent_Flyer_Card__c = '42343242';
        	newAcc.R1_ACC_TXT_Identification_number__c = '4325232';

            //setUp custom Setting
            Id rtExp =  Schema.SObjectType.Case.getRecordTypeInfosByName().get('Expediente').getRecordTypeId();
           Id rtPas =  Schema.SObjectType.Case.getRecordTypeInfosByName().get('Pasaje').getRecordTypeId();

           	Case cas2 = new Case();
            cas2.Status = 'Abierto';
            cas2.RecordTypeId = rtExp;
            cas2.R1_CAS_PKL_Idioma__c = 'es';
            cas2.R2_CAS_PKL_Vip_Type__c = 'Estándar';
            cas2.R2_CAS_PKL_Type1__c = '5';
            cas2.R1_CAS_PKL_Subtype__c = '2';
            cas2.R2_CAS_CHK_Is_Proactive__c = true;

            insert cas2;

            Case cas = new Case();
            cas.Status = 'Abierto';
            cas.RecordTypeId = rtPas;
            cas.R1_CAS_PKL_Idioma__c = 'es';
            cas.R2_CAS_PKL_Vip_Type__c = 'Estándar';
            cas.R2_CAS_PKL_Type1__c = '5';
            cas.R1_CAS_PKL_Subtype__c = '2';
            cas.R2_CAS_CHK_Is_Proactive__c = true;
            cas.ParentId = cas2.id;
            insert cas;


            Blob body = Blob.valueOf('Test');
            System.debug('ID DE CASO TEST: '+ cas.id);
             Attachment attNew = new Attachment(name = 'Test', body = body, ParentId = cas.id);

             Test.startTest();
             insert attNew;
             Test.stopTest();
	}
	@isTest static void exceptionTest(){
		R2_CLS_ATT_AttachmentTriggerMethods.asignarAttachmentAPadre(null);
	}
	
}