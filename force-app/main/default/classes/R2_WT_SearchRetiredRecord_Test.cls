@isTest
private class R2_WT_SearchRetiredRecord_Test {

	@isTest 
	static void search_RetiredRecords_bag_Test() {
		R1_CLS_LogHelper.throw_exception = false;
        R1_CS_Endpoints__c ep = new R1_CS_Endpoints__c();
        ep.Name='R2_InactiveBags';
        ep.R1_CHK_Activo__c = true	;
        ep.R1_TXT_EndPoint__c='R2_InactiveBags';
        insert ep;
        R1_CS_Endpoints__c ep2 = new R1_CS_Endpoints__c();
        ep2.Name = 'ETL_Login';
        ep2.R1_CHK_Activo__c = true;
        ep2.R1_TXT_EndPoint__c = 'PruebaLogin';
        insert ep2;
        TimeOut_Integrations__c cst = new TimeOut_Integrations__c();
		cst.Name = 'WorldTracer';
		cst.setTimeOut__c = 8000;
		insert cst;
        String wrapper = '{"recordType":"DAMAGED","refStationAirline":{"stationCode":"MAD","airlineCode":"IB"},"date":"2015-05-20","name":{"name":"JOHN"},"agentId":"TESTAGENT"}';
		Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());
		Test.startTest();
		String dateTemp = String.valueOf(Date.today());
        R2_WT_SearchRetiredRecord.search_RetiredRecords_bag(wrapper, 0,  dateTemp);
		Test.stopTest();
        //System.assertEquals(0, [SELECT count() FROM R1_Log__c]);
	}

	@isTest 
	static void search_RetiredRecords_bag_Error401_Test() {
		R1_CLS_LogHelper.throw_exception = false;
        R1_CS_Endpoints__c ep = new R1_CS_Endpoints__c();
        ep.Name='R2_InactiveBags';
        ep.R1_CHK_Activo__c = true	;
        ep.R1_TXT_EndPoint__c='R2_InactiveBags401';
        insert ep;
        R1_CS_Endpoints__c ep2 = new R1_CS_Endpoints__c();
        ep2.Name = 'ETL_Login';
        ep2.R1_CHK_Activo__c = true;
        ep2.R1_TXT_EndPoint__c = 'PruebaLogin';
        insert ep2;
        TimeOut_Integrations__c cst = new TimeOut_Integrations__c();
		cst.Name = 'WorldTracer';
		cst.setTimeOut__c = 8000;
		insert cst;
        String wrapper = '{"recordType":"DAMAGED","refStationAirline":{"stationCode":"MAD","airlineCode":"IB"},"date":"2015-05-20","name":{"name":"JOHN"},"agentId":"TESTAGENT"}';
		Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());
		Test.startTest();
        String dateTemp = String.valueOf(Date.today());
        R2_WT_SearchRetiredRecord.search_RetiredRecords_bag(wrapper, 0,  dateTemp);
		Test.stopTest();
        //System.assertEquals(0, [SELECT count() FROM R1_Log__c]);
	}

	@isTest 
	static void search_RetiredRecords_bag_Error400_Test() {
		R1_CLS_LogHelper.throw_exception = false;
        R1_CS_Endpoints__c ep = new R1_CS_Endpoints__c();
        ep.Name='R2_InactiveBags';
        ep.R1_CHK_Activo__c = true	;
        ep.R1_TXT_EndPoint__c='R2_InactiveBags400';
        insert ep;
        R1_CS_Endpoints__c ep2 = new R1_CS_Endpoints__c();
        ep2.Name = 'ETL_Login';
        ep2.R1_CHK_Activo__c = true;
        ep2.R1_TXT_EndPoint__c = 'PruebaLogin';
        insert ep2;
        TimeOut_Integrations__c cst = new TimeOut_Integrations__c();
		cst.Name = 'WorldTracer';
		cst.setTimeOut__c = 8000;
		insert cst;
        String wrapper = '{"recordType":"DAMAGED","refStationAirline":{"stationCode":"MAD","airlineCode":"IB"},"date":"2015-05-20","name":{"name":"JOHN"},"agentId":"TESTAGENT"}';
		Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());
		Test.startTest();
        String dateTemp = String.valueOf(Date.today());
        R2_WT_SearchRetiredRecord.search_RetiredRecords_bag(wrapper, 0,  dateTemp);
		Test.stopTest();
        //System.assertEquals(0, [SELECT count() FROM R1_Log__c]);
	}

	@isTest 
	static void search_RetiredRecords_bag_Error403_Test() {
		R1_CLS_LogHelper.throw_exception = false;
        R1_CS_Endpoints__c ep = new R1_CS_Endpoints__c();
        ep.Name='R2_InactiveBags';
        ep.R1_CHK_Activo__c = true	;
        ep.R1_TXT_EndPoint__c='R2_InactiveBags403';
        insert ep;
        R1_CS_Endpoints__c ep2 = new R1_CS_Endpoints__c();
        ep2.Name = 'ETL_Login';
        ep2.R1_CHK_Activo__c = true;
        ep2.R1_TXT_EndPoint__c = 'PruebaLogin';
        insert ep2;
        TimeOut_Integrations__c cst = new TimeOut_Integrations__c();
		cst.Name = 'WorldTracer';
		cst.setTimeOut__c = 8000;
		insert cst;
        String wrapper = '{"recordType":"DAMAGED","refStationAirline":{"stationCode":"MAD","airlineCode":"IB"},"date":"2015-05-20","name":{"name":"JOHN"},"agentId":"TESTAGENT"}';
		Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());
		Test.startTest();
        String dateTemp = String.valueOf(Date.today());
        R2_WT_SearchRetiredRecord.search_RetiredRecords_bag(wrapper, 0,  dateTemp);
		Test.stopTest();
        //System.assertEquals(0, [SELECT count() FROM R1_Log__c]);
	}

	@isTest 
	static void search_RetiredRecords_bag_Error500_Test() {
		R1_CLS_LogHelper.throw_exception = false;
        R1_CS_Endpoints__c ep = new R1_CS_Endpoints__c();
        ep.Name='R2_InactiveBags';
        ep.R1_CHK_Activo__c = true	;
        ep.R1_TXT_EndPoint__c='R2_InactiveBags500';
        insert ep;
        R1_CS_Endpoints__c ep2 = new R1_CS_Endpoints__c();
        ep2.Name = 'ETL_Login';
        ep2.R1_CHK_Activo__c = true;
        ep2.R1_TXT_EndPoint__c = 'PruebaLogin';
        insert ep2;
        TimeOut_Integrations__c cst = new TimeOut_Integrations__c();
		cst.Name = 'WorldTracer';
		cst.setTimeOut__c = 8000;
		insert cst;
        String wrapper = '{"recordType":"DAMAGED","refStationAirline":{"stationCode":"MAD","airlineCode":"IB"},"date":"2015-05-20","name":{"name":"JOHN"},"agentId":"TESTAGENT"}';
		Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());
		Test.startTest();
        String dateTemp = String.valueOf(Date.today());
        R2_WT_SearchRetiredRecord.search_RetiredRecords_bag(wrapper, 0,  dateTemp);
		Test.stopTest();
        //System.assertEquals(0, [SELECT count() FROM R1_Log__c]);
	}

	@isTest 
	static void search_RetiredRecords_bag_Error_Test() {
		R1_CLS_LogHelper.throw_exception = false;
        R1_CS_Endpoints__c ep = new R1_CS_Endpoints__c();
        ep.Name='R2_InactiveBags';
        ep.R1_CHK_Activo__c = true	;
        ep.R1_TXT_EndPoint__c='R2_InactiveBagsError';
        insert ep;
        R1_CS_Endpoints__c ep2 = new R1_CS_Endpoints__c();
        ep2.Name = 'ETL_Login';
        ep2.R1_CHK_Activo__c = true;
        ep2.R1_TXT_EndPoint__c = 'PruebaLogin';
        insert ep2;
        TimeOut_Integrations__c cst = new TimeOut_Integrations__c();
		cst.Name = 'WorldTracer';
		cst.setTimeOut__c = 8000;
		insert cst;
        String wrapper = '{"recordType":"DAMAGED","refStationAirline":{"stationCode":"MAD","airlineCode":"IB"},"date":"2015-05-20","name":{"name":"JOHN"},"agentId":"TESTAGENT"}';
		Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());
		Test.startTest();
        String dateTemp = String.valueOf(Date.today());
        R2_WT_SearchRetiredRecord.search_RetiredRecords_bag(wrapper, 0,  dateTemp);
		Test.stopTest();
        //System.assertEquals(0, [SELECT count() FROM R1_Log__c]);
	}

	@isTest 
	static void search_RetiredRecords_bag_ErrorLogin_Test() {
		R1_CLS_LogHelper.throw_exception = false;
        R1_CS_Endpoints__c ep = new R1_CS_Endpoints__c();
        ep.Name='R2_InactiveBags';
        ep.R1_CHK_Activo__c = true	;
        ep.R1_TXT_EndPoint__c='R2_InactiveBags';
        insert ep;
        TimeOut_Integrations__c cst = new TimeOut_Integrations__c();
		cst.Name = 'WorldTracer';
		cst.setTimeOut__c = 8000;
		insert cst;
        String wrapper = '{"recordType":"DAMAGED","refStationAirline":{"stationCode":"MAD","airlineCode":"IB"},"date":"2015-05-20","name":{"name":"JOHN"},"agentId":"TESTAGENT"}';
		Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());
		Test.startTest();
        String dateTemp = String.valueOf(Date.today());
        R2_WT_SearchRetiredRecord.search_RetiredRecords_bag(wrapper, 0,  dateTemp);
		Test.stopTest();
        //System.assertEquals(0, [SELECT count() FROM R1_Log__c]);
	}
	
	@isTest 
	static void search_RetiredRecords_bag_ErrorEndpoint_Test() {
		R1_CLS_LogHelper.throw_exception = false;
        R1_CS_Endpoints__c ep2 = new R1_CS_Endpoints__c();
        ep2.Name = 'ETL_Login';
        ep2.R1_CHK_Activo__c = true;
        ep2.R1_TXT_EndPoint__c = 'PruebaLogin';
        insert ep2;
        TimeOut_Integrations__c cst = new TimeOut_Integrations__c();
		cst.Name = 'WorldTracer';
		cst.setTimeOut__c = 8000;
		insert cst;
        String wrapper = '{"recordType":"DAMAGED","refStationAirline":{"stationCode":"MAD","airlineCode":"IB"},"date":"2015-05-20","name":{"name":"JOHN"},"agentId":"TESTAGENT"}';
		Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());
		Test.startTest();
        String dateTemp = String.valueOf(Date.today());
        R2_WT_SearchRetiredRecord.search_RetiredRecords_bag(wrapper, 0,  dateTemp);
		Test.stopTest();
        //System.assertEquals(0, [SELECT count() FROM R1_Log__c]);
	}

}