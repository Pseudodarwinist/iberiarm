global class R2_JOB_resendEmailProrrateo implements Schedulable {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:       	Alvaro García
    Company:       	Accenture
    Description: 	Job to active the process to call a method to resend an email that failed previously
    
    IN:				
	OUT:			

    History: 
     <Date>                     <Author>                <Change Description>
    06/03/2018                  Alvaro García            Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	public R2_JOB_resendEmailProrrateo (){

	}

	global void execute(SchedulableContext sc) {
		try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');} //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;    
			
			System.abortJob(sc.getTriggerId());
		}
		catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_JOB_resendEmailProrrateo.execute', 'Error System.abortJob(sc.getTriggerId());', exc.getmessage()+', '+exc.getLineNumber(), '');
        }    	

		callSendEmail();

	}

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:       	Alvaro García
    Company:       	Accenture
    Description: 	get the prorrateos that fail sending the email and call the method to resend them 
    
    IN:				
	OUT:			

    History: 
     <Date>                     <Author>                <Change Description>
    06/03/2018                  Alvaro García            Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

	public static void callSendEmail () {
		try{
			if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');} //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;    

			Set<Id> proId_Set = new Set<Id>();
			List<R2_Prorrateo__c> proLst = [SELECT Id FROM R2_Prorrateo__c WHERE R2_PRO_PKL_Status__c!='Prorrateo OK' AND (R2_PRO_NUM_attempts_sent__c < 5 AND R2_PRO_CHK_Error_envio_email__c = true)];
			for (R2_Prorrateo__c pro : proLst) {
				proId_Set.add(pro.Id);
			}
			R2_CLS_ProrrateoTriggerMethods.sendEmailProrrateo(proId_Set, true);

		}catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_JOB_resendEmailProrrateo.callSendEmail', '', exc.getmessage()+', '+exc.getLineNumber(), 'R2_Prorrateo__c');
        }    
	}
}