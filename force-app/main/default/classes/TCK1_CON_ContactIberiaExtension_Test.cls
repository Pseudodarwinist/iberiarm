@isTest

public class TCK1_CON_ContactIberiaExtension_Test {
    
    
    @isTest static List<R3_CS_ColasBuzones__c> customSetting_Test(){
        
        List<R3_CS_ColasBuzones__c> listCs = new List<R3_CS_ColasBuzones__c>();
        R3_CS_ColasBuzones__c csColas1 = new R3_CS_ColasBuzones__c();
        csColas1.Name='CCPO_Oro';
        csColas1.Api_Name__c='R3_CCPO_Oro'; 
        R3_CS_ColasBuzones__c csColas2 = new R3_CS_ColasBuzones__c();
        csColas2.Name='Serviberia_Plata';
        csColas2.Api_Name__c='R3_Serviberia_Plata';
        R3_CS_ColasBuzones__c csColas3 = new R3_CS_ColasBuzones__c();
        csColas3.Name='CCPO_Platino';
        csColas3.Api_Name__c='R3_CCPO_Platino';
        R3_CS_ColasBuzones__c csColas4 = new R3_CS_ColasBuzones__c();
        csColas4.Name='Serviberia_Clasica';
        csColas4.Api_Name__c='R3_Serviberia_Clasica';
        R3_CS_ColasBuzones__c csColas5 = new R3_CS_ColasBuzones__c();
        csColas5.Name='Serviberia_Baby';
        csColas5.Api_Name__c='R3_Serviberia_Baby';
        R3_CS_ColasBuzones__c csColas6 = new R3_CS_ColasBuzones__c();
        csColas6.Name='Serviberia_IbPlusMenores';
        csColas6.Api_Name__c='R3_Serviberia_IbPlusMenores';
        R3_CS_ColasBuzones__c csColas7 = new R3_CS_ColasBuzones__c();
        csColas7.Name='Serviberia_MenoresIberia';
        csColas7.Api_Name__c='R3_Serviberia_MenoresIberia';
        R3_CS_ColasBuzones__c csColas8 = new R3_CS_ColasBuzones__c();
        csColas8.Name='Serviberia_HotelesAvios';
        csColas8.Api_Name__c='R3_Serviberia_HotelesAvios';
        listCs.add(csColas1);
        listCs.add(csColas2);
        listCs.add(csColas3);
        listCs.add(csColas4);
        listCs.add(csColas5);
        listCs.add(csColas6);
        listCs.add(csColas7);
        listCs.add(csColas8);
        return listCs;
        
        
    }
    
    @isTest static Account AccountInsert_Test(){
        
        Account cuenta = new Account();
        cuenta.PersonEmail = 'test@test.com';
        cuenta.R1_ACC_PKL_Card_Type__c = '2';
        cuenta.FirstName = 'test';
        cuenta.LastName = 'test';
        cuenta.PersonMailingPostalCode = '12333' ;
        cuenta.PersonMailingCity = 'madrid';
        cuenta.PersonMailingCountry = 'spain';
        cuenta.R1_ACC_TXT_Primary_Frecuent_Flyer_Card__c = 'IB26591834';
        return cuenta;
        
        
    }
    
    
    @isTest static void formContactIberia_Test(){
        
        TCK1_CON_ContactIberiaExtension c = new TCK1_CON_ContactIberiaExtension();
        
        Account cuenta = AccountInsert_Test();   
        insert cuenta;
        
        List<R3_CS_ColasBuzones__c> listCs = new List<R3_CS_ColasBuzones__c>();
        listCs = customSetting_Test();
        insert listCs;
        
        
		c.contentType = 'test';
        c.targetLevel = 'test';
        c.document = Blob.valueOf('Test Data');
        c.document2 = Blob.valueOf('Test Data');
        c.document3 = Blob.valueOf('Test Data'); 
        c.document4 = Blob.valueOf('Test Data');
        c.fileName = 'test';
        c.fileName2 =  'test';
        c.fileName3 =  'test';
        c.fileName4 =  'test';
        
        c.caso.R2_CAS_TXT_Primary_Frecuent_Flyer_Card__c = '26591834';
        c.save();
    }
    
    
    @isTest static void formContactIberia2_Test(){
        
        TCK1_CON_ContactIberiaExtension c = new TCK1_CON_ContactIberiaExtension();
        
        List<Account> listAcc = new List<Account>();
        Account cuenta = AccountInsert_Test();
        Account cuenta2 = AccountInsert_Test();    
        listAcc.add(cuenta);
        listAcc.add(cuenta2);
        insert listAcc;
        
        List<R3_CS_ColasBuzones__c> listCs = new List<R3_CS_ColasBuzones__c>();
        listCs = customSetting_Test();
        insert listCs;
        
        c.caso.R2_CAS_EMA_Email__c = 'test@test.com';
        c.save();
    }
    
    @isTest static void formContactIberia3_Test(){
        
        TCK1_CON_ContactIberiaExtension c = new TCK1_CON_ContactIberiaExtension();
        
        List<Account> listAcc = new List<Account>();
        Account cuenta = AccountInsert_Test(); 
        listAcc.add(cuenta);
        insert listAcc;
        
        List<R3_CS_ColasBuzones__c> listCs = new List<R3_CS_ColasBuzones__c>();
        listCs = customSetting_Test();
        insert listCs;
        
        c.caso.R2_CAS_EMA_Email__c = 'test@test.com';
        c.save();
        c.retrieveMailInfo();
    }
    
    @isTest static void formContactIberia4_Test(){
        
        TCK1_CON_ContactIberiaExtension c = new TCK1_CON_ContactIberiaExtension();
        
        List<Account> listAcc = new List<Account>();
        Account cuenta = AccountInsert_Test(); 
        listAcc.add(cuenta);
        insert listAcc;
        
        List<R3_CS_ColasBuzones__c> listCs = new List<R3_CS_ColasBuzones__c>();
        listCs = customSetting_Test();
        insert listCs;
        
        c.caso.R2_CAS_EMA_Email__c = 'test2@test.com';
        c.save();
        c.retrieveMailInfo();
    }
    
    @isTest static void formContactIberia5_Test(){
        
        TCK1_CON_ContactIberiaExtension c = new TCK1_CON_ContactIberiaExtension();
        
        List<Account> listAcc = new List<Account>();
        Account cuenta = AccountInsert_Test();
        Account cuenta2 = AccountInsert_Test();
        cuenta.R1_ACC_PKL_Card_Type__c = '2';
        cuenta2.R1_ACC_PKL_Card_Type__c = '5';
        listAcc.add(cuenta);
        listAcc.add(cuenta2);
        insert listAcc;
        
        List<R3_CS_ColasBuzones__c> listCs = new List<R3_CS_ColasBuzones__c>();
        listCs = customSetting_Test();
        insert listCs;
        
        c.caso.R2_CAS_EMA_Email__c = 'test@test.com';
        c.save();
    }
    
        @isTest static void formContactIberia6_Test(){
        
        TCK1_CON_ContactIberiaExtension c = new TCK1_CON_ContactIberiaExtension();
        
        List<Account> listAcc = new List<Account>();
        Account cuenta = AccountInsert_Test();
        Account cuenta2 = AccountInsert_Test();
        cuenta.R1_ACC_PKL_Card_Type__c = '1';
        cuenta2.R1_ACC_PKL_Card_Type__c = '4';
        listAcc.add(cuenta);
        listAcc.add(cuenta2);
        insert listAcc;
        
        List<R3_CS_ColasBuzones__c> listCs = new List<R3_CS_ColasBuzones__c>();
        listCs = customSetting_Test();
        insert listCs;
        
        c.caso.R2_CAS_EMA_Email__c = 'test@test.com';
        c.save();
        c.retrieveMailInfo();
    }
    
    @isTest static void formContactIberia7_Test(){
        
        TCK1_CON_ContactIberiaExtension c = new TCK1_CON_ContactIberiaExtension();
        
        Account cuenta = AccountInsert_Test();   
        insert cuenta;
        
        List<R3_CS_ColasBuzones__c> listCs = new List<R3_CS_ColasBuzones__c>();
        listCs = customSetting_Test();
        insert listCs;

        
        c.caso.R2_CAS_TXT_Primary_Frecuent_Flyer_Card__c = '0000000000';
        c.save();
    }
    
    @isTest static void formChangeES_Test(){
        TCK1_CON_ContactIberiaExtension c = new TCK1_CON_ContactIberiaExtension();
        c.changeSpanish();
        
    }
    
    @isTest static void formChangeEN_Test(){
        TCK1_CON_ContactIberiaExtension c = new TCK1_CON_ContactIberiaExtension();
        c.changeEnglish();
        
    }
    
   @isTest static void getTypes_Test(){
        TCK1_CON_ContactIberiaExtension c = new TCK1_CON_ContactIberiaExtension();
        c.getTypes(); 
    }
    
   @isTest static void getSubTypes_Test(){
        TCK1_CON_ContactIberiaExtension c = new TCK1_CON_ContactIberiaExtension();
        c.getSubTypes(); 
    }
    
    @isTest static void getAssign_Test(){
        List<R3_CS_ColasBuzones__c> listCs = new List<R3_CS_ColasBuzones__c>();
        listCs = customSetting_Test();
        insert listCs;
        
        TCK1_CON_ContactIberiaExtension c = new TCK1_CON_ContactIberiaExtension();
        String card = '1';
        c.getAssign(card,false,'');
        card = '2';
        c.getAssign(card,false,'');
        card = 'serviberia_clasica';
        c.getAssign(card,false,'');
        card = '4';
        c.getAssign(card,false,'');
        card = '5';
        c.getAssign(card,false,'');
        card = 'serviberia_plata';
        c.getAssign(card,false,'');
        card = 'Serviberia_Baby';
        c.getAssign(card,false,'');
        card = 'Serviberia_IbPlusMenores';
        c.getAssign(card,false,'');
        card = 'Serviberia_MenoresIberia';
        c.getAssign(card,false,'');
        card = 'Serviberia_HotelesAvios';
        c.getAssign(card,false,'');
        
    }
    
    
        @isTest static void getSubtypesDependent_test(){
		        TCK1_CON_ContactIberiaExtension c = new TCK1_CON_ContactIberiaExtension();
            Test.setCurrentPage(Page.R3_VFP_ContactIberia);
			ApexPages.currentPage().getParameters().put('valorSel','--');
            ApexPages.currentPage().getParameters().put('valFromVf','1');
            c.getSubtypesDependent() ;
            c.valueupdate();
        
   		 }
    
    
    
    
    
}