/*---------------------------------------------------------------------------------------------------------------------
    Author:         ICA-LCS
    Company:        LeadClic
    Description:   Aux class to handle Claims Automation II

    History:
     <Date>                     <Author>                         <Change Description>
    12/11/2019              		ICA                  				Initial Version
	12/12/2019						ICA									Modification. Adding restriction to avoid some exceptions.
 ----------------------------------------------------------------------------------------------------------------------*/
/**
* @author ICA
* @date 12/11/2019
* @description Aux class to handle Claims Automation II
* @param R3_Claims_Automation_Rules__c Claim Auto II Rule
*/
public without sharing class R3_CLS_AUX_ClaimsAutomation {
	public R3_Claims_Automation_Rules__c rule;

	/**
	* @author ICA
	* @date 12/11/2019 
	* @description R3_CLS_AUX_ClaimsAutomation builder.
	* @param R3_Claims_Automation_Rules__c Rule to be wrapped.
	*/	
	public R3_CLS_AUX_ClaimsAutomation(R3_Claims_Automation_Rules__c rule){
		this.rule = rule;
	}


	/**
    * @author ICA
    * @date 12/11/2019 
    * @description Evaluates if the case parameters meet the rule criteria.
    * @param Case Incoming case to evaluate.
	* @param R1_Flight__c f Related flight to evaluate.
	* @return Boolean Whether or not meet the rule criteria in the query parameters.
    */	
	public Boolean isCaseSuitable(Case c,R1_Flight__c f){
		Boolean stopAutomation = (f!=null)?f.R2_FLG_TXT_Stop_automation__c:c.R1_CAS_LOO_Flight__r.R2_FLG_TXT_Stop_automation__c;
		String realCompany = (f!=null)?f.R3_FLG_FOR_RealCompany__c:c.R1_CAS_LOO_Flight__r.R3_FLG_FOR_RealCompany__c;
		return (c.R2_CAS_CHK_AUT_Do_not_automate_flag__c!=true && stopAutomation != true
		&& (String.isBlank(rule.R3_CAR_PKL_Subtype__c) || (String.isNotBlank(c.R1_CAS_PKL_Subtype__c) && rule.R3_CAR_PKL_Subtype__c.equalsIgnoreCase(c.R1_CAS_PKL_Subtype__c)))
		&& String.isNotBlank(rule.R3_CAR_PKL_VIP_Type__c) && String.isNotBlank(c.R2_CAS_PKL_client_type__c) && rule.R3_CAR_PKL_VIP_Type__c.split(';').contains(c.R2_CAS_PKL_client_type__c)
		&& String.isNotBlank(rule.R3_CAR_TXT_Categories__c) && String.isNotBlank(c.R2_CAS_PKL_Vip_Type__c) && rule.R3_CAR_TXT_Categories__c.split(';').contains(c.R2_CAS_PKL_Vip_Type__c)
		&& ((String.isBlank(rule.R3_CAR_PKL_InitialManage__c)) || (String.isNotBlank(rule.R3_CAR_PKL_InitialManage__c) && String.isNotBlank(c.R2_CAS_PKL_Manage__c) && rule.R3_CAR_PKL_InitialManage__c.equalsIgnoreCase(c.R2_CAS_PKL_Manage__c)))
		&& String.isNotBlank(rule.R3_CAR_PKL_Status_Init__c) && String.isNotBlank(c.Status) && rule.R3_CAR_PKL_Status_Init__c.split(';').contains(c.Status)
		&& String.isNotBlank(rule.R3_CAR_PKL_Idioma__c) && String.isNotBlank(c.R1_CAS_PKL_Idioma__c) && rule.R3_CAR_PKL_Idioma__c.split(';').contains(c.R1_CAS_PKL_Idioma__c)
		//&& String.isNotBlank(rule.R3_CAR_PKL_File_type__c) && rule.R3_CAR_PKL_File_type__c.split(';').contains(c.R2_CAS_PKL_File_type__c)
		&& String.isNotBlank(rule.R3_CAR_TXT_carrier_code_oper__c) && rule.R3_CAR_TXT_carrier_code_oper__c.equalsIgnoreCase(realCompany)
		);
	}

	/**
    * @author ICA
    * @date 21/11/2019 
    * @description Checks if the case meet the UK, Rebound, CreatedDate and excluded flights conditions stored un the Rule.
    * @param Case Incoming case to evaluate.
	* @return Boolean Whether or not meet the rule specific criteria.
    */	
	public Boolean meetAllConditions(Case c){
		
		return (meetUKCriteria(c) && meetReboundCriteria(c) && meetCreatedDateCriteria(c) && meetFligthConditions(c));
	}

	/**
    * @author ICA
    * @date 21/11/2019 
    * @description Checks if the case meet the UK conditions specified in the rule.
    * @param Case Incoming case to evaluate.
	* @return Boolean Whether or not meet the UK criteria stored in the rule.
    */	
	public Boolean meetUKCriteria(Case c){
		return (String.isBlank(rule.R3_CAR_PKL_UK__c) || (rule.R3_CAR_PKL_UK__c.containsIgnoreCase('No')?!c.R2_CAS_FOR_UK__c:c.R2_CAS_FOR_UK__c));
	}

	/**
    * @author ICA
    * @date 21/11/2019 
    * @description Checks if the case meet the createdDate conditions specified in the rule.
    * @param Case Incoming case to evaluate.
	* @return Boolean Whether or not meet the Waiting days criteria specified in the rule.
    */	
	public Boolean meetCreatedDateCriteria(Case c){
		return (c.CreatedDate<=Datetime.now().addDays(Integer.valueOf(-1*rule.R3_CAR_NUM_Days__c)));
	}

	/**
    * @author ICA
    * @date 21/11/2019 
    * @description Checks if the case meet the Rebound conditions specified in the rule.
    * @param Case Incoming case to evaluate.
	* @return Boolean Whether or not meet the rebound criteria specified in the rule.
    */	
	public Boolean meetReboundCriteria(Case c){
		return (c.R2_CAS_NUM_Count_Number_Reopen__c!=null && c.R2_CAS_NUM_Count_Number_Reopen__c<=rule.R3_CAR_NUM_Number_of_rebounds__c);
	}
	
	/**
    * @author ICA
    * @date 21/11/2019 
    * @description  Checks if the flight related to the case is excluded due the excluded flight numbers specified in the rule.
	* @param R3_Claims_Automation_Rules__c Rule to check.
	* @return Boolean Whether or not meet the flight criteria defined in the rule.
    */	
	public Boolean meetFligthConditions(Case c){
		Integer flightNumber = Integer.valueOf(c.R1_CAS_LOO_Flight__r.R1_FLG_TXT_Flight_number__c);
		return meetFligthConditions(c,flightNumber);
	}

	/**
    * @author ICA
    * @date 21/11/2019 
    * @description 
    * @param Case Incoming case to evaluate.
	* @param Integer flightNumber Flight number related to the case.
	* @return Boolean Whether or not meet the flight criteria defined in the rule.
    */	
	public Boolean meetFligthConditions(Case c,Integer flightNumber){
		Boolean meetConditions = true;
		if(flightNumber!=null && String.isNotBlank(rule.R3_CAR_TXT_Excluded_Flights__c)){
			
			for (String elem: rule.R3_CAR_TXT_Excluded_Flights__c.split(';')){
				if(elem.split('-').size()>1){
					if(Integer.valueOf(elem.split('-')[0])<=flightNumber && Integer.valueOf(elem.split('-')[1])>=flightNumber){
						meetConditions=false;
					}
				}else{
					if(Integer.valueOf(elem)==flightNumber){
						meetConditions=false;
					}
				}
			}
		}
		return meetConditions;
	}

	/**
    * @author ICA
    * @date 21/11/2019 
    * @description Queries the list of active rules. If the rule doesn't have a valid emailtemplate, insert an error.
	* @return List<R3_CLS_AUX_ClaimsAutomation> List of active rules.
    */
	public static List<R3_CLS_AUX_ClaimsAutomation> getAllRules(){
		List<R3_LogErrors__c> errorLogList = new List<R3_LogErrors__c>();
		List<R3_Claims_Automation_Rules__c> rulesList = new List<R3_Claims_Automation_Rules__c>([SELECT Id,Name,R3_CAR_PKL_Type__c, R3_CAR_PKL_Subtype__c,
                                                R3_CAR_NUM_Days__c,R3_CAR_PKL_Idioma__c, R3_CAR_PKL_InitialManage__c,R3_CAS_PKL_FinalManage__c,
                                                R3_CAR_TXT_Email_Templates_Folder__c, R3_CAR_TXT_Categories__c,
                                                R3_CAR_PKL_Email_Address__c, R3_CAR_NUM_Number_of_rebounds__c,
                                                R3_CAR_TXT_Excluded_Flights__c, R3_CAR_TXT_carrier_code_oper__c,
                                                R3_CAR_PKL_Status_Init__c,R3_CAR_PKL_VIP_Type__c,R3_CAR_PKL_File_type__c,
                                                R3_CAR_PKL_Final_Status__c,OwnerId,R3_CAR_PKL_UK__c,R3_CAR_TXT_TemplateName__c
                                            FROM R3_Claims_Automation_Rules__c WHERE R3_CAR_CHK_Active__c=true ORDER BY R3_CAR_NUM_Priority__c ASC]);
		Set<R3_Claims_Automation_Rules__c> errorRulesSet = new Set<R3_Claims_Automation_Rules__c>();
		List<R3_CLS_AUX_ClaimsAutomation> rulesToReturn = new List<R3_CLS_AUX_ClaimsAutomation>();
		
		errorRulesSet.addAll(R3_CLS_ClaimsAutoRulesHandler.validateEmailTemplates(rulesList));
		for(R3_Claims_Automation_Rules__c r : rulesList){
			if(!errorRulesSet.contains(r)){
				rulesToReturn.add(new R3_CLS_AUX_ClaimsAutomation(r));
			}else{
				errorLogList.add(
						new R3_LogErrors__c(R3_LOG_TXT_RecordId__c=r.Id,
							R3_LOG_TXT_JobName__c='Claims Auto II',
							R3_LOG_TXT_ObjectName__c='R3_Claims_Automation_Rules__c',
							R3_LOG_TXT_ErrorDescription__c=Label.ErrorInvalidTemplateForFolder + r.Name
						)
					);
			}
		}
		insert errorLogList;
		return rulesToReturn;
	}

	/**
    * @author ICA
    * @date 26/11/2019 
    * @description Maps the orgWideAddresses with the emails specified in the rule.
	* @param Map<Id,R3_Claims_Automation_Rules__c> idCaseRuleMap Map with the 
	* @return Map<String,Id> mapEmailWithOWAId Map with the email address and the organization wide address related.
    */
	public static Map<String,OrgwideEmailAddress> getOwaMap(Map<Id,R3_Claims_Automation_Rules__c> idCaseRuleMap){
		Map<String,OrgwideEmailAddress> mapEmailWithOWAId = new Map<String,OrgwideEmailAddress>();
		for(R3_Claims_Automation_Rules__c r : idCaseRuleMap.values()){
			mapEmailWithOWAId.put(r.R3_CAR_PKL_Email_Address__c, null);
		}
		for(OrgwideEmailAddress owa:[SELECT Id, Address,DisplayName FROM OrgwideEmailAddress WHERE Address IN:mapEmailWithOWAId.keySet()]){
			mapEmailWithOWAId.put(owa.Address,owa);
		}
		return mapEmailWithOWAId;
	}

	/**
    * @author ICA
    * @date 26/11/2019 
    * @description Send emails for the batch execution. The recipient will be the specified in the correspondant rule. It must be an organization wide address.
	* @param List<Case> caseList List of cases to send the email.
	* @param Map<Id,R3_Claims_Automation_Rules__c> idCaseRuleMap Map with the 
	* @param Map<String,id> mapRuleIdWithOWA 
	* @return List<R3_LogErrors__c> idCaseRuleMap Map with the 
    */
	public static List<R3_LogErrors__c> sendEmails(List<Case> caseList,Map<Id,R3_Claims_Automation_Rules__c> idCaseRuleMap,Map<String,OrgwideEmailAddress> mapRuleIdWithOWA){
		Map<Id, Messaging.SingleEmailMessage> mapCasEmail = new Map<Id, Messaging.SingleEmailMessage>();
		Set<String> setEmailTemplateName= new Set<String>();
		Map<String,EmailTemplate> mapEmailTemplateMapping = new Map<String,EmailTemplate>();
		List<R3_LogErrors__c> errorLogList = new List<R3_LogErrors__c>();
		Map<Id,List<Id>> mapContactIdCaseList = new Map<Id,List<Id>>();
		Set<Id> succesfulEmailId = new Set<Id>();

		for(Case c: caseList){
			if(idCaseRuleMap.containsKey(c.Id)){
				setEmailTemplateName.add(idCaseRuleMap.get(c.Id).R3_CAR_TXT_TemplateName__c);
			}
		}
		for(EmailTemplate em:[Select Id,Name, FolderName from EmailTemplate where Name In: setEmailTemplateName]){
			mapEmailTemplateMapping.put(em.Name,em);
		}
		for(Case c: caseList){
			if(idCaseRuleMap.containsKey(c.Id)){
				if(mapEmailTemplateMapping.containsKey(idCaseRuleMap.get(c.Id).R3_CAR_TXT_TemplateName__c) 
				&& mapEmailTemplateMapping.get(idCaseRuleMap.get(c.Id).R3_CAR_TXT_TemplateName__c).FolderName.equals(idCaseRuleMap.get(c.Id).R3_CAR_TXT_Email_Templates_Folder__c)){
					Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
					email.setOrgWideEmailAddressId(mapRuleIdWithOWA.get(idCaseRuleMap.get(c.Id).R3_CAR_PKL_Email_Address__c).Id);
                    email.setTemplateId(mapEmailTemplateMapping.get(idCaseRuleMap.get(c.Id).R3_CAR_TXT_TemplateName__c).Id);
                    email.setTargetObjectId(c.Account.PersonContactId);
					email.setWhatId(c.Id);
                    email.setTreatTargetObjectAsRecipient(false);
                    email.setSaveAsActivity(false);
                    email.setToAddresses(new String[]{c.Account.PersonEmail});
                    mapCasEmail.put(c.Id, email);
					//Add contact and case to mapping
					succesfulEmailId.add(c.Account.PersonContactId);
					if(mapContactIdCaseList.containsKey(c.Account.PersonContactId)){
						mapContactIdCaseList.get(c.Account.PersonContactId).add(c.Id);
					}else{
						mapContactIdCaseList.put(c.Account.PersonContactId,new List<Id>{c.Id});
					}
				}else{
					errorLogList.add(
						new R3_LogErrors__c(R3_LOG_TXT_RecordId__c=c.Id,
							R3_LOG_TXT_JobName__c='Claims Auto II',
							R3_LOG_TXT_ObjectName__c='Messaging.SingleEmailMessage',
							R3_LOG_TXT_ErrorDescription__c='Email template or folder not found for rule: '+idCaseRuleMap.get(c.Id) + '. Email for case with Id: ' + c.Id + ' has not been sended.'
						)
					);
				}
			}
		}

		if(!mapCasEmail.isEmpty()){
			//SENDS UP TO 10 EMAILS.
			
			Messaging.SendEmailResult[] results = Messaging.sendEmail(mapCasEmail.values(), false);
			for(Messaging.SendEmailResult ser:results){
				if(!ser.isSuccess()){
					for(Messaging.SendEmailError sre:ser.getErrors()){
						errorLogList.add(
						new R3_LogErrors__c(R3_LOG_TXT_RecordId__c=sre.getTargetObjectId(),
							R3_LOG_TXT_JobName__c='Claims Auto II',
							R3_LOG_TXT_ObjectName__c='Messaging.SendEmailResult',
							R3_LOG_TXT_ErrorDescription__c='Error Sending email for this automation: ' + sre.getMessage() + '. For contact: ' + sre.getTargetObjectId() + '. Please review cases with Ids: ' + mapContactIdCaseList.get(sre.getTargetObjectId())
						)
					);
					succesfulEmailId.remove(sre.getTargetObjectId());
					}
				}
			}
			List<EmailMessage> listEmailMsg = new List<EmailMessage>();
			for(Case c : caseList){
				if(succesfulEmailId.contains(c.Account.PersonContactId)){
					EmailMessage e = new EmailMessage();
					e.Incoming = false;
					e.Status = '3';
					e.MessageDate = Datetime.now();
					e.ToAddress = String.join(mapCasEmail.get(c.Id).getToAddresses() ,',');
					e.Subject = mapCasEmail.get(c.Id).getSubject();
					e.FromAddress = mapRuleIdWithOWA.get(idCaseRuleMap.get(c.Id).R3_CAR_PKL_Email_Address__c).Address;
					e.FromName = mapRuleIdWithOWA.get(idCaseRuleMap.get(c.Id).R3_CAR_PKL_Email_Address__c).Displayname;
					e.HtmlBody = mapCasEmail.get(c.Id).getHtmlBody();
					e.TextBody = mapCasEmail.get(c.Id).getPlainTextBody();
					e.ParentId = c.Id;
					listEmailMsg.add(e);
				}
			}
			insert listEmailMsg;

		}
		return errorLogList;
	}

	/**
    * @author LCS
    * @date 22/11/2019 
    * @description Mark the cases to be handled by claims automation II.
    * @param List<Case> triggerNew List incoming values into the trigger.
    */	
	public static void claimsAutoAssignment(List<Case> triggerNew){
		List<R3_CLS_AUX_ClaimsAutomation> rulesList = new List<R3_CLS_AUX_ClaimsAutomation>();
		Map<Id,R1_Flight__c> flightMap = new Map<Id,R1_Flight__c>();
		Set<Id> flightSet = new Set<Id>();
		
		Id rtExp = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Expediente').getRecordTypeId();
		Boolean goOn = false;
		for(Case c : triggerNew){
			if(c.RecordtypeId == rtExp){
				goOn=true;
				flightSet.add(c.R1_CAS_LOO_Flight__c);
			}
		}
		if(goOn){
			//Retrieving all rulles with SOQL
			rulesList.addAll(R3_CLS_AUX_ClaimsAutomation.getAllRules());
			if(!rulesList.isEmpty()){

				//Filling the filters
				for(R1_Flight__c f: [Select Id,R1_FLG_TXT_Flight_number__c,R3_FLG_FOR_RealCompany__c,R2_FLG_TXT_Stop_automation__c From R1_Flight__c Where Id IN: flightSet]){
					flightMap.put(f.Id,f);
				}
				for(Case c : triggerNew){
					if(flightMap.containsKey(c.R1_CAS_LOO_Flight__c)){
						for(R3_CLS_AUX_ClaimsAutomation r : rulesList){
							if(r.isCaseSuitable(c,flightMap.get(c.R1_CAS_LOO_Flight__c)) && r.meetUKCriteria(c) && r.meetFligthConditions(c,Integer.valueOf(flightMap.get(c.R1_CAS_LOO_Flight__c).R1_FLG_TXT_Flight_number__c))){
								c.R2_CAS_CHK_Automation__c=true;
							}
						}
					}
				}
			}
		}
	}
}