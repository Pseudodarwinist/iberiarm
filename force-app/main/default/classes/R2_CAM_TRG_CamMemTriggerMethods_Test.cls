@isTest
public with sharing class R2_CAM_TRG_CamMemTriggerMethods_Test {
	
	@isTest
	static void insertarCasoPorMiembroCampnha_Test(){
		//insertar miembro de campaña
		//el created byId va a ser el usuario de test
		CampaignMember miembroCampanha = new CampaignMember();
		List<CampaignMember> miembros = new List<CampaignMember>();
		
		
        Contact contacto=new Contact(LastName='Contacto_Test');
        insert contacto;
        
	 	Campaign Campana = new Campaign();
        Campana.name = 'campanhaTest';
        
        insert Campana;
	 	
	 	miembroCampanha.campaignid = Campana.id;
	 	miembroCampanha.ContactId = contacto.id;
	 	
	 	miembros.add(miembroCampanha);
	 	
	 	//select para extraer el id del miembro de campaña
		
		//consultar caso
    	//ver que los campos coinciden
    	
    	Test.startTest();
    	insert miembros;
        //R2_CAM_TRG_CamMemTriggerMethods.insertarCasoPorMiembroCampnha(miembros);
        
        List<Case> casos =  [select id, R2_CAS_LOO_Campaigns__c,ContactId FROM case where R2_CAS_LOO_Campaigns__c =: Campana.id];
        System.assertEquals(miembroCampanha.campaignid, casos[0].R2_CAS_LOO_Campaigns__c);
		System.assertEquals(miembroCampanha.ContactId, casos[0].ContactId);
		Test.stopTest();
		
	}
	/*---------------------------------------------------------------------------------------------------------------------
  Author:         Victor Garcia Barquilla
  Company:        Accenture
  Description:    Apex test de la clase apex "R2_CAM_TRG_CamMemTriggerMEthods"
  IN:             
  OUT:            

  History: 
   <Date>                     <Author>                         <Change Description>
  01/10/2018            Victor Garcia Barquilla                  Initial Version
  ----------------------------------------------------------------------------------------------------------------------*/ 
	@isTest
	static void insertarCasoPorMiembroCampnha_Test2(){
		System.debug('aaaaab');
		CampaignMember miembroCampanha = new CampaignMember();
		List<CampaignMember> miembros = new List<CampaignMember>();
		
        Contact contacto=new Contact(LastName='Contacto_Test');
        insert contacto;
        
	 	Campaign Campana = new Campaign();
        Campana.name = 'campanhaTest';
        insert Campana;
	 	
	 	miembroCampanha.campaignid = Campana.id;
	 	miembroCampanha.ContactId = contacto.id;
	 	miembroCampanha.R2_CMM_TXT_upg_surname__c = 'test';
	 	miembroCampanha.R2_CMM_PK_answer__c = 'Enviado';
	 	miembroCampanha.R2_CMM_PK_Avios_Cash__c = 'Cash';
	 	miembroCampanha.R2_CMM_NUM_ticket__c = 12.0;
	 	miembroCampanha.R2_CMM_TXT_class__c = 'test';
	 	miembroCampanha.R2_CMM_TXT_CLTV__c = 'test';
	 	miembroCampanha.R2_CMM_TXT_company__c = 'test';
	 	miembroCampanha.R2_CMM_TXT_personal_contact__c = 'test';
	 	miembroCampanha.R2_CMM_TXT_dest__c = 'tes';
	 	miembroCampanha.R2_CMM_EMA_Email__c = 'test@hotmail.com';
	 	miembroCampanha.R2_CMM_DAT_flight_date2__c = System.today();
	 	miembroCampanha.R2_CMM_DAT_flight_date__c = System.today();
	 	miembroCampanha.R2_CMM_DAT_Answer_date__c = System.today();
	 	miembroCampanha.R2_CMM_TXT_flag_seat_type__c = 'test';
	 	miembroCampanha.R2_CMM_TXT_upg_flag__c = 'test';
	 	miembroCampanha.R2_CMM_TXT_upg_flag_seg2__c = 'test';
	 	miembroCampanha.R2_CMM_PKL_time_zone__c = 'Mañana';
	 	miembroCampanha.R2_CMM_TXT_Contact_schedule__c = 'test';
	 	miembroCampanha.R2_CMM_NUM_frustation__c = 9;
	 	miembroCampanha.R2_CMM_gender__c = 'test';
	 	miembroCampanha.R2_CMM_TXT_flight_hour__c = 'test';
	 	miembroCampanha.R2_CMM_TXT_ib_order__c = 'test';
	 	miembroCampanha.R1_CMM_PKL_Idioma__c = 'Español';
	 	miembroCampanha.R2_CMM_TXT_key__c = 'test';
	 	miembroCampanha.R2_CMM_TXT_Amadeus_Locator__c = 'test';
	 	miembroCampanha.R2_CMM_TXT_Resiber_Locator__c = 'test';
	 	miembroCampanha.R2_CMM_PH_movile_phone__c = 'test';
	 	miembroCampanha.R1_CMM_TXT_Card_Type__c = 'test';
	 	miembroCampanha.R2_CMM_TXT_upg_name__c = 'test';
	 	miembroCampanha.R2_CMM_NUM_number_upgs_euros__c = 10;
	 	miembroCampanha.R2_CMM_TXT_segment_number__c = 'test';
	 	miembroCampanha.R2_CMM_TXT_Flight_number__c = 'test';
	 	miembroCampanha.R2_CMM_TXT_iberiacom_number__c = 'test';
	 	miembroCampanha.R2_CMM_TXT_flight_number_segment2__c = 'test';
	 	miembroCampanha.R2_CMM_NUM_order1__c = 10;
	 	miembroCampanha.R2_CMM_NUM_order2__c = 10;
	 	miembroCampanha.R2_CMM_TXT_origin__c = 'tes';
	 	miembroCampanha.R2_CMM_DAT_time_flight_segment1__c = System.today();
	 	miembroCampanha.R2_CMM_CUR_Highest_Price_Cash_Seg1__c = 5;
	 	miembroCampanha.R2_CMM_CUR_Highest_Price_Avios_Seg1__c = 5;
	 	miembroCampanha.R2_CMM_DAT_date_flight_segment1__c = System.today();
	 	miembroCampanha.R2_CMM_TXT_destination_segment1__c = 'tes';
	 	miembroCampanha.R2_CMM_TXT_company_segment1__c = 'test';
	 	miembroCampanha.R2_CMM_TXT_class_segment1__c = 'te';
	 	miembroCampanha.R2_CMM_TXT_flight_number_segment1__c = 'test';
	 	miembroCampanha.R2_CMM_NUM_number_upgs_TP_S1__c = 5;
	 	miembroCampanha.R2_CMM_NUM_number_upgs_TP_euros_S1__c = 5;
	 	miembroCampanha.R2_CMM_NUM_number_upgs_bus_euros_S1__c = 5;
	 	miembroCampanha.R2_CMM_TXT_segment1_origin__c = 'tes';
	 	miembroCampanha.R2_CMM_NUM_avios_bus_seg1__c = 5;
	 	miembroCampanha.R2_CMM_NUM_avios_TP_seg1__c = 5;
	 	miembroCampanha.R2_CMM_NUM_cash_bus_seg1__c = 5;
	 	miembroCampanha.R2_CMM_NUM_cash_TP_seg1__c = 5;
	 	miembroCampanha.R2_CMM_NUM_bus_limit_seg1__c = 5;
	 	miembroCampanha.R2_CMM_NUM_TP_limit_seg1__c = 5;
	 	miembroCampanha.R2_CMM_DAT_time_flight_segment2__c = System.today();
	 	miembroCampanha.R2_CMM_CUR_Highest_Price_Cash_Seg2__c = 5;
	 	miembroCampanha.R2_CMM_CUR_Highest_Price_Avios_Seg2__c = 5;
	 	miembroCampanha.R2_CMM_DAT_date_flight_segment2__c = System.today();
	 	miembroCampanha.R2_CMM_TXT_destination_segment2__c = 'tes';
	 	miembroCampanha.R2_CMM_TXT_company_segment2__c = 'tes';
	 	miembroCampanha.R2_CMM_TXT_class_segment2__c = 'te';
	 	miembroCampanha.R2_CMM_TXT_segment2_origin__c = 'tes';
	 	miembroCampanha.R2_CMM_PK_client_country__c = 'España';
	 	miembroCampanha.R2_CMM_CUR_avios_TP_seg2__c = 5;
	 	miembroCampanha.R2_CMM_NUM_cash_bus_seg2__c = 5;
	 	miembroCampanha.R2_CMM_NUM_number_upgs_TP_euros_S2__c = 5;
	 	miembroCampanha.R2_CMM_NUM_number_upgs_TP_S2__c = 5;
	 	miembroCampanha.R2_CMM_NUM_bus_limit_seg2__c = 5;
	 	miembroCampanha.R2_CMM_NUM_TP_limit_seg2__c = 5;
	 	miembroCampanha.R2_CMM_NUM_number_upgs_bus_euros_S2__c = 5;
	 	miembroCampanha.R2_CMM_DAT_flight_date_seg2__c = System.today();
	 	miembroCampanha.R2_CMM_NUM_number_upgs__c = 5;
	 	miembroCampanha.R1_CMM_PKL_type__c = 'UPG';
	 	miembroCampanha.R2_CMM_TEL_phone2__c = 'test';
	 	miembroCampanha.R2_CMM_TEL_contact_phone__c = 'test';
	 	//miembroCampanha.Description = 'test';
	 	miembroCampanha.R2_CMM_PK_answer_Seg2__c = 'Enviado';
	 	miembroCampanha.R2_CMM_PK_result__c = 'Iniciado';
	 	miembroCampanha.R2_CMM_PK_result_Seg2__c = 'Iniciado';
	 	miembros.add(miembroCampanha);
	 	
    	Test.startTest();

    	insert miembros;
        
        System.debug('hhhhhhhhhhhhhhhhhh1' + miembros[0].campaignid);
        System.debug('hhhhhhhhhhhhhhhhhh2' + Campana.id);
        List<Case> casos =  [select id, R2_CAS_LOO_Campaigns__c,ContactId FROM Case where R2_CAS_LOO_Campaigns__c =: Campana.id];
        System.debug('hhhhhhhhhhhhhhhhhh'+casos);
        System.assertEquals(miembroCampanha.campaignid, casos[0].R2_CAS_LOO_Campaigns__c);
		System.assertEquals(miembroCampanha.ContactId, casos[0].ContactId);
		Test.stopTest();
	}
}