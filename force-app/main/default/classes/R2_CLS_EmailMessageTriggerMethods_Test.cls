/*---------------------------------------------------------------------------------------------------------------------
Author:         Jaime Ascanta
Company:        Accenture
Description:
IN:
OUT:

History:
<Date>                     <Author>                         <Change Description>
12/03/2018 					jaime ascanta                  	Initial Version
----------------------------------------------------------------------------------------------------------------------*/
@isTest
private class R2_CLS_EmailMessageTriggerMethods_Test {

	/*---------------------------------------------------------------------------------------------------------------------
	Author:         Jaime Ascanta
	Company:        Accenture
	Description:
	IN:
	OUT:

	History:
	 <Date>                     <Author>                         <Change Description>
	12/03/2018       	   		Jaime Ascanta                  		 Initial Version
	24/05/2018       	   		Alberto Puerto                  	 fulfilling seg.R2_SEG_TXT_Company__c (required field)
	10/10/2018					Eva Sanchez ruiz					Actualizado valores de R2_ExchangeRate__c a dimonca.
	----------------------------------------------------------------------------------------------------------------------*/
	@isTest static void relatedEmailSegment_test() {
		R1_CLS_LogHelper.throw_exception = false;

		//R2_SDRtoEUR__c
		R2_ExchangeRate__c sdr = new R2_ExchangeRate__c();
		sdr.Name = 'XDR_960';
		sdr.R2_NUM_Rate_to_EUR__c = 1.1788;
		sdr.R2_TXT_External_Id__c ='1234';
		insert sdr;

		//RT del caso expendiente
        Id rt_expediente = [SELECT Id FROM recordType WHERE sObjectType = 'Case' AND Name = 'Expediente' LIMIT 1].Id;
        Case caso = new Case();
        caso.RecordTypeId = rt_expediente;
        caso.Status = 'Abierto';
        caso.Origin = 'Email';
        caso.Type = 'Retraso';
		insert caso;

		R2_Prorrateo__c pro = new R2_Prorrateo__c();
		pro.R2_PRO_LOO_Case__c = caso.Id;
		insert pro;

		R2_Segmento__c seg = new R2_Segmento__c();
		seg.R2_SEG_MSDT_Prorrate__c = pro.Id;
		seg.R2_SEG_TXT_company_code__c = 'IB';
		seg.R2_SEG_NUM_SDRtoEUR__c = 10;
		seg.R2_SEG_TXT_Company__c = 'IBERIA';
		insert seg;

		Test.startTest();

			R2_Segmento__c segData = [SELECT Id, R2_SEG_FOR_Thread_Id__c FROM R2_Segmento__c WHERE Id=:seg.Id LIMIT 1];

			EmailMessage email = new EmailMessage();
			email.Incoming = false;
			email.Status = '3';
			email.MessageDate = Date.today();
			email.ToAddress = 'test@test.com';
			email.Subject = segData.R2_SEG_FOR_Thread_Id__c;
			email.CcAddress = 'test@test.com';
			email.FromAddress = 'testfrom@test.com';
			email.FromName = 'Test';
			email.HtmlBody = 'Test';
			email.TextBody = 'Test';
			insert email;

			EmailMessage email2 = new EmailMessage();
			email2.Incoming = false;
			email2.Status = '3';
			email2.MessageDate = Date.today();
			email2.ToAddress = 'test@test.com';
			email2.Subject = segData.R2_SEG_FOR_Thread_Id__c;
			email2.CcAddress = 'test@test.com';
			email2.FromAddress = 'testfrom@test.com';
			email2.FromName = 'Test';
			email2.HtmlBody = 'Test';
			email2.TextBody = 'Test';
			email2.R2_EMA_LOO_Segmento__c = segData.Id;
			insert email2;

			List<EmailMessage> listEmails = [SELECT Id FROM EmailMessage WHERE R2_EMA_LOO_Segmento__c=:segData.Id AND ParentId=:caso.Id];
			System.assertEquals(2, listEmails.size());


		Test.stopTest();

		System.assertEquals(0, [SELECT count() FROM R1_Log__c]);
	}

	@isTest static void exceptions_test() {
		R1_CLS_LogHelper.throw_exception = true;

		Test.startTest();
			R2_CLS_EmailMessageTriggerMethods.relatedEmailSegment(null);
			R2_CLS_EmailMessageTriggerMethods.getCaseIdOfProrrate(null);
			R2_CLS_EmailMessageTriggerMethods.getRefSegId(null);
		Test.stopTest();
		System.assertEquals(0, [SELECT count() FROM R1_Log__c]);
	}

	/*-----------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Raúl Julián González
    Company:        Accenture
    Description:

    IN:     
    OUT:    

    History:
    <Date>                      <Author>                         <Change Description>
    24/09/2018               Raúl Julián González                 Initial Version
    -----------------------------------------------------------------------------------------*/
    @isTest static void updatecasoPadreEmail_Test (){
    	Id rtId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Expediente').getRecordTypeId();
    	Case cas = new Case();
	        cas.RecordTypeId = rtId;
	        cas.Status = 'Cerrado';
	        cas.Origin = 'Web';
	        cas.R1_CAS_PKL_Idioma__c = 'es';
	        cas.Type = 'Demora';
        insert cas;
        EmailMessage email = new EmailMessage();
			email.Incoming = true;
			email.Status = '3';
			email.MessageDate = Date.today();
			email.ToAddress = 'test@test.com';
			email.Subject = 'Test';
			email.CcAddress = 'test@test.com';
			email.FromAddress = 'testfrom@test.com';
			email.FromName = 'Test';
			email.HtmlBody = 'Test';
			email.TextBody = 'Test';
			email.Parentid = cas.id;

		Test.startTest();
			insert email;
		Test.stopTest();

    }

    /*-----------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Raúl Julián González
    Company:        Accenture
    Description:

    IN:     
    OUT:    

    History:
    <Date>                      <Author>                         <Change Description>
    24/09/2018               Raúl Julián González                 Initial Version
    -----------------------------------------------------------------------------------------*/
    @isTest static void updatecasoPadreEmail_Test2 (){
    	Id rtId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Expediente').getRecordTypeId();
    	Case cas = new Case();
	        cas.RecordTypeId = rtId;
	        cas.Status = 'Cerrado';
	        cas.Origin = 'Web';
	        cas.R1_CAS_PKL_Idioma__c = 'es';
	        cas.Type = 'Retraso';
        insert cas;
        EmailMessage email = new EmailMessage();
			email.Incoming = true;
			email.Status = '3';
			email.MessageDate = Date.today();
			email.ToAddress = 'test@test.com';
			email.Subject = 'Test';
			email.CcAddress = 'test@test.com';
			email.FromAddress = 'testfrom@test.com';
			email.FromName = 'Test';
			email.HtmlBody = 'Test';
			email.TextBody = 'Test';
			email.Parentid = cas.id;

		Test.startTest();
			insert email;
		Test.stopTest();

    }

    /*-----------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Raúl Julián González
    Company:        Accenture
    Description:

    IN:     
    OUT:    

    History:
    <Date>                      <Author>                         <Change Description>
    24/09/2018               Raúl Julián González                 Initial Version
    -----------------------------------------------------------------------------------------*/
    @isTest static void updatecasoPadreEmail_Test3 (){
    	Id rtId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Comunicaciones entrantes').getRecordTypeId();
    	Case cas = new Case();
	        cas.RecordTypeId = rtId;
	        cas.Status = 'Cerrado';
	        cas.Origin = 'Redes sociales';
	        cas.R1_CAS_PKL_Idioma__c = 'es';
	        cas.Type = 'Retraso';
        insert cas;
        EmailMessage email = new EmailMessage();
			email.Incoming = true;
			email.Status = '3';
			email.MessageDate = Date.today();
			email.ToAddress = 'test@test.com';
			email.Subject = 'Test';
			email.CcAddress = 'test@test.com';
			email.FromAddress = 'testfrom@test.com';
			email.FromName = 'Test';
			email.HtmlBody = 'Test';
			email.TextBody = 'Test';
			email.Parentid = cas.id;

		Test.startTest();
			insert email;
		Test.stopTest();

    }
}