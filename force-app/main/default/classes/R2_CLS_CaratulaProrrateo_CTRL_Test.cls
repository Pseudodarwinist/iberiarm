/*---------------------------------------------------------------------------------------------------------------------
Author:         Jaime Ascanta
Company:        Accenture
Description:
IN:
OUT:

History:
<Date>                     <Author>                         <Change Description>
14/03/2018 					jaime ascanta                  	Initial Version
----------------------------------------------------------------------------------------------------------------------*/
@isTest
private class R2_CLS_CaratulaProrrateo_CTRL_Test {

	/*---------------------------------------------------------------------------------------------------------------------
	Author:         Jaime Ascanta
	Company:        Accenture
	Description:
	IN:
	OUT:

	History:
	 <Date>                     <Author>                         <Change Description>
	14/03/2018       	   		Jaime Ascanta                  		 Initial Version
	10/10/2018				Victor Garcia Barquilla				Actualizado valores de R2_ExchangeRate__c a dimonca.
	----------------------------------------------------------------------------------------------------------------------*/
	@isTest static void caratulaProrrateo_Test() {

		R1_CLS_LogHelper.throw_exception = false;

		//R2_SDRtoEUR__c
		R2_ExchangeRate__c exchange = new R2_ExchangeRate__c();
		exchange.Name = 'XDR_960';
		exchange.R2_NUM_Rate_to_EUR__c = 1.1788;
		exchange.R2_TXT_External_Id__c = 'test';
		insert exchange;

		R2_Diccionario_Cias__c cia1 = new R2_Diccionario_Cias__c();
		cia1.R2_DIC_TXT_Codigo_Compania__c = 'test';
		cia1.R2_DIC_TXT_Nombre_Compania__c = 'Test Cia';
		cia1.R2_DIC_TXT_Correo__c = 'test@email.com';
		insert cia1;

		// cuenta
		Account acc = new Account();
		acc.Name = 'Test';
		acc.R1_ACC_TLF_Phone_Marketing__c = '348787877';
		acc.R1_ACC_PKL_identification_Type__c = '2';
		acc.R1_ACC_TXT_Identification_number__c = '123456789';
		insert acc;

		//caso
		Id rt_expediente = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Expediente').getRecordTypeId();
		Case caso = new Case();
		caso.AccountId = acc.Id;
		caso.RecordTypeId = rt_expediente;
		caso.Status = 'Abierto';
		caso.Origin = 'Email';
		caso.Type = 'Demora';
		insert caso;

		// incidencia
		Id rtDelay = [select id from RecordType where SobjectType='R1_Incident__c' AND DeveloperName='DELAYED'].Id;
		R1_Incident__c inc = new R1_Incident__c ();
		inc.RecordTypeId = rtDelay;
		inc.R2_INC_TXT_PIR_Station_Code__c = 'MAD';
		inc.R2_INC_TXT_PIR_Airline_Code__c = 'IB';
		inc.R2_INC_TXT_PIR_Reference_Number__c = '1111';
		insert inc;

		// prorrateos
		R2_Prorrateo__c pro1 = new R2_Prorrateo__c();
		pro1.R2_PRO_DAT_Date__c = Date.today();
		pro1.R2_PRO_LOO_Case__c = caso.Id;
		pro1.R2_PRO_LOO_Passenger_Name__c = acc.Id;
		pro1.R2_PRO_LOO_PIR__c = inc.Id;
		pro1.R2_PRO_PKL_Status__c = 'Prorrateo OK';
		pro1.R2_PRO_TXT_Our_Ref__c = '123456789';
		pro1.R2_PRO_TXT_File_Reference__c = '1234567890';
		insert pro1;

		R2_Segmento__c seg1 = new R2_Segmento__c();
		seg1.R2_SEG_TXT_company_code__c = 'test1';
		seg1.R2_SEG_MSDT_Prorrate__c = pro1.Id;
		seg1.R2_SEG_CHK_guilty_company__c = true;
		seg1.R2_SEG_DAT_Date__c = Date.today();
		seg1.R2_SEG_TXT_Company__c = 'IB';
		insert seg1;

		Test.startTest();

			PageReference pageRef = Page.R2_CaratulaProrrateo;
			Test.setCurrentPage(pageRef);
			ApexPages.currentPage().getParameters().put('id', (String) pro1.Id);
			ApexPages.currentPage().getParameters().put('cia', (String) cia1.Id);
			ApexPages.currentPage().getParameters().put('seg', (String) seg1.Id);
			

			R2_CLS_CaratulaProrrateo_CTRL controller = new R2_CLS_CaratulaProrrateo_CTRL();

			R2_CLS_CaratulaProrrateo_CTRL.ProrrateoWP proData = controller.getProrrateoData();
			System.assertNotEquals(null, proData);
			System.assertEquals('Test Cia', proData.to);
			System.assertEquals(Date.today().format(), proData.date_now);
			System.assertEquals('123456789', proData.our_ref);

			List<R2_CLS_CaratulaProrrateo_CTRL.SegmentoWP> listSeg = controller.getSegmentsData();
			System.assertEquals(1, listSeg.size());
			System.assertEquals('IB', listSeg[0].airline);

		Test.stopTest();

		//System.assertEquals(0, [SELECT count() FROM R1_Log__c]);
	}

		/*---------------------------------------------------------------------------------------------------------------------
	Author:         Jaime Ascanta
	Company:        Accenture
	Description:   
	IN:
	OUT:

	History:
		<Date>                     <Author>                         <Change Description>
	13/03/2018       	   		Jaime Ascanta                   		Initial Version
	----------------------------------------------------------------------------------------------------------------------*/
	@isTest
	static void exception_Test() {
		R1_CLS_LogHelper.throw_exception = true;

		R2_CLS_CaratulaProrrateo_CTRL controller = new R2_CLS_CaratulaProrrateo_CTRL();

		Test.startTest();
			controller.getProrrateoData();
			controller.getSegmentsData();
			controller.getProrrateo(null);
			controller.getSegmento(null);
			controller.getSegmentos(null);
			controller.getCia(null);
			controller.getBagsTagNumbers(null);
			controller.getBaggages(null);
			controller.formatearFecha(null);
		Test.stopTest();
		
		System.assertEquals(0, [SELECT count() FROM R1_Log__c]);
	}
}