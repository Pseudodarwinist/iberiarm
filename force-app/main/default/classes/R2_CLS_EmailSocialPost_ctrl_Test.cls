@isTest
private class R2_CLS_EmailSocialPost_ctrl_Test {
	
	@isTest static void getSocialPosts_Test() {
          Id recordTypeIdAcc = [SELECT Id FROM recordType WHERE Name like 'Cuenta personal' LIMIT 1].Id;
          Id rtId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Comunicaciones entrantes').getRecordTypeId();
          Account acc = new Account();
            acc.RecordTypeId = recordTypeIdAcc;
            acc.LastName = 'CuentaVieja';
            acc.FirstName = 'Abc';
            acc.PersonBirthdate = date.newInstance(1998, 10, 21);
          insert acc;
          R2_CLS_EmailSocialPost_ctrl cont = new R2_CLS_EmailSocialPost_ctrl();
          Case case1 = new Case(AccountId = acc.Id, RecordTypeId = rtId);
          insert case1;
          cont.caseId = case1.Id;
          System.assertEquals(cont.getSocialPosts().size(), 0);
    }
	
}