@isTest
public class TCK1_CLS_WSResiber_Test {
	        
    
    @isTest static void WSUpg_Test(){
        
        
        
        Tarifas_UPG__c cs = new Tarifas_UPG__c();
        cs.Name='MADABC';
        cs.Tramo__c='MADABC';
        cs.Precio_Avio_TUR_PE__c= 1;
        cs.Precio_Avio_TUR_BUS__c= 100000;
        cs.Precio_Avio_PE_BUS__c= 1;
        //cs.Other fiels values
     	insert cs;  
        
        List<Acceso_Sala_VIP__c> listCs = new List<Acceso_Sala_VIP__c>();
        Acceso_Sala_VIP__c cs2 = new Acceso_Sala_VIP__c();
        cs2.Name='ClaseTUR';
        cs2.ValorTexto__c='A';
        Acceso_Sala_VIP__c cs3 = new Acceso_Sala_VIP__c();
        cs3.Name='Margen_BUS';
        cs3.ValorTexto__c='3';
        Acceso_Sala_VIP__c cs4 = new Acceso_Sala_VIP__c();
        cs4.Name='Margen_PE';
        cs4.ValorTexto__c='3';
        Acceso_Sala_VIP__c cs5 = new Acceso_Sala_VIP__c();
        cs5.Name='ClasePE';
        cs5.ValorTexto__c='W';
        Acceso_Sala_VIP__c cs6 = new Acceso_Sala_VIP__c();
        cs6.Name='ClaseBUS';
        cs6.ValorTexto__c='J';
        
        listCs.add(cs2);
        listCs.add(cs3);
        listCs.add(cs4);
        listCs.add(cs5);
        listCs.add(cs6);
        insert listCs;
        
        List<Mensajes_Sala_VIP__c> listMssg = new List<Mensajes_Sala_VIP__c>();

        Mensajes_Sala_VIP__c Mssg1 = new Mensajes_Sala_VIP__c();
        Mssg1.Name='Mssg PE-BUS';
        Mssg1.MensajesSIP__c	='El pasajero [nameCliente] del vuelo [vuelo] puede acceder a un upgrade a la clase BUS con un coste de [aviosPE] avios.';
        Mensajes_Sala_VIP__c Mssg2 = new Mensajes_Sala_VIP__c();
        Mssg2.Name='Mssg TUR-BUS';
        Mssg2.MensajesSIP__c	='El pasajero [nameCliente] del vuelo [vuelo] puede acceder a un upgrade a la clase BUS con un coste de [aviosPE] avios.';
        Mensajes_Sala_VIP__c Mssg3 = new Mensajes_Sala_VIP__c();
        Mssg3.Name='Mssg TUR-PE';
        Mssg3.MensajesSIP__c	='El pasajero [nameCliente] del vuelo [vuelo] puede acceder a un upgrade a la clase BUS con un coste de [aviosPE] avios.';
        Mensajes_Sala_VIP__c Mssg4 = new Mensajes_Sala_VIP__c();
        Mssg4.Name='Mssg TUR/PE-BUS';
        Mssg4.MensajesSIP__c	='El pasajero [nameClient] del vuelo [vuelo] puede acceder a un upgrade a las clases PE y BUS con un coste de [aviosTUR1] y [aviosTUR2] avios respectivamente.';

        
        listMssg.add(Mssg1);
        listMssg.add(Mssg2);
        listMssg.add(Mssg3);
        listMssg.add(Mssg4);
        insert listMssg;
        
        Account pasajero = new Account();
        pasajero.R1_ACC_NUM_Number_of_Avios__c = 10000;
        pasajero.Name = 'test';
        insert pasajero;
        
        R1_VIP_Lounge_Access__c acceso = new R1_VIP_Lounge_Access__c();
        acceso.R1_VLI_PKL_Sala_Vip__c = 'Autoridades';
        acceso.R1_VLI_TXT_Frecuent_Flyer_number__c = 'IB12345678';
        acceso.R1_VLI_PKL_Vip_lounge_position__c = 'Puesto 1';
        acceso.R1_VLI_TXT_Flight_number__c = '123';
        acceso.R1_VLI_DAT_Date__c = Date.newInstance(2017, 08, 21);
        acceso.R1_VLI_TXT_Comp_Flight__c = 'compTest';
        acceso.R1_VLI_TXT_Class__c = 'A';
        acceso.R1_VLI_PKL_OneWolrd_code__c = 'codeTest';
        acceso.R1_VLI_TXT_Mkt_Flight__c = '1234';
        acceso.R1_VLI_NUM_Guest_number__c = 3;
        acceso.R1_VLI_TXT_Origin__c = 'MAD';
        acceso.R1_VLI_TXT_Destination__c= 'ABC';
        acceso.R1_VLI_PKL_Access_status__c= 'Aprobado';
        acceso.R1_VLI_LOO_Cliente__c = pasajero.id;
        insert acceso;
    
        Decimal aviosPasajero = 10000;
        Decimal aviosPasajero2 = 10000;
        Integer aviosPE = 1;
        Integer aviosTUR1 = 1;
        Integer aviosTUR2 = 1;
        String clase = 'ClaseTUR';
        String carrierCode = 'IB';
        String flightNumber = '0730';
        String localLegDate = '20190808';
        String departureStation = 'MAD';
        String arrivalStation = 'BCN';
        String tramo = 'MADBCN';
        String requestingCarrier = '666';
        String requestingSystem = '777';
        String nameCliente = 'Test';
        Date diaVuelo = Date.newInstance(2017, 08, 21);
        String accesoAcc = acceso.ID;
        String clienteID = pasajero.ID;
        Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());   
        TCK1_CLS_WSResiber.getDatos(aviosPasajero,aviosPasajero2, aviosPE, aviosTUR1, aviosTUR2, clase, carrierCode,  flightNumber,  localLegDate,  departureStation, arrivalStation,  requestingCarrier,  requestingSystem, nameCliente, diaVuelo, accesoAcc , clienteID,tramo);  
    }
    
     @isTest static void WSUpg_Test2(){
        
        
        
        Tarifas_UPG__c cs = new Tarifas_UPG__c();
        cs.Name='MADABC';
        cs.Tramo__c='MADABC';
        cs.Precio_Avio_TUR_PE__c= 1;
        cs.Precio_Avio_TUR_BUS__c= 100000;
        cs.Precio_Avio_PE_BUS__c= 1;
        //cs.Other fiels values
     	insert cs;  
        
        List<Acceso_Sala_VIP__c> listCs = new List<Acceso_Sala_VIP__c>();
        Acceso_Sala_VIP__c cs2 = new Acceso_Sala_VIP__c();
        cs2.Name='ClaseTUR';
        cs2.ValorTexto__c='A';
        Acceso_Sala_VIP__c cs3 = new Acceso_Sala_VIP__c();
        cs3.Name='Margen_BUS';
        cs3.ValorTexto__c='3';
        Acceso_Sala_VIP__c cs4 = new Acceso_Sala_VIP__c();
        cs4.Name='Margen_PE';
        cs4.ValorTexto__c='3';
        Acceso_Sala_VIP__c cs5 = new Acceso_Sala_VIP__c();
        cs5.Name='ClasePE';
        cs5.ValorTexto__c='W';
        Acceso_Sala_VIP__c cs6 = new Acceso_Sala_VIP__c();
        cs6.Name='ClaseBUS';
        cs6.ValorTexto__c='J';
        
        listCs.add(cs2);
        listCs.add(cs3);
        listCs.add(cs4);
        listCs.add(cs5);
        listCs.add(cs6);
        insert listCs;
        
        List<Mensajes_Sala_VIP__c> listMssg = new List<Mensajes_Sala_VIP__c>();

        Mensajes_Sala_VIP__c Mssg1 = new Mensajes_Sala_VIP__c();
        Mssg1.Name='Mssg PE-BUS';
        Mssg1.MensajesSIP__c	='El pasajero [nameCliente] del vuelo [vuelo] puede acceder a un upgrade a la clase BUS con un coste de [aviosPE] avios.';
        Mensajes_Sala_VIP__c Mssg2 = new Mensajes_Sala_VIP__c();
        Mssg2.Name='Mssg TUR-BUS';
        Mssg2.MensajesSIP__c	='El pasajero [nameCliente] del vuelo [vuelo] puede acceder a un upgrade a la clase BUS con un coste de [aviosPE] avios.';
        Mensajes_Sala_VIP__c Mssg3 = new Mensajes_Sala_VIP__c();
        Mssg3.Name='Mssg TUR-PE';
        Mssg3.MensajesSIP__c	='El pasajero [nameCliente] del vuelo [vuelo] puede acceder a un upgrade a la clase BUS con un coste de [aviosPE] avios.';
        Mensajes_Sala_VIP__c Mssg4 = new Mensajes_Sala_VIP__c();
        Mssg4.Name='Mssg TUR/PE-BUS';
        Mssg4.MensajesSIP__c	='El pasajero [nameClient] del vuelo [vuelo] puede acceder a un upgrade a las clases PE y BUS con un coste de [aviosTUR1] y [aviosTUR2] avios respectivamente.';

        
        listMssg.add(Mssg1);
        listMssg.add(Mssg2);
        listMssg.add(Mssg3);
        listMssg.add(Mssg4);
        insert listMssg;
        
        Account pasajero = new Account();
        pasajero.R1_ACC_NUM_Number_of_Avios__c = 10000;
        pasajero.Name = 'test';
        insert pasajero;
        
        R1_VIP_Lounge_Access__c acceso = new R1_VIP_Lounge_Access__c();
        acceso.R1_VLI_PKL_Sala_Vip__c = 'Autoridades';
        acceso.R1_VLI_TXT_Frecuent_Flyer_number__c = 'IB12345678';
        acceso.R1_VLI_PKL_Vip_lounge_position__c = 'Puesto 1';
        acceso.R1_VLI_TXT_Flight_number__c = '123';
        acceso.R1_VLI_DAT_Date__c = Date.newInstance(2017, 08, 21);
        acceso.R1_VLI_TXT_Comp_Flight__c = 'compTest';
        acceso.R1_VLI_TXT_Class__c = 'A';
        acceso.R1_VLI_PKL_OneWolrd_code__c = 'codeTest';
        acceso.R1_VLI_TXT_Mkt_Flight__c = '1234';
        acceso.R1_VLI_NUM_Guest_number__c = 3;
        acceso.R1_VLI_TXT_Origin__c = 'MAD';
        acceso.R1_VLI_TXT_Destination__c= 'ABC';
        acceso.R1_VLI_PKL_Access_status__c= 'Aprobado';
        acceso.R1_VLI_LOO_Cliente__c = pasajero.id;
        insert acceso;
    
        Decimal aviosPasajero = 5000;
        Decimal aviosPasajero2 = 5000;
        Integer aviosPE = 1;
        Integer aviosTUR1 = 1;
        Integer aviosTUR2 = 10000;
        String clase = 'ClaseTUR';
        String carrierCode = 'IB';
        String flightNumber = '0730';
        String localLegDate = '20190808';
        String departureStation = 'MAD';
        String arrivalStation = 'BCN';
        String tramo = 'MADBCN';
        String requestingCarrier = '666';
        String requestingSystem = '777';
        String nameCliente = 'Test';
        Date diaVuelo = Date.newInstance(2017, 08, 21);
        String accesoAcc = acceso.ID;
        String clienteID = pasajero.ID;
        Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());     
        TCK1_CLS_WSResiber.getDatos(aviosPasajero,aviosPasajero2, aviosPE, aviosTUR1, aviosTUR2, clase, carrierCode,  flightNumber,  localLegDate,  departureStation, arrivalStation,  requestingCarrier,  requestingSystem, nameCliente, diaVuelo, accesoAcc , clienteID,tramo);

        
    }
    
      
    @isTest static void WSUpg_Test3(){
        
        
        
        Tarifas_UPG__c cs = new Tarifas_UPG__c();
        cs.Name='MADABC';
        cs.Tramo__c='MADABC';
        cs.Precio_Avio_TUR_PE__c= 1;
        cs.Precio_Avio_TUR_BUS__c= 100000;
        cs.Precio_Avio_PE_BUS__c= 1;
        //cs.Other fiels values
     	insert cs;  
        
        List<Acceso_Sala_VIP__c> listCs = new List<Acceso_Sala_VIP__c>();
        Acceso_Sala_VIP__c cs2 = new Acceso_Sala_VIP__c();
        cs2.Name='ClaseTUR';
        cs2.ValorTexto__c='A';
        Acceso_Sala_VIP__c cs3 = new Acceso_Sala_VIP__c();
        cs3.Name='Margen_BUS';
        cs3.ValorTexto__c='3';
        Acceso_Sala_VIP__c cs4 = new Acceso_Sala_VIP__c();
        cs4.Name='Margen_PE';
        cs4.ValorTexto__c='3';
        Acceso_Sala_VIP__c cs5 = new Acceso_Sala_VIP__c();
        cs5.Name='ClasePE';
        cs5.ValorTexto__c='W';
        Acceso_Sala_VIP__c cs6 = new Acceso_Sala_VIP__c();
        cs6.Name='ClaseBUS';
        cs6.ValorTexto__c='J';
        
        listCs.add(cs2);
        listCs.add(cs3);
        listCs.add(cs4);
        listCs.add(cs5);
        listCs.add(cs6);
        insert listCs;
        
        List<Mensajes_Sala_VIP__c> listMssg = new List<Mensajes_Sala_VIP__c>();

        Mensajes_Sala_VIP__c Mssg1 = new Mensajes_Sala_VIP__c();
        Mssg1.Name='Mssg PE-BUS';
        Mssg1.MensajesSIP__c	='El pasajero [nameCliente] del vuelo [vuelo] puede acceder a un upgrade a la clase BUS con un coste de [aviosPE] avios.';
        Mensajes_Sala_VIP__c Mssg2 = new Mensajes_Sala_VIP__c();
        Mssg2.Name='Mssg TUR-BUS';
        Mssg2.MensajesSIP__c	='El pasajero [nameCliente] del vuelo [vuelo] puede acceder a un upgrade a la clase BUS con un coste de [aviosPE] avios.';
        Mensajes_Sala_VIP__c Mssg3 = new Mensajes_Sala_VIP__c();
        Mssg3.Name='Mssg TUR-PE';
        Mssg3.MensajesSIP__c	='El pasajero [nameCliente] del vuelo [vuelo] puede acceder a un upgrade a la clase BUS con un coste de [aviosPE] avios.';
        Mensajes_Sala_VIP__c Mssg4 = new Mensajes_Sala_VIP__c();
        Mssg4.Name='Mssg TUR/PE-BUS';
        Mssg4.MensajesSIP__c	='El pasajero [nameClient] del vuelo [vuelo] puede acceder a un upgrade a las clases PE y BUS con un coste de [aviosTUR1] y [aviosTUR2] avios respectivamente.';

        
        listMssg.add(Mssg1);
        listMssg.add(Mssg2);
        listMssg.add(Mssg3);
        listMssg.add(Mssg4);
        insert listMssg;
        
        Account pasajero = new Account();
        pasajero.R1_ACC_NUM_Number_of_Avios__c = 10000;
        pasajero.Name = 'test';
        insert pasajero;
        
        R1_VIP_Lounge_Access__c acceso = new R1_VIP_Lounge_Access__c();
        acceso.R1_VLI_PKL_Sala_Vip__c = 'Autoridades';
        acceso.R1_VLI_TXT_Frecuent_Flyer_number__c = 'IB12345678';
        acceso.R1_VLI_PKL_Vip_lounge_position__c = 'Puesto 1';
        acceso.R1_VLI_TXT_Flight_number__c = '123';
        acceso.R1_VLI_DAT_Date__c = Date.newInstance(2017, 08, 21);
        acceso.R1_VLI_TXT_Comp_Flight__c = 'compTest';
        acceso.R1_VLI_TXT_Class__c = 'A';
        acceso.R1_VLI_PKL_OneWolrd_code__c = 'codeTest';
        acceso.R1_VLI_TXT_Mkt_Flight__c = '1234';
        acceso.R1_VLI_NUM_Guest_number__c = 3;
        acceso.R1_VLI_TXT_Origin__c = 'MAD';
        acceso.R1_VLI_TXT_Destination__c= 'ABC';
        acceso.R1_VLI_PKL_Access_status__c= 'Aprobado';
        acceso.R1_VLI_LOO_Cliente__c = pasajero.id;
        insert acceso;
    
        Decimal aviosPasajero = 10000;
        Decimal aviosPasajero2 = 10000;
        Integer aviosPE = 1;
        Integer aviosTUR1 = 1;
        Integer aviosTUR2 = 1;
        String clase = 'ClasePE';
        String carrierCode = 'IB';
        String flightNumber = '0730';
        String localLegDate = '20190808';
        String departureStation = 'MAD';
        String arrivalStation = 'BCN';
        String requestingCarrier = '666';
        String requestingSystem = '777';
        String tramo = 'MADBCN';
        String nameCliente = 'Test';
        Date diaVuelo = Date.newInstance(2017, 08, 21);
        String accesoAcc = acceso.ID;
        String clienteID = pasajero.ID;
        Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());     
        TCK1_CLS_WSResiber.getDatos(aviosPasajero,aviosPasajero2, aviosPE, aviosTUR1, aviosTUR2, clase, carrierCode,  flightNumber,  localLegDate,  departureStation, arrivalStation,  requestingCarrier,  requestingSystem, nameCliente, diaVuelo, accesoAcc , clienteID,tramo);

        
    }
    
    
          
    @isTest static void WSUpg_Test4(){
        
        
        
        Tarifas_UPG__c cs = new Tarifas_UPG__c();
        cs.Name='MADABC';
        cs.Tramo__c='MADABC';
        cs.Precio_Avio_TUR_PE__c= 1;
        cs.Precio_Avio_TUR_BUS__c= 1;
        cs.Precio_Avio_PE_BUS__c= 1;
        //cs.Other fiels values
     	insert cs;  
        
        List<Acceso_Sala_VIP__c> listCs = new List<Acceso_Sala_VIP__c>();
        Acceso_Sala_VIP__c cs2 = new Acceso_Sala_VIP__c();
        cs2.Name='ClaseTUR';
        cs2.ValorTexto__c='A';
        Acceso_Sala_VIP__c cs3 = new Acceso_Sala_VIP__c();
        cs3.Name='Margen_BUS';
        cs3.ValorTexto__c='3';
        Acceso_Sala_VIP__c cs4 = new Acceso_Sala_VIP__c();
        cs4.Name='Margen_PE';
        cs4.ValorTexto__c='60';
        Acceso_Sala_VIP__c cs5 = new Acceso_Sala_VIP__c();
        cs5.Name='ClasePE';
        cs5.ValorTexto__c='W';
        Acceso_Sala_VIP__c cs6 = new Acceso_Sala_VIP__c();
        cs6.Name='ClaseBUS';
        cs6.ValorTexto__c='J';
        
        listCs.add(cs2);
        listCs.add(cs3);
        listCs.add(cs4);
        listCs.add(cs5);
        listCs.add(cs6);
        insert listCs;
        
        List<Mensajes_Sala_VIP__c> listMssg = new List<Mensajes_Sala_VIP__c>();

        Mensajes_Sala_VIP__c Mssg1 = new Mensajes_Sala_VIP__c();
        Mssg1.Name='Mssg PE-BUS';
        Mssg1.MensajesSIP__c	='El pasajero [nameCliente] del vuelo [vuelo] puede acceder a un upgrade a la clase BUS con un coste de [aviosPE] avios.';
        Mensajes_Sala_VIP__c Mssg2 = new Mensajes_Sala_VIP__c();
        Mssg2.Name='Mssg TUR-BUS';
        Mssg2.MensajesSIP__c	='El pasajero [nameCliente] del vuelo [vuelo] puede acceder a un upgrade a la clase BUS con un coste de [aviosPE] avios.';
        Mensajes_Sala_VIP__c Mssg3 = new Mensajes_Sala_VIP__c();
        Mssg3.Name='Mssg TUR-PE';
        Mssg3.MensajesSIP__c	='El pasajero [nameCliente] del vuelo [vuelo] puede acceder a un upgrade a la clase BUS con un coste de [aviosPE] avios.';
        Mensajes_Sala_VIP__c Mssg4 = new Mensajes_Sala_VIP__c();
        Mssg4.Name='Mssg TUR/PE-BUS';
        Mssg4.MensajesSIP__c	='El pasajero [nameClient] del vuelo [vuelo] puede acceder a un upgrade a las clases PE y BUS con un coste de [aviosTUR1] y [aviosTUR2] avios respectivamente.';

        
        listMssg.add(Mssg1);
        listMssg.add(Mssg2);
        listMssg.add(Mssg3);
        listMssg.add(Mssg4);
        insert listMssg;
        
        Account pasajero = new Account();
        pasajero.R1_ACC_NUM_Number_of_Avios__c = 10000;
        pasajero.Name = 'test';
        insert pasajero;
        
        R1_VIP_Lounge_Access__c acceso = new R1_VIP_Lounge_Access__c();
        acceso.R1_VLI_PKL_Sala_Vip__c = 'Autoridades';
        acceso.R1_VLI_TXT_Frecuent_Flyer_number__c = 'IB12345678';
        acceso.R1_VLI_PKL_Vip_lounge_position__c = 'Puesto 1';
        acceso.R1_VLI_TXT_Flight_number__c = '123';
        acceso.R1_VLI_DAT_Date__c = Date.newInstance(2017, 08, 21);
        acceso.R1_VLI_TXT_Comp_Flight__c = 'compTest';
        acceso.R1_VLI_TXT_Class__c = 'A';
        acceso.R1_VLI_PKL_OneWolrd_code__c = 'codeTest';
        acceso.R1_VLI_TXT_Mkt_Flight__c = '1234';
        acceso.R1_VLI_NUM_Guest_number__c = 3;
        acceso.R1_VLI_TXT_Origin__c = 'MAD';
        acceso.R1_VLI_TXT_Destination__c= 'ABC';
        acceso.R1_VLI_PKL_Access_status__c= 'Aprobado';
        acceso.R1_VLI_LOO_Cliente__c = pasajero.id;
        insert acceso;
    
        Decimal aviosPasajero = 10000;
        Decimal aviosPasajero2 = 10000;
        Integer aviosPE = 1;
        Integer aviosTUR1 = 1;
        Integer aviosTUR2 = 1;
        String clase = 'ClasePE';
        String carrierCode = 'IB';
        String flightNumber = '0730';
        String localLegDate = '20190808';
        String departureStation = 'MAD';
        String arrivalStation = 'BCN';
        String tramo = 'MADBCN';
        String requestingCarrier = '666';
        String requestingSystem = '777';
        String nameCliente = 'Test';
        Date diaVuelo = Date.newInstance(2017, 08, 21);
        String accesoAcc = acceso.ID;
        String clienteID = pasajero.ID;
        Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());      
        TCK1_CLS_WSResiber.getDatos(aviosPasajero,aviosPasajero2, aviosPE, aviosTUR1, aviosTUR2, clase, carrierCode,  flightNumber,  localLegDate,  departureStation, arrivalStation,  requestingCarrier,  requestingSystem, nameCliente, diaVuelo, accesoAcc , clienteID,tramo);

        
    }
    
              
    @isTest static void WSUpg_Test5(){
        
        
        
        Tarifas_UPG__c cs = new Tarifas_UPG__c();
        cs.Name='MADABC';
        cs.Tramo__c='MADABC';
        cs.Precio_Avio_TUR_PE__c= 1;
        cs.Precio_Avio_TUR_BUS__c= 1;
        cs.Precio_Avio_PE_BUS__c= 1;
        //cs.Other fiels values
     	insert cs;  
        
        List<Acceso_Sala_VIP__c> listCs = new List<Acceso_Sala_VIP__c>();
        Acceso_Sala_VIP__c cs2 = new Acceso_Sala_VIP__c();
        cs2.Name='ClaseTUR';
        cs2.ValorTexto__c='A';
        Acceso_Sala_VIP__c cs3 = new Acceso_Sala_VIP__c();
        cs3.Name='Margen_BUS';
        cs3.ValorTexto__c='60';
        Acceso_Sala_VIP__c cs4 = new Acceso_Sala_VIP__c();
        cs4.Name='Margen_PE';
        cs4.ValorTexto__c='300';
        Acceso_Sala_VIP__c cs5 = new Acceso_Sala_VIP__c();
        cs5.Name='ClasePE';
        cs5.ValorTexto__c='W';
        Acceso_Sala_VIP__c cs6 = new Acceso_Sala_VIP__c();
        cs6.Name='ClaseBUS';
        cs6.ValorTexto__c='J';
        
        listCs.add(cs2);
        listCs.add(cs3);
        listCs.add(cs4);
        listCs.add(cs5);
        listCs.add(cs6);
        insert listCs;
        
        List<Mensajes_Sala_VIP__c> listMssg = new List<Mensajes_Sala_VIP__c>();

        Mensajes_Sala_VIP__c Mssg1 = new Mensajes_Sala_VIP__c();
        Mssg1.Name='Mssg PE-BUS';
        Mssg1.MensajesSIP__c	='El pasajero [nameCliente] del vuelo [vuelo] puede acceder a un upgrade a la clase BUS con un coste de [aviosPE] avios.';
        Mensajes_Sala_VIP__c Mssg2 = new Mensajes_Sala_VIP__c();
        Mssg2.Name='Mssg TUR-BUS';
        Mssg2.MensajesSIP__c	='El pasajero [nameCliente] del vuelo [vuelo] puede acceder a un upgrade a la clase BUS con un coste de [aviosPE] avios.';
        Mensajes_Sala_VIP__c Mssg3 = new Mensajes_Sala_VIP__c();
        Mssg3.Name='Mssg TUR-PE';
        Mssg3.MensajesSIP__c	='El pasajero [nameCliente] del vuelo [vuelo] puede acceder a un upgrade a la clase BUS con un coste de [aviosPE] avios.';
        Mensajes_Sala_VIP__c Mssg4 = new Mensajes_Sala_VIP__c();
        Mssg4.Name='Mssg TUR/PE-BUS';
        Mssg4.MensajesSIP__c	='El pasajero [nameClient] del vuelo [vuelo] puede acceder a un upgrade a las clases PE y BUS con un coste de [aviosTUR1] y [aviosTUR2] avios respectivamente.';

        
        listMssg.add(Mssg1);
        listMssg.add(Mssg2);
        listMssg.add(Mssg3);
        listMssg.add(Mssg4);
        insert listMssg;
        
        Account pasajero = new Account();
        pasajero.R1_ACC_NUM_Number_of_Avios__c = 10000;
        pasajero.Name = 'test';
        insert pasajero;
        
        R1_VIP_Lounge_Access__c acceso = new R1_VIP_Lounge_Access__c();
        acceso.R1_VLI_PKL_Sala_Vip__c = 'Autoridades';
        acceso.R1_VLI_TXT_Frecuent_Flyer_number__c = 'IB12345678';
        acceso.R1_VLI_PKL_Vip_lounge_position__c = 'Puesto 1';
        acceso.R1_VLI_TXT_Flight_number__c = '123';
        acceso.R1_VLI_DAT_Date__c = Date.newInstance(2017, 08, 21);
        acceso.R1_VLI_TXT_Comp_Flight__c = 'compTest';
        acceso.R1_VLI_TXT_Class__c = 'A';
        acceso.R1_VLI_PKL_OneWolrd_code__c = 'codeTest';
        acceso.R1_VLI_TXT_Mkt_Flight__c = '1234';
        acceso.R1_VLI_NUM_Guest_number__c = 3;
        acceso.R1_VLI_TXT_Origin__c = 'MAD';
        acceso.R1_VLI_TXT_Destination__c= 'ABC';
        acceso.R1_VLI_PKL_Access_status__c= 'Aprobado';
        acceso.R1_VLI_LOO_Cliente__c = pasajero.id;
        insert acceso;
    
        Decimal aviosPasajero = 10000;
        Decimal aviosPasajero2 = 10000;
        Integer aviosPE = 1;
        Integer aviosTUR1 = 1;
        Integer aviosTUR2 = 1;
        String clase = 'ClasePE';
        String carrierCode = 'IB';
        String flightNumber = '0730';
        String localLegDate = '20190808';
        String departureStation = 'MAD';
        String arrivalStation = 'BCN';
        String requestingCarrier = '666';
        String requestingSystem = '777';
        String tramo = 'MADBCN';
        String nameCliente = 'Test';
        Date diaVuelo = Date.newInstance(2017, 08, 21);
        String accesoAcc = acceso.ID;
        String clienteID = pasajero.ID;
        Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());     
        TCK1_CLS_WSResiber.getDatos(aviosPasajero,aviosPasajero2, aviosPE, aviosTUR1, aviosTUR2, clase, carrierCode,  flightNumber,  localLegDate,  departureStation, arrivalStation,  requestingCarrier,  requestingSystem, nameCliente, diaVuelo, accesoAcc , clienteID,tramo);

        
    }

    public HTTPResponse WSUpg_TestResiber(){
        HTTPResponse result = new HTTPResponse();
        result.setStatusCode(200);
        result.setBody('{"legData":{"carrierCode":"IB","flightNumber":"730","departureDateTime":"2019-08-09T00:00:00.000Z","origin":"MAD","destination":"BCN","legCabinData":[{"cabinCode":"J","actualCapacity":"24","authorizedCapacity":"25","seatsSold":"7","seatsAvailable":"12"},{"cabinCode":"W","actualCapacity":"24","authorizedCapacity":"25","seatsSold":"7","seatsAvailable":"12"},{"cabinCode":"Y","actualCapacity":"144","authorizedCapacity":"154","seatsSold":"99","seatsAvailable":"35"}]}}');
        return result;
    }
    public HTTPResponse WSUpg_TestResiberKO401(){
        HTTPResponse result = new HTTPResponse();
        result.setStatusCode(401);
        result.setBody('{}');
        return result;
    }
    
}