global with sharing class R2_CLS_Flight_TriggerMethods{
    

/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Ismael Yubero Moreno                        
    Company:        Accenture
    Description:    
    
    IN:       
    OUT:      

    History: 
    <Date>                     <Author>                <Change Description>
    07/08/2017                 Ismael Yubero Moreno    Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

    

    public class WpListaPasajero{
        public String fullFlightNumber;
        public String flightDate;
        public String flightOrigin;
        public String transactionResult;
        public WpVuelo flight;
    }

    public class WpVuelo{
        public Integer totalPassengers;
        public WpPasajero [] passengers = new WpPasajero[]{};
    }

    public class WpPasajero{
        public String paxSequenceNumber;
        public String originCity;
        public String specialTreatment;
        public String passengerName;
        public String companyFf;
        public String tierFf;
        public String companyFr;
        public String tierFr;
        public String numFr;
        public String deniedBoardingIndicator;
        public String downgradeIndicator;
        public String misconnectionIndicator;
        public String customerCategory;
        public String employeeIndicator;
        public String flightClass;
        public String inboundConnectionIndicator;
        public String outboundConnectionIndicator;
        public String disembarkAvailabilityIndicator;
        public String waitingListIndicator;
        public String vipIndicator;
        public String upgradeIndicator;
        public String seatAvailabilityIndicator;
        public String deadHeadCrewIndicator;
        public String voluntarySeatIndicator;
        public String sectionId;
        public String pnr;
        public String contactId;
        public String boardingNumber;
        public String seatNumber;
        public String cabin;
        public String etktNum;
        
    }
    
    
    public static String formatoFecha(Date fechaVuelo){
        try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');} //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;           

            String fecha;
            system.debug('!!! fecha: ' +  fechaVuelo);
            if(fechaVuelo.month()<10){
                system.debug('!!! mes if: ' +  fechaVuelo.month());
                fecha = '0'+String.valueOf(fechaVuelo.month());
            }else{
                fecha = String.valueOf(fechaVuelo.month());
            }
            if(fechaVuelo.day()<10){
                system.debug('!!! dia if: ' +  fechaVuelo.day());
                fecha+= '0'+ String.valueOf(fechaVuelo.day());
            }else{
                fecha+= String.valueOf(fechaVuelo.day());
            }
            
            fecha+= String.valueOf(fechaVuelo.year());
            
            return fecha;
        }catch(Exception exc){
                R1_CLS_LogHelper.generateErrorLog('R2_CLS_Flight_TriggerMethods__c.formatoFecha', '', exc.getmessage()+', '+exc.getLineNumber(), 'R1_Flight__c');
                return null;

        }      
        
    }


public static List<R2_CKI_info__c> imprimirlista(List<R2_CKI_info__c> listaPas){
    System.debug(listaPas);
    return listaPas;
}

@future(callout=true)
    webservice static void listaPasajeros(String idVuelo){
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Ismael Yubero Moreno
    Company:        Accenture
    Description:    
    IN:         

    OUT:           
    
    History:
    
    <Date>              <Author>                 <Description>
    11/08/2017          Ismael Yubero Moreno     Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/   
           try{
                if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');} //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;           
                    Id k = Id.valueOf(idVuelo);
                    R1_Flight__c vuelo = [SELECT id, R1_FLG_TXT_Flight_number__c, R1_FLG_DAT_Flight_date_local__c,R1_FLG_TXT_Airport_depart__c, R1_FLG_TXT_Carrier_code__c FROM R1_Flight__c WHERE id=:k];                    
                    WpListaPasajero resp = getPassengerFromResiber(vuelo.R1_FLG_TXT_Flight_number__c,vuelo.R1_FLG_DAT_Flight_date_local__c, vuelo.R1_FLG_TXT_Airport_depart__c,vuelo.R1_FLG_TXT_Carrier_code__c, 0);
                    System.debug('resp: ' + resp);
                    List<R2_CKI_info__c> listPassengers =  new List<R2_CKI_info__c>();
                    Integer numPas =  0;
                    if(resp != null){
                        if(resp.transactionResult == 'OK' && resp.flight != null){
                            if(resp.flight.totalPassengers != null && resp.flight.totalPassengers != 0){
                                numPas = resp.flight.totalPassengers;
                            }
                            if(numPas > 0){
                                Set<String> numFrs = new Set<String>();
                                for(Integer i = 0; i < numPas; i++){
                                    if(resp.flight.passengers[i].numFr != null && resp.flight.passengers[i].numFr != ''){
                                        numFrs.add(resp.flight.passengers[i].numFr);
                                    }
                                }
                                System.debug('Este son los FF: ' + numFrs);
                                List<Account> lstAcc = [SELECT id, R1_ACC_TXT_Primary_FF_Card_CCPO__c FROM Account WHERE R1_ACC_TXT_Primary_FF_Card_CCPO__c IN :numFrs];
                                System.debug('Por que salen estos 4 :' +lstAcc);
                                Map<String,Account> mapAcc = new Map<String,Account>();
                                //mapAcc.put()
                                System.debug('MapACC'+  mapAcc);

                                for(Integer l = 0; l< lstAcc.size();l++){
                                    mapAcc.put(lstAcc[l].R1_ACC_TXT_Primary_FF_Card_CCPO__c,lstAcc[l]);
                                }
                                //System.debug(mapAcc.get(lstAcc[0].R1_ACC_TXT_Primary_FF_Card_CCPO__c));

                                for(integer i = 0; i<numPas; i++){

                                    R2_CKI_info__c pass = new R2_CKI_info__c();
                                    pass.R2_CKI_LOO_Flight__c = vuelo.Id;
                                    System.debug('Este es el id del vuelo: ' + vuelo.Id);
                                    if(resp.flight.passengers[i].passengerName != null && resp.flight.passengers[i].passengerName != ''){
                                        pass.Name = resp.flight.passengers[i].passengerName;
                                    }
                                    if(resp.flight.passengers[i].paxSequenceNumber != null && resp.flight.passengers[i].paxSequenceNumber != ''){
                                        pass.R2_CKI_NUM_Pax_Number__c = integer.valueof(resp.flight.passengers[i].paxSequenceNumber);
                                    }
                                    if(resp.flight.passengers[i].originCity != null && resp.flight.passengers[i].originCity != ''){
                                        pass.R2_CKI_TXT_Origin__c= resp.flight.passengers[i].originCity;                            
                                    }
                                    if(resp.flight.passengers[i].specialTreatment != null && resp.flight.passengers[i].specialTreatment != ''){   
                                        pass.R2_CKI_PKLM_Special__c =  resp.flight.passengers[i].specialTreatment;
                                    }
                                    if(resp.flight.passengers[i].companyFf != null && resp.flight.passengers[i].companyFf != ''){
                                        pass.R2_CKI_TXT_CompanyFF__c= resp.flight.passengers[i].companyFf;
                                    }
                                    if(resp.flight.passengers[i].tierFf != null && resp.flight.passengers[i].tierFf != ''){
                                        pass.R2_CKI_TXT_TierFF__c=resp.flight.passengers[i].tierFf;
                                    }
                                    //pass.         Preguntar a Virginia que campos es companyFr
                                    //pass.         Preguntar a Virginia que campos es tierFr
                                    //pass.R2_CKI_LOO_Account__r.R1_ACC_TXT_Primary_Frecuent_Flyer_Card__c =  resp.flight.passengers[i].numFr;
                                    if(resp.flight.passengers[i].deniedBoardingIndicator != null && resp.flight.passengers[i].deniedBoardingIndicator != ''){
                                        pass.R2_CKI_CHK_CAP__c =  boolean.valueof(resp.flight.passengers[i].deniedBoardingIndicator);
                                    }
                                    if(resp.flight.passengers[i].downgradeIndicator != null && resp.flight.passengers[i].downgradeIndicator != ''){
                                        pass.R2_CKI_CHK_DNG__c = boolean.valueof(resp.flight.passengers[i].downgradeIndicator);
                                    }
                                    if(resp.flight.passengers[i].misconnectionIndicator != null && resp.flight.passengers[i].misconnectionIndicator != ''){
                                        pass.R2_CKI_CHK_XC__c =  boolean.valueof(resp.flight.passengers[i].misconnectionIndicator);
                                    }
                                    //pass.R2_CKI_FOR_Customer_category__c =  resp.flight.passengers[i].customerCategory;
                                    if(resp.flight.passengers[i].employeeIndicator != null && resp.flight.passengers[i].employeeIndicator != ''){
                                        pass.R2_CKI_TXT_ID__c =  resp.flight.passengers[i].employeeIndicator;
                                    }
                                    if(resp.flight.passengers[i].flightClass != null && resp.flight.passengers[i].flightClass != ''){
                                        pass.R2_CKI_TXT_Flight_class__c=resp.flight.passengers[i].flightClass;
                                    }
                                    if(resp.flight.passengers[i].inboundConnectionIndicator != null && resp.flight.passengers[i].inboundConnectionIndicator != ''){
                                        pass.R2_CKI_CHK_ICONN__c = boolean.valueof(resp.flight.passengers[i].inboundConnectionIndicator);
                                    }
                                    if(resp.flight.passengers[i].outboundConnectionIndicator != null && resp.flight.passengers[i].outboundConnectionIndicator != ''){
                                        pass.R2_CKI_CHK_OCONN__c = boolean.valueof(resp.flight.passengers[i].outboundConnectionIndicator);
                                    }
                                    if(resp.flight.passengers[i].disembarkAvailabilityIndicator != null && resp.flight.passengers[i].disembarkAvailabilityIndicator != ''){
                                        pass.R2_CKI_CHK_PAD__c =  boolean.valueof(resp.flight.passengers[i].disembarkAvailabilityIndicator); 
                                    }
                                    if(resp.flight.passengers[i].waitingListIndicator != null && resp.flight.passengers[i].waitingListIndicator != ''){
                                        pass.R2_CKI_CHK_WL__c = boolean.valueof(resp.flight.passengers[i].waitingListIndicator);
                                    }
                                    if(resp.flight.passengers[i].vipIndicator != null && resp.flight.passengers[i].vipIndicator != ''){
                                        pass.R2_CKI_CHK_VIP__c = boolean.valueof(resp.flight.passengers[i].vipIndicator);
                                    }
                                    if(resp.flight.passengers[i].upgradeIndicator != null && resp.flight.passengers[i].upgradeIndicator != ''){
                                        pass.R2_CKI_CHK_UPG__c =  boolean.valueof(resp.flight.passengers[i].upgradeIndicator);
                                    }
                                    if(resp.flight.passengers[i].seatAvailabilityIndicator != null && resp.flight.passengers[i].seatAvailabilityIndicator != ''){
                                        pass.R2_CKI_CHK_SA__c =  boolean.valueof(resp.flight.passengers[i].seatAvailabilityIndicator); 
                                    }
                                    if(resp.flight.passengers[i].deadHeadCrewIndicator != null && resp.flight.passengers[i].deadHeadCrewIndicator != ''){
                                        pass.R2_CKI_CHK_DHC__c =  resp.flight.passengers[i].deadHeadCrewIndicator;
                                    }
                                    if(resp.flight.passengers[i].voluntarySeatIndicator != null && resp.flight.passengers[i].voluntarySeatIndicator != ''){
                                        pass.R2_CKI_CHK_PCVP__c = boolean.valueof(resp.flight.passengers[i].voluntarySeatIndicator);
                                    }
                                    //pass.         Preguntar sectionId
                                    if(resp.flight.passengers[i].pnr != null && resp.flight.passengers[i].pnr != ''){
                                        pass.R2_CKI_TXT_PNR_Number__c =  resp.flight.passengers[i].pnr;
                                    }
                                    //pass.         Preguntar contactId
                                    if(resp.flight.passengers[i].boardingNumber != null && resp.flight.passengers[i].boardingNumber != ''){
                                        pass.R2_CKI_TXT_Boarding_number__c = resp.flight.passengers[i].boardingNumber;
                                    }
                                    if(resp.flight.passengers[i].seatNumber != null && resp.flight.passengers[i].seatNumber != ''){
                                        pass.R2_CKI_TXT_Seat_number__c =  resp.flight.passengers[i].seatNumber;
                                    }
                                    if(resp.flight.passengers[i].cabin != null && resp.flight.passengers[i].cabin != ''){
                                        pass.R2_CKI_TEX_Cabin_flight__c = resp.flight.passengers[i].cabin;
                                    }
                                    if(resp.flight.passengers[i].etktNum != null && resp.flight.passengers[i].etktNum != ''){
                                        pass.R2_CKI_TEX_Ticket_number__c = resp.flight.passengers[i].etktNum;
                                    }
                                    if(mapAcc.containsKey(resp.flight.passengers[i].numFr)){
                                        Account newP = new Account();
                                        newP = mapAcc.get(resp.flight.passengers[i].numFr);
                                        pass.R2_CKI_LOO_Account__c = newP.Id;
                                    }else{
                                        //Que da pendiente de revisar esta logica
                                    }
                                    listPassengers.add(pass);
                                }
                            }
                                    
                                
                        }else{
                                System.debug('El estado de la integracion no es OK');
                            }
                    }
                        
                    if(!listPassengers.isEmpty()){                
                        System.debug('Se va a insertar la lista de pasajeros' + listPassengers);
                        insert listPassengers;
                     }   

                        
                    

            }catch(Exception exc){
                R1_CLS_LogHelper.generateErrorLog('R2_CLS_Flight_TriggerMethods__c.listaPasajeros', '', exc.getmessage()+', '+exc.getLineNumber(), 'R1_Flight__c');
            }
    }
    public static WpListaPasajero getPassengerFromResiber (String numeroVuelo, Date fechaVuelo, String aeropuerto, String carrierCode, Integer intentos){
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Ismael Yubero Moreno
    Company:        Accenture
    Description:    
    
    IN:       
    OUT:      

    History: 
    <Date>                     <Author>                <Change Description>
    10/08/2017                 Ismael Yubero Moreno    Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
        try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');} //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;    
            
            string wsMethod = 'PasajerosVuelo';
            System.debug('Numero de vuelo: ' + numeroVuelo);

            if(R1_CS_Endpoints__c.getInstance(wsMethod) == null || !R1_CS_Endpoints__c.getInstance(wsMethod).R1_CHK_Activo__c){
                    return null;
            }
            String token = R1_CLS_Utilities.getCache('local.sessionCache.token');
            if(token ==null){
                if(intentos<3){
                    intentos= intentos+1;
                    R1_CLS_SendCustomerMDM.login();
                    return getPassengerFromResiber(numeroVuelo,fechaVuelo,aeropuerto,carrierCode,intentos);
                }else{
                    return null;
                }
            }
            System.debug('Ha funcionado el login: '+ token);

            String fecha = formatoFecha(fechaVuelo);
            HttpRequest req = new HttpRequest();
            String endPoint = R1_CS_Endpoints__c.getInstance(wsMethod).R1_TXT_EndPoint__c+ carrierCode + '%20'  +numeroVuelo +'-'+fecha+'-'+aeropuerto+'/passengers';
            System.debug('EndPoint: ' +  endPoint);
            req.setHeader('Authorization', 'Bearer ' + token); 
            req.setEndpoint(endPoint);
            req.setMethod('GET');
            req.setHeader('Content-Type', 'application/json');
            //req.setTimeout(8000);
            TimeOut_Integrations__c timeOut = TimeOut_Integrations__c.getInstance('CKI_Info');
            req.setTimeout((Integer)timeOut.setTimeOut__c);
            
            Http http = new Http();
            HTTPResponse res;
            res = http.send(req);
            System.debug('Body: '+ res.getBody());
            System.debug('Status: '+ res.getStatusCode());
            if(res.getStatusCode()==200 || res.getStatusCode()==201 || res.getStatusCode()==202 || res.getStatusCode()==203 || res.getStatusCode() == 0){
                WpListaPasajero resp = (WpListaPasajero)JSON.deserialize(res.getBody(),WpListaPasajero.class);
                if(resp == null){
                    return null;
                }
                return resp;
            }else{
                if(intentos<3 && res.getStatusCode() == 401){
                    intentos+=1;
                        R1_CLS_SendCustomerMDM.login();
                        return getPassengerFromResiber(numeroVuelo,fechaVuelo,aeropuerto,carrierCode,intentos);
                }else{
                    return null;
                }
            }

        }catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_CLS_Flight_TriggerMethods.getPassengerFromResiber()', '', exc.getmessage()+', '+exc.getLineNumber(), 'Flight');
            return null;
        }
    }

   public static void checkAutomaticCases(List<R1_Flight__c> news, List<R1_Flight__c> olds){
        try{
            Id rtIdExp = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Expediente').getRecordTypeId();
            Set<String> setCheck = new Set<String>();
            for(Integer i=0; i< news.size();i++){
                if(news[i].R1_FLG_TXT_Airport_depart__c!=olds[i].R1_FLG_TXT_Airport_depart__c || news[i].R2_FLG_TXT_Stop_automation__c != olds[i].R2_FLG_TXT_Stop_automation__c || 
                    (news[i].R1_FLG_TXT_Flight_no_oper__c != olds[i].R1_FLG_TXT_Flight_no_oper__c && String.isBlank(olds[i].R1_FLG_TXT_Flight_no_oper__c)) || 
                    (news[i].R1_FLG_TXT_Ap_arrive_orig__c != olds[i].R1_FLG_TXT_Ap_arrive_orig__c && String.isBlank(olds[i].R1_FLG_TXT_Ap_arrive_orig__c))){
                        setCheck.add(news[i].Id);
                    }
            }
            if(!setCheck.isEmpty()){
                List<Case> checkCases = [SELECT Id FROM Case WHERE RecordtypeId=:rtIdExp AND R1_CAS_LOO_Flight__c IN :setCheck AND Type IN ('Retraso','Cancelación') AND Status='Abierto' AND R2_CAS_NUM_Count_Number_Reopen__c =0 AND
                                            R2_CAS_CHK_AUT_Do_not_automate_flag__c = FALSE AND R1_CAS_TXT_PNR__c!=null AND R1_CAS_TXT_PNR__c!='' AND R2_CAS_TXT_TKT_Ticket__c!=null AND R2_CAS_TXT_TKT_Ticket__c!='' AND
                                            R2_CAS_PKL_Country__c !=null AND R2_CAS_PKL_Country__c!='' AND R2_CAS_EMA_Email__c!=null AND R2_CAS_EMA_Email__c!='' AND (NOT R2_CAS_PKL_Vip_Type__c IN ('Jurídico','RPM','OMIC','Demandas','Aviación civil','Vuelos futuros'))];
                if(!checkCases.isEmpty()){
                    updateCases(JSON.serialize(checkCases));
                }
            }
        }catch(Exception e){
            R1_CLS_LogHelper.generateErrorLog('R2_CLS_Flight_TriggerMethods.checkAutomaticCases()', '', e.getmessage()+', '+e.getLineNumber(), 'R1_Flight__c');
        }
    }

    @future
    public static void updateCases(String casos){
        List<Case> checkCases = (List<Case>) JSON.deserialize(casos,List<Case>.class);
        Database.DMLOptions dmo =new Database.DMLOptions();
        dmo.AssignmentRuleHeader.UseDefaultRule= true;
        Database.update(checkCases,dmo);
    }

    public static void mergeFlights(List<R1_Flight__c> news,List<R1_Flight__c> olds){
        try{
            String profileName = [SELECT Name FROM Profile WHERE Id =:UserInfo.getProfileId()].Name;
            if(profileName!='R1_DataAdmin' && olds==null){
                System.debug('CLAIMS');
                List<R1_Flight__c> duplicados = [SELECT Id,Name FROM R1_Flight__c WHERE R1_FLG_TXT_Carrier_code__c =:news[0].R1_FLG_TXT_Carrier_code__c 
                                                                                    AND R1_FLG_TXT_Flight_number__c=:news[0].R1_FLG_TXT_Flight_number__c
                                                                                    AND R1_FLG_DAT_Flight_date_local__c=:news[0].R1_FLG_DAT_Flight_date_local__c];
                if(!duplicados.isEmpty()){
                    news[0].addError('Ya existe el vuelo ' + duplicados[0].Name + ' en el sistema, si piensa que es un error, contacte con su administrador');
                }
            }else if(profileName =='R1_DataAdmin' && olds!=null){
                System.debug('R1_DataAdmin');
                Set<String> clavesVuelo = new Set<String>();
                Set<R1_Flight__c> integracion = new Set<R1_Flight__c>();
                for(Integer i=0; i<news.size(); i++){
                    System.debug(news[i]);
                    System.debug(olds[i]);
                    if(news[i].Name!=olds[i].Name && news[i].R2_FLG_CHK_UpdatingName__c && !olds[i].R2_FLG_CHK_UpdatingName__c){
                        clavesVuelo.add(news[i].Name);
                        integracion.add(news[i]);
                    }
                }
                if(!clavesVuelo.isEmpty()){
                    System.debug('CLAVES DE VUELO');
                    Set<R1_Flight__c> manuales = new Set<R1_Flight__c>([SELECT Id,Name, R2_FLG_TXT_Instructions__c FROM R1_Flight__c WHERE Name IN :clavesVuelo AND CreatedBy.Profile.Name !='R1_DataAdmin']);
                    if(!manuales.isEmpty()){
                        String nuevos = JSON.serialize(news);
                        String antiguos = JSON.serialize(olds);
                        String agentes = JSON.serialize(manuales);
                        String api = JSON.serialize(integracion);
                        changerelatedrecords(nuevos,antiguos,agentes,api);
                    }
                }
            }
        }catch(Exception e){
            R1_CLS_LogHelper.generateErrorLog('R2_CLS_Flight_TriggerMethods.mergeFlights()', '', e.getmessage()+', '+e.getLineNumber(), 'R1_Flight__c');
        }
    }

    @future
    public static void changerelatedrecords(String nuevos, String antiguos,String agentes,String api){
        List<R1_Flight__c> news = (List<R1_Flight__c>)JSON.deserialize(nuevos, List<R1_Flight__c>.class);
        List<R1_Flight__c> olds = (List<R1_Flight__c>)JSON.deserialize(antiguos,List<R1_Flight__c>.class);
        List<R1_Flight__c> manuales = (List<R1_Flight__c>)JSON.deserialize(agentes,List<R1_Flight__c>.class);
        List<R1_Flight__c> integracion = (List<R1_Flight__c>)JSON.deserialize(api,List<R1_Flight__c>.class);
        List<SObject> lst_update = new List<SObject>();
        //Mappeamos Ids Antiguo-Nuevo
        Map<String,String> mapAntiguoNuevo = new Map<String,String>();
        for(R1_Flight__c antiguo : manuales){
            for(R1_Flight__c nuevo : integracion){
                if(antiguo.Name == nuevo.Name){
                    mapAntiguoNuevo.put(antiguo.Id,nuevo.Id);
                    //nuevo.R2_FLG_TXT_Instructions__c = antiguo.R2_FLG_TXT_Instructions__c;
                    //lst_update.add((SObject) nuevo); 
                }
            }
        }
        //Buscamos todos los registros relacionados
        if(!mapAntiguoNuevo.isEmpty()){
            Set<String> claves = mapAntiguoNuevo.keySet();
            //No se puede hacer con SOSL porque las lookup no están soportadas
            //Casos
            for(Case caso : [SELECT Id, R1_CAS_LOO_Flight__c FROM CASE WHERE R1_CAS_LOO_Flight__c IN :claves]){
                caso.R1_CAS_LOO_Flight__c = mapAntiguoNuevo.get(caso.R1_CAS_LOO_Flight__c);
                lst_update.add((SObject) caso);
            }
            //Incidencias
            for(R1_Incident__c incidencia : [SELECT Id, R2_INC_LOO_Flight__c, R2_INC_LOO_PIR_Flight_and_date_FD__c FROM R1_Incident__c WHERE R2_INC_LOO_Flight__c IN :claves 
                                                                                                                                    OR R2_INC_LOO_PIR_Flight_and_date_FD__c IN :claves]){
                incidencia.R2_INC_LOO_Flight__c = (mapAntiguoNuevo.get(incidencia.R2_INC_LOO_Flight__c) !=null ? 
                    mapAntiguoNuevo.get(incidencia.R2_INC_LOO_Flight__c) : incidencia.R2_INC_LOO_Flight__c);
                incidencia.R2_INC_LOO_PIR_Flight_and_date_FD__c = (mapAntiguoNuevo.get(incidencia.R2_INC_LOO_PIR_Flight_and_date_FD__c) !=null ? 
                    mapAntiguoNuevo.get(incidencia.R2_INC_LOO_PIR_Flight_and_date_FD__c) : incidencia.R2_INC_LOO_PIR_Flight_and_date_FD__c);
                lst_update.add((SObject) incidencia);
            }
            //Estados SIP
            for(R1_SIP_Status__c estadoSIP :[SELECT Id, R1_SIS_LOO_Flight__c FROM R1_SIP_Status__c WHERE R1_SIS_LOO_Flight__c IN :claves]){
                estadoSIP.R1_SIS_LOO_Flight__c = mapAntiguoNuevo.get(estadoSIP.R1_SIS_LOO_Flight__c);
                lst_update.add((SObject) estadoSIP);
            }
            //Accesos Sala Vip
            for(R1_VIP_Lounge_Access__c accesoSVip : [SELECT Id, R1_VLI_LOO_Flight__c FROM R1_VIP_Lounge_Access__c WHERE R1_VLI_LOO_Flight__c IN :claves]){
                accesoSVip.R1_VLI_LOO_Flight__c = mapAntiguoNuevo.get(accesoSVip.R1_VLI_LOO_Flight__c);
                lst_update.add((SObject) accesoSVip);
            }
            //Invitados Sala Vip
            for(R1_VIP_lounge_invitees__c invitadoSVip : [SELECT Id, R1_IV_LOO_Flight__c FROM R1_VIP_lounge_invitees__c WHERE R1_IV_LOO_Flight__c IN :claves]){
                invitadoSVip.R1_IV_LOO_Flight__c = mapAntiguoNuevo.get(invitadoSVip.R1_IV_LOO_Flight__c);
                lst_update.add((SObject) invitadoSVip);
            }
            //CKI Info
            for(R2_CKI_info__c infoCKI : [SELECT Id,R2_CKI_LOO_Flight__c FROM R2_CKI_info__c WHERE R2_CKI_LOO_Flight__c IN:claves]){
                infoCKI.R2_CKI_LOO_Flight__c = mapAntiguoNuevo.get(infoCKI.R2_CKI_LOO_Flight__c);
                lst_update.add((SObject) infoCKI);
            }
            //Pagos
            for(R2_Compensation__c pago : [SELECT Id,R2_COM_LOO_Flight__c FROM R2_Compensation__c WHERE R2_COM_LOO_Flight__c IN :claves]){
                pago.R2_COM_LOO_Flight__c = mapAntiguoNuevo.get(pago.R2_COM_LOO_Flight__c);
                lst_update.add((SObject) pago);
            }
            //Prorrateos
            for(R2_Prorrateo__c prorrateo : [SELECT Id,R2_VOU_LOO_Flight__c FROM R2_Prorrateo__c WHERE R2_VOU_LOO_Flight__c IN :claves]){
                prorrateo.R2_VOU_LOO_Flight__c = mapAntiguoNuevo.get(prorrateo.R2_VOU_LOO_Flight__c);
                lst_update.add((SObject) prorrateo);
            }
            //Vouchers
            for(R2_Voucher__c voucher : [SELECT Id, R2_VOU_LOO_Flight__c FROM R2_Voucher__c WHERE R2_VOU_LOO_Flight__c IN :claves]){
                voucher.R2_VOU_LOO_Flight__c = mapAntiguoNuevo.get(voucher.R2_VOU_LOO_Flight__c);
                lst_update.add((SObject) voucher);
            }
            if(!lst_update.isEmpty()){
                update lst_update;
            }
            System.debug('manuales:' + manuales);
            delete new List<R1_Flight__c> (manuales);
        }
    }
}