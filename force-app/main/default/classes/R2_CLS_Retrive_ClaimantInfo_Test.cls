@isTest
private class R2_CLS_Retrive_ClaimantInfo_Test {

	@isTest static void getClaimantInfo_Tests() {
		R1_CLS_LogHelper.throw_exception = false;

			R1_CS_Endpoints__c ep=new R1_CS_Endpoints__c();
			ep.Name='R2_Retrive_Claimant_info';
			ep.R1_CHK_Activo__c=true;
			ep.R1_TXT_EndPoint__c='R2_Retrive_Claimant_info_test';
			insert ep;

			Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());

			List<Case> listCase = [SELECT Id,R1_CAS_TXT_PNR__c,R2_CAS_TXT_TKT_Ticket__c FROM Case WHERE R1_CAS_TXT_PNR__c='MYRVH' AND R2_CAS_TXT_TKT_Ticket__c='0751213052499'];
			
			Test.startTest();
					R2_CLS_Retrive_ClaimantInfo.getClaimantInfo(listCase[0].R1_CAS_TXT_PNR__c, listCase[0].R2_CAS_TXT_TKT_Ticket__c, listCase[0].Id);
		    Test.stopTest();

			List<Case> lstCaseUpdate = [SELECT Id,R2_CAS_TXT_PASS_Name_Passenger__c, AccountId, R1_CAS_LOO_Flight__c FROM Case WHERE Id=:listCase[0].Id];

			// System.assertNotEquals(null, lstCaseUpdate[0].R2_CAS_TXT_PASS_Name_Passenger__c);
			// System.assertNotEquals(null, lstCaseUpdate[0].AccountId);
			// System.assertNotEquals(null, lstCaseUpdate[0].R1_CAS_LOO_Flight__c);

	}

	@isTest static void errorEndpoint() {
		R1_CLS_LogHelper.throw_exception = false;
			Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());

			Test.startTest();
					R2_CLS_Retrive_ClaimantInfo.getClaimantInfo(null, null, null);
		    Test.stopTest();
	}

	@isTest static void getClaimantInfoError401_Tests() {
		R1_CLS_LogHelper.throw_exception = false;

			R1_CS_Endpoints__c ep=new R1_CS_Endpoints__c();
			ep.Name='R2_Retrive_Claimant_info';
			ep.R1_CHK_Activo__c=true;
			ep.R1_TXT_EndPoint__c='R2_Retrive_Claimant_info_test_Error401';
			insert ep;

			Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());

			List<Case> listCase = [SELECT Id,R1_CAS_TXT_PNR__c,R2_CAS_TXT_TKT_Ticket__c FROM Case WHERE R1_CAS_TXT_PNR__c='MYRVH' AND R2_CAS_TXT_TKT_Ticket__c='0751213052499'];

			Test.startTest();
					R2_CLS_Retrive_ClaimantInfo.getClaimantInfo(listCase[0].R1_CAS_TXT_PNR__c, listCase[0].R2_CAS_TXT_TKT_Ticket__c, listCase[0].Id);
		    Test.stopTest();
	}

	@testSetup 
	static void setupData() {

			R1_CS_Endpoints__c eplogin = new R1_CS_Endpoints__c();
			eplogin.Name = 'ETL_Login';
			eplogin.R1_CHK_Activo__c = true;
			eplogin.R1_TXT_EndPoint__c = 'PruebaLogin';
			insert eplogin;

			Id rtAcc = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente').getRecordTypeId();
            Account acc = new Account();
            acc.RecordTypeId = rtAcc;
            acc.LastName = 'ClienteTest';
            acc.PersonEmail = 'test@test.com';
            acc.R1_ACC_PKL_Gender_description__c = 'M';
            acc.R1_ACC_TLF_Phone_Marketing__c = '123456789';
            acc.R1_ACC_PKL_identification_Type__c = '02';
            acc.R1_ACC_TXT_Identification_number__c = '123456789';
			acc.R1_ACC_TXT_Id_Golden_record__c = '10680733';
			insert acc;

			R1_Flight__c vuelo = new R1_Flight__c();
			vuelo.R1_FLG_TXT_Origin__c = 'MAD';
			vuelo.R1_FLG_TXT_Destination__c = 'ORY';
			vuelo.R1_FLG_TXT_Carrier_code__c = 'IB';
			vuelo.R1_FLG_DAT_Flight_date_local__c = Date.newInstance(2017, 07, 04);
			vuelo.R1_FLG_TXT_Flight_number__c = '3400';
			insert vuelo;

			Id rt_expediente = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Expediente').getRecordTypeId();
			Case caso = new Case();
			caso.RecordTypeId = rt_expediente;
			caso.Status = 'Abierto';
			caso.Origin = 'Email';
			caso.Type = 'Demora';
			caso.R1_CAS_TXT_PNR__c = 'MYRVH';
			caso.R2_CAS_TXT_TKT_Ticket__c = '0751213052499';
			insert caso;

	}

	static testMethod void exception_Test(){
			R1_CLS_LogHelper.throw_exception = true;

			R2_CLS_Retrive_ClaimantInfo.getClaimantInfo(null,null,null);
			R2_CLS_Retrive_ClaimantInfo.getAccount(null);
			R2_CLS_Retrive_ClaimantInfo.getflight(null);
			R2_CLS_Retrive_ClaimantInfo.retriveClaimantInfo(null, 0);
			R2_CLS_Retrive_ClaimantInfo.formatFrecuentFly(null);
			R2_CLS_Retrive_ClaimantInfo.formatflightName(null, null);
	}


}