/*---------------------------------------------------------------------------------------------------------------------
Author:         Alberto Puerto Collado
Company:        Accenture
Description:    R2_CLS_InboxTelex class
IN:             
OUT:            

History: 
 <Date>                     <Author>                         <Change Description>
02/03/2018             Alberto Puerto Collado                  Initial Version
----------------------------------------------------------------------------------------------------------------------*/
@isTest
private class R2_CLS_InboxTelex_Test {
	/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Alberto Puerto Collado
    Company:        Accenture
    Description:    Class to test Inbox Message TELEX

    IN:		
    OUT:         

    History:

    <Date>              <Author>           		<Description>
    06/03/2018    Alberto Puerto Collado   		Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest
    public static void initGetTelex_test(){
    	R1_CLS_LogHelper.throw_exception = false;

		// endpoint
		List<R1_CS_Endpoints__c> lstEndPoints = new List<R1_CS_Endpoints__c>();
		R1_CS_Endpoints__c epLogin = new R1_CS_Endpoints__c();
		epLogin.Name = 'ETL_Login';
		epLogin.R1_CHK_Activo__c = true;
		epLogin.R1_TXT_EndPoint__c = 'PruebaLogin';
		lstEndPoints.add(epLogin);

		R1_CS_Endpoints__c ep = new R1_CS_Endpoints__c();
		ep.Name='R2_ReadTelex';
		ep.R1_CHK_Activo__c = true;
		ep.R1_TXT_EndPoint__c = 'R2_ReadTelex';
		lstEndPoints.add(ep);

		insert lstEndPoints;

		//timeOut
		TimeOut_Integrations__c timOut = new TimeOut_Integrations__c();
		timOut.Name = 'WorldTracer';
		timOut.setTimeOut__c = 8000;
		insert timOut;


		Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());
		
		Test.startTest();

		R2_CLS_InboxTelex.initGetTelex();

		Test.stopTest();
		System.assertEquals(0, [SELECT count() FROM R1_Log__c]);

    }


    /*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Alberto Puerto Collado
    Company:        Accenture
    Description:    Exception test management

    IN:		
    OUT:         

    History:

    <Date>              <Author>           		<Description>
    06/03/2018    Alberto Puerto Collado      	Initial Version
    15/10/2018       Eva Sanchez Ruiz           endpoints
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    @isTest
    public static void ServiceRead_TELEXFail_Text(){

        R1_CLS_LogHelper.throw_exception = false;

        // endpoint
        List<R1_CS_Endpoints__c> lstEndPoints = new List<R1_CS_Endpoints__c>();
        R1_CS_Endpoints__c epLogin = new R1_CS_Endpoints__c();
        epLogin.Name = 'ETL_Login';
        epLogin.R1_CHK_Activo__c = true;
        epLogin.R1_TXT_EndPoint__c = 'PruebaLogin';
        lstEndPoints.add(epLogin);

        R1_CS_Endpoints__c ep = new R1_CS_Endpoints__c();
        ep.Name='R2_ReadTelex';
        ep.R1_CHK_Activo__c = true;
        ep.R1_TXT_EndPoint__c = 'TELEXFail';
        lstEndPoints.add(ep);

        insert lstEndPoints;

        //timeOut
        TimeOut_Integrations__c timOut = new TimeOut_Integrations__c();
        timOut.Name = 'WorldTracer';
        timOut.setTimeOut__c = 8000;
        insert timOut;


        Integer intentos = 1;

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());
        R2_CLS_InboxTelex.ServiceRead_TELEX(intentos);
        Test.stopTest();
    }

    @isTest
    public static void ServiceRead_TELEXFail2_Text(){

        R1_CLS_LogHelper.throw_exception = false;

        // endpoint
        List<R1_CS_Endpoints__c> lstEndPoints = new List<R1_CS_Endpoints__c>();
        R1_CS_Endpoints__c epLogin = new R1_CS_Endpoints__c();
        epLogin.Name = 'ETL_Login';
        epLogin.R1_CHK_Activo__c = true;
        epLogin.R1_TXT_EndPoint__c = 'PruebaLogin';
        lstEndPoints.add(epLogin);

        R1_CS_Endpoints__c ep = new R1_CS_Endpoints__c();
        ep.Name='R2_ReadTelex';
        ep.R1_CHK_Activo__c = true;
        ep.R1_TXT_EndPoint__c = 'TELEXFail2';
        lstEndPoints.add(ep);

        insert lstEndPoints;

        //timeOut
        TimeOut_Integrations__c timOut = new TimeOut_Integrations__c();
        timOut.Name = 'WorldTracer';
        timOut.setTimeOut__c = 8000;
        insert timOut;

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());
        R2_CLS_InboxTelex.ServiceRead_TELEX(1);
        Test.stopTest();
    }

        @isTest
    public static void ServiceRead_TELEXFail3_Text(){

        R1_CLS_LogHelper.throw_exception = false;

        // endpoint
        List<R1_CS_Endpoints__c> lstEndPoints = new List<R1_CS_Endpoints__c>();
        R1_CS_Endpoints__c epLogin = new R1_CS_Endpoints__c();
        epLogin.Name = 'ETL_Login';
        epLogin.R1_CHK_Activo__c = true;
        epLogin.R1_TXT_EndPoint__c = 'PruebaLogin';
        lstEndPoints.add(epLogin);

        R1_CS_Endpoints__c ep = new R1_CS_Endpoints__c();
        ep.Name='R2_ReadTelex';
        ep.R1_CHK_Activo__c = true;
        ep.R1_TXT_EndPoint__c = 'TELEXFail3';
        lstEndPoints.add(ep);

        insert lstEndPoints;

        //timeOut
        TimeOut_Integrations__c timOut = new TimeOut_Integrations__c();
        timOut.Name = 'WorldTracer';
        timOut.setTimeOut__c = 8000;
        insert timOut;

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());
        R2_CLS_InboxTelex.ServiceRead_TELEX(1);
        //Para que entre por el error de 3 intentos.
        R2_CLS_InboxTelex.ServiceRead_TELEX(2);
        Test.stopTest();
    }

            @isTest
    public static void exception_Test(){

        R1_CLS_LogHelper.throw_exception = false;

        // endpoint
        List<R1_CS_Endpoints__c> lstEndPoints = new List<R1_CS_Endpoints__c>();
        R1_CS_Endpoints__c epLogin = new R1_CS_Endpoints__c();
        epLogin.Name = 'ETL_Login';
        epLogin.R1_CHK_Activo__c = true;
        epLogin.R1_TXT_EndPoint__c = 'PruebaLogin';
        lstEndPoints.add(epLogin);

        R1_CS_Endpoints__c ep = new R1_CS_Endpoints__c();
        ep.Name='R2_ReadTelex';
        ep.R1_CHK_Activo__c = true;
        ep.R1_TXT_EndPoint__c = 'TELEXFail3';
        lstEndPoints.add(ep);

        insert lstEndPoints;

        //timeOut
        TimeOut_Integrations__c timOut = new TimeOut_Integrations__c();
        timOut.Name = 'WorldTracer';
        // Para que de el error de CalloutException, le bajo el tiempo a 0 y asi no le da tiempo a hacer la llamada.
        timOut.setTimeOut__c = 0;
        insert timOut;

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());
        R2_CLS_InboxTelex.ServiceRead_TELEX(1);
        Test.stopTest();
    }


    @isTest
    static void exception_Test2() {

    // Para que pase po rel ultimo catch.
        R1_CLS_LogHelper.throw_exception = true;


        Test.startTest();
                R2_CLS_InboxTelex.ServiceRead_TELEX(1);
        Test.stopTest();
    } 

}