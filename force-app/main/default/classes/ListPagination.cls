public with sharing class ListPagination {

    public Integer pagenumber {get; set;}
    public Integer numberOfRecords {get; set;}
    public Integer pageSize {get; set;}

}