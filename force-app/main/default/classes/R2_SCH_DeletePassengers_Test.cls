@isTest
public with sharing class R2_SCH_DeletePassengers_Test {
	@isTest
  static void execute_Test() {

    R1_CLS_LogHelper.throw_exception = false;

    String CRON_EXP = '0 0 0 3 9 ? 2022';
    String CRON_EXP_IN_DATETIME = '2022-09-03 00:00:00';
    String CRON_JOB_NAME = 'R2_SCH_Retrive_Compensation_Job';

    //List<Case> listCases = [SELECT Id, Parent.Status,CreatedDate, R2_CAS_FOR_Destination__c,R2_CAS_PKL_Country__c, R2_CAS_TXT_TKT_Ticket__c,R1_CAS_TXT_PNR__c,Type FROM Case];

    
    Test.startTest();
        // Schedule the test job
        String jobId = System.schedule(CRON_JOB_NAME, CRON_EXP, new R2_SCH_DeletePassengers());
        // Get the information from the CronTrigger API object
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime, CronJobDetailId FROM CronTrigger WHERE Id = :jobId];
        //verica que se agrego la tarea
        //System.assertNotEquals(null, ct);
        ////verificamos nombre tarea
        //CronJobDetail cjd = [SELECT Name FROM CronJobDetail WHERE Id= :ct.CronJobDetailId];
        //System.assertEquals(CRON_JOB_NAME, cjd.Name);
        //// verifica la expresion
        //System.assertEquals(CRON_EXP, ct.CronExpression);
        //// verifica la froxima fecha de ejecucion
        //System.assertEquals(CRON_EXP_IN_DATETIME, String.valueOf(ct.NextFireTime));
        //// verifica que el trabajo no se ha ejecutado
        //System.assertEquals(0, ct.TimesTriggered);

    Test.stopTest();

    System.assertEquals(0, [SELECT count() FROM R1_Log__c]);
  }
}