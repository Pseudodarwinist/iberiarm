@isTest
private class R2_SP_SocialPostTriggerMethods_Test {
	
	@isTest static void completeMilestone_Test() {
		R1_CLS_LogHelper.throw_exception = false;
        Id rtId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Comunicaciones entrantes').getRecordTypeId();
        Id recordTypeIdAcc = [SELECT Id FROM recordType WHERE Name like 'Cuenta personal' LIMIT 1].Id;
        Id rtIdCon = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Consulta').getRecordTypeId();
        Id sla = [Select id from SlaProcess where IsVersionDefault =true and name ='Casos RRSS Tinkle' limit 1].Id;
        Account acc = new Account();
            acc.RecordTypeId = recordTypeIdAcc;
            acc.LastName = 'CuentaVieja';
            acc.FirstName = 'Abc';
            acc.PersonBirthdate = date.newInstance(1998, 10, 21);
        insert acc;
        Entitlement ent = new Entitlement();
            ent.Name = 'Test';
            ent.SlaProcessId = sla;
            ent.AccountId = acc.Id;
        insert ent;
        Case cas = new Case();
            cas.RecordTypeId = rtId;
            cas.Status = 'Abierto a la espera de la info del Cliente';
            cas.Origin = 'Redes sociales';
            cas.AccountId = acc.Id; 
            cas.R1_CAS_PKL_Idioma__c = 'es';
            cas.Type = 'Demora';
            cas.R2_CAS_CHK_FirstResponse__c = false;
            cas.EntitlementId = ent.id;
        insert cas;

        SocialPersona persona = new SocialPersona();
            persona.Name = 'Test';
            persona.RealName ='Test';
            persona.Provider = 'Twitter';
            persona.MediaProvider = 'Test';
            persona.ExternalId = 'Test';
            persona.ParentId = acc.id;
        insert persona;

        SocialPost post = new SocialPost();
            post.Name = 'Test';
            post.Content = 'Test';
            post.Posted = DateTime.now();
            post.PostUrl = 'Test';
            post.Provider = 'Twitter';
            post.MessageType = 'Post';
            post.ExternalPostId = 'Test';   
            post.R6PostId = 'Test';
            post.ParentId = cas.Id;
            post.PersonaId = persona.Id;
        Test.startTest();
        	insert post;
        Test.stopTest();
	}
	
	@isTest static void completeMilestone_Test2() {
		R1_CLS_LogHelper.throw_exception = false;
        Id rtId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Comunicaciones entrantes').getRecordTypeId();
        Id recordTypeIdAcc = [SELECT Id FROM recordType WHERE Name like 'Cuenta personal' LIMIT 1].Id;
        Id rtIdCon = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Consulta').getRecordTypeId();
        Id sla = [Select id from SlaProcess where IsVersionDefault =true and name ='Casos RRSS Tinkle' limit 1].Id;
        Account acc = new Account();
            acc.RecordTypeId = recordTypeIdAcc;
            acc.LastName = 'CuentaVieja';
            acc.FirstName = 'Abc';
            acc.PersonBirthdate = date.newInstance(1998, 10, 21);
        insert acc;
        Entitlement ent = new Entitlement();
            ent.Name = 'Test';
            ent.SlaProcessId = sla;
            ent.AccountId = acc.Id;
        insert ent;
        Case cas = new Case();
            cas.RecordTypeId = rtId;
            cas.Status = 'Abierto';
            cas.Origin = 'Redes sociales';
            cas.AccountId = acc.Id; 
            cas.R1_CAS_PKL_Idioma__c = 'es';
            cas.Type = 'Demora';
            cas.R2_CAS_CHK_FirstResponse__c = false;
            cas.EntitlementId = ent.id;
        insert cas;

        SocialPost post = new SocialPost();
            post.Name = 'Test';
            post.Content = 'Test';
            post.Posted = DateTime.now();
            post.PostUrl = 'Test';
            post.Provider = 'Twitter';
            post.MessageType = 'Post';
            post.ExternalPostId = 'Test';   
            post.R6PostId = 'Test';
            post.ParentId = cas.Id;
        Test.startTest();
        	insert post;
        Test.stopTest();
	}

	@isTest static void exception_test(){
		R1_CLS_LogHelper.throw_exception = true;

		R2_SP_SocialPostTriggerMethods.completeMilestone(null);
	}
}