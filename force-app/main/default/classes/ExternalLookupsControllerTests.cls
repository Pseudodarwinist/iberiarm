@IsTest
private with sharing class ExternalLookupsControllerTests {
    
    @IsTest
    static void validategetExternalLookupWithVisibility() {

        Test.startTest();

        generateMockedData();
        ExternalLookupsController.lookupsVisible = true;

        List<archive_lookups__x> lookups = ExternalLookupsController.getExternalLookup(ExternalLookupsController.LOOKUP_TYPE_EMAIL_TYPE);

        System.assertEquals(lookups.size(),  ExternalLookupsController.mockedEmailLookups.size(), 'Lookups incorrect');

        Test.stopTest();
    }

    @IsTest
    static void validategetExternalLookupWithNoVisibility() {
        Test.startTest();

        generateMockedData();
        ExternalLookupsController.lookupsVisible = false;

        List<archive_lookups__x> lookups = ExternalLookupsController.getExternalLookup(ExternalLookupsController.LOOKUP_TYPE_EMAIL_TYPE);

        System.assertEquals(lookups,  null, 'Lookups should be null as there is no visibility');

        Test.stopTest();
    }


    private static void generateMockedData() {
        ExternalLookupsController.mockedEmailLookups = new List<archive_lookups__x>();

        archive_lookups__x lookupPredeparture = new archive_lookups__x(
            id_lookup__c = 'emailtype',
            value__c = 'Predeparture'
        );
        ExternalLookupsController.mockedEmailLookups.add(lookupPredeparture);

        archive_lookups__x lookupCheckin = new archive_lookups__x(
            id_lookup__c = 'emailtype',
            value__c = 'Checkin'
        );

        ExternalLookupsController.mockedEmailLookups.add(lookupCheckin);
    }
}