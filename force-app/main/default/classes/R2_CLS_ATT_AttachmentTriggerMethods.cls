public class R2_CLS_ATT_AttachmentTriggerMethods {

    public static void asignarAttachmentAPadre(List<Attachment> lstAtt){
        try{
         if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}

        Id rtIdPas = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Pasaje').getRecordTypeId();
        Id rtIdEq = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Equipaje').getRecordTypeId();
        List<Case> lstCasos = new List<Case>();
        
        lstCasos = [SELECT id,RecordTypeId, ParentId from Case where id = :lstAtt[0].parentid limit 1];
        
        if(!lstCasos.isEmpty()){
            if(lstCasos[0].RecordTypeId == rtIdPas || lstCasos[0].RecordTypeId == rtIdEq){
                for(Attachment att : lstAtt){
                    att.parentId = lstCasos[0].ParentId;
                }
            }
            }
        }
        catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_CLS_ATT_AttachmentTriggerMethods.asignarAttachmentAPadre()', '', exc.getmessage()+', '+exc.getLineNumber(), 'Case');
        }
        
    }

}