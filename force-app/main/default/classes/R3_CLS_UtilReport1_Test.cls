/*---------------------------------------------------------------------------------------------------------------------
    Author:         ICA
    Company:        LeadClic
    Description:    Report1 Utils

    History:
     <Date>                     <Author>                         <Change Description>
    08/08/2019            		ICA-LCS                				Initial Version
	19/09/2019					ICA-LCS                				Updated. Change the Setup method, delete R2_REA_PKL_Delay_Code__c='CE' to use R2_REA_TXT_Delay_No__c ='93.0' instead
 ----------------------------------------------------------------------------------------------------------------------*/

/**
* @author TCK&LCS
* @date 08/08/2019
* @description Test class for R3_CLS_UtilReport1 and R3_JOB_ExecuteReport1Creation classes
*/
@isTest
public with sharing class R3_CLS_UtilReport1_Test {

	/**
	* @author TCK&LCS
	* @date 08/08/2019
	* @description Setup method to insert required objects to test execution
	*/
   @testSetup
	public static void setup(){
		R1_byPass__c bpass = new R1_byPass__c();
        bpass.Name = 'bypass(User)';
        bpass.SetupOwnerId = userinfo.getuserid();
        bpass.R1_CHK_skip_trigger__c = true;
        bpass.R1_TXT_Objects__c = 'Account';
        insert bpass;

		List<Case> case2Insert = new List<Case>();
		List<R2_Reasons__c> reasons2Insert = new List<R2_Reasons__c>();
		List<R1_Incident__c> incident2Insert = new List<R1_Incident__c>();

		R1_Flight__c vuelo = new R1_Flight__c();
        vuelo.Name = 'TestVuelo';
        vuelo.R1_FLG_TXT_Flight_number__c='1234';
        vuelo.R1_FLG_DAT_Flight_date_local__c= Date.newInstance(1991, 2, 4);
        vuelo.R1_FLG_DATH_Schedule_depart_time__c = Datetime.newInstance(1991, 2, 4);
		vuelo.R1_FLG_DATH_Onblocks_act__c = Datetime.now().addHours(4);
		vuelo.R1_FLG_DATH_Sched_arrive__c= Datetime.now();
        vuelo.R1_FLG_TXT_Carrier_code__c='IB';
        vuelo.R1_FLG_TXT_Destination__c='BCN';
        vuelo.R1_FLG_TXT_Origin__c='MAD';
        insert vuelo;
		
		//RT del caso expendiente
        Id rt_expediente = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('R2_File').getRecordTypeId();
        Case caso1 = new Case();
        caso1.RecordTypeId = rt_expediente;
        caso1.Status = 'Abierto';
        caso1.Origin = 'Email';
        caso1.Type = 'Retraso';
		caso1.R1_CAS_LOO_Flight__c = vuelo.Id;
		case2Insert.add(caso1);

		Case caso2 = new Case();
        caso2.RecordTypeId = rt_expediente;
        caso2.Status = 'Abierto';
        caso2.Origin = 'Email';
        caso2.Type = 'Retraso';
		caso2.R1_CAS_LOO_Flight__c = vuelo.Id;
		case2Insert.add(caso2);

		Case caso3 = new Case();
        caso3.RecordTypeId = rt_expediente;
        caso3.Status = 'Abierto';
        caso3.Origin = 'Email';
        caso3.Type = 'Retraso';
		caso3.R1_CAS_LOO_Flight__c = vuelo.Id;
		case2Insert.add(caso3);

		insert case2Insert;

		Case ch = new Case();
        ch.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Equipaje').getRecordTypeId();
        ch.origin = 'sample2';
        ch.parentId = caso1.Id;
        insert ch;

		R1_Incident__c incidencia1 = new R1_Incident__c();
        incidencia1.R1_INC_LOO_Case__c = ch.Id;
        incidencia1.R2_INC_LOO_Case__c = caso1.Id;
		incidencia1.R2_INC_LOO_Flight__c = vuelo.Id;
        insert incidencia1;

		R2_Reasons__c reason1 = new R2_Reasons__c();
		reason1.R2_REA_TXT_Delay_No__c ='DN1';
		reason1.R2_REA_TXT_Delay_Code__c ='DC1';
		reason1.R2_REA_NUM_Delay_Time__c = 10;
		reason1.R2_REA_TXT_Delay_No__c ='93.0';
		reason1.R2_REA_MSDT_Incidence__c = incidencia1.Id;
		reasons2Insert.add(reason1);

		R2_Reasons__c reason2 = new R2_Reasons__c();
		reason2.R2_REA_TXT_Delay_No__c ='DN2';
		reason2.R2_REA_TXT_Delay_Code__c ='DC2';
		reason2.R2_REA_NUM_Delay_Time__c = 20;
		reason2.R2_REA_TXT_Delay_No__c ='93.0';
		reason2.R2_REA_MSDT_Incidence__c = incidencia1.Id;
		reasons2Insert.add(reason2);

		R2_Reasons__c reason3 = new R2_Reasons__c();
		reason3.R2_REA_TXT_Delay_No__c ='DN3';
		reason3.R2_REA_TXT_Delay_Code__c ='DC3';
		reason3.R2_REA_NUM_Delay_Time__c = 30;
		reason3.R2_REA_TXT_Delay_No__c ='93.0';
		reason3.R2_REA_MSDT_Incidence__c = incidencia1.Id;
		reasons2Insert.add(reason3);
		insert reasons2Insert;

	}

	/**
	* @author TCK&LCS
	* @date 08/08/2019
	* @description Checks if report 1 was created properly
	*/
	@isTest
	public static void testOK(){
		test.startTest();
			Map<Id,R1_Flight__c> flightMap = new Map<Id,R1_Flight__c>([Select Id From R1_Flight__c]);
			R3_CLS_UtilReport1.checkReport1Creation(flightMap.keySet());
			List<TCK1_Report1__c> flightList = [Select Id,TCK1_RP1_TEX_Causas_Retraso__c from TCK1_Report1__c];
			System.assertEquals(1, flightList.size());
			R3_CLS_UtilReport1.checkReport1Creation(flightMap.keySet());
			List<TCK1_Report1__c> flightList1 = [Select Id,Name,TCK1_RP1_TEX_Causas_Retraso__c from TCK1_Report1__c];
			//Check anymore Report1 were created
			System.assertEquals(1, flightList1.size(),'Report should has been created');
		test.stopTest();

	}

	/**
	* @author TCK&LCS
	* @date 08/08/2019
	* @description Checks if report 1 is not created due to its departure airport is one should not considered as special condition
	*/
	@isTest
	public static void testKO(){
		String departAirport;
		for(TCK_CodigoCausaCE__mdt ce :[SELECT Id,MasterLabel, TCK_Aeropuerto_salida__c  FROM TCK_CodigoCausaCE__mdt Where TCK_Aeropuerto_salida__c Like '!%' Limit 1]){
			departAirport = ce.TCK_Aeropuerto_salida__c.remove('!');
		}
		R1_Flight__c v = [Select Id, R1_FLG_TXT_Airport_depart__c From R1_Flight__c];
		v.R1_FLG_TXT_Airport_depart__c=departAirport;
		update v;
		test.startTest();
			Map<Id,R1_Flight__c> flightMap = new Map<Id,R1_Flight__c>([Select Id From R1_Flight__c]);
			R3_CLS_UtilReport1.checkReport1Creation(flightMap.keySet());
			List<TCK1_Report1__c> flightList = [Select Id,TCK1_RP1_TEX_Causas_Retraso__c from TCK1_Report1__c];
			System.assertEquals(0, flightList.size(),'No report should has been created');
		test.stopTest();

	}


	/**
	* @author TCK&LCS
	* @date 08/08/2019
	* @description Test for R3_JOB_ExecuteReport1Creation schedule
	*/
	@isTest
	public static void testSch(){
		test.startTest();

			SchedulableContext sc = null;
			R3_JOB_ExecuteReport1Creation tsc = new R3_JOB_ExecuteReport1Creation();
			tsc.execute(sc);
			List<TCK1_Report1__c> flightList1 = [Select Id,Name,TCK1_RP1_TEX_Causas_Retraso__c from TCK1_Report1__c];
			System.assertEquals(1, flightList1.size());
			//Run assynchronous job again to check there aren't more reports created
			R3_JOB_ExecuteReport1Creation tsc2 = new R3_JOB_ExecuteReport1Creation();
			tsc2.execute(sc);
			List<TCK1_Report1__c> flightList2 = [Select Id,Name,TCK1_RP1_TEX_Causas_Retraso__c from TCK1_Report1__c];
			System.assertEquals(1, flightList2.size(),'Report should has been created');
		test.stopTest();
	}

	/**
	* @author TCK&LCS
	* @date 08/08/2019
	* @description Method to force a DML exception.Read Only I2 Profile is required
	*/
	@isTest
	public static void testDMLExcept(){
		//
		Profile p = [SELECT Id FROM Profile WHERE Name = 'Read Only I2'];
        if(p!=null){

			User u1 = new User(Alias = 'standt1',Country='Spain',Email='user1@example.com',EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',LocaleSidKey='en_US',ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='user1@example.com.iberia.dev');
			insert u1;
			
			Map<Id,R1_Flight__c> flightMap = new Map<Id,R1_Flight__c>([Select Id From R1_Flight__c]);

			System.runAs(u1){
				R3_CLS_UtilReport1.checkReport1Creation(flightMap.keySet());
			}
		}
	}

	/**
	* @author TCK&LCS
	* @date 08/08/2019
	* @description Test to check report1 is not created if there is less than 3 Cases
	*/
	@isTest
	public static void testCheckNotReport1(){
		Map<Id,R1_Flight__c> flightMap = new Map<Id,R1_Flight__c>([Select Id From R1_Flight__c]);
		//Delete 1 case in order to check not report1 is created
		Case c = [Select Id from case limit 1];
		delete c;
		R3_CLS_UtilReport1.checkReport1Creation(flightMap.keySet());
		List<TCK1_Report1__c> flightList = [Select Id,TCK1_RP1_TEX_Causas_Retraso__c from TCK1_Report1__c];
		System.assertEquals(0, flightList.size(),'Report should not has been created');
		R3_CLS_UtilReport1.checkReport1Creation(flightMap.keySet());
	}

	/**
	* @author TCK&LCS
	* @date 08/08/2019
	* @description Checks after testCheckNotReport1, if a case is inserted,the report will be created
	*/	
	@isTest
	public static void afterTestCheckNotReport1(){
		testCheckNotReport1();
		//Creates the 3rd case
		Map<Id,R1_Flight__c> flightMap = new Map<Id,R1_Flight__c>([Select Id From R1_Flight__c]);
		Case caso = new Case();
        caso.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('R2_File').getRecordTypeId();
        caso.Status = 'Abierto';
        caso.Origin = 'Email';
        caso.Type = 'Retraso';
		caso.R1_CAS_LOO_Flight__c = flightMap.values().get(0).Id;
		insert caso;
		List<Case> cList=[Select Id,R1_CAS_LOO_Flight__c,R1_CAS_LOO_Flight__r.Report_1__c from Case where R1_CAS_LOO_Flight__c IN: flightMap.keySet() AND RecordType.DeveloperName =: Label.RTExpedienteCaso];
		test.startTest();
			R3_CLS_UtilReport1.checkReport1Creation(flightMap.keySet());
			List<TCK1_Report1__c> flightList = [Select Id,TCK1_RP1_TEX_Causas_Retraso__c from TCK1_Report1__c];
			System.assertEquals(1, flightList.size());
			R3_CLS_UtilReport1.checkReport1Creation(flightMap.keySet());
		test.stopTest();
	}
	

	/**
	* @author TCK&LCS
	* @date 08/08/2019
	* @description Validates the report is not created if there are no reasons with R2_REA_TXT_Delay_No__c ='93.0'
	*/	
	@isTest
	public static void checkNoReportWithoutReasons(){
		List<R2_Reasons__c> reasons2Update = new List<R2_Reasons__c>();
		for(R2_Reasons__c r : [Select Id, R2_REA_TXT_Delay_No__c From R2_Reasons__c Where R2_REA_TXT_Delay_No__c ='93.0']){
			r.R2_REA_TXT_Delay_No__c = '94.5';
			reasons2Update.add(r);
		}
		update reasons2Update;

		test.startTest();
			System.debug('Start');
			Map<Id,R1_Flight__c> flightMap = new Map<Id,R1_Flight__c>([Select Id From R1_Flight__c]);
			R3_CLS_UtilReport1.checkReport1Creation(flightMap.keySet());
			List<TCK1_Report1__c> flightList = [Select Id,TCK1_RP1_TEX_Causas_Retraso__c from TCK1_Report1__c];
			//Check any Report1 was created
			System.assertEquals(0, flightList.size());
		test.stopTest();
	}
}