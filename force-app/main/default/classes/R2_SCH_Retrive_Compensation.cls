/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Jaime Ascanta
    Company:        Accenture
    Description:      Job to get cases

    IN:
    OUT:

    History:
     <Date>                     <Author>                <Change Description>
    07/11/2017                  Jaime Ascanta            Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
global class R2_SCH_Retrive_Compensation implements Schedulable{ //implements Schedulable 

    //public static final Id idRT = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Pasaje').getRecordTypeId();
    
    global static void execute(System.SchedulableContext sc) { //System.SchedulableContext sc
        try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}

                // recuperamos casos para automatizacion
                List<Case> casList = getCases();
                System.debug('*** casos para poner en cola '+ casList.size() +': ' +  casList);

                if(!casList.isEmpty()) {
                    System.debug('*** envia lista casos a R2_QUE_Retrive_Compensation');
                    R2_QUE_Retrive_Compensation job = new R2_QUE_Retrive_Compensation(casList);
                    ID jobID =  System.enqueueJob(job);
                    System.debug('*** primer proceso, jobID: ' + jobID);
                }else{
                    System.debug('*** no hay casos que cumplan los criterios para automatizacion.');
                }
        }catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_SCH_Retrive_Compensation.execute()', '', exc.getmessage()+', '+exc.getLineNumber(), '');
        }
    }

    /*---------------------------------------------------------------------------------------------------------------------
    Author:         Jaime Ascanta
    Company:        Accenture
    Description:   Extrae casos
    IN:
    OUT:

    History:
     <Date>                     <Author>                         <Change Description>
     08/11/2017                         Jaime Ascanta                   Initial Version
    ----------------------------------------------------------------------------------------------------------------------*/
    @TestVisible private static List<Case> getCases(){
        try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');}

            Id idRT = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Pasaje').getRecordTypeId();

            // days number - recoge numero dias desde custom label
            Integer daysForCompensation = getDayCompensation();
            System.debug('*** dias para compensacion: ' + daysForCompensation);
            // daysAgo = fecha actual - numero daysForCompensation
            DateTime daysAgo = DateTime.now().addDays(-daysForCompensation);
            if(Test.isRunningTest()) daysAgo = DateTime.now().addDays(8);
            Map<String,String> mapColas;
                mapColas = new Map<String,String>();
                List<Group> lstColas = new List<Group>();
                lstColas = [SELECT id,Name from Group where Type = 'Queue'];
                for(Group cola : lstColas){
                    mapColas.put(cola.Name,cola.id);
                }
            String idAutomatica;
            idAutomatica = mapColas.get('Automáticos');

            System.debug('*** daysAgo: ' + daysAgo);
            List<Case> lstprocess = new List<Case>();
            List<Case> lstmanual = new List<Case>();
            List<Case> casList = [SELECT Id,(SELECT Id, RecordTypeId, Parent.R1_CAS_TXT_PNR__c, Parent.R2_CAS_TXT_TKT_Ticket__c, AccountId,R1_CAS_LOO_Flight__c,R1_CAS_LOO_Incidences__c, 
                                            Parent.R1_CAS_LOO_Flight__c,Parent.R1_CAS_LOO_Incidences__c,Parent.AccountId, Parent.Type, Parent.R1_CAS_PKL_Subtype__c, ParentId,
                                            Parent.R1_CAS_TXT_Charge_account__c, Parent.R1_CAS_TXT_Budgetary_center__c, Parent.R2_CAS_FOR_Destination__c, Parent.R1_CAS_PKL_Idioma__c,Parent.R1_CAS_LOO_Flight__r.R1_FLG_TXT_Ap_arrive_orig__c,Parent.R1_CAS_LOO_Flight__r.R1_FLG_TXT_Flight_no_oper__c,
                                            Parent.R2_CAS_CHK_AUT_Do_not_automate_flag__c, Parent.R2_CAS_PKL_Country__c,Parent.R2_CAS_TXT_Primary_Frecuent_Flyer_Card__c, Parent.R1_CAS_LOO_Flight__r.R1_FLG_TXT_Carrier_code_oper__c, 
                                            Parent.R1_CAS_LOO_Flight__r.R1_FLG_TXT_Flight_number__c,Parent.R2_CAS_EMA_PASS_Email_Passenger__c, Parent.Id, R2_CAS_PKL_Manage__c, R2_CAS_FOR_carrier_code_oper__c,
                                            Parent.R1_CAS_LOO_Flight__r.R1_FLG_DAT_Flight_date_local__c , Type,R1_CAS_PKL_Subtype__c,R1_CAS_TXT_Charge_account__c,R1_CAS_TXT_Budgetary_center__c,R2_CAS_TXT_Primary_Frecuent_Flyer_Card__c,Parent.R2_CAS_EMA_Email__c
                                            FROM CASES 
                                            WHERE 
                                                CASE.STATUS='Abierto' 
                                                AND (CASE.Type = 'Retraso' OR CASE.Type = 'Cancelación') 
                                                AND RecordType.Name = 'Pasaje' limit 1) 
                                FROM Case
                                WHERE CreatedDate <=:daysAgo 
                                    AND R2_CAS_NUM_Number_of_rebounds__c = 0 
                                    AND OwnerId=:idAutomatica 
                                    AND Status = 'Abierto'
                                    AND (Type = 'Retraso' OR Type = 'Cancelación') 
                                    AND (R2_CAS_CHK_AUT_Do_not_automate_flag__c=false AND R2_CAS_PKL_Exp_Pago_Claims_Auto__c=null)
                                    AND (R1_CAS_LOO_Flight__c != null AND R1_CAS_LOO_Flight__r.R2_FLG_TXT_Stop_automation__c != true )
                                    AND (R1_CAS_TXT_PNR__c != '' AND R1_CAS_TXT_PNR__c != null)
                                    AND (Type != '' AND Type != null)
                                    AND (R2_CAS_TXT_TKT_Ticket__c != '' AND R2_CAS_TXT_TKT_Ticket__c != null)
                                    AND (R2_CAS_PKL_Country__c != '' AND R2_CAS_PKL_Country__c != null)
                                    AND (R2_CAS_FOR_Destination__c != '' AND R2_CAS_FOR_Destination__c != null) 
                                    AND (NOT R1_CAS_LOO_Flight__r.Name LIKE 'IB5%')
                                    AND RecordType.Name = 'Expediente'];

            for(Case padre : casList){
                if(padre.CASES.size()==1){
                    lstprocess.add(padre.CASES[0]);
                }
                else{
                    padre.R2_CAS_CHK_AUT_Do_not_automate_flag__c=true;
                    lstmanual.add(padre);
                }
            }
            if(!lstmanual.isEmpty()){
                Database.DMLOptions dmo =new Database.DMLOptions();
                dmo.AssignmentRuleHeader.UseDefaultRule= true;
                Database.update(lstmanual,dmo);
            }
            System.debug('Estos son los dos casos que van a tratarse' +casList);     
            return lstprocess;

        }catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_SCH_Retrive_Compensation.getCases()', '', exc.getmessage()+', '+exc.getLineNumber(), 'Case');
            return null;
        }
    }

    /*---------------------------------------------------------------------------------------------------------------------
    Author:         Jaime Ascanta
    Company:        Accenture
    Description:    extrae numero de dias validos para que se aplique la compensacion,a partir de un custom label
    IN:
    OUT:

    History:
     <Date>                     <Author>                         <Change Description>
     08/11/2017                         Jaime Ascanta                   Initial Version
    ----------------------------------------------------------------------------------------------------------------------*/
    @TestVisible private static Integer getDayCompensation(){
        try{
            Integer days = (Integer) Integer.valueOf(Label.R2_Days_for_compensation);
            return days;
        }catch(Exception e){
            System.debug('*** Exception getDayCompensation(): ' + e);
            return 8;
        }
    }


}