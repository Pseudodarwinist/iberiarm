@RestResource(urlMapping='/fasttrack/')
global with sharing class R2_CLS_Fast_Track {

	
	global class WPResponse{
        public String status;
        public String comments;
    }


	@HttpPost
    global static WPResponse createAccess(String passengerName, String pnr,	String origin, String oneWolrdCode, String destination, String operationCompany, String flight,String flightDate, String classe, String vipPermissions, String idAdIndicator,String vipLounge, String seatNumber,String boardingNumber, String marketingCode, String flightMarketing,  String status, String etkdNumber, String ffNumber, String vipLoungePosition, String specialMeals, String specialServices, String specialPAX, String boardingGate, String departureTime, String minor, String inboundAirline, String inboundFlight, String inboundClass, String inboundOrigin, String operationType, String recordId) {
    	
	    WPResponse wp = new WPResponse();
    	if(recordId != null && recordId != ''){
    		List<R1_VIP_Lounge_Access__c> accs = [SELECT Id,R1_VLI_PKL_Access_status__c, R2_VLI_Record_Id__c FROM R1_VIP_Lounge_Access__c WHERE   R2_VLI_Record_Id__c =:recordId];
    		if(operationType == 'Actualizar'){
	    		System.debug('Esta es una petidcion de cancelacion ' + recordId);
	    		//List<R1_VIP_Lounge_Access__c> accs = [SELECT Id,R1_VLI_PKL_Access_status__c, R2_VLI_Record_Id__c FROM R1_VIP_Lounge_Access__c WHERE   R2_VLI_Record_Id__c =:recordId];
	    		//System.debug('Esta es una petidcion de cancelacion ' + accs[0]);
	    		if(!accs.isEmpty()){
	    			accs[0].R1_VLI_PKL_Access_status__c = 'Denegado';
	    			update accs[0];
		    		wp.status = 'KO';
		    		wp.comments = 'POR FAVOR, DIRIJASE AL MOSTRADOR';		
		    	}else{
		    		wp.status = 'KO';
		    		wp.comments = 'POR FAVOR, DIRIJASE AL MOSTRADOR';
		    	}
	    	}else{
			    if(accs.isEmpty()){
			    	System.debug('Se ejecuta la funcion de comprobar si esta activo el puesto de la sala vip');
			        System.debug('NAME DEL PUESTO: ' + vipLounge + ' ' + vipLoungePosition);
			        String aux = vipLounge + ' ' + vipLoungePosition;
			        List<Salas_Vip__c> sala = [SELECT Id, Status__c , Name FROM Salas_VIP__c WHERE Name = : aux];
			        //System.debug('ESTADO DEL PUESTO: ' + sala[0].Status__c);
			        if(!sala.isEmpty()){
				        if(sala[0].Status__c == true){			             
					        R1_VIP_Lounge_Access__c acceso = new R1_VIP_Lounge_Access__c();
					        if(passengerName != null && passengerName != ''){
					        	acceso.R1_VLI_TXT_passenger_name__c = passengerName;
					        }
					        if(pnr != null && pnr != ''){
					        	acceso.R1_VLI_TXT_PNR__c = pnr;
					        }
					        if(origin != null && origin != ''){
					        	acceso.R1_VLI_TXT_Origin__c = origin;
					        }
					        if(oneWolrdCode != null && oneWolrdCode != ''){
					        	acceso.R1_VLI_TXT_OneWolrd_code__c = oneWolrdCode;
					        }
					        if(destination != null && destination != ''){
					        	acceso.R1_VLI_TXT_Destination__c = destination;
					        }
					        if(operationCompany != null && operationCompany != ''){
					        	acceso.R1_VLI_TXT_Comp_Flight__c = operationCompany;
					        }
					        if(flight != null && flight != ''){
					        	acceso.R1_VLI_TXT_Flight_number__c = flight;
					        }
					        //Resivar este para poner la fecha con el formato correcto
					        if(flightDate != null && flightDate != ''){
					        	acceso.R1_VLI_DAT_Date__c = Date.valueOf(flightDate);
					        }
					        if(classe != null && classe != ''){
					        	acceso.R1_VLI_TXT_Class__c = classe;
					        }
					        if(vipPermissions != null && vipPermissions != ''){
					        	acceso.R1_VLI_TXT_vip_lounge_permition__c = vipPermissions;
					        }
					        if(idAdIndicator != null && idAdIndicator != ''){
					        	acceso.R1_VLI_TXT_ID_AD_indicator__c = idAdIndicator;
					        }
					        if(seatNumber != null && seatNumber != ''){
					        	acceso.R1_VLI_TXT_Seat_number__c = seatNumber;
					        }
					        if(boardingNumber != null && boardingNumber != ''){
					        	acceso.R1_VLI_TXT_Bn__c = boardingNumber;
					        }
					        if(marketingCode != null && marketingCode != ''){
					        	acceso.R1_VLI_TXT_Comp_Mkt_flight__c = marketingCode;
					        }
					        if(flightMarketing != null && flightMarketing != ''){
					        	acceso.R1_VLI_TXT_Mkt_Flight__c = flightMarketing;
					        }
					        if(status != null && status != ''){
					        	acceso.R1_VLI_TXT_passenger_status__c = status;
					        }
					        if(etkdNumber != null && etkdNumber != ''){
					        	acceso.R1_VLI_TXT_ETKD__c = etkdNumber;
					        }
					        if(ffNumber != null && ffNumber != ''){
					        	acceso.R1_VLI_TXT_Frecuent_Flyer_number__c = ffNumber;
					        }
					        if(vipLounge != null && vipLounge != ''){
					        	acceso.R1_VLI_PKL_Sala_Vip__c = vipLounge;
					        }
					        if(vipLoungePosition != null && vipLoungePosition != ''){
					        	acceso.R1_VLI_PKL_Vip_lounge_position__c = vipLoungePosition;
					        }
					        if(specialMeals != null && specialMeals != ''){
					        	acceso.R1_VLI_TXT_Meal__c = specialMeals;
					        }
					        if(specialServices != null && specialServices != ''){
					        	acceso.R1_VLI_TXT_Special_Services__c = specialServices;
					        }
					        if(specialPAX != null && specialPAX != ''){
					        	acceso.R1_VLI_TXT_Special_PAX__c = specialPAX;
					        }
					        if(boardingGate != null && boardingGate != ''){
					        	acceso.R1_VLI_TXT_boarding_gate_cki__c = boardingGate;
					        }
					        if(departureTime != null && departureTime != ''){
					        	acceso.R1_VLI_NUM_departure_tieme_cki__c = departureTime;
					        }
					        if(minor != null && minor != ''){
					        	acceso.R1_VIP_CHK_Minor__c = minor;
					        }
					        if(inboundAirline != null && inboundAirline != ''){
					        	acceso.R1_VLI_TXT_Inbound__c = inboundAirline;
					        }
					        if(inboundFlight != null && inboundFlight != ''){
					        	acceso.R1_VLI_TXT_Inbound_flight__c = inboundFlight;
					        }
					        if(inboundClass != null && inboundClass != ''){
					        	acceso.R1_VLI_TXT_Inbound_Class__c = inboundClass;
					        }
					        if(inboundOrigin != null && inboundOrigin != ''){
					        	acceso.R1_VLI_TXT_Inb_origin__c = inboundOrigin;
					        }
					        //if(operationType != null && operationType != ''){
					        //	acceso.R2_VLI_Operation_type__c = operationType;
					        //}
					        if(recordId != null && recordId != ''){
					        	acceso.R2_VLI_Record_Id__c = recordId;
					        }

					        insert acceso;
					        List<R1_VIP_Lounge_Access__c> accinsertado = [SELECT id, R1_VLI_PKL_Access_status__c, R1_VLI_ATXT_Observations__c FROM R1_VIP_Lounge_Access__c WHERE id = : acceso.id];
					        System.debug('Estado del acceso: ' +  accinsertado[0].R1_VLI_PKL_Access_status__c);
					        if(accinsertado[0].R1_VLI_PKL_Access_status__c == 'Aprobado' || accinsertado[0].R1_VLI_PKL_Access_status__c == 'Forzado'){
					        	wp.status ='OK';
					        	wp.comments = null;
					        }else{
					        	wp.status ='KO';
					        	//wp.comments = 'Error, dirijase a uno de los agentes';
					        	wp.comments = 'POR FAVOR, DIRIJASE AL MOSTRADOR';
					        }
					        //wp.comments = accinsertado[0].R1_VLI_ATXT_Observations__c;
					    }else{
					    	wp.status = 'KO';
					    	//wp.comments = 'Error, el puesto esta desactivado';
					    	wp.comments = 'POR FAVOR, DIRIJASE AL MOSTRADOR';
					    }
					}else{
						wp.status = 'KO';
						//wp.comments = 'Error, el puesto no existe';
						wp.comments = 'POR FAVOR, DIRIJASE AL MOSTRADOR';
					}
				}else{
					wp.status = 'KO';
					//wp.comments = 'Record Id duplicado';
					wp.comments = 'POR FAVOR, DIRIJASE AL MOSTRADOR';
				}
			}
		}else{
			wp.status = 'KO';
			//wp.comments ='Record Id  no informado';
			wp.comments = 'POR FAVOR, DIRIJASE AL MOSTRADOR';
		}

	
        return wp;
         
    }	
    
}