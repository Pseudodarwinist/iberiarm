public class R2_CLS_Serbiberia_VF_CTRL{
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Borja Gay
    Company:        Accenture
    Description:    CTI VisualForce Controller Class.
    
    History:
    
    <Date>              <Author>            <Description>
    25/04/2017          Borja Gay          Initial version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
   
    String tlfLlamante = '';
    String tlfLlamado = '';
    String idLlamada = '';
    String infoAdicional = '';
    public String idc{get;set;}
    public String urlCaso{get;set;}
    public List<Account> acc {get;set;}
    public String accid {get;set;}
    public boolean noCliente {get;set;}
    public String recordid {get;set;}
    public String tipoTab {get;set;}
    public String tlfencuesta {get;set;}
    List<String> idreconocidos = new List<String>();
    
    /*---------------------------------------------------------------------------------------------------------------------
  Author:         Borja Gay Flores
  Company:        Accenture
  Description:    Constructor de la clase
  IN:             
  OUT:            

  History: 
   <Date>                     <Author>                         <Change Description>
  05/07/2017               Borja Gay Flores                       Initial Version
  23/08/2017                  David Barco Infante               Add the timeInQueue field in the VF (value in seconds)
  ----------------------------------------------------------------------------------------------------------------------*/ 
    
    public R2_CLS_Serbiberia_VF_CTRL(ApexPages.StandardController controller) {
         try{

            tlfLlamante = ApexPages.currentPage().getParameters().get('ani');
            tlfLlamado = ApexPages.currentPage().getParameters().get('dnis');
            idLlamada = ApexPages.currentPage().getParameters().get('cid');
            infoAdicional =ApexPages.currentPage().getParameters().get('uui');
            recordId = ApexPages.currentPage().getParameters().get('recordid');
            acc = new List<Account>();
            accid = '';
            tipoTab = '';
            idc=ApexPages.currentPage().getParameters().get('tabid');
            
              if(recordId!=null && recordId!=''){
                idreconocidos = recordId.split(';');
                if(idreconocidos.size() == 1){
                  accid = idreconocidos[0]; 
                }
              }
            
        }catch(Exception exc){
          R1_CLS_LogHelper.generateErrorLog('R2_CLS_Serbiberia_VF_CTRL.R2_CLS_Serbiberia_VF_CTRL', '', exc.getmessage()+', '+exc.getLineNumber(), 'R2_CLS_Serbiberia_VF_CTRL');
        }
    }
 
    /*---------------------------------------------------------------------------------------------------------------------
  Author:         Borja Gay Flores
  Company:        Accenture
  Description:    Metodo que asigna valor con la ID de un Caso a la variable urlCaso
  IN:             
  OUT:            

  History: 
   <Date>                     <Author>                         <Change Description>
  05/07/2017               Borja Gay Flores                       Initial Version
  ----------------------------------------------------------------------------------------------------------------------*/ 
    public void asignarCaso(){
        try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');} //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;  
              R1_CLS_LogHelper.generateErrorLog('R2_CLS_Serbiberia_VF_CTRL.logParamServiberia', '','recordid: ' + recordId + ', accid: ' + accid + ', ani: ' + tlfLlamante + ', dnis: ' + tlfLlamado + ', cid: ' + idLlamada + ', uui: ' + infoAdicional, 'R2_CLS_Serbiberia_VF_CTRL');
              String url=cargarCaso();
              urlCaso=url;
              System.debug('Valor :'+urlCaso); 
        }catch(Exception exc){
          R1_CLS_LogHelper.generateErrorLog('R2_CLS_Serbiberia_VF_CTRL.asignarCaso()', '', exc.getmessage()+', '+exc.getLineNumber(), 'R2_CLS_Serbiberia_VF_CTRL');
        }
    }
    
/*---------------------------------------------------------------------------------------------------------------------
  Author:         Borja Gay Flores
  Company:        Accenture
  Description:    Método que crea un caso con los datos de la llamada
  IN:             
  OUT: String "/"+id caso+"/e"            

  History: 
   <Date>                     <Author>                         <Change Description>
  05/07/2017               Borja Gay Flores                       Initial Version
  ----------------------------------------------------------------------------------------------------------------------*/ 
    public String cargarCaso (){      
       try{
           if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');} //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;  
            Case duplicado = new Case();
            try{
              DateTime fecha = DateTime.Now().addMinutes(-3);
              duplicado = [SELECT Id FROM Case WHERE R1_CAS_TXT_Call__c =:idLlamada AND CreatedDate>=:fecha];
              System.debug('caso encontrado.duplicado distinto de null: ' + duplicado);
            }catch(Exception e){
              duplicado=null;
              System.debug('caso no encontrado.duplicado igual null: ' + duplicado);
            }
            if(duplicado==null){
              Case caso = new Case();
              if(idLlamada != null || idLlamada != ''){
                caso.R1_CAS_TXT_Call__c=idLlamada;
              }
              Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Comunicaciones entrantes').getRecordTypeId();  
              caso.recordTypeId=recordTypeId;
              caso.status='Cerrado';
              caso.Origin = 'Llamada';
              System.debug('telefono al que llaman:' + tlfLlamado);
              R2_SITEL_SERV_Services__c servicio = R2_SITEL_SERV_Services__c.getInstance(tlfLlamado);
              System.debug('custom setting:' + servicio);
              if (servicio != null) {
                //COMPAÑIA
                caso.R2_CAS_TXT_company__c = servicio.R2_SIT_TXT_Company__c;
                //PAIS
                caso.R2_CAS_PKL_Country__c = servicio.R2_SIT_TXT_Country__c;
                //Servicio Serviberia
                caso.R2_CAS_TXT_Service_Serviberia__c = servicio.R2_SIT_TXT_Service__c;
                //Centro de servicio
                caso.R2_CAS_TXT_Service_Center__c  = servicio.R2_SIT_TXT_Service_center__c;
                //VDN
                caso.R2_CAS_TXT_VDN__c = servicio.R2_SIT_TXT_VDN__c;
                caso.R2_CAS_TXT_GEOGRAFIC_ARE__c = servicio.R2_SIT_TXT_GeographyArea__c;
                caso.R2_CAS_TXT_ENTERPRAISE__c = servicio.R2_SIT_TXT_Company__c;
                caso.R2_CAS_TXT_Market__c	= servicio.R2_SIT_TXT_Market__c;
                caso.R2_CAS_TXT_DDI__c = servicio.R2_SIT_TXT_DDI__c;
                caso.R2_CAS_TXT_Subservice__c	 = servicio.R2_SIT_TXT_SubService__c;
                caso.R2_CAS_CHECK_Active__c	 = servicio.R2_SIT_CHK_Active__c;
                caso.R1_CAS_PKL_Idioma__c = servicio.R2_SIT_TXT_Language__c;
                caso.R2_CAS_TXT_TERRITORI__c = servicio.R2_SIT_TXT_Territory__c;
                tlfencuesta = servicio.R2_SIT_TXT_Survey_VDN__c;

                System.debug('Telefono encuesta: ' + tlfencuesta);
              }
              if(accid != ''){
                caso.AccountId = accid;
                tipoTab = 'sub';
              }
              else{
                caso.AccountId = Label.ClienteAnonimoServiIberia;
                tipoTab = 'pri';
              }
              caso.Type='Sin información';
              insert caso;
              System.debug('ID Caso: '+ caso.id );
              return '/'+caso.id+'/e';
            }
            return null;
       }catch(Exception exc){
            R1_CLS_LogHelper.generateErrorLog('R2_CLS_Serbiberia_VF_CTRL.cargarCaso', '', exc.getmessage()+', '+exc.getLineNumber(), 'R2_CLS_Serbiberia_VF_CTRL');
          return '';
       }
        
    }

}