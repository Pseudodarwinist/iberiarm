global with sharing class R2_CLS_IberiaPay_Services {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Ismael Yubero
    Company:        Accenture
    Description:    
    
    IN:       
    OUT:      

    History: 
    <Date>                  <Author>                <Change Description>
    25/01/2019              Ismael Yuebro           Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/

public String source {get; set;}
public String libreria {get;set;}
public String resultado {get;set;}
public static String accessToken;
public String expiry_date {get;set;}
public String visible {get;set;}
public boolean errorResp {get;set;}

public class WS_login_response {
        public String access_token;
        public String token_type;
        public Integer expires_in;
        public String scope;
        public String jti;
    }

public class WS_session_response{

    public String vaultId;

}

public class WS_IberiaPay{
    public String access_token;
}

public class WR_IberiaPay_Card{
    public String bin;
    public String tokenizedCreditCard; 
    public String maskedCreditCard;
    public String creditCardType;
}


public R2_CLS_IberiaPay_Services (ApexPages.StandardController stdController) {
    libreria = Label.IberiaPay_library; 
    accessToken = loginIberiaPay();
    String sessionId = obtainSessionId(accessToken);
    System.debug('relleno source con el vault id: ' + sessionId);
    source = sessionId; 
    resultado = '';
    visible = 'hidden';
    errorResp = true; 
}

//@future(Callout = true)
public  static String loginIberiaPay() {
/*-------------------------------------------------------------------------------------------------------------------------------------------------------
    Author:         Ismael Yubero
    Company:        Accenture
    Description:    Method to login in PCI Pal system
    
    IN:       
    OUT:      

    History: 
    <Date>                     <Author>                <Change Description>
    25/01/2019                 Ismael Yubero            Initial Version
    --------------------------------------------------------------------------------------------------------------------------------------------------------*/
    HTTPResponse res;
    try{
        if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');} //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;    

        string wsMethod = 'IberiaPay Services';
        System.debug('Se ejecuta el metodo de Iberia Pay');
        if(R1_CS_Endpoints__c.getInstance(wsMethod) == null || !R1_CS_Endpoints__c.getInstance(wsMethod).R1_CHK_Activo__c){
                //System.debug('Peto en primer If');
                return null;
                
        }
        System.debug('endPoint ' + R1_CS_Endpoints__c.getInstance(wsMethod).R1_TXT_EndPoint__c);
        
        HttpRequest req = new HttpRequest();

        Blob client_authotization = Blob.valueOf(Label.IberiaPayCredentials);       
        String client_encode = EncodingUtil.base64Encode(client_authotization);
        req.setHeader('Authorization', 'Basic ' +  client_encode);
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        req.setHeader('Accept', 'application/json');

        String body = 'grant_type=client_credentials';
        req.setbody(body);
        req.setEndpoint(R1_CS_Endpoints__c.getInstance(wsMethod).R1_TXT_EndPoint__c + 'oauth/token');
        
        req.setTimeout(35000);
        
        Http http = new Http();
        

        
            res = http.send(req);
            System.debug('!!!res.getBody(): ' + res.getBody());
            WS_login_response responseToken = new WS_login_response();
            responseToken = (WS_login_response)JSON.deserialize(res.getBody(),WS_login_response.class);
            
            System.debug('responseToken: ' + responseToken);
            System.debug('responseToken.access_token: ' + responseToken.access_token);
            // obtainSessionId(responseToken.access_token);
            accessToken = responseToken.access_token;
            return accessToken;
        
        

    }catch(Exception exc){
        System.debug('Peta aqui');
        // R1_CLS_LogHelper.generateErrorLog('R2_CLS_PCIPal_Services.loginIberiaPay()', '', exc.getmessage()+', '+exc.getLineNumber(), 'PCI Pal Services');
        return null;
    }
}

public static String obtainSessionId(String accessToken) {
        HTTPResponse res;
        try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');} //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;    

            string wsMethod = 'IberiaPay Services';
        System.debug('Se ejecuta el metodo de IberiaPay');
        if(R1_CS_Endpoints__c.getInstance(wsMethod) == null || !R1_CS_Endpoints__c.getInstance(wsMethod).R1_CHK_Activo__c){
                //System.debug('Peto en primer If');
                return null;
                
        }
        System.debug('endPoint ' + R1_CS_Endpoints__c.getInstance(wsMethod).R1_TXT_EndPoint__c);
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint(R1_CS_Endpoints__c.getInstance(wsMethod).R1_TXT_EndPoint__c + 'api/v1/vault');
        req.setMethod('POST');
        req.setHeader('Authorization', 'Bearer ' +  accessToken);
        req.setHeader('Accept', 'application/json');
        req.setHeader('Content-Type', 'application/json');
        req.setTimeout(35000);



        system.debug('!!!req: ' + req);
        
        Http http = new Http();
        

            System.debug('Va a lanzar el send');
            res = http.send(req);
            WS_session_response responseToken = new WS_session_response();
            responseToken = (WS_session_response)JSON.deserialize(res.getBody(),WS_session_response.class);
            System.debug('Aqui ha petado ya');
            System.debug('!!!res.getBody(): ' + res.getBody());
            System.debug('responseToken vaultId: ' + responseToken.vaultId);
            return responseToken.vaultId;
            // System.debug('responseToken.access_token: ' + responseToken.access_token);
            // obtainSessionId(responseToken.access_token);
        

        }catch(Exception exc){

            // R1_CLS_LogHelper.generateErrorLog('R2_CLS_PCIPal_Services.obtainSessionId()', '', exc.getmessage()+', '+exc.getLineNumber(), 'PCI Pal Services');
           System.debug('R2_CLS_PCIPal_Services.obtainSessionId()'+ ''+ exc.getmessage()+', '+exc.getLineNumber());
            return null;
        }
    }

   public void imprimirResultado(){
        System.debug('Viene por el metodo de imprimir Hola');
        resultado = 'FPMS-TT';  
        String accessTokenPrint = loginIberiaPay();
        System.debug('Access_token: ' + accessTokenPrint);
        System.debug('resuldato: ' + source);
        System.debug('accessToken: ' + expiry_date);
        System.debug('Expiry date: ' +expiry_date);
        
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        HTTPResponse res;
        try{
            if(R1_CLS_LogHelper.isRunningTest()){throw new R1_CLS_LogHelper.R1_Exception('test');} //Excepcion para cubrir codigo de la clase de test activar con R1_CLS_LogHelper.throw_exception = true;    

        String wsMethod = 'IberiaPay Services';
        System.debug('Se ejecuta el metodo de IberiaPay');
        if(R1_CS_Endpoints__c.getInstance(wsMethod) == null || !R1_CS_Endpoints__c.getInstance(wsMethod).R1_CHK_Activo__c){
                //System.debug('Peto en primer If');
                // return null;
                
        }
        System.debug('endPoint ' + R1_CS_Endpoints__c.getInstance(wsMethod).R1_TXT_EndPoint__c);
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint(R1_CS_Endpoints__c.getInstance(wsMethod).R1_TXT_EndPoint__c + 'api/v1/vault/' + source + '/credit-card');
        req.setMethod('GET');
        req.setHeader('Authorization', 'Bearer ' +  accessTokenPrint);
        req.setHeader('Accept', 'application/json');
        req.setHeader('Content-Type', 'application/json');
        req.setTimeout(35000);



        system.debug('!!!req: ' + req);
        
        Http http = new Http();
        

            System.debug('Va a lanzar el send');
            res = http.send(req);
            System.debug('Aqui ha petado ya');
            System.debug('!!!res.getBody(): ' + res.getBody());
            WR_IberiaPay_Card responseToken = new WR_IberiaPay_Card();
            System.debug('Aqui ha petado ya');
            responseToken = (WR_IberiaPay_Card)JSON.deserialize(res.getBody(),WR_IberiaPay_Card.class);
           
           if(responseToken.creditCardType == null || responseToken.tokenizedCreditCard == null){
               resultado = 'Ha habido un error durante el proceso. Revise la informacion e intentelo de nuevo';
            visible = 'hidden';
            System.debug('Viene por el false');
            errorResp = false;
           }else{
            resultado = resultado +',' + responseToken.creditCardType + responseToken.tokenizedCreditCard + '-';
            System.debug('resultado::::' + resultado);
            visible = 'visible';
            System.debug('Viene por el true'); 
            errorResp = true; 
           }
            // System.debug('Expirity Date : ' + expiry_date);
            // resultado = 'FPMS-TTVI,80000000000123456/' + expiry_date+',V12345689';
       
        // return null;

        }catch(Exception exc){

            // R1_CLS_LogHelper.generateErrorLog('R2_CLS_PCIPal_Services.obtainSessionId()', '', exc.getmessage()+', '+exc.getLineNumber(), 'PCI Pal Services');
           System.debug('R2_CLS_PCIPal_Services.obtainSessionId()'+ ''+ exc.getmessage()+', '+exc.getLineNumber());
            // return null;
        }
    }  
}