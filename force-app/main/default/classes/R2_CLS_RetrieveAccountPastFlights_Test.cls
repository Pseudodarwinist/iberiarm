@isTest
private class R2_CLS_RetrieveAccountPastFlights_Test
{
@isTest
static void getPastFlightsTest() {

      R1_CLS_LogHelper.throw_exception = false;  
      List<R1_CS_Endpoints__c> lst_ep=new List<R1_CS_Endpoints__c>();

        R1_CS_Endpoints__c ep=new R1_CS_Endpoints__c();
        ep.Name='Retrieve Past Flights';
        ep.R1_CHK_Activo__c=true;
        ep.R1_TXT_EndPoint__c='Retrieve Past Flights';
        lst_ep.add(ep);

        R1_CS_Endpoints__c ep2 = new R1_CS_Endpoints__c();
        ep2.Name = 'ETL_Login';
        ep2.R1_CHK_Activo__c = true;
        ep2.R1_TXT_EndPoint__c = 'PruebaLogin';
        lst_ep.add(ep2);

        insert lst_ep;

        Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());

        Account acc = new Account();
        acc.R1_ACC_TXT_Id_Golden_record__c='96764130';
        acc.LastName = 'Test';
        acc.R1_ACC_TXT_Primary_Frecuent_Flyer_Card__c = 'IB47663091';
        acc.R1_ACC_PKL_Platinum_level_description__c = '04';
        
        insert acc;

        Test.startTest();
        R2_CLS_RetrieveAccountPastFlights.getPastFlightsWRP(acc.id);
        Test.stopTest();
        //System.assertEquals(0, [SELECT count() FROM R1_Log__c]);
      }
        
@isTest
static void getPastFlightsFail_Test()
        {
                R1_CLS_LogHelper.throw_exception = false;  
                
                List<R1_CS_Endpoints__c> lst_ep=new List<R1_CS_Endpoints__c>();

        R1_CS_Endpoints__c ep2 = new R1_CS_Endpoints__c();
        ep2.Name = 'ETL_Login';
        ep2.R1_CHK_Activo__c = true;
        ep2.R1_TXT_EndPoint__c = 'PruebaLogin';
        lst_ep.add(ep2);

        insert lst_ep;

        Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());

        Account acc = new Account();
        acc.R1_ACC_TXT_Id_Golden_record__c='96764130';
        acc.LastName = 'Test';
        acc.R1_ACC_TXT_Primary_Frecuent_Flyer_Card__c = 'IB47663091';
        acc.R1_ACC_PKL_Platinum_level_description__c = '04';
        
        insert acc;

        Test.startTest();
        R2_CLS_RetrieveAccountPastFlights.getPastFlightsWRP(acc.id);
        Test.stopTest();
        System.assertEquals(1, [SELECT count() FROM R1_Log__c]);
        }
@isTest
static void getPastFlightsLoginFailTest(){

                R1_CLS_LogHelper.throw_exception = false;  
                
                List<R1_CS_Endpoints__c> lst_ep=new List<R1_CS_Endpoints__c>();
        R1_CS_Endpoints__c ep=new R1_CS_Endpoints__c();
        ep.Name='Retrieve Past Flights';
        ep.R1_CHK_Activo__c=true;
        ep.R1_TXT_EndPoint__c='ListaVuelos';
        lst_ep.add(ep);


        insert lst_ep;

        Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());

        Account acc = new Account();
        acc.R1_ACC_TXT_Id_Golden_record__c='96764130';
        acc.LastName = 'Test';
        acc.R1_ACC_TXT_Primary_Frecuent_Flyer_Card__c = 'IB47663091';
        acc.R1_ACC_PKL_Platinum_level_description__c = '04';
        
        insert acc;

        Test.startTest();
        R2_CLS_RetrieveAccountPastFlights.getPastFlightsWRP(acc.id);
        Test.stopTest();
        System.assertEquals(2, [SELECT count() FROM R1_Log__c]);
        }
@isTest
static void exceptionTest()
        {
        R1_CLS_LogHelper.throw_exception = false;  
                
        List<R1_CS_Endpoints__c> lst_ep=new List<R1_CS_Endpoints__c>();
        R1_CS_Endpoints__c ep=new R1_CS_Endpoints__c();
        ep.Name='Retrieve Past Flights';
        ep.R1_CHK_Activo__c=true;
        ep.R1_TXT_EndPoint__c='ListaVuelos';
        lst_ep.add(ep);

        insert lst_ep;

        

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());
        R2_CLS_RetrieveAccountPastFlights.getPastFlightsWRP(null);
        Test.stopTest();
        System.assertEquals(0, [SELECT count() FROM R1_Log__c]);
        }

@isTest
static void savePastFlights_Test(){


        R1_CLS_LogHelper.throw_exception = false;  
        List<R1_CS_Endpoints__c> lst_ep=new List<R1_CS_Endpoints__c>();
        R1_CS_Endpoints__c ep=new R1_CS_Endpoints__c();
        ep.Name='Retrieve Past Flights';
        ep.R1_CHK_Activo__c=true;
        ep.R1_TXT_EndPoint__c='ListaVuelos';
        lst_ep.add(ep);

        R1_CS_Endpoints__c ep2 = new R1_CS_Endpoints__c();
        ep2.Name = 'ETL_Login';
        ep2.R1_CHK_Activo__c = true;
        ep2.R1_TXT_EndPoint__c = 'PruebaLogin';
        lst_ep.add(ep2);

        insert lst_ep;

        Account acc = new Account();
        acc.R1_ACC_TXT_Id_Golden_record__c='96764130';
        acc.LastName = 'Test';
        acc.R1_ACC_TXT_Primary_Frecuent_Flyer_Card__c = 'IB47663091';
        acc.R1_ACC_PKL_Platinum_level_description__c = '04';
        
        insert acc;

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());
        R2_CLS_RetrieveAccountPastFlights.savePastFlights(acc.id);
        Test.stopTest();

        }
@isTest
static void getPastFlights_Test(){


        R1_CLS_LogHelper.throw_exception = false;  

        List<R1_CS_Endpoints__c> lst_ep=new List<R1_CS_Endpoints__c>();
        R1_CS_Endpoints__c ep=new R1_CS_Endpoints__c();
        ep.Name='Retrieve Past Flights';
        ep.R1_CHK_Activo__c=true;
        ep.R1_TXT_EndPoint__c='ListaVuelos';
        lst_ep.add(ep);

        R1_CS_Endpoints__c ep2 = new R1_CS_Endpoints__c();
        ep2.Name = 'ETL_Login';
        ep2.R1_CHK_Activo__c = true;
        ep2.R1_TXT_EndPoint__c = 'PruebaLogin';
        lst_ep.add(ep2);

        insert lst_ep;

        Id recordTypeIdAcc = [SELECT Id FROM recordType WHERE Name like 'Cuenta personal' LIMIT 1].Id;
        Id recordTypeIdCase = [SELECT Id FROM recordType WHERE Name like 'Asistencia' LIMIT 1].Id;
        List<Account> lstAcc = new List<Account>();
        for(Integer i = 1; i <= 2; i++){
            Account accTest = new Account();
            accTest.LastName = 'Test' + i;
            accTest.RecordTypeId = recordTypeIdAcc;
            accTest.R1_ACC_TXT_Id_Golden_record__c = '' + i;
            accTest.PersonBirthdate = Date.newInstance(1990, 12, 31);
            lstAcc.add(accTest);
        }
        insert lstAcc;          
        

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new R1_CLS_Utilities.mockCallOut());
        R2_CLS_RetrieveAccountPastFlights.getPastFlights(lstAcc[0].id);
        Test.stopTest();

        }

        @isTest
        static void exception_Test() {
                R1_CLS_LogHelper.throw_exception = true;

                Test.startTest();

                        R2_CLS_RetrieveAccountPastFlights.savePastFlights(null);
                        R2_CLS_RetrieveAccountPastFlights.getPastFlights(null);
                        R2_CLS_RetrieveAccountPastFlights.getPastFlightsWRP(null);
                        R2_CLS_RetrieveAccountPastFlights.retrievePasFlights(null, null);

                Test.stopTest();
                System.assertEquals(0, [SELECT count() FROM R1_Log__c]);
        }
}