@IsTest
private with sharing class EmailDatatableWrapperTests {
    
    @IsTest
    static void validateWrapperWhenAllIsSet() {

        Test.startTest();

        Account ac = new Account(name='Test Account');
        insert ac;

        archive_v_email__x archivedEmail = createTestArchivedEmail(ac.id);

        // Mocks
        EmailDatatableWrapper.testDateTime = Datetime.now();
        EmailDatatableWrapper.archivedEmailTrackingCanBeRead = true;
        EmailDatatableWrapper.AccountCanBeRead = true;

        runAsserts(archivedEmail, false, false);

        Test.stopTest();
    }


    @IsTest
    static void validateWrapperWithNoAccount() {

        Test.startTest();

        archive_v_email__x archivedEmail = createTestArchivedEmail(null);

        // Mocks
        EmailDatatableWrapper.testDateTime = Datetime.now();
        EmailDatatableWrapper.archivedEmailTrackingCanBeRead = true;
        EmailDatatableWrapper.AccountCanBeRead = true;

        runAsserts(archivedEmail, true, false);

        Test.stopTest();
    }

    @IsTest
    static void validateWrapperWithNoExternalEmailTracking() {

        Test.startTest();

        Account ac = new Account(name='Test Account');
        insert ac;
        archive_v_email__x archivedEmail = createTestArchivedEmail(ac.id);

        // Mocks
        EmailDatatableWrapper.testDateTime = Datetime.now();
        EmailDatatableWrapper.archivedEmailTrackingCanBeRead = false;
        EmailDatatableWrapper.AccountCanBeRead = true;

        runAsserts(archivedEmail, false, true);

        Test.stopTest();
    }

    @IsTest
    static void validateWrapperWithNoReadPermissions() {

        Test.startTest();

        Account ac = new Account(name='Test Account');
        insert ac;

        archive_v_email__x archivedEmail = createTestArchivedEmail(ac.id);

        // Mocks
        EmailDatatableWrapper.testDateTime = Datetime.now();
        EmailDatatableWrapper.archivedEmailTrackingCanBeRead = false;
        EmailDatatableWrapper.AccountCanBeRead = false;

        runAsserts(archivedEmail, true, true);
        
        Test.stopTest();
    }

    private static archive_v_email__x createTestArchivedEmail(String accountId) {

        archive_v_email__x archivedEmail = new archive_v_email__x(
            filename__c = '7283808_352723_70_59_105475582_FAKE.eml',
            nombre_st__c = 'TEST_NAME',
            primer_apellido_st__c = 'TEST_SURNAME',
            email__c = 'abc@def.com',
            flight_date__c = Datetime.newInstance(2019, 1, 1, 0, 0, 0),
            eventdate__c = Datetime.newInstance(2019, 2, 2, 0, 0, 0),
            id_golden_record__c = 'ASDFJKLM',
            mercado_comunicaciones_st__c = 'ES',
            seg_communication_language__c = 'ES',
            num_tarjeta_ibp_st__c = 'IBP1234',
            pnr_amadeus__c = 'PNR_AMADEUS',
            pnr_resiber__c = 'PNR_RESIBER',
            tipo_cliente_bl__c = 'IBERIA PLUS',
            tkt__c = 'TKT1234',
            processname__c = '352723_70_59_105475582',
            subjectline__c = 'test email subject',
            contactkey__c = accountId,
            flight_id__c = 'IB0000'
        );

        return archivedEmail;
    }

    private static void runAsserts(archive_v_email__x archivedEmail, Boolean accessToAccount, Boolean accessToArchivedEmailTracking) {
        // Create the wrapper object
        EmailDatatableWrapper generatedWrapper = new EmailDatatableWrapper(archivedEmail);

        // Asserts
        System.assertEquals(generatedWrapper.filename, archivedEmail.filename__c, 'Id does not match');
        System.assertEquals(generatedWrapper.name, archivedEmail.nombre_st__c, 'Name does not match');
        System.assertEquals(generatedWrapper.surname, archivedEmail.primer_apellido_st__c, 'Surname does not match');
        System.assertEquals(generatedWrapper.email, archivedEmail.email__c, 'Email does not match');
        System.assertEquals(generatedWrapper.flightDate, Util.convertDateToHumanReadableWithNoTime(archivedEmail.flight_date__c), 'Flight date does not match');
        System.assertEquals(generatedWrapper.eventTime, Util.convertDateToHumanReadable(archivedEmail.eventdate__c), 'Event date does not match');
        System.assertEquals(generatedWrapper.goldenRecord, archivedEmail.id_golden_record__c, 'Golden record does not match');
        System.assertEquals(generatedWrapper.comMkt, archivedEmail.mercado_comunicaciones_st__c, 'Communication marketing does not match');
        System.assertEquals(generatedWrapper.langMkt, archivedEmail.seg_communication_language__c, 'Communication language does not match');
        System.assertEquals(generatedWrapper.iberiaPlus, archivedEmail.num_tarjeta_ibp_st__c, 'Iberia plus card does not match');
        System.assertEquals(generatedWrapper.pnrAmadeus, archivedEmail.pnr_amadeus__c, 'PNR Amadeus does not match');
        System.assertEquals(generatedWrapper.pnrResiber, archivedEmail.pnr_resiber__c, 'PNR Resiber does not match');
        System.assertEquals(generatedWrapper.clientType, archivedEmail.tipo_cliente_bl__c, 'Client type does not match');
        System.assertEquals(generatedWrapper.tkt, archivedEmail.tkt__c, 'TKT does not match');
        System.assertEquals(generatedWrapper.processName, archivedEmail.processname__c, 'Process name does not match');
        System.assertEquals(generatedWrapper.subject, archivedEmail.subjectline__c, 'Subject does not match');
        System.assertEquals(generatedWrapper.contactKey, archivedEmail.contactkey__c, 'Contact key does not match');
        System.assertEquals(generatedWrapper.flightId, archivedEmail.flight_id__c, 'Flight id key does not match');
        System.assertEquals(generatedWrapper.contactDataNotPresent, accessToAccount, 'ContactDataNotPresent is wrong');
        System.assertEquals(generatedWrapper.trackingDataNotPresent, accessToArchivedEmailTracking, 'TrackingDataNotPresent is wrong');

        String folder = 'archive/' + generatedWrapper.processName + '/' ;
        String[] splitted = generatedWrapper.filename.split('\\_');
        
        if (splitted.size() >= 5) {
            folder += splitted[1] + '/';
            folder += splitted[2] + '/';
            folder += splitted[3] + '/';
        }

        folder += generatedWrapper.filename;

        System.assertEquals(generatedWrapper.externalURLpdf, AWSLinkGenerator.getSignedURL(folder + '.pdf', EmailDatatableWrapper.testDateTime), 'ExternalURLpdf does not match');
        System.assertEquals(generatedWrapper.externalURLhtml, AWSLinkGenerator.getSignedURL(folder + '.html', EmailDatatableWrapper.testDateTime), 'ExternalURLhtml does not match');
        System.assertEquals(generatedWrapper.externalURLeml, AWSLinkGenerator.getSignedURL(folder + '.zip', EmailDatatableWrapper.testDateTime), 'ExternalURLeml does not match');
    }
}